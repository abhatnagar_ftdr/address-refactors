package main

import (
	"go.ftdr.com/go-utils/common/errors"
	"go.ftdr.com/go-utils/common/logging"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/server"
)

func main() {

	err := server.RunServer()
	if err != nil {
		logging.Log.WithField("Error", err).Fatal(errors.ServerQuitError)
	}
}
