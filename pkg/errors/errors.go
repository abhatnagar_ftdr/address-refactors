package errors

import (
	"go.ftdr.com/go-utils/common/errors"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors/status"
)

var (
	// AddressIDNotFoundError ...
	AddressIDNotFoundError = ServerError{
		Message:    "could not find addressID: %s",
		Code:       "ADDRESS_ID_NOT_FOUND_ERROR",
		statusCode: status.NotFound,
	}

	// AddressDPBCNotFoundError ...
	AddressDPBCNotFoundError = ServerError{
		Message:    "could not find address DPBC: %s",
		Code:       "ADDRESS_DPBC_NOT_FOUND_ERROR",
		statusCode: status.NotFound,
	}

	// AddressNotFoundError ...
	AddressNotFoundError = ServerError{
		Message:    "could not find address",
		Code:       "ADDRESS_NOT_FOUND_ERROR",
		statusCode: status.NotFound,
	}

	// AddressExistsError ...
	AddressExistsError = ServerError{
		Message:    "cannot post existing address",
		Code:       "ADDRESS_EXISTS_ERROR",
		statusCode: status.AlreadyExists,
	}

	// AddressNotVerifiableError ...
	AddressNotVerifiableError = ServerError{
		Message:    "address was not found in SmartyStreets and cannot be verified",
		Code:       "ADDRESS_NOT_VERIFIABLE_ERROR",
		statusCode: status.NotFound,
	}

	// CacheDeleteError ...
	CacheDeleteError = ServerError{
		Message:    "unable to delete key from cache",
		Code:       "CACHE_DELETE_ERROR",
		statusCode: status.Error,
	}

	// CacheGetError ...
	CacheGetError = ServerError{
		Message:    "unable to get key from cache",
		Code:       "CACHE_GET_ERROR",
		statusCode: status.Error,
	}

	// CacheKeyExistenceError ...
	CacheKeyExistenceError = ServerError{
		Message:    "unable to check key existence in cache",
		Code:       "CACHE_KEY_EXISTENCE_ERROR",
		statusCode: status.Error,
	}

	// CachePingError ...
	CachePingError = ServerError{
		Message:    "failed to ping cache",
		Code:       "CACHE_PING_ERROR",
		statusCode: status.Error,
	}

	// CacheSetExError ...
	CacheSetExError = ServerError{
		Message:    "unable to set key with ttl in cache",
		Code:       "CACHE_SET_EX_ERROR",
		statusCode: status.Error,
	}

	// CacheUnavailableError ...
	CacheUnavailableError = ServerError{
		Message:    "cache is disabled",
		Code:       "CACHE_UNAVAILABLE_ERROR",
		statusCode: status.Unavailable,
	}

	// ContentTypeError ...
	ContentTypeError = ServerError{
		Message:    "content type not accepted",
		Code:       "CONTENT_TYPE_ERROR",
		statusCode: status.Invalid,
	}

	// ConversionError ...
	ConversionError = ServerError{
		Message:    "failed to convert record to response",
		Code:       "RESPONSE_CONVERSION_ERROR",
		statusCode: status.Error,
	}

	// DaoDecodeError ...
	DaoDecodeError = ServerError{
		Message:    "unable to decode record",
		Code:       "DAO_DECODE_ERROR",
		statusCode: status.Error,
	}

	// DaoInsertError ...
	DaoInsertError = ServerError{
		Message:    "unable to insert address record",
		Code:       "DAO_INSERT_ERROR",
		statusCode: status.Error,
	}

	// DaoQueryError ...
	DaoQueryError = ServerError{
		Message:    "unable to query database",
		Code:       "DAO_QUERY_ERROR",
		statusCode: status.Error,
	}

	// DaoUpdateError ...
	DaoUpdateError = ServerError{
		Message:    "unable to update address record",
		Code:       "DAO_UPDATE_ERROR",
		statusCode: status.Error,
	}

	// DaoTransactionStartError ...
	DaoTransactionError = ServerError{
		Message:    "unable to start transaction",
		Code:       "DAO_START_TRANSACTION_ERROR",
		statusCode: status.Error,
	}

	// DaoUUIDGenerationError ...
	DaoUUIDGenerationError = ServerError{
		Message:    "unable to generate uuid",
		Code:       "DAO_UUID_GENERATION_ERROR",
		statusCode: status.Error,
	}

	// DaoUUIDParseError ...
	DaoUUIDParseError = ServerError{
		Message:    "unable to parse uuid",
		Code:       "DAO_UUID_PARSE_ERROR",
		statusCode: status.Error,
	}

	// EmptyBodyError ...
	EmptyBodyError = ServerError{
		Message:    "request body was empty",
		Code:       "EMPTY_BODY_ERROR",
		statusCode: status.Invalid,
	}

	// EmptyDPBCError ...
	EmptyDPBCError = ServerError{
		Message:    "address returned from SmartyStreets with empty delivery point barcode",
		Code:       "EMPTY_DPBC_ERROR",
		statusCode: status.Error,
	}

	// EncoderError ...
	EncoderError = ServerError{
		Message:    "could not encode the response",
		Code:       "RESPONSE_ENCODING_ERROR",
		statusCode: status.Error,
	}

	// ExecuteRequestError ...
	ExecuteRequestError = ServerError{
		Message:    "failed to execute request",
		Code:       "EXECUTE_REQUEST_ERROR",
		statusCode: status.Error,
	}

	// InvalidAddressCityStateAndZipError ...
	InvalidAddressCityStateAndZipError = ServerError{
		Message:    "city, state and zip Code are required fields",
		Code:       "INVALID_ADDRESS_CITY_STATE_ZIP_ERROR",
		statusCode: status.Invalid,
	}

	// InvalidAddressCityStateOrZipError ...
	InvalidAddressCityStateOrZipError = ServerError{
		Message:    "either city and state are required or zip code",
		Code:       "INVALID_ADDRESS_CITY_STATE_ZIP_ERROR",
		statusCode: status.Invalid,
	}

	// InvalidStreetError if street contains city, state, zip or country information
	InvalidStreetError = ServerError{
		Message:    "street should not contain city, state, zip or country information",
		Code:       "INVALID_STREET_ERROR",
		statusCode: status.Invalid,
	}

	// TooManyCharactersError if a given field has more than (currently) 200 characters
	// Pass field name
	TooManyCharactersError = ServerError{
		Message:    "field contains too many characters: %s",
		Code:       "TOO_MANY_CHARACTERS_ERROR",
		statusCode: status.NotFound,
	}
	// ContainsInvalidCharactersError if a field contains forbidden characters
	// Pass field name
	ContainsInvalidCharactersError = ServerError{
		Message:    "field contains invalid characters: %s",
		Code:       "INVALID_CHARACTERS_ERROR",
		statusCode: status.NotFound,
	}

	// InvalidAddressCountryError ...
	InvalidAddressCountryError = ServerError{
		Message: "invalid country iso: %s",
		Code:    "INVALID_ADDRESS_COUNTRY_CODE",
	}

	// EmptyAddressStreet1Error ...
	EmptyAddressStreet1Error = ServerError{
		Message:    "street1 is a required field",
		Code:       "EMPTY_ADDRESS_STREET_1_ERROR",
		statusCode: status.Invalid,
	}

	// InvalidAddressStreet1Error ...
	InvalidAddressStreet1Error = ServerError{
		Message:    "invalid street_1",
		Code:       "INVALID_ADDRESS_STREET_1_ERROR",
		statusCode: status.Invalid,
	}

	// InvalidTypeaheadCityAndStateError ...
	InvalidTypeaheadCityAndStateError = ServerError{
		Message:    "invalid typeahead: state field cannot be empty when city is passed",
		Code:       "INVALID_TYPEAHEAD_CITY_AND_STATE_ERROR",
		statusCode: status.Invalid,
	}

	// InvalidAddressFieldTooLongError ...
	InvalidAddressFieldTooLongError = ServerError{
		Message:    "invalid address: field %s too long (got: %d, max: %d)",
		Code:       "INVALID_ADDRESS_FIELD_TOO_LONG_ERROR",
		statusCode: status.Invalid,
	}

	// InvalidDPBCError ...
	InvalidDPBCError = ServerError{
		Message:    "invalid delivery point barcode",
		Code:       "INVALID_DPBC_ERROR",
		statusCode: status.Error,
	}

	// InvalidIDError ...
	InvalidIDError = ServerError{
		Message:    "invalid ID: %s",
		Code:       "INVALID_ID_ERROR",
		statusCode: status.Invalid,
	}

	// InvalidLegacyIDError ...
	InvalidLegacyIDError = ServerError{
		Message:    "invalid legacyID: %v",
		Code:       "INVALID_LEGACY_ID_ERROR",
		statusCode: status.Invalid,
	}

	// InvalidIDAndLegacyIDError ...
	InvalidIDAndLegacyIDError = ServerError{
		Message:    "invalid ID or legacyID: %s",
		Code:       "INVALID_ID_AND_LEGACY_ID_ERROR",
		statusCode: status.Invalid,
	}

	// LegacyIDNotFoundInStarDBError ...
	LegacyIDNotFoundInStarDBError = ServerError{
		Message:    "legacyID not found in Star DB: %v",
		Code:       "LEGACY_ID_NOT_FOUND_IN_STAR_DB",
		statusCode: status.Invalid,
	}

	// InvalidFilterError ...
	InvalidFilterError = ServerError{
		Message:    "filter is invalid",
		Code:       "INVALID_FILTER_ERROR",
		statusCode: status.Invalid,
	}

	// InvalidZipCodeError ...
	InvalidZipCodeError = ServerError{
		Message:    "invalid zip code: %s",
		Code:       "INVALID_ZIP_CODE",
		statusCode: status.Invalid,
	}

	// InvalidStateError ...
	InvalidStateError = ServerError{
		Message:    "invalid state: %s",
		Code:       "INVALID_STATE_CODE",
		statusCode: status.Invalid,
	}

	// LegacyIDAlreadyExistsError ...
	LegacyIDAlreadyExistsError = ServerError{
		Message:    "legacyID: %s already exists on record: %s",
		Code:       "LEGACY_ID_ALREADY_EXISTS_ERROR",
		statusCode: status.AlreadyExists,
	}

	// ExternalSourceIDAlreadyExistsError ...
	ExternalSourceIDAlreadyExistsError = ServerError{
		Message:    "externalSourceID: %s already exists on record: %s",
		Code:       "EXTERNAL_SOURCE_ID_ALREADY_EXISTS_ERROR",
		statusCode: status.AlreadyExists,
	}

	// UnknownExternalSourceNameError ...
	UnknownExternalSourceNameError = ServerError{
		Message:    "unknown external source name: %s",
		Code:       "UNKNOWN_EXTERNAL_SOURCE_NAME_ERROR",
		statusCode: status.Error,
	}

	// LegacyIDNotFoundError ...
	LegacyIDNotFoundError = ServerError{
		Message:    "could not find legacyID: %d",
		Code:       "LEGACY_ID_NOT_FOUND_ERROR",
		statusCode: status.NotFound,
	}

	// JSONDecodeError ...
	JSONDecodeError = ServerError{
		Message:    "failed to decode JSON into struct",
		Code:       "JSON_DECODE_ERROR",
		statusCode: status.Error,
	}

	// MarshalError ...
	MarshalError = ServerError{
		Message:    "could not marshal the response",
		Code:       "RESPONSE_MARSHAL_ERROR",
		statusCode: status.Invalid,
	}

	// NilResponseError ...
	NilResponseError = ServerError{
		Message:    "nil response received",
		Code:       "NIL_RESPONSE_ERROR",
		statusCode: status.Invalid,
	}

	// NilPointerError ...
	NilPointerError = ServerError{
		Message:    "unexpected nil pointer found",
		Code:       "NIL_POINTER_ERROR",
		statusCode: status.Error,
	}

	// NoRecordsUpdatedError ...
	NoRecordsUpdatedError = ServerError{
		Message:    "no records were updated",
		Code:       "NO_RECORDS_UPDATED_ERROR",
		statusCode: status.Error,
	}
	// RequestBodyReadError ...
	RequestBodyReadError = ServerError{
		Message:    "could not read request body",
		Code:       "REQUEST_BODY_READ_ERROR",
		statusCode: status.Invalid,
	}

	// RequestBuildError ...
	RequestBuildError = ServerError{
		Message:    "failed to build request",
		Code:       "REQUEST_BUILD_ERROR",
		statusCode: status.Error,
	}
	// SmartyStreetsLookupError ...
	SmartyStreetsLookupError = ServerError{
		Message:    "SmartyStreets lookup failed",
		Code:       "SMARTY_STREETS_LOOKUP_ERROR",
		statusCode: status.Error,
	}

	// SmartyStreetsPingError ...
	SmartyStreetsPingError = ServerError{
		Message:    "SmartyStreets ping failed",
		Code:       "SMARTY_STREETS_PING_ERROR",
		statusCode: status.Error,
	}

	// SmartyStreetsTypeaheadError ...
	SmartyStreetsTypeaheadError = ServerError{
		Message:    "SmartyStreets typeahead failed",
		Code:       "SMARTY_STREETS_TYPEAHEAD_ERROR",
		statusCode: status.Error,
	}

	// ExperianSearchError ...
	ExperianSearchError = ServerError{
		Message:    "Experian search failed",
		Code:       "EXPERIAN_SEARCH_ERROR",
		statusCode: status.Error,
	}

	// ExperianFormatError ...
	ExperianFormatError = ServerError{
		Message:    "Experian format failed",
		Code:       "EXPERIAN_FORMAT_ERROR",
		statusCode: status.Error,
	}

	// ExperianEnrichmentError ...
	ExperianEnrichmentError = ServerError{
		Message:    "Experian enrichment failed",
		Code:       "EXPERIAN_ENRICHMENT_ERROR",
		statusCode: status.Error,
	}

	// ExperianPingError ...
	ExperianPingError = ServerError{
		Message:    "Experian ping failed",
		Code:       "EXPERIAN_PING_ERROR",
		statusCode: status.Error,
	}

	// TypeaheadNotFoundError ...
	TypeaheadNotFoundError = ServerError{
		Message:    "could not find any typeahead suggestions",
		Code:       "TYPEAHEAD_NOT_FOUND_ERROR",
		statusCode: status.NotFound,
	}

	// TypeaheadInvalidRequestError ...
	TypeaheadInvalidRequestError = ServerError{
		Message:    "invalid typeahead request: %s",
		Code:       "TYPEAHEAD_INVALID_REQUEST_ERROR",
		statusCode: status.Invalid,
	}

	// UnknownStatusCodeError ...
	UnknownStatusCodeError = ServerError{
		Message:    "unknown status Code",
		Code:       "UNKNOWN_STATUS_CODE",
		statusCode: status.Error,
	}

	// UnmarshalError ...
	UnmarshalError = ServerError{
		Message:    "could not unmarshal the request",
		Code:       "REQUEST_UNMARSHAL_ERROR",
		statusCode: status.Invalid,
	}

	// VerifiableAddressError ...
	VerifiableAddressError = ServerError{
		Message:    "cannot post unverified verifiable address, verifiable ID: %s",
		Code:       "VERIFIABLE_ADDRESS_ERROR",
		statusCode: status.AlreadyExists,
	}

	// ZipNotFoundError ...
	ZipNotFoundError = ServerError{
		Message:    "zip code not found in %s: %s",
		Code:       "ZIP_NOT_FOUND_ERROR",
		statusCode: status.NotFound,
	}

	// UnknownProtoTypeError ...
	UnknownProtoTypeError = ServerError{
		Message:    "unknown prototype in struct",
		Code:       "UNKNOWN_PROTOTYPE_ERROR",
		statusCode: status.Invalid,
	}

	// InvalidAddressRecordType ...
	InvalidAddressRecordType = ServerError{
		Message:    "address record type value is not valid : %s",
		Code:       "VERIFIABLE_ADDRESS_RECORD_TYPE_ERROR",
		statusCode: status.NotFound,
	}

	// StatusCodeError ..
	StatusCodeError = ServerError{
		Message:    "incorrect response status: %d",
		Code:       "STATUS_CODE_ERROR",
		statusCode: status.Error,
	}

	StarTransactionError = ServerError{
		Message:    "star transaction creation failed",
		Code:       "STAR_TRANSACTION_ERROR",
		statusCode: status.Error,
	}

	StarOperationError = ServerError{
		Message:    "star operation failed",
		Code:       "STAR_OPERATION_ERROR",
		statusCode: status.Error,
	}

	OptimizelyFlagNA = errors.ServerError{
		Message: "MISSING_FEATURE_FLAG: Flag doesn't exists or hasn't been enabled for the current env.",
		Code:    literals.EnvPrefix + "_BLE_0016:",
	}
	SwitchTypeNA = errors.ServerError{
		Message: "INVALID_SWITCH_CONFIG: The switch-type in the config ,%s is not of expected type %s",
		Code:    literals.EnvPrefix + "_BLE_0017:",
	}
)
