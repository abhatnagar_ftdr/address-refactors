package errors

import (
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors/status"
)

var (
	// RequiredFieldMissing ...
	RequiredFieldMissing = ServerError{
		Message:    "required field missing: %s",
		Code:       "REQUIRED_FIELD_MISSING_ERROR",
		statusCode: status.Invalid,
	}
	// StarQueryError ...
	StarQueryError = ServerError{
		Message:    "star query error: %s",
		Code:       "STAR_QUERY_ERROR",
		statusCode: status.Error,
	}

	// PropertyNotFound ...
	PropertyNotFound = ServerError{
		Message:    "property not found in star",
		Code:       "PROPERTY_NOT_FOUND_ERROR",
		statusCode: status.NotFound,
	}

	// CityCodeNotFound ...
	CityCodeNotFound = ServerError{
		Message:    "city code not found in star",
		Code:       "CITY_CODE_NOT_FOUND_ERROR",
		statusCode: status.NotFound,
	}

	// CityNameNotFound ...
	CityNameNotFound = ServerError{
		Message:    "city name not found in star",
		Code:       "CITY_NAME_NOT_FOUND_ERROR",
		statusCode: status.NotFound,
	}

	// CountyCodeNotFound ...
	CountyCodeNotFound = ServerError{
		Message:    "county code not found in star",
		Code:       "COUNTY_CODE_NOT_FOUND_ERROR",
		statusCode: status.NotFound,
	}

	// CountyNameNotFound ...
	CountyNameNotFound = ServerError{
		Message:    "county name not found in star",
		Code:       "COUNTY_NAME_NOT_FOUND_ERROR",
		statusCode: status.NotFound,
	}

	// CountryNameNotFound ...
	CountryNameNotFound = ServerError{
		Message:    "country name not found in star",
		Code:       "COUNTRY_NAME_NOT_FOUND_ERROR",
		statusCode: status.NotFound,
	}

	// ZipCodeNotFound ...
	ZipCodeNotFound = ServerError{
		Message:    "zip code not found in star",
		Code:       "ZIP_CODE_NOT_FOUND_ERROR",
		statusCode: status.NotFound,
	}

	// CountyZipCodeNotFound ...
	CountyZipCodeNotFound = ServerError{
		Message:    "county zip code not found in star",
		Code:       "COUNTY_ZIP_CODE_NOT_FOUND_ERROR",
		statusCode: status.NotFound,
	}

	// ForeignKeyViolationError ...
	ForeignKeyViolationError = ServerError{
		Message:    "missing one or more required foreign key",
		Code:       "FOREIGN_KEY_VIOLATION_ERROR",
		statusCode: status.Invalid,
	}

	// ZipCodeInvalid ...
	ZipCodeInvalid = ServerError{
		Message:    "invalid zipcode",
		Code:       "ZIP_CODE_INVALID_ERROR",
		statusCode: status.Invalid,
	}

	// CountyCodeParseError ...
	CountyCodeParseError = ServerError{
		Message:    "parsing county code failed",
		Code:       "COUNTRY_CODE_PARSE_ERROR",
		statusCode: status.Invalid,
	}

	// UnexpectedExternalSourceError ...
	UnexpectedExternalSourceError = ServerError{
		Message:    "unexpected external source",
		Code:       "UNEXPECTED_EXTERNAL_SOURCE_ERROR",
		statusCode: status.Error,
	}
)
