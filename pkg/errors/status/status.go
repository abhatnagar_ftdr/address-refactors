package status

import (
	"net/http"
)

// Code way to determine how function execution completed
type Code uint8

// Defines the possible codes that can occur during an error
const (
	Error Code = iota
	NotFound
	AlreadyExists
	Unavailable
	Invalid
)

var (
	codeToString = map[Code]string{
		Error:         "Error",
		NotFound:      "NotFound",
		AlreadyExists: "AlreadyExists",
		Unavailable:   "Unavailable",
		Invalid:       "Invalid",
	}

	codeToHTTPCode = map[Code]int{
		Error:         http.StatusInternalServerError,
		NotFound:      http.StatusNotFound,
		AlreadyExists: http.StatusConflict,
		Unavailable:   http.StatusServiceUnavailable,
		Invalid:       http.StatusBadRequest,
	}
)

// String converts a Code to it's string counter part, default is "Unknown"
func (c Code) String() string {
	if val, ok := codeToString[c]; ok {
		return val
	}

	return "Unknown"
}

// HTTPStatusCode maps a Code to a specific HTTP status code int, default is 500
func (c Code) HTTPStatusCode() int {
	if val, ok := codeToHTTPCode[c]; ok {
		return val
	}

	return http.StatusInternalServerError
}
