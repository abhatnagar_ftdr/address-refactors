package errors

import (
	"fmt"

	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors/status"
)

// ServerError is any error (user or internal) that could go wrong during the duration of a request.
// This could include bad requests, not found errors, query errors, etc. The statusCode maps to the
// http status code associated as defined in package status.
type ServerError struct {
	Message    string
	Code       string
	statusCode status.Code
}

// New returns a new instance of the server error to avoid message or code editing on const errors
func (err *ServerError) New() *ServerError {
	return &ServerError{
		Message:    err.Message,
		Code:       err.Code,
		statusCode: err.statusCode,
	}
}

// NewFormatted returns a new formatted message and passes all args into the formatted Message string on a ServerError
func (err *ServerError) NewFormatted(messageArg ...interface{}) *ServerError {
	return &ServerError{
		Message:    fmt.Sprintf(err.Message, messageArg...),
		Code:       err.Code,
		statusCode: err.statusCode,
	}
}

// HTTPStatusCode returns the associated status code with an error based on it's definition
func (err *ServerError) HTTPStatusCode() int {
	return err.statusCode.HTTPStatusCode()
}

// Error writes out the code and message of an error as a string in the form of "<code>: <message>"
func (err *ServerError) Error() string {
	return err.Code + ": " + err.Message
}

// StatusCode return status code
func (err *ServerError) IsServerFault() bool {
	return err.statusCode == status.Error || err.statusCode == status.Unavailable
}
