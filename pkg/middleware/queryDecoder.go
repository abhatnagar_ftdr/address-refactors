package middleware

import (
	"net/http"

	commonMiddleware "go.ftdr.com/go-utils/common/middleware"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/handlers"
)

// QueryDecoderWrapper will wrap the common query decoder to ensure it is only used
// when the content type is protobuf.
func QueryDecoderWrapper(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if handlers.GetRequestContentType(r) == handlers.ProtobufContentType {
			commonMiddleware.QueryDecoderMiddleware(next).ServeHTTP(w, r)
		} else {
			next.ServeHTTP(w, r)
		}
	})
}
