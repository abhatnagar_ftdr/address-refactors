package middleware

import (
	"net/http"

	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/handlers"
)

// ContentTypeUnifierMiddleware will unify the content types of the request/response
func ContentTypeUnifierMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		unifyContentTypes(w, r)

		next.ServeHTTP(w, r)
	})
}

// Sets the writer content type to the accept type of the request. Default case
// is to set the response writer to the request content type
func unifyContentTypes(w http.ResponseWriter, r *http.Request) {
	switch handlers.GetRequestAcceptType(r) {
	case handlers.JSONContentType:
		handlers.SetResponseContentType(w, handlers.JSONContentType)
	case handlers.ProtobufContentType:
		handlers.SetResponseContentType(w, handlers.ProtobufContentType)
	default:
		if handlers.GetRequestContentType(r) != "" {
			handlers.SetResponseContentType(w, handlers.GetRequestContentType(r))
		}
	}
}

func setCORSHeaders(w http.ResponseWriter, r *http.Request) {

}
