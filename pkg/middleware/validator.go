package middleware

import (
	"net/http"
	"regexp"
	"strconv"

	"github.com/gorilla/mux"
	"go.ftdr.com/go-utils/common/logging"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/handlers"
	addressErrors "golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors"
	"golang.frontdoorhome.com/go-utils/validators"
)

var (
	zip9Regex = regexp.MustCompile("^[0-9]{5}-?[0-9]{4}$")
)

// RouteContent defines a route and whether the route accepts JSON
type RouteContent struct {
	DefaultContent string
	AcceptsJSON    bool
}

// ValidatorMiddleWare validates the content types of a request
type ValidatorMiddleWare struct {
	AcceptedContent map[string]*RouteContent
}

// SetAcceptedContent actually sets the accepted content for a http post
func (vmw *ValidatorMiddleWare) SetAcceptedContent(accepted map[string]*RouteContent) {
	vmw.AcceptedContent = accepted
}

// Middleware is the handler for content type validation
func (vmw *ValidatorMiddleWare) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		routeName := mux.CurrentRoute(r).GetName()

		validPathVariables := validatePathVariables(r, w)
		if !validPathVariables {
			return
		}

		success := validate(r, vmw.AcceptedContent[routeName])
		if !success {
			log := logging.GetTracedLogEntry(r.Context())
			fields := map[string]interface{}{
				"DB":  r.RemoteAddr,
				"Uri": r.RequestURI,
			}
			log.WithFields(fields).Debug()

			handlers.SendErrorMessage(r.Context(), w, addressErrors.ContentTypeError.New())
			return
		}

		next.ServeHTTP(w, r)
	})
}

func validate(r *http.Request, route *RouteContent) (success bool) {
	if handlers.GetRequestContentType(r) == "" {
		(*r).Header.Set("Content-Type", route.DefaultContent)
	}

	if route.AcceptsJSON && handlers.GetRequestContentType(r) == handlers.JSONContentType {
		return true
	}

	return handlers.GetRequestContentType(r) == route.DefaultContent
}

func validatePathVariables(r *http.Request, w http.ResponseWriter) (success bool) {
	ctx := (*r).Context()
	log := logging.GetTracedLogEntry(ctx)

	if val, ok := mux.Vars(r)["legacy_id"]; ok {
		_, conversionErr := strconv.ParseUint(val, 10, 32)
		if conversionErr != nil {
			err := addressErrors.InvalidLegacyIDError.NewFormatted(val)
			log.WithField("LegacyID", val).WithField("Error", conversionErr).Info(err.Error())
			handlers.SendErrorMessage(r.Context(), w, err)
			return false
		}
	}

	if id, ok := mux.Vars(r)["uuid"]; ok {
		if !validators.IsValidGUID(id) {
			err := addressErrors.InvalidIDError.NewFormatted(id)
			log.WithField("ID", id).Info(err.Error())
			handlers.SendErrorMessage(r.Context(), w, err)
			return false
		}
	}

	if id, ok := mux.Vars(r)["uuid_or_legacy_id"]; ok {
		if _, conversionErr := strconv.ParseUint(id, 10, 32); conversionErr != nil && !validators.IsValidGUID(id) {
			err := addressErrors.InvalidIDAndLegacyIDError.NewFormatted(id)
			log.WithField("ID or LegacyID", id).Info(err.Error())
			handlers.SendErrorMessage(r.Context(), w, err)
			return false
		}
	}

	if zip, ok := mux.Vars(r)["zip"]; ok {
		if !validators.IsValidZip(zip) && !IsValidZip9(zip) {
			err := addressErrors.InvalidZipCodeError.NewFormatted(zip)
			log.WithField("Zip", zip).Info(err.Error())
			handlers.SendErrorMessage(r.Context(), w, err)
			return false
		}
	}

	return true
}

// IsValidZip9 the validators package regex for zip9Regex doesn't account for a hyphen, i.e. 80202-2111
// RGMC: Update validators, then remove from here.
func IsValidZip9(zip9 string) bool {
	return zip9Regex.MatchString(zip9)
}
