module golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor

go 1.18

require (
	github.com/garyburd/redigo v1.6.0
	github.com/godror/godror v0.36.0
	github.com/gogo/protobuf v1.3.2
	github.com/golang/mock v1.6.0
	github.com/golang/protobuf v1.5.2
	github.com/google/uuid v1.3.0
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.3.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/pkg/errors v0.9.1
	github.com/rafaeljusto/redigomock v2.3.0+incompatible
	github.com/sijms/go-ora/v2 v2.5.33
	github.com/sirupsen/logrus v1.8.1
	github.com/smartystreets/smartystreets-go-sdk v1.15.1
	github.com/stretchr/testify v1.8.1
	go.ftdr.com/go-utils/common v1.1.9
	go.ftdr.com/go-utils/instrumentation/v3 v3.5.3
	go.ftdr.com/go-utils/mongo/v2 v2.6.9
	go.ftdr.com/go-utils/switch/v2 v2.3.1
	go.ftdr.com/go-utils/xormwrapper/v2 v2.0.4
	go.mongodb.org/mongo-driver v1.10.6
	golang.frontdoorhome.com/go-utils/validators v0.2.0
	golang.frontdoorhome.com/software/protos v1.0.3349
	golang.org/x/exp v0.0.0-20230118134722-a68e582fa157
	google.golang.org/protobuf v1.28.1
	gotest.tools v2.2.0+incompatible
)

require (
	github.com/armon/go-radix v1.0.0 // indirect
	github.com/avast/retry-go v2.6.0+incompatible // indirect
	github.com/badoux/checkmail v1.2.1 // indirect
	github.com/bits-and-blooms/bitset v1.3.1 // indirect
	github.com/bits-and-blooms/bloom/v3 v3.3.1 // indirect
	github.com/cespare/xxhash v1.1.0 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/coocood/freecache v1.1.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/elastic/go-licenser v0.4.0 // indirect
	github.com/elastic/go-sysinfo v1.7.1 // indirect
	github.com/elastic/go-windows v1.0.1 // indirect
	github.com/emirpasic/gods v1.12.0 // indirect
	github.com/felixge/httpsnoop v1.0.1 // indirect
	github.com/fsnotify/fsnotify v1.5.4 // indirect
	github.com/go-logfmt/logfmt v0.5.1 // indirect
	github.com/go-logr/logr v1.2.3 // indirect
	github.com/go-redis/redis/v8 v8.11.5 // indirect
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/goccy/go-json v0.10.0 // indirect
	github.com/godror/knownpb v0.1.0 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/gomodule/redigo v2.0.0+incompatible // indirect
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/hashicorp/errwrap v1.0.0 // indirect
	github.com/hashicorp/go-multierror v1.1.0 // indirect
	github.com/jcchavezs/porto v0.1.0 // indirect
	github.com/joeshaw/multierror v0.0.0-20140124173710-69b34d4ec901 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/klauspost/compress v1.15.9 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/montanaflynn/stats v0.5.0 // indirect
	github.com/opentracing-contrib/go-stdlib v1.0.0 // indirect
	github.com/opentracing/opentracing-go v1.2.0 // indirect
	github.com/optimizely/go-sdk v1.8.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/prometheus/procfs v0.7.3 // indirect
	github.com/robfig/cron v1.2.0 // indirect
	github.com/rs/cors v1.7.0 // indirect
	github.com/santhosh-tekuri/jsonschema v1.2.4 // indirect
	github.com/splitio/go-client/v6 v6.2.1 // indirect
	github.com/splitio/go-split-commons/v4 v4.2.0 // indirect
	github.com/splitio/go-toolkit/v5 v5.2.2 // indirect
	github.com/syndtr/goleveldb v1.0.0 // indirect
	github.com/ttacon/builder v0.0.0-20170518171403-c099f663e1c2 // indirect
	github.com/ttacon/libphonenumber v1.2.1 // indirect
	github.com/twmb/murmur3 v1.1.6 // indirect
	github.com/uber/jaeger-client-go v2.29.1+incompatible // indirect
	github.com/uber/jaeger-lib v2.4.1+incompatible // indirect
	github.com/xdg-go/pbkdf2 v1.0.0 // indirect
	github.com/xdg-go/scram v1.1.1 // indirect
	github.com/xdg-go/stringprep v1.0.3 // indirect
	github.com/youmark/pkcs8 v0.0.0-20181117223130-1be2e3e5546d // indirect
	go.elastic.co/apm/module/apmhttp/v2 v2.0.0 // indirect
	go.elastic.co/apm/module/apmlogrus/v2 v2.0.0 // indirect
	go.elastic.co/apm/v2 v2.1.0 // indirect
	go.elastic.co/fastjson v1.1.0 // indirect
	go.ftdr.com/go-utils/common/v2 v2.0.0 // indirect
	go.ftdr.com/go-utils/oauth-client v0.1.5 // indirect
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.7.0 // indirect
	go.uber.org/zap v1.18.1 // indirect
	golang.org/x/crypto v0.7.0 // indirect
	golang.org/x/mod v0.9.0 // indirect
	golang.org/x/net v0.8.0 // indirect
	golang.org/x/oauth2 v0.5.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
	golang.org/x/text v0.8.0 // indirect
	golang.org/x/tools v0.6.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/genproto v0.0.0-20230306155012-7f2fa6fef1f4 // indirect
	google.golang.org/grpc v1.53.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	howett.net/plist v1.0.0 // indirect
	xorm.io/builder v0.3.12 // indirect
	xorm.io/xorm v1.3.3-0.20230212031653-52855dae32d7 // indirect
)

