include:
  - project: "ftdr/ftdr-pipeline/deployinator-go"
    ref: "main"
    file: ".go-ms.gitlab-ci.yml"

variables:
  JIRA_PROJECT_ID: "CSVC"

stages:
  - validate
  - test
  - build
  - docker-image
  - container-scan
  - deploy-dev
  - integration_tests
  - deploy-test
  - smokeTest-test
  - deploy-training
  - deploy-sandbox
  - deploy-staging
  - deploy-production

test:
  stage: test
  coverage: '/Average test coverage: ([0-9\.]+%)/'
  image: golang:1.18

build:
  image: golang:1.18

deploy-dev:
  environment:
    name: development
  stage: deploy-dev
  extends: .deployinator
  variables:
    INPUT_DSL: deployment/dev_input_caas_v2.yaml
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: "$CI_COMMIT_REF_NAME"
  needs: ["docker-image"]

deploy-test:
  environment:
    name: test
  stage: deploy-test
  extends: .deployinator
  variables:
    INPUT_DSL: deployment/test_input_caas_v2.yaml
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: manual
  needs: ["deploy-dev"]

go-review:
  stage: test
  before_script:
    - export GO111MODULE=on
    - export GOPRIVATE=go.ftdr.com*,golang.frontdoorhome.com*
    - export MY_POD_NAMESPACE=development
    - export REVIEWDOG_GITLAB_API_TOKEN=${GITLAB_TOKEN}
  script:
    - git config --global url."https://$GITLAB_ID:$GITLAB_TOKEN@gitlab.com/ftdr".insteadOf "https://gitlab.com/ftdr"
    - git config --global url."https://$GITLAB_ID:$GITLAB_TOKEN@golang.frontdoorhome.com/software".insteadOf "https://golang.frontdoorhome.com/software"
    - go install go.ftdr.com/software/go-review@latest
    - go install github.com/reviewdog/reviewdog/cmd/reviewdog@latest
    - go-review  --SA1029.enabled=true --errorCodeCategoryChecker.enabled=true --hardcodedStringChecker.enabled=true --errorCodePrefixChecker.enabled=true ./... | reviewdog -f=staticcheck -name=goreview-discussion -reporter=gitlab-mr-discussion
  rules:
    - if: $CI_COMMIT_REF_NAME == "master"
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: never
    - when: always
  allow_failure: true

regression-test:
  stage: smokeTest-test
  trigger:
    project: ftdr/qa-automation/Address_MS_Api_Automation
    branch: "main"
    strategy: depend
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
  needs: [ "deploy-test" ]

deploy-training:
  environment:
    name: training
  stage: deploy-training
  extends: .deployinator
  variables:
    INPUT_DSL: deployment/training_input_caas_v2.yaml
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: manual
  needs: ["deploy-test"]

deploy-sandbox:
  environment:
    name: sandbox
  stage: deploy-sandbox
  extends: .deployinator
  variables:
    INPUT_DSL: deployment/sandbox_input_caas_v2.yaml
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: manual
  needs: ["docker-image"]

deploy-staging:
  environment:
    name: staging
  stage: deploy-staging
  extends: .deployinator
  variables:
    INPUT_DSL: deployment/staging_input_caas_v2.yaml
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: manual
  needs: ["deploy-test"]

deploy-production:
  environment:
    name: production
  stage: deploy-production
  extends: .deployinator
  variables:
    INPUT_DSL: deployment/prod_input_caas_v2.yaml
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: manual
  needs: ["deploy-staging"]
