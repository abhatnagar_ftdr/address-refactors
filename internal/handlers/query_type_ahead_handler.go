package handlers

import (
	"net/http"

	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
	addressErrors "golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors"
	pb "golang.frontdoorhome.com/software/protos/go/addresspb"
)

// QueryTypeAheadHandler is the handler for the GET /address_type_ahead route using query parameters
func (h *Handler) QueryTypeAheadHandler(w http.ResponseWriter, r *http.Request) {
	ctx, log := initializeHandlerBasics(r)

	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.QueryTypeAheadAPIName, literals.ComponentTypeAddress)
	componentContextData := map[string]interface{}{}

	typeaheadReq := typeaheadFromQuery(r)
	if errs := ValidateTypeAheadRequest(ctx, typeaheadReq); len(errs) != 0 {
		errString := ExtractErrorMessages(errs)
		log.Infof("query type ahead request failed validation check: %s", errString)
		componentContextData[literals.ContextErrorMessage] = errString
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, addressErrors.TypeaheadInvalidRequestError.NewFormatted(errString))
		return
	}

	suggestions := h.service.GetTypeAheadSuggestions(ctx, typeaheadReq)
	if len(suggestions) == 0 {
		suggestErr := addressErrors.TypeaheadNotFoundError.New()
		log.Infof("query type ahead request failed to find suggestions: %s", suggestErr.Error())
		componentContextData[literals.ContextErrorMessage] = suggestErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, suggestErr)
		return
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	SendSuccessMessage(ctx, w, &pb.AddressByTypeAheadResponse{Addresses: suggestions})
}

// GetTypeAheadHandler is the handler for the GET /address_type_ahead route
func (h *Handler) GetTypeAheadHandler(w http.ResponseWriter, r *http.Request) {
	ctx, log := initializeHandlerBasics(r)

	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.GetTypeAheadAPIName, literals.ComponentTypeAddress)
	componentContextData := map[string]interface{}{}

	var getTypeAheadRequest pb.AddressByTypeAheadRequest
	if unmarshalErr := unmarshalRequest(ctx, r, &getTypeAheadRequest); unmarshalErr != nil {
		log.Errorf("get type ahead request unmarshaling failed: %s", unmarshalErr.Error())
		componentContextData[literals.ContextErrorMessage] = unmarshalErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, unmarshalErr)
		return
	}

	if errs := ValidateTypeAheadRequest(ctx, &getTypeAheadRequest); len(errs) != 0 {

		errString := ExtractErrorMessages(errs)
		log.Infof("get type ahead request failed validation check: %s", errString)
		componentContextData[literals.ContextErrorMessage] = errString
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, addressErrors.TypeaheadInvalidRequestError.NewFormatted(errString))
		return
	}

	suggestions := h.service.GetTypeAheadSuggestions(ctx, &getTypeAheadRequest)
	if len(suggestions) == 0 {
		suggestErr := addressErrors.TypeaheadNotFoundError.New()
		log.Infof("get type ahead request failed to find suggestions: %s", suggestErr.Error())
		componentContextData[literals.ContextErrorMessage] = suggestErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, suggestErr)
		return
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	SendSuccessMessage(ctx, w, &pb.AddressByTypeAheadResponse{Addresses: suggestions})
}
