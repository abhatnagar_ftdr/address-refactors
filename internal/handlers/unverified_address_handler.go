package handlers

import (
	"context"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/convert"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
	pb "golang.frontdoorhome.com/software/protos/go/addresspb"
	"net/http"
	"regexp"
)

// PostUnverifiedAddressHandler is the handler for the POST /unverified_address route
func (h *Handler) PostUnverifiedAddressHandler(w http.ResponseWriter, r *http.Request) {
	ctx, log := initializeHandlerBasics(r)
	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.PostUnverifiedAddressAPIName, literals.ComponentTypeAddress)
	componentContextData := map[string]interface{}{}

	request := &pb.UnverifiedAddressRequest{}
	if unmarshalErr := unmarshalRequest(ctx, r, request); unmarshalErr != nil {
		log.Errorf("post unverified address request unmarshaling failed: %s", unmarshalErr.Error())
		componentContextData[literals.ContextErrorMessage] = unmarshalErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, unmarshalErr)
		return
	}

	StandardizeUnverifiedAddressRequest(ctx, request)

	if validationErrs := ValidateUnverifiedAddressRequest(ctx, request); len(validationErrs) != 0 {
		errString := ExtractErrorMessages(validationErrs)
		log.Infof("post unverified address request failed validation check: %s", errString)
		componentContextData[literals.ContextErrorMessage] = errString
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendValidationErrorsMessage(ctx, w, validationErrs)
		return
	}

	convertedAddress, err := convert.PBAddressToRecord(ctx, request.Address)
	if err != nil {
		log.Errorf("post unverified address request conversion failed: %s", err.Error())
		componentContextData[literals.ContextErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, err)
		return
	}
	address, err := h.service.PostUnverifiedAddress(ctx, convertedAddress, request.ExternalSource, request.Force)
	if err != nil {
		log.Infof("post unverified address request failed: %s", err.Error())
		componentContextData[literals.ContextErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, err)
		return
	}

	if unverifiedAddress, err := convert.RecordToPBAddress(ctx, address); err != nil {
		componentContextData[literals.ContextErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		SendErrorMessage(ctx, w, err)
	} else {
		instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
		SendSuccessMessage(ctx, w, &pb.UnverifiedAddressResponse{Address: unverifiedAddress})
	}
}

func StandardizeUnverifiedAddressRequest(ctx context.Context, request *pb.UnverifiedAddressRequest) {
	request.Address.UnitType, request.Address.UnitValue = StandardizeUnitTypeAndValue(request.Address.UnitType,
		request.Address.UnitValue)
	trimmedStreet, removed := TrimStreet1(ctx, request.Address.Street1, request.Address.City,
		request.Address.State, request.Address.Zip, request.Address.CountryIso3)
	if removed > 0 {
		request.Address.Street1 = trimmedStreet
	}
	re := regexp.MustCompile(InvalidCharactersPattern)
	request.Address.Street1 = re.ReplaceAllString(request.Address.Street1, "")
	request.Address.Street2 = re.ReplaceAllString(request.Address.Street2, "")
}
