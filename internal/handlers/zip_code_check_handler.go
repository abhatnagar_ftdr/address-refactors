package handlers

import (
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
	pb "golang.frontdoorhome.com/software/protos/go/addresspb"
	"net/http"
)

// ZipCodeCheckHandler is the handler for the GET /address/zipcodecheck
func (h *Handler) ZipCodeCheckHandler(w http.ResponseWriter, r *http.Request) {
	ctx, log := initializeHandlerBasics(r)

	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.ZipCodeCheckAPIName, literals.ComponentTypeAddress)
	componentContextData := map[string]interface{}{}

	zipCodeStateCheckRequest := &pb.ZipCodeStateCheckRequest{}
	if unmarshalErr := unmarshalRequest(ctx, r, zipCodeStateCheckRequest); unmarshalErr != nil {
		log.Errorf("get address request unmarshaling failed: %s", unmarshalErr.Error())
		componentContextData[literals.ContextErrorMessage] = unmarshalErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, unmarshalErr)
		return
	}

	// TODO: validate request

	matched, notMatched, err := h.service.ZipCodeStateCheck(ctx, zipCodeStateCheckRequest.ZipCodes, zipCodeStateCheckRequest.States)
	if err != nil {
		log.Errorf("zipcode state check request failed: %s", err.Error())
		componentContextData[literals.ContextErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, err)
		return
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	SendSuccessMessage(ctx, w, &pb.ZipCodeStateCheckResponse{
		ZipCodesInStates:    matched,
		ZipCodesNotInStates: notMatched,
	})
}
