package handlers

import (
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/mongo_dal"
	"net/http"
	"strconv"

	addressErrors "golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors"

	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/convert"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
	pb "golang.frontdoorhome.com/software/protos/go/addresspb"
)

// GetAddressByIDHandler is the handler for the GET /address/{uuid} route
func (h *Handler) GetAddressByIDHandler(w http.ResponseWriter, r *http.Request) {
	ctx, log := initializeHandlerBasics(r)

	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.GetAddressByUUIDOrLegacyIDAPIName, literals.ComponentTypeAddress)
	componentContextData := map[string]interface{}{}

	id, idType := getValidatedUUIDOrLegacyIDPathVariable(r)
	var addressRecord *mongo_dal.AddressRecord
	var err *addressErrors.ServerError
	if idType == UUID {
		addressRecord, err = h.service.GetAddressRecordByID(ctx, id)
	} else if idType == LegacyID {
		legacyID, _ := strconv.ParseUint(id, 10, 32)
		addressRecord, err = h.service.GetAddressByLegacyID(ctx, uint32(legacyID), pb.ExternalSource_UNKNOWN_SOURCE)
	}
	if err != nil {
		if err.IsServerFault() {
			log.Errorf("get address by uuid or legacyID request failed: %s", err.Error())
			componentContextData[literals.ContextErrorMessage] = err.Error()
		}
		instrumentation.ComponentSpanEnd(ctx, spanID, !err.IsServerFault(), componentContextData)

		SendErrorMessage(ctx, w, err)
		return
	}

	if addressRecord.Status == mongo_dal.UnverifiedStatus {
		verifiedAddressRecord, err := h.service.VerifyAddress(ctx, addressRecord)
		if err != nil {
			if err.Code != addressErrors.AddressNotVerifiableError.Code {
				log.Errorf("address verification failed failed: %s", err.Error())
				componentContextData[literals.ContextErrorMessage] = err.Error()
				instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
				SendErrorMessage(ctx, w, err)
				return
			} else {
				log.Infof("address for id=%s could not be verified: %s", id, err.Error())
			}
		} else {
			log.Infof("address for id=%s verified succesfully", id)
			addressRecord = verifiedAddressRecord
		}
	}

	if addresspbAddress, err := convert.RecordToPBAddress(ctx, addressRecord); err != nil {
		log.Errorf("get address by id request failed for address conversion: %s", err.Error())
		componentContextData[literals.ContextErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, err)
	} else {
		instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
		SendSuccessMessage(ctx, w, &pb.AddressByUUIDResponse{Address: addresspbAddress})
	}
}

// GetAddressByLegacyIDHandler is the handler for the GET /address/by_legacy_id/{legacy_id} route
func (h *Handler) GetAddressByLegacyIDHandler(w http.ResponseWriter, r *http.Request) {
	ctx, log := initializeHandlerBasics(r)

	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.GetAddressByLegacyIDAPIName, literals.ComponentTypeAddress)
	componentContextData := map[string]interface{}{}

	legacyID := getValidatedLegacyIDPathVariable(r)

	request := &pb.AddressByLegacyIDRequest{}
	if unmarshalErr := unmarshalRequest(ctx, r, request); unmarshalErr != nil && unmarshalErr.Code != addressErrors.EmptyBodyError.Code {
		log.Errorf("get address request unmarshaling failed: %s", unmarshalErr.Error())
		componentContextData[literals.ContextErrorMessage] = unmarshalErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, unmarshalErr)
		return
	}

	addressRecord, err := h.service.GetAddressByLegacyID(ctx, legacyID, request.ExternalSource)

	if err != nil {
		log.Errorf("get address by legacy id request failed: %s", err.Error())
		componentContextData[literals.ContextErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, err)
		return
	}

	if addressRecord.Status == mongo_dal.UnverifiedStatus {
		verifiedAddressRecord, err := h.service.VerifyAddress(ctx, addressRecord)
		if err != nil {
			if err.Code != addressErrors.AddressNotVerifiableError.Code {
				log.Errorf("get address by uuid or legacyID request failed: %s", err.Error())
				componentContextData[literals.ContextErrorMessage] = err.Error()
				instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
				SendErrorMessage(ctx, w, err)
				return
			} else {
				log.Infof("address for could not be verified: %s", err.Error())
			}
		} else {
			log.Info("address for verified succesfully")
			addressRecord = verifiedAddressRecord
		}
	}

	log.WithField("Fetched Address", addressRecord).Info("fetched address record")
	if addresspbAddress, err := convert.RecordToPBAddress(ctx, addressRecord); err != nil {
		log.Errorf("get address by legacy id request failed at address conversion: %s", err.Error())
		componentContextData[literals.ContextErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, err)
	} else {
		instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
		SendSuccessMessage(ctx, w, &pb.AddressByUUIDResponse{Address: addresspbAddress})
	}
}
