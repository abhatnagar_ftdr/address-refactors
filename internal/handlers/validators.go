package handlers

import (
	"context"
	"fmt"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
	"reflect"
	"regexp"
	"strings"

	"go.ftdr.com/go-utils/common/logging"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/utils"
	addressErrors "golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors"
	pb "golang.frontdoorhome.com/software/protos/go/addresspb"
)

var (
	iso3Regex = regexp.MustCompile(`^[A-Za-z]{3}`)
	zipRegex  = regexp.MustCompile("^[0-9]{5}(?:-[0-9]{4})?$")
)

const (
	streetNameMaxLen      = 45
	streetNumberMaxLen    = 25
	streetDirectionMaxLen = 2
)

// ValidateTypeAheadRequest validates that a search has a non empty valid street line
func ValidateTypeAheadRequest(ctx context.Context, r *pb.AddressByTypeAheadRequest) (errs []*addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)
	log = log.WithField("Request", r)

	errs = make([]*addressErrors.ServerError, 0)
	// first check to see if the request is nil
	if r == nil {
		err := addressErrors.AddressNotFoundError.New()
		log.WithError(err).Warn("address by type ahead request is not defined")
		errs = append(errs, err)
		return errs
	}

	if utils.IsBlank(r.StreetLine) || containsInvalidCharacters(ctx, r.StreetLine) {
		err := addressErrors.InvalidAddressStreet1Error.New()
		log.WithError(err).Infof("invalid street line: %s", r.StreetLine)
		errs = append(errs, err)
	}

	if utils.IsNotBlank(r.City) && utils.IsBlank(r.State) {
		err := addressErrors.InvalidTypeaheadCityAndStateError.New()
		log.WithError(err).Info("invalid city&state combination: state is blank")
		errs = append(errs, err)
	}

	errs = append(errs, UniversalStringValidation(ctx, reflect.ValueOf(r).Elem())...)
	return errs
}

// ValidateAddressRequest validates AddressRequest fields
func ValidateAddressRequest(ctx context.Context, r *pb.AddressRequest) (errs []*addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)
	log = log.WithField("Request", r)
	log.Debug("validating request")

	errs = make([]*addressErrors.ServerError, 0)
	// first check to see if the request is nil
	if r == nil {
		err := addressErrors.AddressNotFoundError.New()
		log.WithError(err).Warn("address request is not defined")
		errs = append(errs, err)
		return errs
	}

	if utils.IsBlank(r.Street1) {
		err := addressErrors.EmptyAddressStreet1Error.New()
		log.WithError(err).Info(err.Error())
		errs = append(errs, err)
	} else {
		sc := utils.ParseStreetAddress(r.Street1)
		if utils.IsBlank(sc.StreetName) {
			err := addressErrors.InvalidAddressStreet1Error.New()
			log.WithError(err).Info(err.Error())
			errs = append(errs, err)
		}

		if len(sc.StreetName) > streetNameMaxLen {
			err := addressErrors.InvalidAddressFieldTooLongError.
				NewFormatted("StreetName", len(sc.StreetName), streetNameMaxLen)
			log.WithError(err).Info(err.Error())
			errs = append(errs, err)
		}

		if len(sc.StreetNumber) > streetNumberMaxLen {
			err := addressErrors.InvalidAddressFieldTooLongError.
				NewFormatted("StreetNumber", len(sc.StreetNumber), streetNumberMaxLen)
			log.WithError(err).Info(err.Error())
			errs = append(errs, err)
		}

		if len(sc.StreetDirection) > streetDirectionMaxLen {
			err := addressErrors.InvalidAddressFieldTooLongError.
				NewFormatted("StreetDirection", len(sc.StreetDirection), streetDirectionMaxLen)
			log.WithError(err).Info(err.Error())
			errs = append(errs, err)
		}
	}

	if (utils.IsBlank(r.City) || utils.IsBlank(r.State)) && utils.IsBlank(r.Zip) {
		err := addressErrors.InvalidAddressCityStateOrZipError.New()
		log.WithError(err).Info(err.Error())
		errs = append(errs, err)
	}

	if utils.IsNotBlank(r.State) && !isValidState(r.State) {
		err := addressErrors.InvalidStateError.NewFormatted(r.State)
		log.WithError(err).Info(err.Error())
		errs = append(errs, err)
	}

	if utils.IsNotBlank(r.Zip) && !isValidUSZipCode(r.Zip) {
		err := addressErrors.InvalidZipCodeError.NewFormatted(r.Zip)
		log.WithError(err).Info(err.Error())
		errs = append(errs, err)
	}

	if !iso3Regex.MatchString(r.CountryIso3) {
		err := addressErrors.InvalidAddressCountryError.NewFormatted(r.CountryIso3)
		log.WithError(err).Info(err.Error())
		errs = append(errs, err)
	}

	errs = append(errs, UniversalStringValidation(ctx, reflect.ValueOf(r).Elem())...)
	return errs
}

// ValidateUnverifiedAddressRequest validates UnverifiedAddressRequest fields
func ValidateUnverifiedAddressRequest(ctx context.Context, r *pb.UnverifiedAddressRequest) (errs []*addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)
	log = log.WithField("Request", r)
	log.Debug("validating request")

	errs = make([]*addressErrors.ServerError, 0)
	// first check to see if the request is nil
	if r == nil {
		err := addressErrors.AddressNotFoundError.New()
		log.WithError(err).Warn("unverified address request is not defined")
		errs = append(errs, err)
		return errs
	}

	// check to see if the Address field is nil or not
	if r.Address == nil {
		err := addressErrors.AddressNotFoundError.New()
		log.
			WithError(err).
			WithField("Request", r).
			Warn("unverified address request does not contain any address that needs verification")
		errs = append(errs, err)
		return errs
	}

	if utils.IsBlank(r.Address.Street1) {
		err := addressErrors.EmptyAddressStreet1Error.New()
		log.WithError(err).Info(err.Error())
		errs = append(errs, err)
	} else {
		sc := utils.ParseStreetAddress(r.Address.Street1)
		if utils.IsBlank(sc.StreetName) && utils.IsBlank(utils.ParseStreetAddress(r.Address.Street2).StreetName) {
			err := addressErrors.InvalidAddressStreet1Error.New()
			log.WithError(err).WithField("Request", r).Info(err.Error())
			errs = append(errs, err)
		}

		if len(sc.StreetName) > streetNameMaxLen {
			err := addressErrors.InvalidAddressFieldTooLongError.
				NewFormatted("StreetName", len(sc.StreetName), streetNameMaxLen)
			log.WithError(err).Info(err.Error())
			errs = append(errs, err)
		}

		if len(sc.StreetNumber) > streetNumberMaxLen {
			err := addressErrors.InvalidAddressFieldTooLongError.
				NewFormatted("StreetNumber", len(sc.StreetNumber), streetNumberMaxLen)
			log.WithError(err).Info(err.Error())
			errs = append(errs, err)
		}

		if len(sc.StreetDirection) > streetDirectionMaxLen {
			err := addressErrors.InvalidAddressFieldTooLongError.
				NewFormatted("StreetDirection", len(sc.StreetDirection), streetDirectionMaxLen)
			log.WithError(err).Info(err.Error())
			errs = append(errs, err)
		}
	}

	if utils.IsNotBlank(r.Address.UnitValue) && strings.ContainsAny(r.Address.UnitValue, ",") {
		err := addressErrors.ContainsInvalidCharactersError.NewFormatted("UnitValue")
		log.WithError(err).Info(err.Error())
		errs = append(errs, err)
	}

	if match := matchCityStateZip(r.Address.Street1); match != nil {
		log.WithError(addressErrors.InvalidAddressCityStateAndZipError.New()).WithFields(match).
			Info("Overriding validation error due to address detected in `street1`.")
		return UniversalStringValidation(ctx, reflect.ValueOf(r.Address).Elem())
	}

	if utils.IsBlank(r.Address.City) || utils.IsBlank(r.Address.State) || utils.IsBlank(r.Address.Zip) {
		err := addressErrors.InvalidAddressCityStateAndZipError.New()
		log.WithError(err).Info(err.Error())
		errs = append(errs, err)
	}

	if utils.IsNotBlank(r.Address.State) && !isValidState(r.Address.State) {
		err := addressErrors.InvalidStateError.NewFormatted(r.Address.State)
		log.WithError(err).Info(err.Error())
		errs = append(errs, err)
	}

	if !isValidUSZipCode(r.Address.Zip) {
		err := addressErrors.InvalidZipCodeError.NewFormatted(r.Address.Zip)
		log.WithError(err).Info(err.Error())
		errs = append(errs, err)
	}

	if !iso3Regex.MatchString(r.Address.CountryIso3) {
		err := addressErrors.InvalidAddressCountryError.NewFormatted(r.Address.CountryIso3)
		log.WithError(err).Info(err.Error())
		errs = append(errs, err)
	}

	if r.Address.Attributes != nil {
		err := validateAddressRecordType(r.Address.Attributes.RecordType)
		if err != nil {
			errs = append(errs, err)
		}
	}

	errs = append(errs, UniversalStringValidation(ctx, reflect.ValueOf(r.Address).Elem())...)
	return errs
}

// UniversalStringValidation validates strings for length and special characters
func UniversalStringValidation(ctx context.Context, element reflect.Value) []*addressErrors.ServerError {
	log := logging.GetTracedLogEntry(ctx)
	errs := make([]*addressErrors.ServerError, 0)
	for i := 0; i < element.NumField(); i++ {
		if element.Field(i).CanInterface() { // Prevent panic
			fieldName := element.Type().Field(i).Name
			fieldType := fmt.Sprintf("%v", element.Type().Field(i).Type)
			fieldValue := fmt.Sprintf("%v", element.Field(i).Interface())
			if fieldType == "string" {
				if isTooLong(fieldValue) {
					err := addressErrors.TooManyCharactersError.NewFormatted(fieldName)
					log.WithError(err).WithField("Field", fieldName).WithField("Value", fieldValue).Info(err.Error())
					errs = append(errs, err)
				}
				if containsInvalidCharacters(ctx, fieldValue) {
					err := addressErrors.ContainsInvalidCharactersError.NewFormatted(fieldName)
					log.WithError(err).WithField("Field", fieldName).WithField("Value", fieldValue).Info(err.Error())
					errs = append(errs, err)
				}
			}
		}
	}
	return errs
}

func StandardizeUnitTypeAndValue(unitType, unitValue string) (string, string) {
	if unitType == "" && unitValue == "" {
		return unitType, unitValue
	}

	if unitType != "" && unitValue != "" {
		return unitType, unitValue
	}

	// ZZ is a dummy unit type in STAR, we don't want to save it
	if unitType == "ZZ" && unitValue == "" {
		return "", ""
	}

	// at this moment at least one of {unitType, unitValue} is not empty
	var stringToSplit string
	if unitType != "" {
		stringToSplit = unitType
	} else {
		stringToSplit = unitValue
	}

	return parseUnitString(stringToSplit)
}

func parseUnitString(s string) (unitType, unitValue string) {
	words := strings.Fields(s)

	switch len(words) {
	case 0:
		return "", ""
	case 1:
		return "", words[0]
	case 2:
		return words[0], words[1]
	default:
		return words[0], strings.Join(words[1:], " ")
	}
}

func isValidUSZipCode(zip string) bool {
	return zipRegex.MatchString(zip)
}

func isTooLong(field string) bool {
	return len(field) > MaxLengthOfAddressField
}

func containsInvalidCharacters(ctx context.Context, field string) bool {
	matches, err := regexp.MatchString(InvalidCharactersPattern, strings.ToUpper(field))
	if err != nil {
		logging.GetTracedLogEntry(ctx).WithError(err).Error("possible error in regex")
		return false
	}
	return utils.IsNotBlank(field) && matches
}

func matchCityStateZip(in string) map[string]interface{} {
	var states []string
	for state := range literals.StateAbbreviations {
		states = append(states, state, strings.ToLower(state))
	}
	stateRegex := regexp.MustCompile(strings.Join([]string{"^(", ")[\\.,]?$"}, strings.Join(states, "|")))

	parts := strings.FieldsFunc(in, func(r rune) bool {
		return r == ' ' || r == ','
	})
	if len(parts) < 4 {
		return nil
	}

	zip := parts[len(parts)-1]
	state := parts[len(parts)-2]

	if stateRegex.MatchString(state) && zipRegex.MatchString(zip) {
		return map[string]interface{}{
			"Street1":      in,
			"MatchedState": state,
			"MatchedZip":   zip,
		}
	}

	return nil
}

// ValidateTypeAheadRequest validates that a search has a non empty valid street line
func validateAddressRecordType(recordType string) *addressErrors.ServerError {
	if len(recordType) > 1 {
		return addressErrors.InvalidAddressRecordType.NewFormatted("value can have only 1 character")
	}
	//F - Firm; the finest level of match available for an address.
	//G — General Delivery; for mail to be held at local post offices.
	//H — High-rise; address contains apartment or building sub-units.
	//P — Post Office box
	//R — Rural Route or Highway Contract; may have box number ranges.
	//S — Street; address contains a valid primary number range.
	switch strings.ToUpper(recordType) {
	case "F", "G", "H", "P", "R", "S", "":
		return nil

	}

	return addressErrors.InvalidAddressRecordType.NewFormatted("wrong value - " + recordType)

}

func isValidState(state string) bool {
	_, ok := literals.StateAbbreviations[strings.ToUpper(state)]
	return ok
}
