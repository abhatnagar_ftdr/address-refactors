package handlers

import (
	"fmt"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
	"reflect"
	"testing"

	addressErrors "golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors"
	pb "golang.frontdoorhome.com/software/protos/go/addresspb"
)

const (
	testStreet   = "1515 Arapahoe St"
	testZip      = "80202"
	testZipLocal = "80202-2111"
	testCity     = "Denver"
	testState    = "CO"
	testCountry  = "USA"
)

func Test_isValidUSZipCode(t *testing.T) {
	type args struct {
		zip string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"empty zip", args{zip: ""}, false},
		{"with zip 8023X", args{zip: "8023X"}, false},
		{"with valid zip 30303", args{zip: "30303"}, true},
		{"with valid zip 30303-1111", args{zip: "30303-1111"}, true},
		{"with zip 303031111", args{zip: "303031111"}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isValidUSZipCode(tt.args.zip); got != tt.want {
				t.Errorf("isValidUSZipCode() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestValidateAddressRequest(t *testing.T) {
	type args struct {
		r *pb.AddressRequest
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"all fields present should not throw error", args{r: &pb.AddressRequest{
			Street1:     testStreet,
			City:        testCity,
			State:       testState,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}, false},

		{"required street missing - should throw error", args{r: &pb.AddressRequest{
			City:        testCity,
			State:       testState,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}, true},
		{"required country missing - should throw error", args{r: &pb.AddressRequest{
			Street1: testStreet,
			City:    testCity,
			State:   testState,
			Zip:     testZip,
		}}, true},
		{"no zip - should not throw error", args{r: &pb.AddressRequest{
			Street1:     testStreet,
			City:        testCity,
			State:       testState,
			CountryIso3: testCountry,
		}}, false},
		{"no state - should not throw error", args{r: &pb.AddressRequest{
			Street1:     testStreet,
			City:        testCity,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}, false},
		{"no state or zip - should throw error", args{r: &pb.AddressRequest{
			Street1:     testStreet,
			City:        testCity,
			CountryIso3: testCountry,
		}}, true},
		{"no city - should not throw error", args{r: &pb.AddressRequest{
			Street1:     testStreet,
			State:       testState,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}, false},
		{"no city or zip - should throw error", args{r: &pb.AddressRequest{
			Street1:     testStreet,
			State:       testState,
			CountryIso3: testCountry,
		}}, true},
		{"no city or state - should not throw error", args{r: &pb.AddressRequest{
			Street1:     testStreet,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}, false},
		{"no city, state or zip - throws error", args{r: &pb.AddressRequest{
			Street1:     testStreet,
			CountryIso3: testCountry,
		}}, true},
		{"bad zip regardless - should throw error", args{r: &pb.AddressRequest{
			Street1:     testStreet,
			Zip:         "123",
			City:        testCity,
			State:       testState,
			CountryIso3: testCountry,
		}}, true},
		{"zip local - should not throw error", args{r: &pb.AddressRequest{
			Street1:     testStreet,
			Zip:         testZipLocal,
			City:        testCity,
			State:       testState,
			CountryIso3: testCountry,
		}}, false},
		// TODO: Talk to robert. This still is called in the handler.
		//{"street contains zip - should throw error", args{r: &pb.AddressRequest{
		//	Street1:     fmt.Sprintf("%s, %s", testStreet, testZip),
		//	City:        testCity,
		//	State:       testState,
		//	Zip:         testZip,
		//	CountryIso3: testCountry,
		//}}, true},
		//{"street contains zip4 - should throw error", args{r: &pb.AddressRequest{
		//	Street1:     fmt.Sprintf("%s, %s-1234", testStreet, testZip),
		//	City:        testCity,
		//	State:       testState,
		//	Zip:         testZip,
		//	CountryIso3: testCountry,
		//}}, true},
		//{"street contains city and state - should throw error", args{r: &pb.AddressRequest{
		//	Street1:     fmt.Sprintf("%s %s %s", testStreet, testCity, testState),
		//	City:        testCity,
		//	State:       testState,
		//	Zip:         testZip,
		//	CountryIso3: testCountry,
		//}}, true},
		//{"street contains city and state with commas - should throw error", args{r: &pb.AddressRequest{
		//	Street1:     fmt.Sprintf("%s,%s,%s", testStreet, testCity, testState),
		//	City:        testCity,
		//	State:       testState,
		//	Zip:         testZip,
		//	CountryIso3: testCountry,
		//}}, true},
		//{"street contains city and state with commas and spaces - should throw error", args{r: &pb.AddressRequest{
		//	Street1:     fmt.Sprintf("%s, %s, %s", testStreet, testCity, testState),
		//	City:        testCity,
		//	State:       testState,
		//	Zip:         testZip,
		//	CountryIso3: testCountry,
		//}}, true},
		//{"street contains city state and zip with spaces - should throw error", args{r: &pb.AddressRequest{
		//	Street1:     fmt.Sprintf("%s %s %s %s", testStreet, testCity, testState, testZip),
		//	City:        testCity,
		//	State:       testState,
		//	Zip:         testZip,
		//	CountryIso3: testCountry,
		//}}, true},
		{"street contains too many characters - should throw error", args{r: &pb.AddressRequest{
			Street1:     "1234567890 Taumatawhakatangihangakoauauotamateaturipukakapikimaungahoronukupokaiwhenuakitanatahu-Taumatawhakatangihangakoauauotamateaturipukakapikimaungahoronukupokaiwhenuakitanatahu-Taumatawhakatangihangakoauauotamateaturipukakapikimaungahoronukupokaiwhenuakitanatahu-Taumatawhakatangihangakoauauotamateaturipukakapikimaungahoronukupokaiwhenuakitanatahu Road",
			City:        testCity,
			State:       testState,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}, true},
		{"city contains too many characters - should throw error", args{r: &pb.AddressRequest{
			Street1:     testStreet,
			City:        "CityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCity",
			State:       testState,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}, true},
		{"state contains too many characters - should throw error", args{r: &pb.AddressRequest{
			Street1:     testStreet,
			City:        testCity,
			State:       "Missississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississippi",
			Zip:         testZip,
			CountryIso3: testCountry,
		}}, true},
		{"correct state - should not throw error", args{r: &pb.AddressRequest{
			Street1:     testStreet,
			City:        testCity,
			State:       testState,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}, false},
		{"incorrect state - should throw error", args{r: &pb.AddressRequest{
			Street1:     testStreet,
			City:        testCity,
			State:       "PH",
			Zip:         testZip,
			CountryIso3: testCountry,
		}}, true},
		{"too long street number in street1 - should throw error", args{r: &pb.AddressRequest{
			Street1:     "01234567890123456789012345 Arapahoe St",
			City:        testCity,
			State:       testState,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}, true},
		{"street number of len 25 - should not throw error", args{r: &pb.AddressRequest{
			Street1:     "0123456789012345678901234 Arapahoe St",
			City:        testCity,
			State:       testState,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}, false},
		{"too long street name in street1 - should throw error", args{r: &pb.AddressRequest{
			Street1:     "123 N VeryVeryVeryVeryVeryVeryVeryVeryVeryVery Long Street Name",
			City:        testCity,
			State:       testState,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}, true},
		{"street name of len 45 - should not throw error", args{r: &pb.AddressRequest{
			Street1:     "45894 S StreetStreetStreetStreetStreetStreet NameName",
			City:        testCity,
			State:       testState,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := ValidateAddressRequest(testCtx, tt.args.r); (len(err) > 0) != tt.wantErr {
				t.Errorf("ValidateAddressRequest() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestValidateUnverifiedAddressRequest(t *testing.T) {
	type args struct {
		r *pb.UnverifiedAddressRequest
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"happy path", args{r: &pb.UnverifiedAddressRequest{Address: &pb.Address{
			Street1:     testStreet,
			Street2:     "Heritage Hills",
			UnitType:    "Unit",
			UnitValue:   "9",
			City:        testCity,
			State:       testState,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}}, false},
		{"all fields present should not throw error", args{r: &pb.UnverifiedAddressRequest{Address: &pb.Address{
			Street1:     testStreet,
			City:        testCity,
			State:       testState,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}}, false},
		{"required street missing - should throw error", args{r: &pb.UnverifiedAddressRequest{Address: &pb.Address{
			City:        testCity,
			State:       testState,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}}, true},
		{"required country missing - should throw error", args{r: &pb.UnverifiedAddressRequest{Address: &pb.Address{
			Street1: testStreet,
			City:    testCity,
			State:   testState,
			Zip:     testZip,
		}}}, true},
		{"no zip - should throw error", args{r: &pb.UnverifiedAddressRequest{Address: &pb.Address{
			Street1:     testStreet,
			City:        testCity,
			State:       testState,
			CountryIso3: testCountry,
		}}}, true},
		{"no state - should throw error", args{r: &pb.UnverifiedAddressRequest{Address: &pb.Address{
			Street1:     testStreet,
			City:        testCity,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}}, true},
		{"no state or zip - should throw error", args{r: &pb.UnverifiedAddressRequest{Address: &pb.Address{
			Street1:     testStreet,
			City:        testCity,
			CountryIso3: testCountry,
		}}}, true},
		{"no city - should throw error", args{r: &pb.UnverifiedAddressRequest{Address: &pb.Address{
			Street1:     testStreet,
			State:       testState,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}}, true},
		{"no city or zip - should throw error", args{r: &pb.UnverifiedAddressRequest{Address: &pb.Address{
			Street1:     testStreet,
			State:       testState,
			CountryIso3: testCountry,
		}}}, true},
		{"no city or state - should throw error", args{r: &pb.UnverifiedAddressRequest{Address: &pb.Address{
			Street1:     testStreet,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}}, true},
		{"no city, state or zip - throws error", args{r: &pb.UnverifiedAddressRequest{Address: &pb.Address{
			Street1:     testStreet,
			CountryIso3: testCountry,
		}}}, true},
		{"city, state and zip in street1 - no error", args{r: &pb.UnverifiedAddressRequest{Address: &pb.Address{
			Street1:     fmt.Sprintf("%s, %s, %s %s", testStreet, testCity, testState, testZip),
			CountryIso3: testCountry,
		}}}, false},
		{"city, state and zip in street1; city defaulted - no error", args{r: &pb.UnverifiedAddressRequest{Address: &pb.Address{
			// typical for requests related to CM-1424
			Street1:     fmt.Sprintf("%s, %s, %s %s", testStreet, testCity, testState, testZip),
			City:        "City",
			CountryIso3: testCountry,
		}}}, false},
		{"city, state and zip in street1; city, state, and zip also provided - no error", args{r: &pb.UnverifiedAddressRequest{Address: &pb.Address{
			Street1:     fmt.Sprintf("%s, %s, %s %s", testStreet, testCity, testState, testZip),
			City:        testCity,
			State:       testState,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}}, false},
		{"city and state are in street1 but zip is missing - throws error", args{r: &pb.UnverifiedAddressRequest{Address: &pb.Address{
			Street1:     fmt.Sprintf("%s, %s, %s", testStreet, testCity, testState),
			City:        "City",
			CountryIso3: testCountry,
		}}}, true},
		{"city and zip are in street1 but state is missing - throws error", args{r: &pb.UnverifiedAddressRequest{Address: &pb.Address{
			Street1:     fmt.Sprintf("%s, %s %s", testStreet, testCity, testZip),
			City:        "City",
			CountryIso3: testCountry,
		}}}, true},
		{"bad zip regardless - should throw error", args{r: &pb.UnverifiedAddressRequest{Address: &pb.Address{
			Street1:     testStreet,
			Zip:         "123",
			City:        testCity,
			State:       testState,
			CountryIso3: testCountry,
		}}}, true},
		{"zip local - should not throw error", args{r: &pb.UnverifiedAddressRequest{Address: &pb.Address{
			Street1:     testStreet,
			Zip:         testZipLocal,
			City:        testCity,
			State:       testState,
			CountryIso3: testCountry,
		}}}, false},
		{"street contains too many characters - should throw error", args{r: &pb.UnverifiedAddressRequest{Address: &pb.Address{
			Street1:     "1234567890 Taumatawhakatangihangakoauauotamateaturipukakapikimaungahoronukupokaiwhenuakitanatahu-Taumatawhakatangihangakoauauotamateaturipukakapikimaungahoronukupokaiwhenuakitanatahu-Taumatawhakatangihangakoauauotamateaturipukakapikimaungahoronukupokaiwhenuakitanatahu-Taumatawhakatangihangakoauauotamateaturipukakapikimaungahoronukupokaiwhenuakitanatahu Road",
			City:        testCity,
			State:       testState,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}}, true},
		{"city contains too many characters - should throw error", args{r: &pb.UnverifiedAddressRequest{Address: &pb.Address{
			Street1:     testStreet,
			City:        "CityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCityCity",
			State:       testState,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}}, true},
		{"state contains too many characters - should throw error", args{r: &pb.UnverifiedAddressRequest{Address: &pb.Address{
			Street1:     testStreet,
			City:        testCity,
			State:       "Missississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississississippi",
			Zip:         testZip,
			CountryIso3: testCountry,
		}}}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := ValidateUnverifiedAddressRequest(testCtx, tt.args.r); (len(err) > 0) != tt.wantErr {
				t.Errorf("ValidateUnverifiedAddressRequest() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_containsInvalidCharacters(t *testing.T) {
	type args struct {
		field string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"happy path", args{field: "123 Ricky's Ct., #A-5, Dallas, Texas 00000-1234 USA"}, false},
		{"frontdoor address", args{field: "1515 Arapahoe St, 6th Fl, Denver, CO 80202"}, false},
		{"club 99 gaspanic", args{field: "GasPanic [9•9], Roppongi"}, false},
		{"all letters are allowed", args{field: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"}, false},
		{"all numbers are allowed", args{field: "0123456789"}, false},
		{"spaces are allowed", args{field: " "}, false},
		{"we are pretty liberal when it comes to characters", args{field: "(╯°□°）╯︵ suᴉɯɯnɔ"}, false},
		{"<> characters not allowed", args{field: "<>"}, true},
		{"` character not allowed", args{field: "`"}, true},
		{"@ character not allowed", args{field: "@"}, true},
		{"! character not allowed", args{field: "!"}, true},
		{"$ character not allowed", args{field: "$"}, true},
		{"% character not allowed", args{field: "%"}, true},
		{"^ character not allowed", args{field: "^"}, true},
		{"= character not allowed", args{field: "="}, true},
		{"? character not allowed", args{field: "?"}, true},
		{"| character not allowed", args{field: "|"}, true},
		{"\\ character not allowed", args{field: "\\"}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := containsInvalidCharacters(testCtx, tt.args.field); got != tt.want {
				t.Errorf("containsInvalidCharacters() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestValidateTypeAheadRequest(t *testing.T) {
	type args struct {
		r *pb.AddressByTypeAheadRequest
	}
	tests := []struct {
		name     string
		args     args
		wantErrs []*addressErrors.ServerError
	}{
		{
			name: "success with minimal requirements",
			args: args{r: &pb.AddressByTypeAheadRequest{
				StreetLine: "1",
			}},
			wantErrs: make([]*addressErrors.ServerError, 0),
		},
		{
			name: "success with full request",
			args: args{r: &pb.AddressByTypeAheadRequest{
				StreetLine: "12344 S Street",
				UnitType:   "APT",
				UnitValue:  "43",
				City:       "Nowhereville",
				State:      "CO",
			}},
			wantErrs: make([]*addressErrors.ServerError, 0),
		},
		{
			name:     "empty street line fails validation",
			args:     args{r: &pb.AddressByTypeAheadRequest{}},
			wantErrs: []*addressErrors.ServerError{addressErrors.InvalidAddressStreet1Error.New()},
		},
		{
			name: "invalid chars in street line fails validation",
			args: args{r: &pb.AddressByTypeAheadRequest{
				StreetLine: "1234 \\ S St",
			}},
			wantErrs: []*addressErrors.ServerError{addressErrors.InvalidAddressStreet1Error.New(), addressErrors.ContainsInvalidCharactersError.NewFormatted("StreetLine")},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotErrs := ValidateTypeAheadRequest(testCtx, tt.args.r); !reflect.DeepEqual(gotErrs, tt.wantErrs) {
				t.Errorf("ValidateTypeAheadRequest() = %v, want %v", gotErrs, tt.wantErrs)
			}
		})
	}
}

func TestValidateAddressRequestValidStates(t *testing.T) {
	type args struct {
		r *pb.AddressRequest
	}
	type testCase struct {
		name    string
		args    args
		wantErr bool
	}

	var tests []testCase

	for k, v := range literals.StateAbbreviations {
		tcName := fmt.Sprintf("valid state %s %s", k, v)
		tc := testCase{tcName, args{r: &pb.AddressRequest{
			State:       k,
			Street1:     testStreet,
			City:        testCity,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}, false}

		tests = append(tests, tc)
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := ValidateAddressRequest(testCtx, tt.args.r); (len(err) > 0) != tt.wantErr {
				t.Errorf("ValidateAddressRequest() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestValidateAddressRequestInvalidStates(t *testing.T) {
	type args struct {
		r *pb.AddressRequest
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"invalid state 1", args{r: &pb.AddressRequest{
			State:       "invalid state 1",
			Street1:     testStreet,
			City:        testCity,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}, true},
		{"invalid state 2", args{r: &pb.AddressRequest{
			State:       "ABC",
			Street1:     testStreet,
			City:        testCity,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}, true},
		{"invalid state 3 - Puerto Rico", args{r: &pb.AddressRequest{
			State:       "PR",
			Street1:     testStreet,
			City:        testCity,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := ValidateAddressRequest(testCtx, tt.args.r); (len(err) > 0) != tt.wantErr {
				t.Errorf("ValidateAddressRequest() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestValidateUnverifiedAddressRequestValidStates(t *testing.T) {
	type args struct {
		r *pb.UnverifiedAddressRequest
	}
	type testCase struct {
		name    string
		args    args
		wantErr bool
	}

	var tests []testCase

	for k, v := range literals.StateAbbreviations {
		tcName := fmt.Sprintf("valid state %s %s", k, v)
		tc := testCase{tcName, args{r: &pb.UnverifiedAddressRequest{Address: &pb.Address{
			State:       k,
			Street1:     testStreet,
			Street2:     "Heritage Hills",
			UnitType:    "Unit",
			UnitValue:   "9",
			City:        testCity,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}}, false}

		tests = append(tests, tc)
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := ValidateUnverifiedAddressRequest(testCtx, tt.args.r); (len(err) > 0) != tt.wantErr {
				t.Errorf("ValidateUnverifiedAddressRequest() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestValidateUnverifiedAddressRequestInvalidStates(t *testing.T) {
	type args struct {
		r *pb.UnverifiedAddressRequest
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"invalid state 1", args{r: &pb.UnverifiedAddressRequest{Address: &pb.Address{
			State:       "GOLANG",
			Street1:     testStreet,
			Street2:     "Heritage Hills",
			UnitType:    "Unit",
			UnitValue:   "9",
			City:        testCity,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}}, true},
		{"invalid state 2", args{r: &pb.UnverifiedAddressRequest{Address: &pb.Address{
			State:       "ABC",
			Street1:     testStreet,
			Street2:     "Heritage Hills",
			UnitType:    "Unit",
			UnitValue:   "9",
			City:        testCity,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}}, true},
		{"invalid state 3 - Puerto Rico", args{r: &pb.UnverifiedAddressRequest{Address: &pb.Address{
			State:       "PR",
			Street1:     testStreet,
			Street2:     "Heritage Hills",
			UnitType:    "Unit",
			UnitValue:   "9",
			City:        testCity,
			Zip:         testZip,
			CountryIso3: testCountry,
		}}}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := ValidateUnverifiedAddressRequest(testCtx, tt.args.r); (len(err) > 0) != tt.wantErr {
				t.Errorf("ValidateUnverifiedAddressRequest() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestStandardizeUnitTypeAndValue(t *testing.T) {
	type args struct {
		unitType  string
		unitValue string
	}

	type want struct {
		unitType  string
		unitValue string
	}

	tests := []struct {
		name string
		args args
		want want
	}{
		{"both empty - no change",
			args{
				unitType:  "",
				unitValue: "",
			},
			want{
				unitType:  "",
				unitValue: ""}},
		{"both populated - no change",
			args{
				unitType:  "type",
				unitValue: "value",
			},
			want{
				unitType:  "type",
				unitValue: "value"}},
		{"unitType populated, unitValue empty",
			args{
				unitType:  "Apt 123",
				unitValue: "",
			},
			want{
				unitType:  "Apt",
				unitValue: "123"}},
		{"unitType empty, unitValue populated",
			args{
				unitType:  "",
				unitValue: "Suite A",
			},
			want{
				unitType:  "Suite",
				unitValue: "A"}},
		{"unitType as one word, unitValue empty",
			args{
				unitType:  "B",
				unitValue: "",
			},
			want{
				unitType:  "",
				unitValue: "B"}},
		{"unitType empty, unitValue 3 words",
			args{
				unitType:  "",
				unitValue: "Suite A B",
			},
			want{
				unitType:  "Suite",
				unitValue: "A B"}},
		{"unitType empty, unitValue 3 words, many whitespace characters",
			args{
				unitType:  "",
				unitValue: "  	Suite	 A 			B   ",
			},
			want{
				unitType:  "Suite",
				unitValue: "A B"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ut, uv := StandardizeUnitTypeAndValue(tt.args.unitType, tt.args.unitValue)
			if ut != tt.want.unitType || uv != tt.want.unitValue {
				t.Errorf("StandardizeUnitTypeAndValue(): got %v, %v, want %v, %v", ut, uv, tt.want.unitType, tt.want.unitValue)
			}
		})
	}
}
