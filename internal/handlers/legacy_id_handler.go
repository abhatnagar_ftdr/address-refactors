package handlers

import (
	"net/http"

	addressErrors "golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors"

	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
	pb "golang.frontdoorhome.com/software/protos/go/addresspb"
)

// PostLegacyIDHandler is the handler for the POST /address/{uuid}/legacy_ids/{legacy_id}
func (h *Handler) PostLegacyIDHandler(w http.ResponseWriter, r *http.Request) {
	ctx, log := initializeHandlerBasics(r)

	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.PostLegacyIDAPIName, literals.ComponentTypeAddress)
	componentContextData := map[string]interface{}{}

	legacyID := getValidatedLegacyIDPathVariable(r)
	uuid := getValidatedUUIDPathVariable(r)

	request := &pb.PostLegacyIDToAddressRequest{}
	if unmarshalErr := unmarshalRequest(ctx, r, request); unmarshalErr != nil && unmarshalErr.Code != addressErrors.EmptyBodyError.Code {
		log.Errorf("get address request unmarshaling failed: %s", unmarshalErr.Error())
		componentContextData[literals.ContextErrorMessage] = unmarshalErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, unmarshalErr)
		return
	}

	err := h.service.PostLegacyID(ctx, uuid, legacyID, request.ExternalSource)
	if err != nil {
		log.Errorf("post legacy id request failed: %s", err.Error())
		componentContextData[literals.ContextErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, err)
		return
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	w.WriteHeader(http.StatusNoContent)
}

// DeleteLegacyIDHandler is the handler for the DELETE /address/{uuid}/legacy_ids/{legacy_id}
func (h *Handler) DeleteLegacyIDHandler(w http.ResponseWriter, r *http.Request) {
	ctx, log := initializeHandlerBasics(r)

	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.DeleteLegacyIDAPIName, literals.ComponentTypeAddress)
	componentContextData := map[string]interface{}{}

	id := getValidatedUUIDPathVariable(r)
	legacyID := getValidatedLegacyIDPathVariable(r)

	request := &pb.DeleteLegacyIDFromAddressRequest{}
	if unmarshalErr := unmarshalRequest(ctx, r, request); unmarshalErr != nil && unmarshalErr.Code != addressErrors.EmptyBodyError.Code {
		log.Errorf("get address request unmarshaling failed: %s", unmarshalErr.Error())
		componentContextData[literals.ContextErrorMessage] = unmarshalErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, unmarshalErr)
		return
	}

	err := h.service.DeleteLegacyID(ctx, id, legacyID, request.ExternalSource)
	if err != nil {
		log.Errorf("delete legacy id request failed: %s", err.Error())
		componentContextData[literals.ContextErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, err)
		return
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	w.WriteHeader(http.StatusNoContent)
}

// DeleteAllLegacyIDHandler is the handler for the DELETE /address/by_legacy_id/{legacy_id}
func (h *Handler) DeleteAllLegacyIDHandler(w http.ResponseWriter, r *http.Request) {
	ctx, log := initializeHandlerBasics(r)

	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.DeleteAllLegacyIDURI, literals.ComponentTypeAddress)
	componentContextData := map[string]interface{}{}

	legacyID := getValidatedLegacyIDPathVariable(r)

	request := &pb.DeleteLegacyIDRequest{}
	if unmarshalErr := unmarshalRequest(ctx, r, request); unmarshalErr != nil && unmarshalErr.Code != addressErrors.EmptyBodyError.Code {
		log.Errorf("get address request unmarshaling failed: %s", unmarshalErr.Error())
		componentContextData[literals.ContextErrorMessage] = unmarshalErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, unmarshalErr)
		return
	}

	err := h.service.DeleteAllLegacyID(ctx, legacyID, request.ExternalSource)
	if err != nil {
		log.Errorf("delete legacy id request failed: %s", err.Error())
		componentContextData[literals.ContextErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, err)
		return
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	w.WriteHeader(http.StatusNoContent)
}
