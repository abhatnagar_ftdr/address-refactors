package handlers

import (
	"context"
	"encoding/json"
	"fmt"
	"go.ftdr.com/go-utils/common/logging"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"strings"

	"golang.frontdoorhome.com/go-utils/validators"

	"github.com/golang/protobuf/proto"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/utils"
	addressErrors "golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors"
	"golang.frontdoorhome.com/software/protos/go/addresspb"
	"golang.frontdoorhome.com/software/protos/go/common"
)

// Constants, kinda useful
const (
	// ProtobufContentType is the standard content type for protobuf
	ProtobufContentType = "application/x-protobuf"

	// JSONContentType is the standard content type for JSON
	JSONContentType = "application/json"

	MaxLengthOfAddressField  = 200
	InvalidCharactersPattern = "[|`<>@!$%=^?\\\\]" // Invalid Chars: ^ | ` < > @ ! $ % = ? \
)

type IDType int

const (
	None IDType = iota
	UUID
	LegacyID
)

var stateMatchesStreet = map[string]bool{
	"CT": true, // Connecticut - Court
	"MT": true, // Montana - Mount
	"KY": true, // Kentucky - Key
	"PR": true, // Puerto Rico - Prairie
}

// unmarshalRequest takes a request, writer, and a target and unmarshals the body of the request
// into the target proto message based on the content type of the request
func unmarshalRequest(ctx context.Context, r *http.Request, target proto.Message) (err *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)
	data, bodyReadErr := ioutil.ReadAll(r.Body)
	if bodyReadErr != nil {
		fields := map[string]interface{}{
			"Error":  bodyReadErr,
			"Remote": r.RemoteAddr,
			"URI":    r.RequestURI,
		}
		log.WithFields(fields).Info(addressErrors.RequestBodyReadError.Error())
		return addressErrors.RequestBodyReadError.New()
	}

	if len(data) == 0 {
		log.WithField("Error", addressErrors.EmptyBodyError.Error()).Info("body length was zero")
		return addressErrors.EmptyBodyError.New()
	}

	contentType := GetRequestContentType(r)
	var unmarshalErr error
	switch contentType {
	case ProtobufContentType:
		unmarshalErr = proto.Unmarshal(data, target)
	case JSONContentType:
		unmarshalErr = json.Unmarshal(data, target)
	}

	if unmarshalErr != nil {
		fields := map[string]interface{}{
			"ContentType": contentType,
			"Error":       unmarshalErr,
			"Remote":      r.RemoteAddr,
			"URI":         r.RequestURI,
		}
		log.WithFields(fields).Info(addressErrors.UnmarshalError.Error())
		return addressErrors.UnmarshalError.New()
	}

	return nil
}

// marshal the proto message and write it to the response writer
func sendMarshaledResponse(
	ctx context.Context,
	w http.ResponseWriter,
	httpStatus int, response proto.Message) {
	contentType := GetResponseContentType(w)
	fields := map[string]interface{}{"Response": response, "HTTPStatus": getHTTPStatusString(httpStatus), "ContentType": contentType}
	log := logging.GetTracedLogEntry(ctx)
	log = log.WithFields(fields)

	var marshaledResponse []byte
	var marshalErr error
	switch contentType {
	case ProtobufContentType:
		marshaledResponse, marshalErr = proto.Marshal(response)
	case JSONContentType:
		marshaledResponse, marshalErr = json.Marshal(response)
	}

	if marshalErr != nil {
		log.WithField("Error", marshalErr.Error()).Error(addressErrors.MarshalError.Error())
		sendErrorResponse(ctx, w, http.StatusInternalServerError, addressErrors.MarshalError.Error())
		return
	}

	w.WriteHeader(httpStatus)
	_, encoderErr := w.Write(marshaledResponse)
	if encoderErr != nil {
		log.WithField("Error", encoderErr.Error()).Error(addressErrors.EncoderError.Error())
		sendErrorResponse(ctx, w, http.StatusInternalServerError, addressErrors.EncoderError.Error())
		return
	}
	log.Info("sent")
}

// GetRequestAcceptType gets the accepted content type of the request
func GetRequestAcceptType(r *http.Request) string {
	return (*r).Header.Get("Accept")
}

// GetRequestContentType gets the content type of the request
func GetRequestContentType(r *http.Request) string {
	return (*r).Header.Get("Content-Type")
}

// SetResponseContentType sets the header Content-Type to the passed content type
func SetResponseContentType(w http.ResponseWriter, contentType string) {
	w.Header().Set("Content-Type", contentType)
}

// GetResponseContentType gets the header Content-Type to marshal the data correctly.
// If it is blank, it is set to the default protobuf content type
func GetResponseContentType(w http.ResponseWriter) string {
	contentType := w.Header().Get("Content-Type")
	if contentType == "" {
		contentType = ProtobufContentType
		SetResponseContentType(w, ProtobufContentType)
	}

	return contentType
}

func getValidatedLegacyIDPathVariable(r *http.Request) uint32 {
	// "legacy_id" is validated in validatePathVariables
	idString := mux.Vars(r)["legacy_id"]
	idString = strings.TrimSpace(idString)
	if !utils.VerifyIsDigit(idString) {
		return 0
	}

	i64, _ := strconv.ParseUint(idString, 10, 32)
	return uint32(i64)
}

func getValidatedUUIDPathVariable(r *http.Request) string {
	// "uuid" is validated in validatePathVariables
	uid := mux.Vars(r)["uuid"]
	if !validators.IsValidGUID(uid) {
		return ""
	}
	return uid
}

func getValidatedUUIDOrLegacyIDPathVariable(r *http.Request) (string, IDType) {
	// "uuid" is validated in validatePathVariables
	id := mux.Vars(r)["uuid_or_legacy_id"]
	if validators.IsValidGUID(id) {
		return id, UUID
	}
	if utils.VerifyIsDigit(id) {
		return id, LegacyID
	}
	return "", None
}

func getValidatedZipPathVariable(r *http.Request) string {
	// "zip" is validated in validatePathVariables
	zipCode := mux.Vars(r)["zip"]
	zipCode = strings.TrimSpace(zipCode)
	if !utils.VerifyIsDigit(zipCode) {
		return ""
	}
	return zipCode
}

func getHTTPStatusString(httpStatus int) string {
	return fmt.Sprintf("%d %s", httpStatus, http.StatusText(httpStatus))
}

// SendErrorMessage marshals and sends a single error to occur in the system
func SendErrorMessage(ctx context.Context, w http.ResponseWriter, err *addressErrors.ServerError) {
	errResponse := &common.ErrorResponse{
		Errors: []*common.Error{{
			Message: err.Message,
			Code:    err.Code,
		}},
	}
	sendMarshaledResponse(ctx, w, err.HTTPStatusCode(), errResponse)
}

// ExtractErrorMessages ...
func ExtractErrorMessages(errs []*addressErrors.ServerError) string {
	errMsgs := []string{}
	for _, err := range errs {
		errMsgs = append(errMsgs, err.Error())
	}
	return strings.Join(errMsgs, ",")
}

// SendValidationErrorsMessage will send multiple errors as a response to any validation errors that occur
func SendValidationErrorsMessage(ctx context.Context, w http.ResponseWriter, errs []*addressErrors.ServerError) {
	pbErrs := make([]*common.Error, len(errs))
	for index, err := range errs {
		pbErrs[index] = &common.Error{
			Message: err.Message,
			Code:    err.Code,
		}
	}

	errResponse := &common.ErrorResponse{
		Errors: pbErrs,
	}
	sendMarshaledResponse(ctx, w, http.StatusBadRequest, errResponse)
}

// SendSuccessMessage sends a successful status, as well as the proto message of the successful response
func SendSuccessMessage(ctx context.Context, w http.ResponseWriter, message proto.Message) {
	sendMarshaledResponse(ctx, w, http.StatusOK, message)
}

func sendErrorResponse(ctx context.Context, w http.ResponseWriter, httpStatus int, message string) {
	http.Error(w, message, httpStatus)
	log := logging.GetTracedLogEntry(ctx)
	log.WithField("HTTPStatus", getHTTPStatusString(httpStatus)).WithField("Message", message).Info("sent")
}

// TrimStreet1 trims country, zip, state & city from street1. Can be used on street2 if desired.
func TrimStreet1(ctx context.Context, street1, city, state, zip, country string) (street string, removed int) {
	log := logging.GetTracedLogEntry(ctx)

	street = utils.TrimWhitespace(strings.ReplaceAll(strings.ReplaceAll(street1, ".", ""), ",", ", "))
	upperStreet := utils.TrimAndUpper(street)

	elements := []string{utils.TrimAndUpper(country), utils.TrimAndUpper(zip), utils.TrimAndUpper(literals.StateAbbreviations[utils.TrimAndUpper(state)]), utils.TrimAndUpper(state), utils.TrimAndUpper(city)}
	for index, fieldValue := range elements {
		if len(fieldValue) > 0 && (strings.HasSuffix(upperStreet, fmt.Sprintf(" %s", fieldValue)) || upperStreet == fieldValue) {
			if index != 3 || !stateMatchesStreet[fieldValue] || strings.Count(upperStreet, fmt.Sprintf(" %s", fieldValue)) != 1 {
				upperStreet = utils.TrimWhitespace(strings.TrimSuffix(upperStreet, fieldValue))
				removed++
			}
		} else if index == 1 && isValidUSZipCode(fieldValue) {
			zipRegex := regexp.MustCompile(`[\s,]+(?P<zip>` + string([]rune(zip)[0:5]) + `(?:-[0-9]{4})?)$`)
			if zipRegex.MatchString(upperStreet) {
				matches := zipRegex.FindStringSubmatch(upperStreet)
				lastIndex := zipRegex.SubexpIndex("zip")
				upperStreet = utils.TrimWhitespace(strings.TrimSuffix(upperStreet, fmt.Sprintf(" %s", matches[lastIndex])))
				removed++
			}
		}
		if strings.HasSuffix(upperStreet, ",") {
			upperStreet = utils.TrimWhitespace(strings.TrimSuffix(upperStreet, ","))
		}
	}

	if len(upperStreet) < len(street) {
		street = street[0:len(upperStreet)]
	}

	log.WithField("Street", street).WithField("Removed", removed).Info()
	return street, removed
}

func initializeHandlerBasics(r *http.Request) (ctx context.Context, log *logrus.Entry) {
	ctx = (*r).Context()
	log = logging.GetTracedLogEntry(ctx)

	return ctx, log
}

func typeaheadFromQuery(r *http.Request) *addresspb.AddressByTypeAheadRequest {
	q := r.URL.Query()
	return &addresspb.AddressByTypeAheadRequest{
		StreetLine: q.Get("street_line"),
		UnitType:   q.Get("unit_type"),
		UnitValue:  q.Get("unit_value"),
		City:       q.Get("city"),
		State:      q.Get("state"),
	}
}
