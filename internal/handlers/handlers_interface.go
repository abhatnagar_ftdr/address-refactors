package handlers

import (
	"net/http"

	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/config"
)

// RequestHandler interface is all of the handler functions for the server
type RequestHandler interface {
	DeleteLegacyIDHandler(w http.ResponseWriter, r *http.Request)

	DeleteAllLegacyIDHandler(w http.ResponseWriter, r *http.Request)

	GetAddressHandler(w http.ResponseWriter, r *http.Request)

	GetAddressByIDHandler(w http.ResponseWriter, r *http.Request)

	GetAddressByLegacyIDHandler(w http.ResponseWriter, r *http.Request)

	GetHealthCheckHandler(ciConfig *config.CIConfig) http.HandlerFunc

	QueryTypeAheadHandler(w http.ResponseWriter, r *http.Request)

	GetTypeAheadHandler(w http.ResponseWriter, r *http.Request)

	GetZipCodeDetailsHandler(w http.ResponseWriter, r *http.Request)

	LogCollectionStatsHandler(w http.ResponseWriter, r *http.Request)

	LogFilterStatsHandler(w http.ResponseWriter, r *http.Request)

	PostLegacyIDHandler(w http.ResponseWriter, r *http.Request)

	PostUnverifiedAddressHandler(w http.ResponseWriter, r *http.Request)

	VerifyAddressIDHandler(w http.ResponseWriter, r *http.Request)

	GetStatesHandler(w http.ResponseWriter, r *http.Request)

	ZipCodeCheckHandler(w http.ResponseWriter, r *http.Request)

	StateZipCodesHandler(w http.ResponseWriter, r *http.Request)
}
