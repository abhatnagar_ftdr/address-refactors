package handlers

import (
	"net/http"

	"go.ftdr.com/go-utils/common/logging"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/convert"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
	addressErrors "golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors"
	pb "golang.frontdoorhome.com/software/protos/go/addresspb"
)

// LogCollectionStatsHandler logs all collection to document counts in the DB
func (h *Handler) LogCollectionStatsHandler(w http.ResponseWriter, r *http.Request) {
	ctx, log := initializeHandlerBasics(r)
	log.Info()

	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.LogCollectionStatsAPIName, literals.ComponentTypeAddress)
	componentContextData := map[string]interface{}{}

	if err := h.service.LogCollectionStats(ctx); err != nil {

		logging.
			GetTracedLogEntry(ctx).
			Errorf("log collection stats request failed: %s", err.Error())
		componentContextData[literals.ContextErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, err)
		return
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	w.WriteHeader(http.StatusNoContent)
}

// LogFilterStatsHandler takes in a filter and collection and logs the document counts
// matching the collection-filter combination specified
func (h *Handler) LogFilterStatsHandler(w http.ResponseWriter, r *http.Request) {
	ctx, log := initializeHandlerBasics(r)
	log.Info()

	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.LogFilterStatsAPIName, literals.ComponentTypeAddress)
	componentContextData := map[string]interface{}{}

	var adminLogFilterStatsRequest pb.AdminLogFilterStatsRequest
	if err := unmarshalRequest(ctx, r, &adminLogFilterStatsRequest); err != nil {

		logging.
			GetTracedLogEntry(ctx).
			Errorf("log filter stats request unmarshaling failed: %s", err.Error())
		componentContextData[literals.ContextErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, addressErrors.UnmarshalError.New())
		return
	}

	filter, err := convert.PBStructToMap(adminLogFilterStatsRequest.Filter)
	if err != nil {

		logging.
			GetTracedLogEntry(ctx).
			Errorf("log filter stats request failed at conversion: %s", err.Error())
		componentContextData[literals.ContextErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, addressErrors.InvalidFilterError.New())
		return
	}

	if err := h.service.LogFilterStats(ctx, adminLogFilterStatsRequest.Collection, filter); err != nil {

		logging.
			GetTracedLogEntry(ctx).
			Errorf("log filter stats request failed: %s", err.Error())
		componentContextData[literals.ContextErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, err)
		return
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	w.WriteHeader(http.StatusNoContent)
}
