package handlers

import (
	"net/http"

	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/mongo_dal"

	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/convert"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
	addressErrors "golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors"
	pb "golang.frontdoorhome.com/software/protos/go/addresspb"
)

// GetAddressHandler is the handler for the GET /address route
func (h *Handler) GetAddressHandler(w http.ResponseWriter, r *http.Request) {
	ctx, log := initializeHandlerBasics(r)

	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.GetAddressAPIName, literals.ComponentTypeAddress)
	componentContextData := map[string]interface{}{}

	request := &pb.AddressRequest{}
	if unmarshalErr := unmarshalRequest(ctx, r, request); unmarshalErr != nil {
		log.Errorf("get address request unmarshaling failed: %s", unmarshalErr.Error())
		componentContextData[literals.ContextErrorMessage] = unmarshalErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, unmarshalErr)
		return
	}

	request.UnitType, request.UnitValue = StandardizeUnitTypeAndValue(request.UnitType, request.UnitValue)

	if validationErrs := ValidateAddressRequest(ctx, request); len(validationErrs) != 0 {
		errString := ExtractErrorMessages(validationErrs)
		log.Infof("get address request failed validation check: %s", errString)
		componentContextData[literals.ContextErrorMessage] = errString
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendValidationErrorsMessage(ctx, w, validationErrs)
		return
	}

	addressRecord, err := h.
		service.
		GetAddressRecord(ctx, convert.PBAddressRequestToAddressRecord(request, request.ExternalSource), request.ExternalSource)

	if err != nil {
		if err.Code == addressErrors.AddressNotVerifiableError.Code {
			trimmedStreet, removed := TrimStreet1(ctx,
				request.Street1,
				request.City,
				request.State,
				request.Zip,
				request.CountryIso3)

			if removed > 0 && trimmedStreet != "" {
				log.WithFields(map[string]interface{}{
					"OriginalStreet1": request.Street1,
					"TrimmedStreet1":  trimmedStreet,
				}).Info("try again with trimmed street1")
				request.Street1 = trimmedStreet
				addressRecord, err = h.service.GetAddressRecord(ctx,
					convert.PBAddressRequestToAddressRecord(request, request.ExternalSource), request.ExternalSource)
			}
		}
	}

	if err != nil {
		log.WithFields(map[string]interface{}{
			"HTTPStatus":     getHTTPStatusString(err.HTTPStatusCode()),
			"AddressRequest": request,
		}).Info("could not get address")

		componentContextData[literals.ContextErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, err)
		return
	}

	if addressRecord != nil && addressRecord.Status == mongo_dal.UnverifiedStatus {
		verifiedAddressRecord, err := h.service.VerifyAddress(ctx, addressRecord)
		if err != nil {
			if err.Code != addressErrors.AddressNotVerifiableError.Code {
				log.Errorf("get address by uuid or legacyID request failed: %s", err.Error())
				componentContextData[literals.ContextErrorMessage] = err.Error()
				instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
				SendErrorMessage(ctx, w, err)
				return
			} else {
				log.Infof("address could not be verified: %s", err.Error())
			}
		} else {
			log.Info("address for verified succesfully")
			addressRecord = verifiedAddressRecord
		}
	}

	if addresspbAddress, err := convert.RecordToPBAddress(ctx, addressRecord); err != nil {
		componentContextData[literals.ContextErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, err)
	} else {
		instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
		SendSuccessMessage(ctx, w, &pb.AddressResponse{Address: addresspbAddress})
	}
}
