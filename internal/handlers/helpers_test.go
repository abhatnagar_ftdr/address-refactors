package handlers

import (
	"bytes"
	"context"
	"go.ftdr.com/go-utils/common/constants"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gogo/protobuf/proto"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"go.ftdr.com/go-utils/common/logging"
	addressErrors "golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors"
	pb "golang.frontdoorhome.com/software/protos/go/addresspb"
)

var (
	testCtx = context.WithValue(context.Background(), constants.TraceIDContextKey, "00000000-0000-0000-0000-000000000000")
)

func TestUnmarshalRequest_WithValidData_ShouldUnmarshal(t *testing.T) {
	testPBMessage := &pb.AddressByTypeAheadRequest{
		StreetLine: testStreet,
		City:       testCity,
		State:      testState,
	}
	addresspbBody, _ := proto.Marshal(testPBMessage)

	testRequest := new(http.Request)
	testRequest.Header = http.Header{"Content-Type": []string{ProtobufContentType}}
	testRequest.Body = ioutil.NopCloser(bytes.NewReader(addresspbBody))

	targetMessage := &pb.AddressByTypeAheadRequest{}
	err := unmarshalRequest(testCtx, testRequest, targetMessage)

	assert.Nil(t, err)
	assert.Equal(t, testStreet, targetMessage.StreetLine)
	assert.Equal(t, testCity, targetMessage.City)
	assert.Equal(t, testState, targetMessage.State)
}

func TestUnmarshalRequest_WithNoBody_ShouldError(t *testing.T) {
	testRequest := new(http.Request)
	testRequest.Header = http.Header{"Content-Type": []string{ProtobufContentType}}
	testRequest.Body = http.NoBody

	var logBuffer bytes.Buffer
	level := logging.Log.GetLevel()
	logging.Log.SetLevel(logrus.InfoLevel)
	logging.Log.SetOutput(&logBuffer)
	defer func() {
		logging.Log.SetOutput(os.Stderr)
		logging.Log.SetLevel(level)
	}()

	targetMessage := &pb.AddressByTypeAheadRequest{}
	err := unmarshalRequest(testCtx, testRequest, targetMessage)

	assert.Contains(t, logBuffer.String(), addressErrors.EmptyBodyError.Message)
	assert.Equal(t, err.Error(), addressErrors.EmptyBodyError.Error())
}

func TestUnmarshalRequest_WithOverflowBody_ShouldError(t *testing.T) {
	testRequest := new(http.Request)
	testRequest.Header = http.Header{"Content-Type": []string{ProtobufContentType}}
	testRequest.Body = ioutil.NopCloser(bytes.NewReader(make([]byte, 100)))

	var logBuffer bytes.Buffer
	level := logging.Log.GetLevel()
	logging.Log.SetLevel(logrus.InfoLevel)
	logging.Log.SetOutput(&logBuffer)
	defer func() {
		logging.Log.SetOutput(os.Stderr)
		logging.Log.SetLevel(level)
	}()

	targetMessage := &pb.AddressByTypeAheadRequest{}
	err := unmarshalRequest(testCtx, testRequest, targetMessage)

	assert.Contains(t, logBuffer.String(), addressErrors.UnmarshalError.Message)
	assert.Equal(t, err.Error(), addressErrors.UnmarshalError.Error())
}

func TestUnmarshalRequest_WithInvalidData_ShouldError(t *testing.T) {
	testRequest := new(http.Request)
	testRequest.Header = http.Header{"Content-Type": []string{ProtobufContentType}}
	testRequest.Body = ioutil.NopCloser(bytes.NewReader(make([]byte, 16)))

	var logBuffer bytes.Buffer
	level := logging.Log.GetLevel()
	logging.Log.SetLevel(logrus.InfoLevel)
	logging.Log.SetOutput(&logBuffer)
	defer func() {
		logging.Log.SetOutput(os.Stderr)
		logging.Log.SetLevel(level)
	}()

	targetMessage := &pb.AddressByTypeAheadRequest{}
	err := unmarshalRequest(testCtx, testRequest, targetMessage)

	assert.Contains(t, logBuffer.String(), addressErrors.UnmarshalError.Message)
	assert.Equal(t, err.Error(), addressErrors.UnmarshalError.Error())
}

func TestSendSuccessMessage_WithValidResponse_ShouldWriteToBuffer(t *testing.T) {
	testWriter := new(httptest.ResponseRecorder)
	SetResponseContentType(testWriter, ProtobufContentType)
	testResponseBodyBuffer := new(bytes.Buffer)
	testWriter.Body = testResponseBodyBuffer

	testResponse := &pb.AddressByUUIDResponse{
		Address: &pb.Address{
			Street1: testStreet,
		},
	}
	expectedBinary, _ := proto.Marshal(testResponse)

	SendSuccessMessage(testCtx, testWriter, testResponse)
	assert.Equal(t, expectedBinary, testWriter.Body.Bytes())
}

func TestMarshalRespond_WithInvalidResponse_ShouldLogError(t *testing.T) {
	testWriter := new(httptest.ResponseRecorder)
	SetResponseContentType(testWriter, ProtobufContentType)
	testResponseBodyBuffer := new(bytes.Buffer)
	testWriter.Body = testResponseBodyBuffer

	var logBuffer bytes.Buffer
	logging.Log.SetOutput(&logBuffer)
	defer func() {
		logging.Log.SetOutput(os.Stderr)
	}()

	sendMarshaledResponse(testCtx, testWriter, http.StatusInternalServerError, nil)
	assert.Contains(t, logBuffer.String(), addressErrors.MarshalError.Error())
}

func TestMarshalRespond_WithInvalidWriter_ShouldLogError(t *testing.T) {
	testWriter := new(httptest.ResponseRecorder)
	SetResponseContentType(testWriter, ProtobufContentType)

	var logBuffer bytes.Buffer
	logging.Log.SetOutput(&logBuffer)
	defer func() {
		logging.Log.SetOutput(os.Stderr)
	}()

	sendMarshaledResponse(testCtx, testWriter, http.StatusInternalServerError, nil)
	assert.Contains(t, logBuffer.String(), addressErrors.MarshalError.Error())
	assert.Equal(t, "text/plain; charset=utf-8", testWriter.Header().Get("Content-Type"))
}

func TestTrimStreet1(t *testing.T) {
	type args struct {
		street1 string
		city    string
		state   string
		zip     string
	}
	want := "123 Some St"
	wantStreet := "6500 Flamingo Way"
	tests := []struct {
		name       string
		args       args
		wantStreet string
	}{
		{"clean street", args{street1: "123 Some St", city: "Denver", state: "CO"}, want},
		{"street has city and state", args{street1: "123 Some St denver co", city: "Denver", state: "CO"}, want},
		{"street has city and state commas", args{street1: "123 Some St,denver,co", city: "Denver", state: "CO"}, want},
		{"street has city and state commas and spaces", args{street1: "123 Some St, denver, co", city: "Denver", state: "CO"}, want},
		{"blank city and state", args{street1: "123 Some St denver co"}, "123 Some St denver co"},
		{"just city and state", args{street1: "denver co", city: "Denver", state: "CO"}, ""},
		{"street has city", args{street1: "123 Some St denver", city: "Denver", state: "CO"}, want},
		{"street has state", args{street1: "123 Some St co", city: "Denver", state: "CO"}, want},
		{"street ending in court in connecticut", args{street1: "13 Jennings Ct", city: "Westport", state: "CT", zip: "06880"}, "13 Jennings Ct"},
		{"street ending in mount in montana", args{street1: "113 Something MT", city: "Someplace", state: "MT", zip: "00000"}, "113 Something MT"},
		{"street ending in key in kentucky", args{street1: "113 Something KY", city: "Someplace", state: "KY", zip: "00000"}, "113 Something KY"},
		{"street ending in prairie in puerto rico", args{street1: "113 Something PR", city: "Someplace", state: "PR", zip: "00000"}, "113 Something PR"},
		{"6500 flamingo way coconut creek fl 33073", args{street1: "6500 FLAMINGO WAY, COCONUT CREEK, FL 33073", city: "Pompano Beach", state: "FL", zip: "33073"}, "6500 FLAMINGO WAY, COCONUT CREEK"},
		{"6500 flamingo way pompano beach florida 33073-4516 usa", args{street1: "6500 Flamingo Way, Pompano Beach, florida, 33073-4516 USA", city: "Pompano Beach", state: "FL", zip: "33073"}, wantStreet},
		{"6500 flamingo way pompano beach fl 33073-4516 usa", args{street1: "6500 Flamingo Way, Pompano Beach, FL, 33073-4516 USA", city: "Pompano Beach", state: "FL", zip: "33073"}, wantStreet},
		{"6500 flamingo way pompano beach florida 33073 usa", args{street1: "6500 Flamingo Way, Pompano Beach, florida, 33073 USA", city: "Pompano Beach", state: "FL", zip: "33073"}, wantStreet},
		{"6500 flamingo way pompano beach fl 33073 usa", args{street1: "6500 Flamingo Way, Pompano Beach, FL, 33073 USA", city: "Pompano Beach", state: "FL", zip: "33073"}, wantStreet},
		{"6500 flamingo way pompano beach florida 33073-4516", args{street1: "6500 Flamingo Way, Pompano Beach, florida, 33073-4516", city: "Pompano Beach", state: "FL", zip: "33073"}, wantStreet},
		{"6500 flamingo way pompano beach fl 33073-4516", args{street1: "6500 Flamingo Way, Pompano Beach, FL, 33073-4516", city: "Pompano Beach", state: "FL", zip: "33073"}, wantStreet},
		{"6500 flamingo way pompano beach florida 33073", args{street1: "6500 Flamingo Way, Pompano Beach, florida, 33073", city: "Pompano Beach", state: "FL", zip: "33073"}, wantStreet},
		{"6500 flamingo way pompano beach fl 33073", args{street1: "6500 Flamingo Way, Pompano Beach, FL, 33073", city: "Pompano Beach", state: "FL", zip: "33073"}, wantStreet},
		{"6500 flamingo way pompano beach florida", args{street1: "6500 Flamingo Way, Pompano Beach, florida", city: "Pompano Beach", state: "FL", zip: "33073"}, wantStreet},
		{"6500 flamingo way pompano beach fl", args{street1: "6500 Flamingo Way, Pompano Beach, FL", city: "Pompano Beach", state: "FL", zip: "33073"}, wantStreet},
		{"6500 flamingo way pompano beach", args{street1: "6500 Flamingo Way, Pompano Beach", city: "Pompano Beach", state: "FL", zip: "33073"}, wantStreet},
		{"6500 flamingo way", args{street1: "6500 Flamingo Way", city: "Pompano Beach", state: "FL", zip: "33073"}, wantStreet},
		{"no spaces just commas 6500 flamingo way pompano beach florida 33073-4516 usa", args{street1: "6500 Flamingo Way,Pompano Beach,florida,33073-4516 USA", city: "Pompano Beach", state: "FL", zip: "33073"}, wantStreet},
		{"no spaces just commas 6500 flamingo way pompano beach fl 33073-4516 usa", args{street1: "6500 Flamingo Way,Pompano Beach,FL,33073-4516 USA", city: "Pompano Beach", state: "FL", zip: "33073"}, wantStreet},
		{"no spaces just commas 6500 flamingo way pompano beach florida 33073 usa", args{street1: "6500 Flamingo Way,Pompano Beach,florida,33073 USA", city: "Pompano Beach", state: "FL", zip: "33073"}, wantStreet},
		{"no spaces just commas 6500 flamingo way pompano beach fl 33073 usa", args{street1: "6500 Flamingo Way,Pompano Beach,FL,33073 USA", city: "Pompano Beach", state: "FL", zip: "33073"}, wantStreet},
		{"no spaces just commas 6500 flamingo way pompano beach florida 33073-4516", args{street1: "6500 Flamingo Way,Pompano Beach,florida,33073-4516", city: "Pompano Beach", state: "FL", zip: "33073"}, wantStreet},
		{"no spaces just commas 6500 flamingo way pompano beach fl 33073-4516", args{street1: "6500 Flamingo Way,Pompano Beach,FL,33073-4516", city: "Pompano Beach", state: "FL", zip: "33073"}, wantStreet},
		{"no spaces just commas 6500 flamingo way pompano beach florida 33073", args{street1: "6500 Flamingo Way,Pompano Beach,florida,33073", city: "Pompano Beach", state: "FL", zip: "33073"}, wantStreet},
		{"no spaces just commas 6500 flamingo way pompano beach fl 33073", args{street1: "6500 Flamingo Way,Pompano Beach,FL,33073", city: "Pompano Beach", state: "FL", zip: "33073"}, wantStreet},
		{"no spaces just commas 6500 flamingo way pompano beach florida", args{street1: "6500 Flamingo Way,Pompano Beach,florida", city: "Pompano Beach", state: "FL", zip: "33073"}, wantStreet},
		{"no spaces just commas 6500 flamingo way pompano beach fl", args{street1: "6500 Flamingo Way,Pompano Beach,FL", city: "Pompano Beach", state: "FL", zip: "33073"}, wantStreet},
		{"no spaces just commas 6500 flamingo way pompano beach", args{street1: "6500 Flamingo Way,Pompano Beach", city: "Pompano Beach", state: "FL", zip: "33073"}, wantStreet},
		{"no spaces just commas 6500 flamingo way", args{street1: "6500 Flamingo Way", city: "Pompano Beach", state: "FL", zip: "33073"}, wantStreet},
		{"33073-4516 pompano beach drive pompano beach fl 33073 usa", args{street1: "33073-4516 Pompano Beach Drive Pompano Beach FL 33073 USA", city: "Pompano Beach", state: "FL", zip: "33073"}, "33073-4516 Pompano Beach Drive"},
		{"33073 pompano beach drive pompano beach fl 33073 usa", args{street1: "33073 Pompano Beach Drive Pompano Beach FL 33073 USA", city: "Pompano Beach", state: "FL", zip: "33073"}, "33073 Pompano Beach Drive"},
		{"33073 pompano beach drive unit 4516 pompano beach fl 33073 usa", args{street1: "33073 Pompano Beach Drive,Unit 4516 Pompano Beach FL 33073 USA", city: "Pompano Beach", state: "FL", zip: "33073"}, "33073 Pompano Beach Drive, Unit 4516"},
		{"33073-4516 pompano beach florida pompano beach florida 33073-4516 usa", args{street1: "33073-4516 Pompano Beach Florida, Pompano Beach, Florida, 33073-4516 USA", city: "Pompano Beach", state: "FL", zip: "33073"}, "33073-4516 Pompano Beach Florida"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotStreet, _ := TrimStreet1(testCtx, tt.args.street1, tt.args.city, tt.args.state, tt.args.zip, "USA"); gotStreet != tt.wantStreet {
				t.Errorf("TrimStreet1() = %v, want %v", gotStreet, tt.wantStreet)
			}
		})
	}
}
