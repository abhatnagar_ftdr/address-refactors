package handlers

import (
	//"context"
	"net/http"
	"net/url"

	commonHandlers "go.ftdr.com/go-utils/common/handlers"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/config"
)

const (
	repoURLBase = "https://gitlab.com/ftdr/software/address/"
)

// GetHealthCheckHandler is the method used to handle health check requests
func (h *Handler) GetHealthCheckHandler(ciConfig *config.CIConfig) http.HandlerFunc {
	repoURL, _ := url.Parse(repoURLBase)
	return commonHandlers.
		HealthCheckHandler(ciConfig.EnvironmentName,
			ciConfig.CommitShortSHA,
			*repoURL)
}
