package handlers

import (
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/services"
)

// Handler ...
type Handler struct {
	service services.AddressService
}

// NewHandler create a new Handler
func NewHandler(service services.AddressService) (handler *Handler) {
	return &Handler{
		service: service,
	}
}
