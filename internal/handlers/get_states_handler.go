package handlers

import (
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/convert"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
	pb "golang.frontdoorhome.com/software/protos/go/addresspb"
	"net/http"
)

// GetStatesHandler is the handler for the GET /address/states
func (h *Handler) GetStatesHandler(w http.ResponseWriter, r *http.Request) {
	ctx, _ := initializeHandlerBasics(r)

	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.GetStatesAPIName, literals.ComponentTypeAddress)
	componentContextData := map[string]interface{}{}

	statesPb := convert.MapToStatesPB(&literals.StateAbbreviations)
	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	SendSuccessMessage(ctx, w, &pb.StatesResponse{States: statesPb})
}
