package handlers

import (
	"net/http"

	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
)

// GetZipCodeDetailsHandler is the method used to get details on a provided zip code
func (h *Handler) GetZipCodeDetailsHandler(w http.ResponseWriter, r *http.Request) {
	ctx, log := initializeHandlerBasics(r)

	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.GetZipCodeDetailAPIName, literals.ComponentTypeAddress)
	componentContextData := map[string]interface{}{}

	zip := getValidatedZipPathVariable(r)
	if details, err := h.service.GetZipCodeDetails(ctx, zip); err != nil {
		log.Infof("get zipcode detail request failed: %s", err.Error())
		componentContextData[literals.ContextErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, err)
	} else {
		instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
		SendSuccessMessage(ctx, w, details)
	}
}
