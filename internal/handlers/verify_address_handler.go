package handlers

import (
	"net/http"

	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
)

// VerifyAddressIDHandler is the method used to get details on a provided zip code
func (h *Handler) VerifyAddressIDHandler(w http.ResponseWriter, r *http.Request) {
	ctx, log := initializeHandlerBasics(r)

	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.VerifyAddressAPIName, literals.ComponentTypeAddress)
	componentContextData := map[string]interface{}{}

	id := getValidatedUUIDPathVariable(r)
	if err := h.service.QueryAddressID(ctx, id); err != nil {
		log.Errorf("verify address request failed: %s", err.Error())
		componentContextData[literals.ContextErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		SendErrorMessage(ctx, w, err)
		return
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	w.WriteHeader(http.StatusNoContent)
}
