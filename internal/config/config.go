package config

import (
	"fmt"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
	"os"

	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
	"go.ftdr.com/go-utils/common/logging"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/services"
)

// WebServerConfig holds all configs including secrets
type WebServerConfig struct {
	Instrumentation *instrumentation.Config `split_words:"true"`
	Port            string                  `default:"50051"`
	CORSEnabled     bool                    `split_words:"true" default:"false"`
	Service         *services.ServiceConfig
	Log             *logging.Config
}

// CIConfig holds all pipeline env vars
type CIConfig struct {
	CommitShortSHA  string `split_words:"true" default:"dev-local"`
	EnvironmentName string `split_words:"true" default:"dev-local"`
}

const (
	ciPrefix = "CI"
)

// FromEnv pulls config from the environment
func FromEnv() (cfg *WebServerConfig, cicfg *CIConfig, err error) {
	fromFileToEnv()

	cfg = &WebServerConfig{}

	err = envconfig.Process(literals.EnvPrefix, cfg)

	if err != nil {
		return nil, nil, err
	}

	cicfg = &CIConfig{}
	err = envconfig.Process(ciPrefix, cicfg)
	if err != nil {
		return nil, nil, err
	}

	return cfg, cicfg, nil
}

func fromFileToEnv() {
	cfgFilename := os.Getenv("ENV_FILE")
	if cfgFilename != "" {
		err := godotenv.Load(cfgFilename)
		if err != nil {
			fmt.Println("ENV_FILE not found. Trying MY_POD_NAMESPACE")
		}
		return
	}

	loc := os.Getenv("MY_POD_NAMESPACE")

	cfgFilename = fmt.Sprintf("../../etc/config/config.%s.env", loc)
	//cfgFilename = fmt.Sprintf("../scripts/etc/config.%s.env", loc)
	err := godotenv.Load(cfgFilename)
	if err != nil {
		fmt.Println("No config files found to load to env. Defaulting to environment.")
	}
}
