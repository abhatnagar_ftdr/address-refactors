package mongo_dal

import (
	"context"
	"go.ftdr.com/go-utils/common/logging"
	"go.mongodb.org/mongo-driver/mongo"
)

func (dao *MongoDAO) StartTransaction(ctx context.Context) (context.Context, error) {
	log := logging.GetTracedLogEntry(ctx)
	txnCtx, err := dao.Client().StartTransaction(ctx, nil, nil)
	if err != nil {
		log.WithError(err).Error("mongo transaction creation failed")
		return nil, err
	}
	log.Info("mongo transaction created")
	return txnCtx, nil
}

// CommitTransaction commits a transaction to the database
func (dao *MongoDAO) CommitTransaction(ctx context.Context) error {
	log := logging.GetTracedLogEntry(ctx)
	log.Info("attempting to commit transaction")

	commitErr := dao.Client().CommitTransaction(ctx)
	if commitErr != nil {
		log.WithField("Error", commitErr).Error("failed to commit transaction")
		return commitErr
	}

	log.Info("successfully committed transaction")
	return nil
}

// AbortTransaction will roll back a transaction
func (dao *MongoDAO) AbortTransaction(ctx context.Context) error {
	log := logging.GetTracedLogEntry(ctx)
	log.Info("attempting to abort transaction")

	abortErr := dao.Client().AbortTransaction(ctx)
	if abortErr != nil {
		log.WithField("Error", abortErr).Error("failed to abort transaction")
		return abortErr
	}

	log.Info("successfully aborted transaction")
	return nil
}

func (dao *MongoDAO) WithTransaction(ctx context.Context, callback func(mongo.SessionContext) (interface{}, error)) error {
	client := dao.Client()

	err := client.WithTransaction(ctx, nil, nil, callback)
	return err
}
