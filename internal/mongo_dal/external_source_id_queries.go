package mongo_dal

import (
	"context"
	"time"

	"go.ftdr.com/go-utils/common/logging"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
	addressErrors "golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors"
)

// InsertExternalSourceID ...
func (dao *MongoDAO) InsertExternalSourceID(ctx context.Context, uuidRaw string, name string, id uint32) (numModified int64, err *addressErrors.ServerError) {
	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.InsertExternalSourceID, literals.ComponentTypeAddressRepository)
	componentContextData := map[string]interface{}{}

	fields := map[string]interface{}{"AddressID": uuidRaw, "ExternalSourceName": name, "ExternalSourceID": id}
	log := logging.GetTracedLogEntry(ctx)
	log.WithFields(fields).Info()

	collection := dao.Collection(AddressCollection)

	filter, err := buildUUIDFilter(ctx, uuidRaw)
	if err != nil {
		componentContextData[literals.ContextErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return 0, err
	}

	externalSource := ExternalSource{
		Name:        name,
		ID:          id,
		LastUpdated: primitive.NewDateTimeFromTime(time.Now()),
	}

	update := bson.A{bson.M{
		"$set": bson.M{
			"external_source_list": bson.M{
				"$ifNull": bson.A{
					bson.M{"$concatArrays": bson.A{"$external_source_list", bson.A{externalSource}}},
					bson.A{externalSource},
				},
			},
		},
	}}

	dbCtx, cancel := dao.createContext(ctx)
	defer cancel()

	updateResult, updateErr := collection.UpdateOne(dbCtx, filter, update)
	if updateErr != nil {
		componentContextData[literals.ContextErrorMessage] = updateErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		log.WithFields(fields).WithFields(generateLogFields(updateErr, filter)).Warn(addressErrors.DaoUpdateError.Error())
		return 0, addressErrors.DaoUpdateError.New()
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return updateResult.ModifiedCount, nil
}

// DeleteExternalSourceID ...
func (dao *MongoDAO) DeleteExternalSourceID(ctx context.Context, uuidRaw string, name string, id uint32) (numModified int64, err *addressErrors.ServerError) {
	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.DeleteExternalSourceID, literals.ComponentTypeAddressRepository)
	componentContextData := map[string]interface{}{}

	log := logging.GetTracedLogEntry(ctx)

	collection := dao.Collection(AddressCollection)

	filter, err := buildUUIDFilter(ctx, uuidRaw)
	if err != nil {
		componentContextData[literals.ContextErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return 0, err
	}

	update := bson.M{
		"$pull": bson.M{
			"external_source_list": ExternalSource{Name: name, ID: id},
		},
	}

	dbCtx, cancel := dao.createContext(ctx)
	defer cancel()

	updateResult, updateErr := collection.UpdateOne(dbCtx, filter, update)
	if updateErr != nil {
		componentContextData[literals.ContextErrorMessage] = updateErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		log.WithFields(generateLogFields(updateErr, filter)).Error(addressErrors.DaoUpdateError.Error())
		return 0, addressErrors.DaoUpdateError.New()
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return updateResult.ModifiedCount, nil
}

// DeleteAllExternalSourceID ...
func (dao *MongoDAO) DeleteAllExternalSourceID(ctx context.Context, name string, id uint32) (numModified int64, err *addressErrors.ServerError) {
	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.DeleteAllExternalSourceID, literals.ComponentTypeAddressRepository)
	componentContextData := map[string]interface{}{"name": name, "id": id}

	log := logging.GetTracedLogEntry(ctx)

	collection := dao.Collection(AddressCollection)

	filter := bson.M{
		"external_source_list.name": name,
		"external_source_list.ID":   id,
	}

	update := bson.M{
		"$pull": bson.M{
			"external_source_list": ExternalSource{Name: name, ID: id},
		},
	}

	dbCtx, cancel := dao.createContext(ctx)
	defer cancel()

	updateResult, updateErr := collection.UpdateMany(dbCtx, filter, update)
	if updateErr != nil {
		componentContextData[literals.ContextErrorMessage] = updateErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		log.WithError(updateErr).Error(addressErrors.DaoUpdateError.Error())
		return 0, addressErrors.DaoUpdateError.New()
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return updateResult.ModifiedCount, nil
}

// DeleteAllExternalSourceIDWithException ...
func (dao *MongoDAO) DeleteAllExternalSourceIDWithException(ctx context.Context, name string, id uint32, uuid primitive.Binary) (numModified int64, err *addressErrors.ServerError) {
	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.DeleteAllExternalSourceID, literals.ComponentTypeAddressRepository)
	componentContextData := map[string]interface{}{"name": name, "id": id}

	log := logging.GetTracedLogEntry(ctx)

	collection := dao.Collection(AddressCollection)

	filter := bson.M{
		"_id":                       bson.M{"$ne": uuid},
		"external_source_list.name": name,
		"external_source_list.ID":   id,
	}

	update := bson.M{
		"$pull": bson.M{
			"external_source_list": ExternalSource{Name: name, ID: id},
		},
	}

	dbCtx, cancel := dao.createContext(ctx)
	defer cancel()

	updateResult, updateErr := collection.UpdateMany(dbCtx, filter, update)
	if updateErr != nil {
		componentContextData[literals.ContextErrorMessage] = updateErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		log.WithError(updateErr).Error(addressErrors.DaoUpdateError.Error())
		return 0, addressErrors.DaoUpdateError.New()
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return updateResult.ModifiedCount, nil
}

// QueryAddressByExternalSourceID ...
func (dao *MongoDAO) QueryAddressByExternalSourceID(ctx context.Context, name string, id uint32) (address *AddressRecord, serr *addressErrors.ServerError) {
	fields := map[string]interface{}{"ExternalSourceID": id}
	log := logging.GetTracedLogEntry(ctx).WithFields(fields)

	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.QueryAddressByExternalSourceID, literals.ComponentTypeAddressRepository)
	componentContextData := map[string]interface{}{}
	log.WithFields(fields) // only set the field in the log

	filter := bson.M{
		"external_source_list.name": name,
		"external_source_list.ID":   id,
	}

	componentContextData[literals.ContextMongoQueryFilter] = asJSON(filter)

	results, err := dao.findOneAddress(ctx, filter)
	if err != nil {
		componentContextData[literals.ContextErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return nil, addressErrors.DaoQueryError.New()
	}

	componentContextData[literals.ContextNoDocumentsFound] = results == nil

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return results, nil
}
