package mongo_dal

const (
	AddressCollection = "address"
	ArchiveCollection = "archive"
	ZipcodeCollection = "zipcode"
)

var MongoCollections = []string{
	AddressCollection,
	ArchiveCollection,
}
