package mongo_dal

import "time"

const (
	dbIndex                = "indexing"
	dbConnectionCtxTimeout = 30 * time.Second
	dbRequestTimeout       = 15 * time.Second
)
