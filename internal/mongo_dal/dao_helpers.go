package mongo_dal

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"go.ftdr.com/go-utils/common/logging"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/external_source"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/utils"
	addressErrors "golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors"
	pb "golang.frontdoorhome.com/software/protos/go/addresspb"
	"golang.org/x/exp/slices"
)

// Format will format the fields that are searched for to make matching easier
func (a *AddressRecord) Format() *AddressRecord {
	a.Street1 = utils.TrimAndUpper(a.Street1)
	a.Street2 = utils.TrimAndUpper(a.Street2)
	a.UnitType = utils.TrimAndUpper(a.UnitType)
	a.UnitValue = utils.TrimAndUpper(a.UnitValue)
	a.City = utils.TrimAndUpper(a.City)
	a.DefaultCity = utils.TrimAndUpper(a.DefaultCity)
	a.State = utils.TrimAndUpper(a.State)
	a.Zip = utils.TrimAndUpper(a.Zip)
	a.ZipLocal = utils.TrimAndUpper(a.ZipLocal)
	a.CountryIso3 = utils.TrimAndUpper(a.CountryIso3)
	a.RecordType = utils.TrimAndUpper(a.RecordType)
	return a
}

// IsEquivalent checks the textual fields of an address against an address record.
// If there are any differences (case sensitive for formatting records correctly),
// return false. Otherwise, return true
func (a *AddressRecord) IsEquivalent(ctx context.Context, b *AddressRecord) bool {
	log := logging.GetTracedLogEntry(ctx)

	if b == nil {
		log.Warn("nil record received for equivalency check")
		return false
	}

	if a.Street1 != b.Street1 {
		log.WithFields(buildDifferingLogFields(a.Street1, b.Street1)).Debug("differing street1 found")
		return false
	}

	if a.Street2 != b.Street2 {
		log.WithFields(buildDifferingLogFields(a.Street2, b.Street2)).Debug("differing street2 found")
		return false
	}

	if a.UnitType != b.UnitType {
		log.WithFields(buildDifferingLogFields(a.UnitType, b.UnitType)).Debug("differing unit type found")
		return false
	}

	if a.UnitValue != b.UnitValue {
		log.WithFields(buildDifferingLogFields(a.UnitValue, b.UnitValue)).Debug("differing unit value found")
		return false
	}

	if a.City != b.City {
		log.WithFields(buildDifferingLogFields(a.City, b.City)).Debug("differing city found")
		return false
	}

	if a.State != b.State {
		log.WithFields(buildDifferingLogFields(a.State, b.State)).Debug("differing state found")
		return false
	}

	if a.County != b.County {
		log.WithFields(buildDifferingLogFields(a.County, b.County)).Debug("differing county found")
		return false
	}

	if a.Zip != b.Zip {
		log.WithFields(buildDifferingLogFields(a.Zip, b.Zip)).Debug("differing zip found")
		return false
	}

	if a.RecordType != b.RecordType {
		log.WithFields(buildDifferingLogFields(a.RecordType, b.RecordType)).Debug("differing record type found")
		return false
	}
	if a.DocVersion != CurrentDocVersion {
		log.Warn("the version of the document is not up-to-date")
		return false
	}

	return true
}

func buildDifferingLogFields(a string, b string) map[string]interface{} {
	return map[string]interface{}{
		"Original":  a,
		"Differing": b,
	}
}

// FormatWhitespace will trim the fields that are saved for to make matching easier
func (a *AddressRecord) FormatWhitespace() *AddressRecord {
	a.Street1 = utils.TrimWhitespace(a.Street1)
	a.Street2 = utils.TrimWhitespace(a.Street2)
	a.UnitType = utils.TrimWhitespace(a.UnitType)
	a.UnitValue = utils.TrimWhitespace(a.UnitValue)
	a.City = utils.TrimWhitespace(a.City)
	a.State = utils.TrimWhitespace(a.State)
	a.Zip = utils.TrimWhitespace(a.Zip)
	a.ZipLocal = utils.TrimWhitespace(a.ZipLocal)
	a.CountryIso3 = utils.TrimWhitespace(a.CountryIso3)
	return a
}

// GetSecondaryUnit builds the secondary string for you
func (a *AddressRecord) GetSecondaryUnit() (secondaryUnit string) {
	return utils.TrimWhitespace(fmt.Sprintf("%s %s", a.UnitType, a.UnitValue))
}

// ToByteArray marshals address to byte array
func (a *AddressRecord) ToByteArray() []byte {
	data, err := json.Marshal(a)
	if err != nil {
		logging.Log.Warn("failed to marshal address record to byte array")
		return nil
	}
	logging.Log.WithField("Data", data).Trace("marshaled address record to byte array")
	return data
}

func (a *AddressRecord) IsExternalSourcePresent(source pb.ExternalSource) bool {
	if source == pb.ExternalSource_UNKNOWN_SOURCE {
		if len(a.LegacyIDList) > 0 {
			return true
		}
		return false
	}

	sourceName := external_source.ExternalSourcePBName(source)
	idx := slices.IndexFunc(a.ExternalSourceList, func(es ExternalSource) bool {
		return es.Name == sourceName
	})
	if idx >= 0 {
		return true
	}
	return false
}

// GenerateUUID will construct a new UUID or return an error
func GenerateUUID(ctx context.Context) (*primitive.Binary, error) {
	log := logging.GetTracedLogEntry(ctx)
	generatedUUID, err := uuid.NewRandom()
	if err != nil {
		log.WithError(err).Error(addressErrors.DaoUUIDGenerationError.Error())
		return nil, err
	}

	data, err := generatedUUID.MarshalBinary()
	if err != nil {
		log.WithError(err).Error(addressErrors.DaoUUIDGenerationError.Error())
		return nil, err
	}

	if data == nil {
		err = errors.New("data was nil after generation")
		log.Error(err.Error())
		return nil, err
	}

	return &primitive.Binary{
		Subtype: UUIDHexCode,
		Data:    data,
	}, nil
}

// UUIDRawToBson will convert a uuid string to binary for the model
func UUIDRawToBson(ctx context.Context, uuidRaw string) (primitive.Binary, error) {
	log := logging.GetTracedLogEntry(ctx)
	uuidBin, _ := uuid.Parse(uuidRaw)
	data, err := uuidBin.MarshalBinary()
	if err != nil {
		log.WithError(err).WithField("uuid", uuidRaw).Warn("failed to convert uuid raw to bson")
		return primitive.Binary{}, err
	}
	return primitive.Binary{Subtype: UUIDHexCode, Data: data}, nil
}

// BsonUUIDToRaw converts a binary uuid to a human readable string
func BsonUUIDToRaw(ctx context.Context, data []byte) (string, error) {
	uuidBin, conversionErr := uuid.FromBytes(data)
	if conversionErr != nil {
		logging.GetTracedLogEntry(ctx).WithField("Error", conversionErr).Error("failed to generate UUID")
		return "", conversionErr
	}
	return uuidBin.String(), conversionErr
}
