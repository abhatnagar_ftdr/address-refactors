package mongo_dal

import "go.mongodb.org/mongo-driver/bson/primitive"

const (
	// VerifiedStatus is the verification status for a verified address and is the strongest verification
	VerifiedStatus string = "VERIFIED"

	// UnverifiedStatus is the verification status for an unverified address
	UnverifiedStatus string = "UNVERIFIED"

	// MatchedStatus is the verification status for a matched address
	MatchedStatus string = "MATCHED"

	region string = "US-IA"

	// CurrentDocVersion is the current version of document form in mongodb.
	//Raised when changes are made to the document that force the document to be automatically updated using the smartyStreet
	CurrentDocVersion uint32 = 2
)

type ExternalSource struct {
	Name        string             `bson:"name,omitempty" json:"name,omitempty"`
	ID          uint32             `bson:"ID,omitempty" json:"ID,omitempty"`
	LastUpdated primitive.DateTime `bson:"last_updated,omitempty" json:"last_updated,omitempty"`
}

// AddressRecord is the Mongo representation of the Address struct
type AddressRecord struct {
	UUID                 primitive.Binary   `bson:"_id" json:"_id"`
	Location             string             `bson:"location" json:"location"`
	LegacyIDList         []uint32           `bson:"legacy_id_list,omitempty" json:"legacy_id_list,omitempty"`
	ExternalSourceList   []ExternalSource   `bson:"external_source_list,omitempty" json:"external_source_list,omitempty"`
	DeliveryPointBarcode string             `bson:"delivery_point_barcode" json:"delivery_point_barcode"`
	Street1              string             `bson:"street_1" json:"street_1"`
	Street2              string             `bson:"street_2" json:"street_2"`
	UnitType             string             `bson:"unit_type" json:"unit_type"`
	UnitValue            string             `bson:"unit_value" json:"unit_value"`
	City                 string             `bson:"city" json:"city"`
	County               string             `bson:"county" json:"county"`
	DefaultCity          string             `bson:"default_city" json:"default_city"`
	State                string             `bson:"state" json:"state"`
	Zip                  string             `bson:"zip" json:"zip"`
	ZipLocal             string             `bson:"zip_local" json:"zip_local"`
	CountryIso3          string             `bson:"country_iso_3" json:"country_iso_3"`
	Latitude             float64            `bson:"latitude" json:"latitude"`
	Longitude            float64            `bson:"longitude" json:"longitude"`
	Status               string             `bson:"status" json:"status"`
	LastUpdated          primitive.DateTime `bson:"last_updated" json:"last_updated"`
	TimeZoneName         string             `bson:"time_zone_name" json:"time_zone_name"`
	UTCOffset            int32              `bson:"utc_offset" json:"utc_offset"`
	DSTObserved          bool               `bson:"dst_observed" json:"dst_observed"`
	RecordType           string             `bson:"record_type" json:"record_type"`

	DocVersion uint32 `bson:"doc_version" json:"doc_version"`
}

type ZipcodeRecord struct {
	UUID      primitive.Binary `bson:"_id" json:"_id"`
	ZipCode   string           `bson:"code" json:"code"`
	City      string           `bson:"city" json:"city"`
	State     string           `bson:"state" json:"state"`
	AreaCode  string           `bson:"area_code" json:"area_code"`
	Latitude  float64          `bson:"lat" json:"lat"`
	Longitude float64          `bson:"lon" json:"lon"`
}
