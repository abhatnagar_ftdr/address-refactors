package mongo_dal

import (
	"context"
	"go.ftdr.com/go-utils/mongo/v2"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// MongoDAO is the Mongo implementation of the DAO interface
type MongoDAO struct {
	mongo.DatabaseHelper
	config *mongo.Config
}

const (
	// UUIDHexCode is the hex code for a bson uuid type
	UUIDHexCode = 0x04
)

// NewMongoDAO returns a configured, connected mongo data access object
func NewMongoDAO(ctx context.Context, config *mongo.Config) (*MongoDAO, error) {
	// client & database options
	var clOpts []*options.ClientOptions
	var dbOpts []*options.DatabaseOptions

	// connect to mongo Db.
	db, err := mongo.NewDatabase(config, clOpts, dbOpts)
	if err != nil {
		return nil, err
	}

	return &MongoDAO{
		DatabaseHelper: db,
		config:         config,
	}, nil
}

// Ping will ping the database
func (dao *MongoDAO) Ping() (err error) {
	return dao.Client().Ping()
}
