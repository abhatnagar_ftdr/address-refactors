package mongo_dal

import (
	"context"
	"go.ftdr.com/go-utils/mongo/v2/mongoerrors"
	"strings"
	"time"

	"go.ftdr.com/go-utils/common/logging"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
	addressErrors "golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors"
	pb "golang.frontdoorhome.com/software/protos/go/addresspb"
)

// QueryAddress queries for an address by street, unit, city, state, and zip
func (dao *MongoDAO) QueryAddress(
	ctx context.Context, lookup *AddressRecord) (address *AddressRecord, serr *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)

	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.QueryOneAddress, literals.ComponentTypeAddressRepository)
	componentContextData := map[string]interface{}{}

	filter := buildAddressQueryFilter(ctx, lookup.Format())
	jsonFilter := asJSON(filter)
	log.WithField("Filter", jsonFilter).
		Info("ADDRESS-MONGO-Query: QueryAddress filter options")

	componentContextData[literals.ContextMongoQueryFilter] = jsonFilter

	results, err := dao.findOneAddress(ctx, filter)
	if err != nil {
		log.WithField("Filter", asJSON(filter)).
			WithError(err).
			Warn("ADDRESS-MONGO-ERROR: QueryAddress returned an error")
		componentContextData[literals.ContextErrorMessage] = err.Error()
		componentContextData[literals.ContextInternalError] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return nil, addressErrors.DaoQueryError.New()
	}

	componentContextData[literals.ContextNoDocumentsFound] = results == nil

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return results, nil
}

// QueryAddressLookahead search for an address from a text address representation
func (dao *MongoDAO) QueryAddressLookahead(ctx context.Context, typeaheadAddress *pb.AddressByTypeAheadRequest, lookaheadLimit int64) (addressSuggestions []*AddressRecord, serr *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)

	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.QueryAddressLookahead, literals.ComponentTypeAddressRepository)
	componentContextData := map[string]interface{}{}

	collection := dao.Collection(AddressCollection)
	dbCtx, cancel := dao.createContext(ctx)
	defer cancel()

	filter := buildAddressTypeAheadQueryFilter(typeaheadAddress)
	cursor, err := collection.Find(dbCtx, filter, options.Find().SetLimit(lookaheadLimit))
	defer cursor.Close(ctx)
	if err != nil {
		componentContextData[literals.ContextErrorMessage] = err.Error()
		componentContextData[literals.ContextInternalError] = err.Error()
		if err.Error() == mongoerrors.MongoDBNoDocumentFoundError.Error() {
			log.WithField("Filter", asJSON(filter)).
				WithError(err).
				Warn("ADDRESS-MONGO-ERROR: QueryAddressLookahead returned an error")
			instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
			return nil, addressErrors.DaoQueryError.New()
		}
		log.WithField("Filter", asJSON(filter)).
			WithError(err).
			Warn("ADDRESS-MONGO-ERROR: QueryAddressLookahead returned no documents")
		instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
		return nil, nil
	}

	addressSuggestions = make([]*AddressRecord, 0, lookaheadLimit)
	index := 0
	for cursor.Next(dbCtx) {
		address := &AddressRecord{}
		decodeErr := cursor.Decode(address)
		if decodeErr != nil {
			log.WithField("Filter", asJSON(filter)).
				WithError(decodeErr).
				Warn("ADDRESS-MONGO-ERROR: QueryAddressLookahead failed to decode mongo returned results")
			componentContextData[literals.ContextErrorMessage] = decodeErr.Error()
			componentContextData[literals.ContextInternalError] = decodeErr.Error()
			instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

			log.WithFields(generateLogFields(decodeErr, filter)).Error(addressErrors.DaoDecodeError.Error())
			return nil, addressErrors.DaoDecodeError.New()
		}
		addressSuggestions = append(addressSuggestions, address)
		index++
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return addressSuggestions, nil
}

// QueryAddressByID searches for an address by a UUID string
func (dao *MongoDAO) QueryAddressByID(ctx context.Context, uuidRaw string) (address *AddressRecord, serr *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)

	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.QueryAddressByID, literals.ComponentTypeAddressRepository)
	componentContextData := map[string]interface{}{}

	fields := map[string]interface{}{"AddressID": uuidRaw}
	log.WithFields(fields).Info()

	filter, serr := buildUUIDFilter(ctx, uuidRaw)
	if serr != nil {
		componentContextData[literals.ContextErrorMessage] = serr.Error()
		componentContextData[literals.ContextInternalError] = serr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return nil, serr
	}

	results, err := dao.findOneAddress(ctx, filter)
	if err != nil {
		componentContextData[literals.ContextErrorMessage] = err.Error()
		componentContextData[literals.ContextInternalError] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return nil, addressErrors.DaoQueryError.New()
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return results, nil
}

// QueryAddressesByID queries for addresses by IDs
func (dao *MongoDAO) QueryAddressesByID(ctx context.Context, uuids []string) (addresses []*AddressRecord, serr *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)

	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.QueryAddressesByID, literals.ComponentTypeAddressRepository)
	componentContextData := map[string]interface{}{}

	fields := map[string]interface{}{"AddressIDs": uuids}
	log.WithFields(fields).Info()

	filter, serr := buildUUIDsFilter(ctx, uuids)
	if serr != nil {
		componentContextData[literals.ContextErrorMessage] = serr.Error()
		componentContextData[literals.ContextInternalError] = serr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return nil, serr
	}

	results, err := dao.findAddresses(ctx, filter)
	if err != nil {
		componentContextData[literals.ContextErrorMessage] = err.Error()
		componentContextData[literals.ContextInternalError] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return nil, addressErrors.DaoQueryError.New()
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return results, nil
}

// QueryAddressesByDPB queries for addresses by delivery point barcode
func (dao *MongoDAO) QueryAddressesByDPB(ctx context.Context, deliveryPointBarcode string) (addresses []*AddressRecord, serr *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)

	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.QueryAddressesByDPB, literals.ComponentTypeAddressRepository)
	componentContextData := map[string]interface{}{}

	log.WithField("DeliveryPointBarcode", deliveryPointBarcode) // only set the field in the log

	filter := bson.M{
		"delivery_point_barcode": deliveryPointBarcode,
	}

	results, err := dao.findAddresses(ctx, filter)
	if err != nil {
		componentContextData[literals.ContextErrorMessage] = err.Error()
		componentContextData[literals.ContextInternalError] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return nil, addressErrors.DaoQueryError.New()
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return results, nil
}

// QueryUnverifiedAddresses finds all addresses with an unverified status
func (dao *MongoDAO) QueryUnverifiedAddresses(ctx context.Context) (unverifiedAddresses []*AddressRecord, serr *addressErrors.ServerError) {
	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.QueryUnverifiedAddresses, literals.ComponentTypeAddressRepository)
	componentContextData := map[string]interface{}{}

	filter := bson.M{
		"status": UnverifiedStatus,
	}

	results, err := dao.findAddresses(ctx, filter)
	if err != nil {
		componentContextData[literals.ContextErrorMessage] = err.Error()
		componentContextData[literals.ContextInternalError] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return nil, addressErrors.DaoQueryError.New()
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return results, nil
}

func (dao *MongoDAO) QueryStateByZipCode(ctx context.Context, zipcode string) (state []*ZipcodeRecord, serr *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)

	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.QueryStateByZipCode, literals.ComponentTypeAddressRepository)
	componentContextData := map[string]interface{}{}

	log.WithField("zipcode", zipcode) // only set the field in the log

	filter := bson.M{"code": zipcode}

	results, err := dao.findZipcodes(ctx, filter)
	if err != nil {
		componentContextData[literals.ContextErrorMessage] = err.Error()
		componentContextData[literals.ContextInternalError] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		if strings.Contains(err.Error(), "ZIP_NOT_FOUND_ERROR:") {
			return nil, addressErrors.ZipNotFoundError.NewFormatted("Mongo", filter["code"])
		}
		return nil, addressErrors.DaoQueryError.New()
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return results, nil
}

func (dao *MongoDAO) QueryZipCodesByState(ctx context.Context, states []string) (zipcode []*ZipcodeRecord, serr *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)

	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.QueryZipCodesByState, literals.ComponentTypeAddressRepository)
	componentContextData := map[string]interface{}{}

	log.WithField("state", states) // only set the field in the log

	filter := bson.M{}
	if len(states) > 0 {
		filter["state"] = bson.M{"$in": states}
	}

	results, err := dao.findZipcodes(ctx, filter)
	if err != nil {
		componentContextData[literals.ContextErrorMessage] = err.Error()
		componentContextData[literals.ContextInternalError] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return nil, addressErrors.DaoQueryError.New()
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return results, nil
}

// InsertAddress will insert a record into the database
func (dao *MongoDAO) InsertAddress(ctx context.Context, address *AddressRecord) (err *addressErrors.ServerError) {
	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.InsertAddress, literals.ComponentTypeAddressRepository)
	componentContextData := map[string]interface{}{}

	fields := map[string]interface{}{"Address": address}
	log := logging.GetTracedLogEntry(ctx)
	log.WithFields(fields).Info()

	collection := dao.Collection(AddressCollection)

	address.LastUpdated = primitive.NewDateTimeFromTime(time.Now())
	if len(address.LegacyIDList) == 0 {
		address.LegacyIDList = make([]uint32, 0)
	}

	address.Location = region
	address.Format()

	dbCtx, cancel := dao.createContext(ctx)
	defer cancel()

	if _, insertErr := collection.InsertOne(dbCtx, address); insertErr != nil {
		componentContextData[literals.ContextErrorMessage] = insertErr.Error()
		componentContextData[literals.ContextInternalError] = insertErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		log.WithError(insertErr).WithFields(fields).Error(addressErrors.DaoInsertError.Error())
		return addressErrors.DaoInsertError.New()
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return nil
}

// UpdateAddress will update a record in the database, not for inserts
func (dao *MongoDAO) UpdateAddress(ctx context.Context, address *AddressRecord) (err *addressErrors.ServerError) {
	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.UpdateAddress, literals.ComponentTypeAddressRepository)
	componentContextData := map[string]interface{}{}

	log := logging.GetTracedLogEntry(ctx)

	address.LastUpdated = primitive.NewDateTimeFromTime(time.Now())

	collection := dao.Collection(AddressCollection)
	filter := bson.M{
		"_id": address.UUID,
	}

	update := bson.M{
		"$set": address,
	}
	componentContextData["filter"] = filter
	componentContextData["update"] = update

	dbCtx, cancel := dao.createContext(ctx)
	defer cancel()

	updateResult, updateErr := collection.UpdateOne(dbCtx, filter, update)
	if updateErr != nil {
		componentContextData[literals.ContextErrorMessage] = updateErr.Error()
		componentContextData[literals.ContextInternalError] = updateErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		err = addressErrors.DaoUpdateError.New()
		log.WithField("Error", updateErr).Error(err.Error())
		return err
	}

	if updateResult.ModifiedCount == 0 {
		componentContextData[literals.ContextErrorMessage] = "updates did not take effect"
		componentContextData[literals.ContextInternalError] = "updates did not take effect"
		instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
		log.Error("update did not modify any documents")
		return addressErrors.AddressIDNotFoundError.NewFormatted(BsonUUIDToRaw(ctx, address.UUID.Data))
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return nil
}

func (dao *MongoDAO) UpdateAddressWithArchiving(ctx context.Context, archive, update *AddressRecord) (address *AddressRecord, err *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)

	fields := map[string]interface{}{
		"archiveAddress": archive,
		"updateAddress":  update,
	}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.UpdateAddressInMongo, literals.ComponentTypeAddressService)
	update.UUID = archive.UUID

	txnCtx, txnErr := dao.StartTransaction(ctx)
	if txnErr != nil {
		fields[literals.LLErrorMessage] = txnErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, addressErrors.DaoTransactionError.New()
	}
	defer func() {
		var txnErr error
		if err != nil {
			txnErr = dao.AbortTransaction(txnCtx)
		} else {
			txnErr = dao.CommitTransaction(txnCtx)
		}
		if txnErr != nil {
			log.WithError(txnErr).Error("error while handling transaction")
		}
	}()
	err = dao.ArchiveAddress(ctx, *archive)
	if err != nil {
		fields[literals.LLErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, err
	}

	err = dao.UpdateAddress(ctx, update)
	if err != nil {
		fields[literals.LLErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, err
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return update, nil
}

// QueryCollectionsStats uses a common helper to return a map of collection names to document count, or an error
func (dao *MongoDAO) QueryCollectionsStats(ctx context.Context) (map[string]interface{}, *addressErrors.ServerError) {
	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.QueryCollectionsStats, literals.ComponentTypeAddressRepository)
	componentContextData := map[string]interface{}{}

	log := logging.GetTracedLogEntry(ctx)
	var colDocumentCount map[string]interface{}

	for _, collection := range MongoCollections {
		count, err := dao.Collection(collection).Count(ctx, bson.D{})
		if err != nil {
			log.WithField("Error", err).WithField("Collection", collection).Warn("failed to get document count")
			componentContextData[literals.ContextErrorMessage] = err.Error()
			componentContextData[literals.ContextInternalError] = err.Error()
			instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
			log.WithError(err).Error(addressErrors.DaoQueryError.Error())
			return nil, addressErrors.DaoQueryError.New()
		}

		colDocumentCount[collection] = count
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return colDocumentCount, nil
}

// QueryFilterStats uses a common helper to return a count of documents matching a filter on a collection, or an error
func (dao *MongoDAO) QueryFilterStats(ctx context.Context, collection string, filter map[string]interface{}) (int64, *addressErrors.ServerError) {
	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.QueryFilterStats, literals.ComponentTypeAddressRepository)
	componentContextData := map[string]interface{}{}

	log := logging.GetTracedLogEntry(ctx)

	convertedFilter, err := convertMapToFilter(ctx, filter)
	if err != nil {
		componentContextData[literals.ContextErrorMessage] = err.Error()
		componentContextData[literals.ContextInternalError] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return -1, err
	}

	count, queryErr := dao.Collection(collection).Count(ctx, convertedFilter)
	if queryErr != nil {
		log.WithField("Error", queryErr).WithField("Collection", collection).Warn("failed to get document count")
		componentContextData[literals.ContextErrorMessage] = queryErr.Error()
		componentContextData[literals.ContextInternalError] = queryErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		log.WithError(queryErr).Error(addressErrors.DaoQueryError.Error())
		return 0, addressErrors.DaoQueryError.New()
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return count, nil
}

// ArchiveAddress appends an address record to the archived address collection at the ID in the record
func (dao *MongoDAO) ArchiveAddress(ctx context.Context, address AddressRecord) *addressErrors.ServerError {
	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.ArchiveAddress, literals.ComponentTypeAddressRepository)
	componentContextData := map[string]interface{}{}

	log := logging.GetTracedLogEntry(ctx)

	collection := dao.Collection(ArchiveCollection)

	dbCtx, cancel := dao.createContext(ctx)
	defer cancel()

	filter := bson.M{"_id": address.UUID}
	update := bson.M{
		"$push": bson.M{
			"archive": bson.M{
				"$each":     []AddressRecord{address},
				"$position": 0,
			},
		},
	}
	upsertResult, err := collection.UpdateOne(dbCtx, filter, update, options.Update().SetUpsert(true))
	if err != nil {
		log.WithField("Error", err.Error()).Error("failed to archive address")
		componentContextData[literals.ContextErrorMessage] = err.Error()
		componentContextData[literals.ContextInternalError] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return addressErrors.DaoUpdateError.New()
	}

	fields := map[string]interface{}{
		"MatchedCount":  upsertResult.MatchedCount,
		"InsertCount":   upsertResult.UpsertedCount,
		"ModifiedCount": upsertResult.ModifiedCount,
	}
	log.WithFields(fields).Info("address archive transaction ready for commit")

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return nil
}
