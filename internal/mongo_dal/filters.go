package mongo_dal

import (
	"context"
	"fmt"
	"go.ftdr.com/go-utils/common/logging"
	"regexp"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/utils"
	addressErrors "golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors"
	"golang.frontdoorhome.com/go-utils/validators"
	pb "golang.frontdoorhome.com/software/protos/go/addresspb"
)

func buildUUIDFilter(ctx context.Context, uuidRaw string) (filter bson.M, err *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)

	uuidBin, uuidErr := UUIDRawToBson(ctx, uuidRaw)
	if uuidErr != nil {
		log.WithError(uuidErr).Error(addressErrors.DaoUUIDParseError.Error())
		return nil, addressErrors.DaoUUIDParseError.New()
	}

	return bson.M{
		"_id": uuidBin,
	}, nil
}

func buildUUIDsFilter(ctx context.Context, uuids []string) (filter bson.M, serr *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)

	uuidBins := make([]primitive.Binary, len(uuids))

	for index, uuidRaw := range uuids {
		var err error
		uuidBins[index], err = UUIDRawToBson(ctx, uuidRaw)
		if err != nil {
			log.WithError(err).WithField("uuid", uuidRaw).Error(addressErrors.DaoUUIDParseError.Error())
			return nil, addressErrors.DaoUUIDParseError.New()
		}
	}

	filter = bson.M{
		"_id": bson.M{
			"$in": uuidBins,
		},
	}

	return filter, nil
}

func buildAddressQueryFilter(ctx context.Context, addressRecord *AddressRecord) (filter bson.M) {
	addressRecord.FormatWhitespace()

	if !addressRecord.UUID.IsZero() {
		return bson.M{"_id": addressRecord.UUID}
	}

	filter = bson.M{"street_1": addressRecord.Street1}
	filter["unit_value"] = addressRecord.UnitValue
	if addressRecord.Street2 != "" {
		filter["street_2"] = addressRecord.Street2
	}
	if addressRecord.UnitType != "" {
		filter["unit_type"] = addressRecord.UnitType
	}
	if addressRecord.City != "" {
		filter["city"] = addressRecord.City
	}
	if addressRecord.State != "" {
		filter["state"] = addressRecord.State
	}
	if addressRecord.Zip != "" {
		filter["zip"] = addressRecord.Zip
	}
	if addressRecord.ZipLocal != "" {
		filter["zip_local"] = addressRecord.ZipLocal
	}
	if addressRecord.DeliveryPointBarcode != "" {
		filter["delivery_point_barcode"] = addressRecord.DeliveryPointBarcode
	}

	log := logging.GetTracedLogEntry(ctx)
	log.WithField("Filter", filter).Debug()
	return filter
}

func buildAddressTypeAheadQueryFilter(typeaheadAddress *pb.AddressByTypeAheadRequest) (filter bson.M) {
	filter = bson.M{
		"street_1": bson.M{
			"$regex": fmt.Sprintf("^%s", regexp.QuoteMeta(utils.TrimAndUpper(typeaheadAddress.GetStreetLine()))),
		},
	}
	if utils.IsNotBlank(typeaheadAddress.GetUnitValue()) {
		filter["unit_type"] = bson.M{
			"$regex": fmt.Sprintf("^%s", regexp.QuoteMeta(utils.TrimAndUpper(typeaheadAddress.GetUnitType()))),
		}
	}
	if utils.IsNotBlank(typeaheadAddress.GetUnitValue()) {
		filter["unit_value"] = bson.M{
			"$regex": fmt.Sprintf("^%s", regexp.QuoteMeta(utils.TrimAndUpper(typeaheadAddress.GetUnitValue()))),
		}
	}
	if utils.IsNotBlank(typeaheadAddress.GetCity()) {
		filter["city"] = bson.M{
			"$regex": fmt.Sprintf("^%s", regexp.QuoteMeta(utils.TrimAndUpper(typeaheadAddress.GetCity()))),
		}
	}
	if utils.IsNotBlank(typeaheadAddress.GetState()) {
		filter["state"] = fmt.Sprintf("^%s", regexp.QuoteMeta(utils.TrimAndUpper(typeaheadAddress.GetState())))
	}
	return filter
}

func convertMapToFilter(ctx context.Context, filter map[string]interface{}) (bson.M, *addressErrors.ServerError) {
	target := bson.M{}
	for key, val := range filter {
		value, err := convertInterfaceToValue(ctx, val)
		if err != nil {
			return nil, err
		}
		target[key] = value
	}
	return target, nil
}

func convertInterfaceToValue(ctx context.Context, value interface{}) (interface{}, *addressErrors.ServerError) {
	switch value.(type) {
	case string:
		value := fmt.Sprintf("%v", value)
		if validators.IsValidUUID(value) {
			id, err := UUIDRawToBson(ctx, value)
			if err != nil {
				return nil, addressErrors.InvalidIDError.NewFormatted(value)
			}
			return id, nil
		}
		return value, nil
	default:
		break
	}
	return value, nil
}
