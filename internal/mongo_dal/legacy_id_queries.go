package mongo_dal

import (
	"context"

	"go.ftdr.com/go-utils/common/logging"
	"go.mongodb.org/mongo-driver/bson"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
	addressErrors "golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors"
)

// InsertLegacyID inserts a legacyID at the document stored at the uuid
func (dao *MongoDAO) InsertLegacyID(ctx context.Context, uuidRaw string, legacyID uint32) (numModified int64, err *addressErrors.ServerError) {
	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.InsertLegacyID, literals.ComponentTypeAddressRepository)
	componentContextData := map[string]interface{}{}

	fields := map[string]interface{}{"AddressID": uuidRaw, "LegacyID": legacyID}
	log := logging.GetTracedLogEntry(ctx)
	log.WithFields(fields).Info()

	collection := dao.Collection(AddressCollection)

	filter, err := buildUUIDFilter(ctx, uuidRaw)
	if err != nil {
		componentContextData[literals.ContextErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return 0, err
	}

	update := bson.A{bson.M{
		"$set": bson.M{
			"legacy_id_list": bson.M{
				"$ifNull": bson.A{
					bson.M{"$concatArrays": bson.A{"legacy_id_list", bson.A{legacyID}}},
					bson.A{legacyID},
				},
			},
		},
	}}

	dbCtx, cancel := dao.createContext(ctx)
	defer cancel()

	updateResult, updateErr := collection.UpdateOne(dbCtx, filter, update)
	if updateErr != nil {
		componentContextData[literals.ContextErrorMessage] = updateErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		log.WithFields(fields).WithFields(generateLogFields(updateErr, filter)).Warn(addressErrors.DaoUpdateError.Error())
		return 0, addressErrors.DaoUpdateError.New()
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return updateResult.ModifiedCount, nil
}

// DeleteLegacyID deletes a legacyID at the document stored at the uuid
func (dao *MongoDAO) DeleteLegacyID(ctx context.Context, uuidRaw string, legacyID uint32) (numModified int64, err *addressErrors.ServerError) {
	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.DeleteLegacyID, literals.ComponentTypeAddressRepository)
	componentContextData := map[string]interface{}{}

	log := logging.GetTracedLogEntry(ctx)

	collection := dao.Collection(AddressCollection)

	filter, err := buildUUIDFilter(ctx, uuidRaw)
	if err != nil {
		componentContextData[literals.ContextErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return 0, err
	}

	update := bson.M{
		"$pull": bson.M{
			"legacy_id_list": legacyID,
		},
	}

	dbCtx, cancel := dao.createContext(ctx)
	defer cancel()

	updateResult, updateErr := collection.UpdateOne(dbCtx, filter, update)
	if updateErr != nil {
		componentContextData[literals.ContextErrorMessage] = updateErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		log.WithFields(generateLogFields(updateErr, filter)).Error(addressErrors.DaoUpdateError.Error())
		return 0, addressErrors.DaoUpdateError.New()
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return updateResult.ModifiedCount, nil
}

// DeleteAllLegacyID deletes a legacyID at the document stored at the uuid
func (dao *MongoDAO) DeleteAllLegacyID(ctx context.Context, legacyID uint32) (numModified int64, err *addressErrors.ServerError) {
	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.DeleteAllLegacyID, literals.ComponentTypeAddressRepository)
	componentContextData := map[string]interface{}{}

	log := logging.GetTracedLogEntry(ctx)

	collection := dao.Collection(AddressCollection)

	//use filter so it doesn't impact performance
	filter := bson.M{
		"legacy_id_list": bson.M{
			"$in": []uint32{legacyID},
		},
	}

	update := bson.M{
		"$pull": bson.M{
			"legacy_id_list": legacyID,
		},
	}

	dbCtx, cancel := dao.createContext(ctx)
	defer cancel()

	updateResult, updateErr := collection.UpdateMany(dbCtx, filter, update)
	if updateErr != nil {
		componentContextData[literals.ContextErrorMessage] = updateErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		log.WithError(updateErr).Error(addressErrors.DaoUpdateError.Error())
		return 0, addressErrors.DaoUpdateError.New()
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return updateResult.ModifiedCount, nil
}

// QueryAddressByLegacyID searches for an address by it's legacyID
func (dao *MongoDAO) QueryAddressByLegacyID(ctx context.Context, legacyID uint32) (address *AddressRecord, serr *addressErrors.ServerError) {
	fields := map[string]interface{}{"LegacyID": legacyID}
	log := logging.GetTracedLogEntry(ctx).WithFields(fields)

	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.QueryAddressByLegacyID, literals.ComponentTypeAddressRepository)
	componentContextData := map[string]interface{}{}
	log.WithFields(fields) // only set the field in the log

	filter := bson.M{
		"legacy_id_list": bson.M{
			"$in": []uint32{legacyID}}}

	componentContextData[literals.ContextMongoQueryFilter] = asJSON(filter)

	results, err := dao.findOneAddress(ctx, filter)
	if err != nil {
		componentContextData[literals.ContextErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return nil, addressErrors.DaoQueryError.New()
	}

	componentContextData[literals.ContextNoDocumentsFound] = results == nil

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return results, nil
}
