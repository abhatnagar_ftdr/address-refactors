package mongo_dal

import (
	"context"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"

	addressErrors "golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors"
)

// DAO is the interface for accessing data
type DAO interface {
	Ping() (err error)

	// queries
	QueryAddress(ctx context.Context, lookup *AddressRecord) (address *AddressRecord, err *addressErrors.ServerError)
	QueryAddressByID(ctx context.Context, uuidRaw string) (address *AddressRecord, err *addressErrors.ServerError)
	QueryAddressesByID(ctx context.Context, uuids []string) (addresses []*AddressRecord, err *addressErrors.ServerError)
	QueryAddressesByDPB(ctx context.Context, deliveryPointBarcode string) (addresses []*AddressRecord, err *addressErrors.ServerError)
	QueryCollectionsStats(ctx context.Context) (map[string]interface{}, *addressErrors.ServerError)
	QueryFilterStats(ctx context.Context, collection string, filter map[string]interface{}) (docCount int64, err *addressErrors.ServerError)
	QueryUnverifiedAddresses(ctx context.Context) (unverifiedAddresses []*AddressRecord, err *addressErrors.ServerError)
	QueryStateByZipCode(ctx context.Context, zipcode string) (state []*ZipcodeRecord, err *addressErrors.ServerError)
	QueryZipCodesByState(ctx context.Context, state []string) (zipcode []*ZipcodeRecord, err *addressErrors.ServerError)

	// operations
	InsertAddress(ctx context.Context, address *AddressRecord) (err *addressErrors.ServerError)
	UpdateAddress(ctx context.Context, address *AddressRecord) (err *addressErrors.ServerError)
	ArchiveAddress(ctx context.Context, address AddressRecord) *addressErrors.ServerError
	UpdateAddressWithArchiving(ctx context.Context, archive, update *AddressRecord) (address *AddressRecord, err *addressErrors.ServerError)

	// legacy id operations
	InsertLegacyID(ctx context.Context, uuidRaw string, legacyID uint32) (numModified int64, err *addressErrors.ServerError)
	DeleteLegacyID(ctx context.Context, uuidRaw string, legacyID uint32) (numModified int64, err *addressErrors.ServerError)
	DeleteAllLegacyID(ctx context.Context, legacyID uint32) (numModified int64, err *addressErrors.ServerError)
	QueryAddressByLegacyID(ctx context.Context, legacyID uint32) (address *AddressRecord, err *addressErrors.ServerError)

	// external source id operations
	InsertExternalSourceID(ctx context.Context, uuidRaw string, name string, id uint32) (numModified int64, err *addressErrors.ServerError)
	DeleteExternalSourceID(ctx context.Context, uuidRaw string, name string, id uint32) (numModified int64, err *addressErrors.ServerError)
	DeleteAllExternalSourceID(ctx context.Context, name string, id uint32) (numModified int64, err *addressErrors.ServerError)
	DeleteAllExternalSourceIDWithException(ctx context.Context, name string, id uint32, uuid primitive.Binary) (numModified int64, err *addressErrors.ServerError)
	QueryAddressByExternalSourceID(ctx context.Context, name string, id uint32) (address *AddressRecord, err *addressErrors.ServerError)

	// transaction operations
	StartTransaction(ctx context.Context) (context.Context, error)
	CommitTransaction(ctx context.Context) error
	AbortTransaction(ctx context.Context) error
	WithTransaction(ctx context.Context, callback func(mongo.SessionContext) (interface{}, error)) error
}
