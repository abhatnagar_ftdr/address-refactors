package mongo_dal

import (
	"context"
	"encoding/json"
	"fmt"

	"go.ftdr.com/go-utils/common/logging"
	"go.ftdr.com/go-utils/mongo/v2/mongoerrors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	addressErrors "golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors"
)

// CreateIndexes creates indexes on the fields needed for running search on addresses
func (dao *MongoDAO) CreateIndexes(ctx context.Context) error {
	// create indexes on the fields that are used for searching for addresses
	opt := options.Find()
	opt.SetLimit(int64(3))

	collection := dao.Collection(AddressCollection)

	dbConnectionCtx, cancelFn := context.WithTimeout(ctx, dbConnectionCtxTimeout)
	defer func() {
		cancelFn()
	}()

	filter := bson.M{dbIndex: 1}
	cursor, err := collection.Find(dbConnectionCtx, filter, opt)

	if err != nil {
		return err
	}

	defer cursor.Close(ctx)
	counter := 0

	for cursor.Next(ctx) {
		dao := AddressRecord{}
		err = cursor.Decode(&dao)
		if err != nil {
			return err
		}

		if !dao.UUID.IsZero() {
			counter++
		}
	}

	if counter > 0 {
		// this means that we had already created indexes before now
		return nil
	}

	// so we create indexes
	keys := bson.D{
		primitive.E{Key: "street_1", Value: "text"},
		primitive.E{Key: "street_2", Value: "text"},
		primitive.E{Key: "unit_value", Value: "text"},
		primitive.E{Key: "unit_type", Value: "text"},
		primitive.E{Key: "city", Value: "text"},
		primitive.E{Key: "state", Value: "text"},
		primitive.E{Key: "zip", Value: "text"},
		primitive.E{Key: "zip_local", Value: "text"},
		primitive.E{Key: "status", Value: "text"},
		primitive.E{Key: "location", Value: "text"},
		primitive.E{Key: "delivery_point_barcode", Value: "text"},
		primitive.E{Key: "default_city", Value: "text"},
		primitive.E{Key: "record_type", Value: "text"},
	}
	_, err = collection.CreateIndex(ctx, keys, &options.IndexOptions{})
	if err != nil {
		return err
	}
	return nil
}

func (dao *MongoDAO) findOneAddress(ctx context.Context, filter bson.M) (*AddressRecord, error) {
	log := logging.GetTracedLogEntry(ctx)
	collection := dao.Collection(AddressCollection)
	dbCtx, cancel := dao.createContext(ctx)
	defer cancel()

	queryResults, err := collection.FindOne(dbCtx, filter)
	if err != nil {
		if err.Error() == mongoerrors.MongoDBNoDocumentFoundError.Error() {
			log.WithField("Filter", asJSON(filter)).Info("ADDRESS-MONGO-ERROR: findOneAddress no results found")
			return nil, nil
		}
		log.WithFields(generateLogFields(err, filter)).
			Error(fmt.Sprintf("ADDRESS-MONGO-ERROR: findOneAddress returned an error: %s", addressErrors.DaoQueryError.Error()))
		return nil, err
	}

	address := &AddressRecord{}
	err = queryResults.Decode(address)
	if err != nil {
		log.WithFields(generateLogFields(err, filter)).
			Error(fmt.Sprintf("ADDRESS-MONGO-ERROR: findOneAddress failed to decode fetched mongo response: %s", err))
		return nil, err
	}
	return address.Format(), nil
}

func (dao *MongoDAO) findAddresses(ctx context.Context, filter bson.M) (addresses []*AddressRecord, err error) {
	log := logging.GetTracedLogEntry(ctx)
	collection := dao.Collection(AddressCollection)

	dbCtx, cancel := dao.createContext(ctx)
	defer cancel() // cancel is called after the curse.Close fxn
	opt := options.Find()
	opt.SetLimit(int64(100))
	cursor, err := collection.Find(dbCtx, filter, opt)
	defer cursor.Close(ctx)
	if err != nil {
		log.WithError(err).WithField("Filter", asJSON(filter)).Error("ADDRESS-MONGO-ERROR: findAddresses returned an error")
		return nil, err
	}

	addresses = make([]*AddressRecord, 0)
	for cursor.Next(ctx) {
		address := &AddressRecord{}
		err := cursor.Decode(address)
		if err != nil {
			log.WithError(err).WithField("Filter", asJSON(filter)).Error("ADDRESS-MONGO-ERROR: findAddresses failed to decode mongo returned results")
			return nil, err
		}
		addresses = append(addresses, address)
	}
	return addresses, nil
}

func (dao *MongoDAO) findZipcodes(ctx context.Context, filter bson.M) (zipcodes []*ZipcodeRecord, err error) {
	log := logging.GetTracedLogEntry(ctx)
	collection := dao.Collection(ZipcodeCollection)

	dbCtx, cancel := dao.createContext(ctx)
	defer cancel() // cancel is called after the curse.Close fxn
	opt := options.Find()
	cursor, err := collection.Find(dbCtx, filter, opt)
	defer cursor.Close(ctx)
	if err != nil {
		log.WithError(err).WithField("Filter", asJSON(filter)).Error("ADDRESS-MONGO-ERROR: findZipcodes returned an error")
		return nil, err
	}

	zipcodes = make([]*ZipcodeRecord, 0)
	for cursor.Next(ctx) {
		zipcode := &ZipcodeRecord{}
		err = cursor.Decode(zipcode)
		if err != nil {
			log.
				WithField("Filter", asJSON(filter)).
				WithContext(ctx).
				WithError(err).
				Warn("ADDRESS-MONGO-ERROR: findAddresses failed to decode mongo returned results")
			log.WithError(err).WithField("Filter", asJSON(filter)).Error("decode addresses failed")
			return nil, err
		}
		zipcodes = append(zipcodes, zipcode)
	}
	return zipcodes, nil
}

func generateLogFields(err error, filter bson.M) map[string]interface{} {
	return map[string]interface{}{
		"Error":  err.Error(),
		"Filter": asJSON(filter),
	}
}

func asJSON(data interface{}) string {
	jsonString, _ := json.Marshal(data)
	return string(jsonString)
}

func (dao *MongoDAO) createContext(ctx context.Context) (context.Context, context.CancelFunc) {
	if ctx == nil {
		ctx = context.Background()
	}

	return context.WithTimeout(ctx, dbRequestTimeout)
}
