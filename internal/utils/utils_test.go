package utils

import (
	"testing"

	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/types"
	"gotest.tools/assert"
)

func TestAddressParsing(t *testing.T) {

	cases := []struct {
		name    string
		address string
		want    *types.Street1Components
	}{
		{
			name:    "all components parsed 1",
			address: "123, E 123th ST",
			want:    &types.Street1Components{StreetName: "123th ST", StreetDirection: "E", StreetNumber: "123"},
		},
		{
			name:    "all components parsed 2",
			address: "123, NE 123th STREET",
			want:    &types.Street1Components{StreetName: "123th STREET", StreetDirection: "NE", StreetNumber: "123"},
		},
		{
			name:    "all components parsed 3",
			address: "980 E 7th Ave",
			want:    &types.Street1Components{StreetName: "7th Ave", StreetDirection: "E", StreetNumber: "980"},
		},
		{
			name:    "all components parsed 4",
			address: "980 E 7th AVENUE",
			want:    &types.Street1Components{StreetName: "7th AVENUE", StreetDirection: "E", StreetNumber: "980"},
		},
		{
			name:    "all components without street number",
			address: "E 7th AVENUE",
			want:    &types.Street1Components{StreetName: "7th AVENUE", StreetDirection: "E", StreetNumber: ""},
		},
		{
			name:    "all components without street direction parsed",
			address: "4017 FORREST CREEK CT",
			want:    &types.Street1Components{StreetName: "FORREST CREEK CT", StreetDirection: "", StreetNumber: "4017"},
		},
		{
			name:    "empty string",
			address: "",
			want:    &types.Street1Components{StreetName: "", StreetDirection: "", StreetNumber: ""},
		},
		{
			name:    "hyphenated street address (Hawaii, Southern CA)",
			address: "112-10 BRONX RD",
			want:    &types.Street1Components{StreetName: "BRONX RD", StreetDirection: "", StreetNumber: "112-10"},
		},
		{
			name:    "grid style street address (Salt Lake City, Utah)",
			address: "842 E 1700 S",
			want:    &types.Street1Components{StreetName: "1700 S", StreetDirection: "E", StreetNumber: "842"},
		},
		{
			name:    "alphanumeric street address (Wisconsin, Northern Illinois)",
			address: "N6W23001 BLUEMOUND RD",
			want:    &types.Street1Components{StreetName: "BLUEMOUND RD", StreetDirection: "", StreetNumber: "N6W23001"},
		},
		{
			name:    "lower case alphanumeric street address (Wisconsin, Northern Illinois)",
			address: "w136n8068 river park dr",
			want:    &types.Street1Components{StreetName: "river park dr", StreetDirection: "", StreetNumber: "w136n8068"},
		},
		{
			name:    "non grid alphanumeric address",
			address: "4029A NIGHTINGALE LN",
			want:    &types.Street1Components{StreetName: "NIGHTINGALE LN", StreetDirection: "", StreetNumber: "4029A"},
		},
		{
			name:    "fractional street address (n/a)",
			address: "123 1/2 MAIN ST",
			want:    &types.Street1Components{StreetName: "MAIN ST", StreetDirection: "", StreetNumber: "123 1/2"},
		},
		{
			name:    "single numeric should return numeric",
			address: "111",
			want:    &types.Street1Components{StreetName: "", StreetDirection: "", StreetNumber: "111"},
		},
		{
			name:    "single numeric should return numeric",
			address: "7",
			want:    &types.Street1Components{StreetName: "", StreetDirection: "", StreetNumber: "7"},
		},
		{
			name:    "single string should return string2",
			address: "ABC",
			want:    &types.Street1Components{StreetName: "ABC", StreetDirection: "", StreetNumber: ""},
		},
		{
			name:    "address with space in front should return properly",
			address: " 111 main st",
			want:    &types.Street1Components{StreetName: "main st", StreetDirection: "", StreetNumber: "111"},
		},
		{
			name:    "address with space at end should return properly",
			address: "111 main st ",
			want:    &types.Street1Components{StreetName: "main st", StreetDirection: "", StreetNumber: "111"},
		},
		{
			name:    "address with letter in street number",
			address: "4029A, NIGHTINGALE LN",
			want: &types.Street1Components{
				StreetName:      "NIGHTINGALE LN",
				StreetDirection: "",
				StreetNumber:    "4029A",
			},
		},
		{
			name:    "P O box",
			address: "P O Box 293",
			want: &types.Street1Components{
				StreetName:      "P O Box 293",
				StreetDirection: "",
				StreetNumber:    "",
			},
		},
		{
			name:    "P.o. box",
			address: "P.o. Box 293",
			want: &types.Street1Components{
				StreetName:      "P.o. Box 293",
				StreetDirection: "",
				StreetNumber:    "",
			},
		},
		{
			name:    "PO box",
			address: "PO Box 293",
			want: &types.Street1Components{
				StreetName:      "PO Box 293",
				StreetDirection: "",
				StreetNumber:    "",
			},
		},
		{
			name:    "P.O box",
			address: "P.O Box 293",
			want: &types.Street1Components{
				StreetName:      "P.O Box 293",
				StreetDirection: "",
				StreetNumber:    "",
			},
		},
		{
			name:    "po box",
			address: "po box 293",
			want: &types.Street1Components{
				StreetName:      "po box 293",
				StreetDirection: "",
				StreetNumber:    "",
			},
		},
		{
			name:    "normal address",
			address: "123 1/2 W Elm St",
			want: &types.Street1Components{
				StreetName:      "Elm St",
				StreetDirection: "W",
				StreetNumber:    "123 1/2",
			},
		},
		{
			name:    "similar address starting with fraction",
			address: "1/2 W Elm St",
			want: &types.Street1Components{
				StreetName:      "Elm St",
				StreetDirection: "W",
				StreetNumber:    "1/2",
			},
		},
		{
			name:    "address starting with fraction",
			address: "1/2 Andrews Ave",
			want: &types.Street1Components{
				StreetName:      "Andrews Ave",
				StreetDirection: "",
				StreetNumber:    "1/2",
			},
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			des := ParseStreetAddress(c.address)
			assert.Equal(t, c.want.StreetDirection, des.StreetDirection)
			assert.Equal(t, c.want.StreetName, des.StreetName)
			assert.Equal(t, c.want.StreetNumber, des.StreetNumber)
		})
	}
}

func TestTrimAndUpper(t *testing.T) {
	tests := []struct {
		name  string
		input string
		want  string
	}{
		{
			name:  "all lowercase",
			input: "all lowercase string",
			want:  "ALL LOWERCASE STRING",
		},
		{
			name:  "all uppercase",
			input: "ALL UPPERCASE STRING",
			want:  "ALL UPPERCASE STRING",
		},
		{
			name:  "mixed cases",
			input: "mixed CASES",
			want:  "MIXED CASES",
		},
		{
			name:  "mixed cases with whitespaces",
			input: "   mixed  CASES \n\t\t\t\t\n   ",
			want:  "MIXED CASES",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := TrimAndUpper(tt.input); got != tt.want {
				t.Errorf("TrimAndUpper() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsBlank(t *testing.T) {
	tests := []struct {
		name  string
		input string
		want  bool
	}{
		{
			name:  "blank - empty string",
			input: "",
			want:  true,
		},
		{
			name:  "blank - with white spaces",
			input: "   \n\n\n\n\t\t\t   \t\t\t \n",
			want:  true,
		},
		{
			name:  "not blank",
			input: "definitely not a blank string",
			want:  false,
		},
		{
			name:  "not blank with white space",
			input: "   \n\n\n\n\t\t\t   \t\t\t \n definitely not a blank string",
			want:  false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsBlank(tt.input); got != tt.want {
				t.Errorf("IsBlank() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestVerifyIsDigit(t *testing.T) {
	tests := []struct {
		name  string
		input string
		want  bool
	}{
		{
			name:  "is digit 1",
			input: "0",
			want:  true,
		},
		{
			name:  "is digit 2",
			input: "235345345",
			want:  true,
		},
		{
			name:  "is digit 3",
			input: "0235345345",
			want:  true,
		},

		{
			name:  "not a digit 1",
			input: "0235 345345",
			want:  false,
		},

		{
			name:  "not a digit 2",
			input: "1234a567",
			want:  false,
		},
		{
			name:  "not a digit 3",
			input: "golang",
			want:  false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := VerifyIsDigit(tt.input); got != tt.want {
				t.Errorf("VerifyIsDigit() = %v, want %v", got, tt.want)
			}
		})
	}
}
