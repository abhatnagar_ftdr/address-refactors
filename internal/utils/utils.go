package utils

import (
	"context"
	"fmt"
	addressErrors "golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors"
	pb "golang.frontdoorhome.com/software/protos/go/addresspb"
	"regexp"
	"strings"
	"time"

	"github.com/google/uuid"
	"go.ftdr.com/go-utils/common/constants"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/types"
)

// TrimWhitespace trims leading and trailing whitespace from strings, replaces multiple whitespaces with a single space
func TrimWhitespace(input string) string {
	return regexp.MustCompile(`\s+`).ReplaceAllString(strings.TrimSpace(input), " ")
}

// TrimAndUpper trims and upper-cases the supplied string
func TrimAndUpper(input string) string {
	return strings.ToUpper(TrimWhitespace(input))
}

// IsBlank helper function to check if a given string is empty or all whitespace
func IsBlank(input string) bool {
	return TrimWhitespace(input) == ""
}

// IsNotBlank helper function to check if a given string is not empty or all whitespace
func IsNotBlank(input string) bool {
	return !IsBlank(input)
}

// GenerateTraceContext will create a context with a Trace-Id
func GenerateTraceContext() (context.Context, error) {
	id, err := uuid.NewRandom()
	if err != nil {
		return nil, err
	}

	return context.WithValue(context.Background(), constants.TraceIDContextKey, id.String()), nil
}

// SetSpecialTraceID decorates TraceID with a unique identifier
func SetSpecialTraceID(ctx context.Context, prefix string, appendTimestamp bool) context.Context {
	traceID := ctx.Value(literals.TraceIDKey(constants.TraceIDContextKey))
	if traceID == nil {
		traceID = ""
	}
	var suffix string
	if appendTimestamp {
		suffix = fmt.Sprintf("-%d", time.Now().Unix())
	}
	newTraceID := strings.ToLower(fmt.Sprintf("%s%s%s", prefix, traceID, suffix))
	return context.WithValue(ctx, constants.TraceIDContextKey, newTraceID)
}

// VerifyIsDigit is used to check the structure of an in coming request parameter
// to make sure that every single characters therein is digits ad not some weird
// unexpected characters
func VerifyIsDigit(id string) bool {
	r := regexp.MustCompile(`[^0-9]+`)

	if id == "" || len(id) == 0 {
		return false
	}

	if r.Match([]byte(id)) {
		return false
	}

	return true
}

// ParseStreetAddress parses out a given street address into its component parts
func ParseStreetAddress(street1 string) *types.Street1Components {
	var sc types.Street1Components

	if street1 == "" {
		return &sc
	}

	street1 = strings.TrimSpace(street1)

	// street number
	streetNumberRegexp := regexp.MustCompile(`(?i)^[A-Z]*\d+[A-Z-/]*\d*(\s+\d+/\d+)?\b`)
	sc.StreetNumber = streetNumberRegexp.FindString(street1)

	// street direction
	streetDirectionRegexp := regexp.MustCompile(`(?i)\b(N|E|S|W|NE|NW|SE|SW)\b`)
	sc.StreetDirection = streetDirectionRegexp.FindString(street1)

	// with the street number and direction captured, capturing the
	// street name can be done by replacing both the street number and direction
	// in the original street1
	name := strings.Replace(street1, sc.StreetNumber, "", 1)

	if sc.StreetDirection != "" {
		name = strings.Replace(name, sc.StreetDirection, "", 1)
	}

	sc.StreetName = strings.TrimSpace(strings.Trim(name, ",."))

	return &sc
}

// ParseExternalSourceName
func ParseExternalSourceName(name string) (pb.ExternalSource, *addressErrors.ServerError) {
	name = strings.ToUpper(name)
	switch name {
	case "UNKNOWN_PROPERTY":
		return pb.ExternalSource_UNKNOWN_SOURCE, nil
	case "PROPERTY":
		return pb.ExternalSource_UNKNOWN_SOURCE, nil
	case "COMMERCIAL_PROPERTY":
		return pb.ExternalSource_COMMERCIAL_PROPERTY, nil
	default:
		return pb.ExternalSource_UNKNOWN_SOURCE, addressErrors.UnknownExternalSourceNameError.NewFormatted(name)
	}
}
