package literals

// literals
const (
	AddressPathPrefix                = "/address"
	AddressPathPrefixV1              = "/address/v1"
	ProtobufContentType              = "application/x-protobuf"
	JSONContentType                  = "application/json"
	HeaderAuthorization              = "Authorization"
	Bearer                           = "Bearer "
	URLParamProtoBody                = "proto_body"
	ComponentTypeAddress             = "AddressAPI"
	ComponentTypeAddressRepository   = "AddressRepository"
	ComponentTypeRedisDAO            = "RedisDAO"
	ComponentTypeAddressExternalCall = "AddressExternalCall"
	ComponentPropertyStarDB          = "PropertyStarDB"
	ComponentTypeSwitch              = "SwitchAPI"
	ComponentTypeAddressService      = "AddressService"
	ComponentTypeStar                = "ComponentTypeStar"
	CountryCodeUSA                   = "USA"
)

// log literals
const (
	LLErrorMessage  = "errorMessage"
	LLInternalError = "InternalError"
)

// Instrumentation Context Literal
const (
	ContextInternalError    = "internalError"
	ContextFailedQuery      = "FailedQuery"
	ContextErrorMessage     = "errorMessage"
	ContextMongoQueryFilter = "queryFilter"
	ContextNoDocumentsFound = "noDocumentsFound"
	ContextStatusCode       = "statusCode"
)

// API name and URI literals
const (
	HealthcheckAPIName = "healthcheck"
	HealthcheckURI     = "/healthcheck"

	GetAddressAPIName = "GetAddress"
	GetAddressURI     = "/address"

	GetAddressByUUIDOrLegacyIDAPIName = "GetAddressByUUIDOrLegacyID"
	GetAddressByUUIDOrLegacyIDURI     = "/address/{uuid_or_legacy_id}"

	GetAddressByLegacyIDAPIName = "GetAddressByLegacyID"
	GetAddressByLegacyIDURI     = "/address/by_legacy_id/{legacy_id}"

	DeleteAllLegacyIDAPIName = "DeleteAllLegacyID"
	DeleteAllLegacyIDURI     = "/address/by_legacy_id/{legacy_id}"

	QueryTypeAheadAPIName = "QueryTypeAhead"
	QueryTypeAheadURI     = "/type_ahead"

	GetTypeAheadAPIName = "GetTypeAhead"
	GetTypeAheadURI     = "/address_type_ahead"

	PostUnverifiedAddressAPIName = "PostUnverifiedAddress"
	PostUnverifiedAddressURI     = "/unverified_address"

	PostLegacyIDAPIName = "PostLegacyID"
	PostLegacyIDURI     = "/{uuid}/legacy_ids/{legacy_id}"

	DeleteLegacyIDAPIName = "DeleteLegacyID"
	DeleteLegacyIDURI     = "/{uuid}/legacy_ids/{legacy_id}"

	GetZipCodeDetailAPIName = "GetZipcodeDetails"
	GetZipcodeDetailURI     = "/zipcode/{zip}"

	VerifyAddressAPIName = "VerifyAddress"
	VerifyAddressURI     = "/verify_address/{uuid}"

	LogCollectionStatsAPIName = "LogCollectionStats"
	LogCollectionStatsURI     = "/admin/log_filter_stats"

	LogFilterStatsAPIName = "LogFilterStats"
	LogFilterStatsURI     = "/admin/log_filter_stats"

	GetStatesAPIName = "GetStates"
	GetStatesURI     = "/states"

	ZipCodeCheckAPIName = "ZipCodeCheck"
	ZipCodeCheckURI     = "/zipcodecheck"

	StateZipCodesAPIName = "StateZipCodes"
	StateZipCodesURI     = "/states/zipcodes"
)

// repository literals
const (
	FindOneAddress                                = "FindOneAddress"
	FindAddresses                                 = "FindAddresses"
	QueryOneAddress                               = "QueryOneAddress"
	QueryAddressLookahead                         = "QueryAddressLookahead"
	QueryAddressByID                              = "QueryAddressByID"
	QueryAddressesByID                            = "QueryAddressesByID"
	QueryAddressByLegacyID                        = "QueryAddressByLegacyID"
	QueryAddressesByDPB                           = "QueryAddressesBDPB"
	QueryStateByZipCode                           = "QueryStateByZipCode"
	QueryZipCodesByState                          = "QueryZipCodesByState"
	QueryUnverifiedAddresses                      = "QueryUnverifiedAddresses"
	InsertAddress                                 = "InsertAddress"
	InsertLegacyID                                = "InsertLegacyID"
	DeleteLegacyID                                = "DeleteLegacyID"
	UpdateAddress                                 = "UpdateAddress"
	QueryCollectionsStats                         = "QueryCollectionsStats"
	QueryFilterStats                              = "QueryFilterStats"
	ArchiveAddress                                = "ArchiveAddress"
	CommitTransaction                             = "CommitTransaction"
	AbortTransaction                              = "AbortTransaction"
	CreatePropertyWithAddress                     = "CreatePropertyWithAddress"
	UpdatePropertyWithAddress                     = "UpdatePropertyWithAddress"
	CreateCommercialPropertyWithAddress           = "CreateCommercialPropertyWithAddress"
	UpdateCommercialPropertyWithAddress           = "UpdateCommercialPropertyWithAddress"
	FetchCityCode                                 = "FetchCityCode"
	FetchCountyCode                               = "FetchCountyCode"
	FetchZipcodeCountyCode                        = "FetchZipcodeCountyCode"
	FetchCityName                                 = "FetchCityName"
	FetchCountyName                               = "FetchCountyName"
	FetchCountryName                              = "FetchCountryName"
	FetchNextPropertyID                           = "FetchNextPropertyID"
	CreateCityCode                                = "CreateCityCode"
	CreateCityCodeZipCode                         = "CreateCityCodeZipCode"
	CreateCountyCode                              = "CreateCountyCode"
	CreateCountyZipCode                           = "CreateCountyZipCode"
	CreateZipCode                                 = "CreateZipCode"
	GetAddressByID                                = "GetAddressByID"
	DeleteAllLegacyID                             = "DeleteAllLegacyID"
	GetAddressRecord                              = "GetAddressRecord"
	GetAddressRecordByID                          = "GetAddressRecordByID"
	GetAddressByLegacyID                          = "GetAddressByLegacyID"
	GetZipCodeDetails                             = "GetZipCodeDetails"
	LogCollectionStats                            = "LogCollectionStats"
	LogFilterStats                                = "LogFilterStats"
	PostLegacyID                                  = "PostLegacyID"
	InsertExternalSourceID                        = "InsertExternalSourceID"
	PostUnverifiedAddress                         = "PostUnverifiedAddress"
	QueryAddressID                                = "QueryAddressID"
	VerifyAddress                                 = "VerifyAddress"
	ZipCodeStateCheck                             = "ZipCodeStateCheck"
	StateZipCodes                                 = "StateZipCodes"
	GetStatesForZipcode                           = "getStatesForZipcode"
	UseAddressValidatorDefinedByFeatureFlag       = "UseAddressValidatorDefinedByFeatureFlag"
	UpsertAddressFromSmartyStreets                = "upsertAddressFromSmartyStreets"
	UpsertAddress                                 = "upsertAddress"
	UpsertEntryInExternalSource                   = "upsertEntryInExternalSource"
	InsertEntryInExternalSource                   = "insertEntryInExternalSource"
	GetAddressFromExternalSource                  = "getAddressFromExternalSource"
	ValidateAddress                               = "validateAddress"
	VerifyUniqueAddress                           = "verifyUniqueAddress"
	GetAddressByDeliveryPointBarcode              = "getAddressByDeliveryPointBarcode"
	SaveAddressToRedis                            = "saveAddressToRedis"
	SaveGetAddressQueryToRedis                    = "saveGetAddressQueryToRedis"
	GetAddressByLookupFromRedis                   = "getAddressByLookupFromRedis"
	GetAddressByDPBAndUnitFromRedis               = "getAddressByDPBAndUnitFromRedis"
	DeleteAddressFromRedis                        = "deleteAddressFromRedis"
	GetRepositorySuggestions                      = "getRepositorySuggestions"
	SaveAddressToMongo                            = "saveAddressToMongo"
	UpdateAddressInMongo                          = "updateAddressInMongo"
	GetAddressesByDeliveryPointBarcodeFromMongoDB = "getAddressesByDeliveryPointBarcodeFromMongoDB"
	GetAddressFromAddressValidator                = "getAddressFromAddressValidator"
	ExecuteAddressLookup                          = "executeAddressLookup"
	StartTransaction                              = "StartTransaction"
	StartSession                                  = "StartSession"
	GetProperty                                   = "GetProperty"
	GetCommercialProperty                         = "GetCommercialProperty"
	GetAddress                                    = "GetAddress"
	GetAddressFromCommercialProperty              = "GetAddressFromCommercialProperty"
	GetCountyCode                                 = "GetCountyCode"
	RollbackTransaction                           = "RollbackTransaction"
	GenerateUUID                                  = "GenerateUUID"
	UUIDRawToBson                                 = "UUIDRawToBson"
	BsonUUIDToRaw                                 = "BsonUUIDToRaw"
	DeleteExternalSourceID                        = "DeleteExernalSourceID"
	DeleteAllExternalSourceID                     = "DeleteAllExternalSourceID"
	QueryAddressByExternalSourceID                = "QueryAddressByExternalSourceID"
	FindZipcodes                                  = "findZipcodes"
)

// Redis APIs
const (
	RedisConnectionDAO = "RedisConnectionDAO"
	RedisGetDAO        = "RedisGetDAO"
	RedisSetEXDAO      = "RedisSetEXDAO"
	RedisExistDAO      = "RedisExistDAO"
	RedisDeleteDAO     = "RedisDeleteDAO"
	RedisGetKeysDAO    = "RedisGetKeysDAO"
)

// Property columns in star db
const (
	StreetNumberColumn       = "STREET_NUMBER"
	StreetNameColumn         = "STREET_NAME"
	StreetDirectionColumn    = "STREET_DIRECTION"
	PropertyIDColumn         = "PROPERTY_ID"
	TypeCodeColumn           = "TYPE_CODE"
	TypePrefixColumn         = "TYPE_PREFIX"
	DwellingTypeCodeColumn   = "DWELLING_TYPE_CODE"
	DwellingTypePrefixColumn = "DWELLING_TYPE_PREFIX"
	UnitNumberColumn         = "UNIT_NUMBER"
	CityCodeColumn           = "CITY_CODE"
	StateColumn              = "STATE"
	CountyCodeColumn         = "COUNTY_CODE"
	ZipCodeColumn            = "ZIP_CODE"
	UnitTypeColumn           = "UNIT_TYPE"
	UnitPrefixColumn         = "UNIT_PREFIX"
	CountryCodeColumn        = "COUNTRY_CODE"
	ZipPlus4Column           = "ZIP_PLUS_4"
	LastModifiedColumn       = "LAST_MODIFIED"
	LastModifiedByColumn     = "LAST_MODIFIED_BY"
)

// ComponentID used for instrumentation of external calls
const (
	SmartyStreets                          = "SmartyStreets"
	Experian                               = "Experian"
	Split                                  = "Split"
	CheckAddressStarIntegrationFeatureFlag = "CheckAddressStarIntegrationFeatureFlag"
	CheckZipcodeDataSourceFeatureFlag      = "CheckZipcodeDataSourceFeatureFlag"
	SmartyStreetsAPI                       = "smartyStreetsAPI"
)

const (
	PropertyTableSequence           = "PROPERTY_ID_SEQ"
	CommercialPropertyTableSequence = "COMMERCIAL_PROPERTY_ID_SEQ"
	CityTableSequence               = "CITY_CODE_SEQ"
	CountyTableSequence             = "COUNTY_CODE_SEQ"
)

// TraceIDKey is used for storinng trace id in a context, this was added
// to prevent lint error related to storing trace id in context
type TraceIDKey string

const (
	AddressValidatorFeatureFlag       = "ADDRESS_VALIDATOR"
	AddressStarIntegrationFeatureFlag = "ADDRESS_STAR_INTEGRATION"
	AddressUseMongoZipcodeData        = "ADDRESS_USE_MONGO_ZIPCODE_DATA"
	SwitchType                        = "Optimizely"
)

const EnvPrefix = "ADDRESS"
