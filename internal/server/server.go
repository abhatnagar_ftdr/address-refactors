package server

import (
	"context"
	"fmt"
	"go.ftdr.com/go-utils/instrumentation/v3"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/external_source/star"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/mongo_dal"
	"net/http"

	_ "github.com/godror/godror" //required for initializing oracle db
	handlersGorilla "github.com/gorilla/handlers"
	_ "github.com/sijms/go-ora/v2"
	"github.com/sirupsen/logrus"
	"go.ftdr.com/go-utils/common/logging"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/config"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/handlers"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/services"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/utils"
)

// Server holds server configs, routers, and data stores
type Server struct {
	config *config.WebServerConfig
	router *Router
}

// newServer creates a new server
func newServer(config *config.WebServerConfig, router *Router) (server *Server) {
	return &Server{
		config: config,
		router: router,
	}
}

// initializeHandler ...
func (s *Server) initializeHandler(ctx context.Context, service *services.AddressServicer) *handlers.Handler {
	logging.GetTracedLogEntry(ctx).Info()
	return handlers.NewHandler(service)
}

// initializeRouter ...
func (s *Server) initializeRouter(ctx context.Context, ciConfig *config.CIConfig, handler *handlers.Handler) {
	logging.GetTracedLogEntry(ctx).Info()
	s.router = NewRouter(handler)
	s.router.InitializeRoutes(ciConfig)
	s.router.InitializeMiddleware()
}

// initializeLogging initialize logging and capitalize error key
func (s *Server) initializeLogging(ctx context.Context) {
	logrus.ErrorKey = "Error"
	if loggingErr := logging.Initialize(s.config.Log); loggingErr != nil {
		logging.GetTracedLogEntry(ctx).WithError(loggingErr).Fatal("failed to initialize logger")
	}
}

// initializeService ...
func (s *Server) initializeService(
	ctx context.Context,
	mdb *mongo_dal.MongoDAO,
	odb star.IStarEngine) (service *services.AddressServicer) {
	logging.GetTracedLogEntry(ctx).Info()

	addressService := services.NewAddressServicer(s.config.Service, mdb, odb)
	addressService.Initialize(ctx)

	return addressService
}

// run starts the server
func (s *Server) run(ctx context.Context) (err error) {
	return s.startHTTPServer(ctx)
}

func (s *Server) startHTTPServer(ctx context.Context) error {

	logging.
		GetTracedLogEntry(ctx).
		WithField("Port", s.config.Port).
		WithField("CORSEnabled", s.config.CORSEnabled).
		Info("starting application server...")

	if !s.config.CORSEnabled {
		return http.ListenAndServe(fmt.Sprintf(":%v", s.config.Port), s.router)
	}

	corsOPT := []handlersGorilla.CORSOption{}
	corsOPT = append(corsOPT, handlersGorilla.AllowedMethods([]string{"GET", "POST", "PUT", "UPDATE", "OPTIONS", "DELETE"}))
	corsOPT = append(corsOPT, handlersGorilla.AllowCredentials())
	corsOPT = append(corsOPT, handlersGorilla.AllowedHeaders(
		[]string{"X-Requested-With", "Authorization", "Content-Type", "Accept", "Tenant",
			"Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Access-Control-Allow-Credentials",
		}))
	corsOPT = append(corsOPT, handlersGorilla.AllowedOrigins([]string{"*"}))

	return http.ListenAndServe(fmt.Sprintf(":%v", s.config.Port), handlersGorilla.CORS(corsOPT...)(s.router))
}

// RunServer initializes the server and starts it up
func RunServer() error {

	ctx := utils.SetSpecialTraceID(context.Background(), "start-up", true)
	serverConfig, ciConfig, err := config.FromEnv()
	if err != nil {
		return err
	}

	appServer := newServer(serverConfig, nil)

	appServer.initializeLogging(ctx)

	// initialize the oracle db
	odb, err := star.NewStarEngine(ctx, &serverConfig.Service.Oracle)
	if err != nil {
		return err
	}

	//connect to mongo database
	mdao, err := mongo_dal.NewMongoDAO(ctx, serverConfig.Service.Mongo)
	if err != nil {
		return err
	}

	// initialize instrumentation
	_ = instrumentation.Initialize(serverConfig.Instrumentation)
	defer instrumentation.Shutdown()

	service := appServer.initializeService(ctx, mdao, odb)

	handler := appServer.initializeHandler(ctx, service)

	appServer.initializeRouter(ctx, ciConfig, handler)

	if err = appServer.run(ctx); err != nil {
		return err
	}

	return nil

}
