package server

import (
	"net/http"

	//"go.ftdr.com/go-utils/common/logging"
	"github.com/gorilla/mux"
	commonMiddleware "go.ftdr.com/go-utils/common/middleware"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/config"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/handlers"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/middleware"
)

var (
	routeAcceptedContents = map[string]*middleware.RouteContent{
		// Public routes will expose JSON
		literals.QueryTypeAheadAPIName:             {DefaultContent: literals.ProtobufContentType, AcceptsJSON: true},
		literals.GetAddressAPIName:                 {DefaultContent: literals.ProtobufContentType, AcceptsJSON: true},
		literals.GetAddressByUUIDOrLegacyIDAPIName: {DefaultContent: literals.ProtobufContentType, AcceptsJSON: true},
		literals.GetZipCodeDetailAPIName:           {DefaultContent: literals.ProtobufContentType, AcceptsJSON: true},
		literals.PostUnverifiedAddressAPIName:      {DefaultContent: literals.ProtobufContentType, AcceptsJSON: true},
		literals.VerifyAddressAPIName:              {DefaultContent: literals.ProtobufContentType, AcceptsJSON: true},
		literals.GetStatesAPIName:                  {DefaultContent: literals.ProtobufContentType, AcceptsJSON: true},
		literals.ZipCodeCheckAPIName:               {DefaultContent: literals.ProtobufContentType, AcceptsJSON: true},
		literals.StateZipCodesAPIName:              {DefaultContent: literals.ProtobufContentType, AcceptsJSON: true},

		// Private routes that will only accept protobuf (for now)
		literals.GetTypeAheadAPIName:         {DefaultContent: literals.ProtobufContentType, AcceptsJSON: false},
		literals.GetAddressByLegacyIDAPIName: {DefaultContent: literals.ProtobufContentType, AcceptsJSON: false},
		literals.HealthcheckAPIName:          {DefaultContent: literals.ProtobufContentType, AcceptsJSON: false},
		literals.PostLegacyIDAPIName:         {DefaultContent: literals.ProtobufContentType, AcceptsJSON: false},
		literals.LogFilterStatsAPIName:       {DefaultContent: literals.ProtobufContentType, AcceptsJSON: false},
		literals.DeleteLegacyIDAPIName:       {DefaultContent: literals.ProtobufContentType, AcceptsJSON: false},
		literals.DeleteAllLegacyIDAPIName:    {DefaultContent: literals.ProtobufContentType, AcceptsJSON: false},
	}
)

// Router is the core router struct
type Router struct {
	*mux.Router
	handler       handlers.RequestHandler
	RouterDetails map[string]URLDetails
}

// URLDetails  Provides detailed information about API endpoint
type URLDetails struct {
	Name    string
	Path    string
	Methods []string
}

// NewRouter creates a new router
func NewRouter(handler handlers.RequestHandler) *Router {
	return &Router{
		mux.NewRouter(),
		handler,
		map[string]URLDetails{},
	}
}
func (r *Router) addRoutes(subRoutes []*mux.Router, d URLDetails, f func(http.ResponseWriter,
	*http.Request)) {
	route := r.Router
	if subRoutes != nil {
		for _, rout := range subRoutes {
			rout.HandleFunc(d.Path, f).
				Methods(d.Methods...).
				Name(d.Name)
			r.RouterDetails[d.Name] = d
		}
		return
	}

	route.HandleFunc(d.Path, f).
		Methods(d.Methods...).
		Name(d.Name)
	r.RouterDetails[d.Name] = d
}

// InitializeRoutes does exactly what you think it does
func (r *Router) InitializeRoutes(ciConfig *config.CIConfig) {
	subRouter := r.PathPrefix(literals.AddressPathPrefix).Subrouter()
	subRouterV1 := r.PathPrefix(literals.AddressPathPrefixV1).Subrouter()

	r.addRoutes([]*mux.Router{subRouter},
		URLDetails{
			Name:    literals.HealthcheckAPIName,
			Path:    literals.HealthcheckURI,
			Methods: []string{http.MethodGet},
		}, r.handler.GetHealthCheckHandler(ciConfig))

	r.addRoutes([]*mux.Router{subRouter, subRouterV1}, URLDetails{
		Name:    literals.GetAddressAPIName,
		Path:    literals.GetAddressURI,
		Methods: []string{http.MethodGet, http.MethodPut},
	}, r.handler.GetAddressHandler)

	r.addRoutes([]*mux.Router{subRouter, subRouterV1}, URLDetails{
		Name:    literals.QueryTypeAheadAPIName,
		Path:    literals.QueryTypeAheadURI,
		Methods: []string{http.MethodGet},
	}, r.handler.QueryTypeAheadHandler)

	r.addRoutes([]*mux.Router{subRouter, subRouterV1}, URLDetails{
		Name:    literals.GetTypeAheadAPIName,
		Path:    literals.GetTypeAheadURI,
		Methods: []string{http.MethodGet},
	}, r.handler.GetTypeAheadHandler)

	r.addRoutes([]*mux.Router{subRouter, subRouterV1}, URLDetails{
		Name:    literals.PostUnverifiedAddressAPIName,
		Path:    literals.PostUnverifiedAddressURI,
		Methods: []string{http.MethodPost},
	}, r.handler.PostUnverifiedAddressHandler)

	r.addRoutes([]*mux.Router{subRouter, subRouterV1}, URLDetails{
		Name:    literals.GetAddressByUUIDOrLegacyIDAPIName,
		Path:    literals.GetAddressByUUIDOrLegacyIDURI,
		Methods: []string{http.MethodGet},
	}, r.handler.GetAddressByIDHandler)

	r.addRoutes([]*mux.Router{subRouter, subRouterV1}, URLDetails{
		Name:    literals.GetAddressByLegacyIDAPIName,
		Path:    literals.GetAddressByLegacyIDURI,
		Methods: []string{http.MethodGet},
	}, r.handler.GetAddressByLegacyIDHandler)

	r.addRoutes([]*mux.Router{subRouter, subRouterV1}, URLDetails{
		Name:    literals.DeleteAllLegacyIDAPIName,
		Path:    literals.DeleteAllLegacyIDURI,
		Methods: []string{http.MethodDelete},
	}, r.handler.DeleteAllLegacyIDHandler)

	r.addRoutes([]*mux.Router{subRouter, subRouterV1}, URLDetails{
		Name:    literals.PostLegacyIDAPIName,
		Path:    literals.PostLegacyIDURI,
		Methods: []string{http.MethodPost},
	}, r.handler.PostLegacyIDHandler)

	r.addRoutes([]*mux.Router{subRouter, subRouterV1}, URLDetails{
		Name:    literals.DeleteLegacyIDAPIName,
		Path:    literals.DeleteLegacyIDURI,
		Methods: []string{http.MethodDelete},
	}, r.handler.DeleteLegacyIDHandler)

	r.addRoutes([]*mux.Router{subRouter, subRouterV1}, URLDetails{
		Name:    literals.GetZipCodeDetailAPIName,
		Path:    literals.GetZipcodeDetailURI,
		Methods: []string{http.MethodGet},
	}, r.handler.GetZipCodeDetailsHandler)

	r.addRoutes([]*mux.Router{subRouter, subRouterV1}, URLDetails{
		Name:    literals.VerifyAddressAPIName,
		Path:    literals.VerifyAddressURI,
		Methods: []string{http.MethodGet},
	}, r.handler.VerifyAddressIDHandler)

	r.addRoutes([]*mux.Router{subRouter, subRouterV1}, URLDetails{
		Name:    literals.LogCollectionStatsAPIName,
		Path:    literals.LogCollectionStatsURI,
		Methods: []string{http.MethodGet},
	}, r.handler.LogCollectionStatsHandler)

	r.addRoutes([]*mux.Router{subRouter, subRouterV1}, URLDetails{
		Name:    literals.LogCollectionStatsAPIName,
		Path:    literals.LogFilterStatsURI,
		Methods: []string{http.MethodGet},
	}, r.handler.LogFilterStatsHandler)

	r.addRoutes([]*mux.Router{subRouter, subRouterV1}, URLDetails{
		Name:    literals.GetStatesAPIName,
		Path:    literals.GetStatesURI,
		Methods: []string{http.MethodGet},
	}, r.handler.GetStatesHandler)

	r.addRoutes([]*mux.Router{subRouter, subRouterV1}, URLDetails{
		Name:    literals.ZipCodeCheckAPIName,
		Path:    literals.ZipCodeCheckURI,
		Methods: []string{http.MethodGet},
	}, r.handler.ZipCodeCheckHandler)

	r.addRoutes([]*mux.Router{subRouter, subRouterV1}, URLDetails{
		Name:    literals.StateZipCodesAPIName,
		Path:    literals.StateZipCodesURI,
		Methods: []string{http.MethodGet},
	}, r.handler.StateZipCodesHandler)
}

// InitializeMiddleware adds necessary middleware to the router
func (r *Router) InitializeMiddleware() {
	// Common middleware
	r.Use(commonMiddleware.Recovery)
	r.Use(commonMiddleware.TraceMiddleware)
	r.Use(instrumentation.Middleware)
	r.Use(commonMiddleware.LoggingMiddleware)

	// Address middleware
	r.Use(middleware.ContentTypeUnifierMiddleware)

	vmw := middleware.ValidatorMiddleWare{}
	vmw.SetAcceptedContent(routeAcceptedContents)
	r.Use(vmw.Middleware)

	r.Use(middleware.QueryDecoderWrapper)
}
