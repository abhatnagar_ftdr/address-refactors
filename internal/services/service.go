package services

import (
	"context"
	"go.ftdr.com/go-utils/mongo/v2"
	"go.ftdr.com/go-utils/xormwrapper/v2/config"
	"golang.frontdoorhome.com/software/address/internal/external_source/star"
	"golang.frontdoorhome.com/software/address/internal/mongo_dal"
	"regexp"

	"go.ftdr.com/go-utils/common/logging"
	"golang.frontdoorhome.com/software/address/internal/clients/addressvalidator"
	"golang.frontdoorhome.com/software/address/internal/clients/experian"
	"golang.frontdoorhome.com/software/address/internal/clients/featureflag"
	"golang.frontdoorhome.com/software/address/internal/clients/smartystreets"
	"golang.frontdoorhome.com/software/address/internal/repository"
)

var (
	deliveryPointBarcodeRegex = regexp.MustCompile(`^\d+$`) // 10(post box) or 12() digits
)

// AddressValidatorClientType is used to recognize which address validator is and should be used
type AddressValidatorClientType int

const (
	//Experian ...
	Experian AddressValidatorClientType = iota
	//SmartyStreets ...
	SmartyStreets
)

// String returns feature flag and human-readable value symbolising which address validator should be used
func (a AddressValidatorClientType) String() string {
	return ([]string{"experian", "smartystreets"})[a]
}

// ServiceConfig holds configs for the service
// must be exported to parse configs
type ServiceConfig struct {
	Mongo          *mongo.Config             `required:"true"`
	Redis          *repository.RedisConfig   `required:"true"`
	Smarty         *smartystreets.Config     `required:"true"`
	Experian       *experian.Config          `required:"true"`
	Switch         *featureflag.SwitchConfig `envconfig:"SWITCH" required:"true"`
	Oracle         config.OracleConfig       `required:"true"`
	LookaheadLimit int64                     `split_words:"true" default:"5"`
}

// AddressServicer ...
type AddressServicer struct {
	DAO                    mongo_dal.DAO
	OracleDB               star.IStarEngine
	Cache                  repository.RedisCache
	AddressValidatorClient addressvalidator.ValidatorService
	// We want to keep both clients in memory until Experian has zip code details available
	// until then SmartyStreets has to be used for ZipCodeDetails even when feature flag is set to Experian
	SmartyStreetsClient         *smartystreets.Client
	ExperianClient              *experian.Client
	addressValidatorClientInUse AddressValidatorClientType
	SwitchClient                featureflag.SwitchService
	serviceConfig               *ServiceConfig
}

// NewAddressServicer creates a new AddressServicer
func NewAddressServicer(
	serviceConfig *ServiceConfig,
	mdb *mongo_dal.MongoDAO,
	odb star.IStarEngine) *AddressServicer {
	return &AddressServicer{
		OracleDB:      odb,
		DAO:           mdb,
		serviceConfig: serviceConfig,
	}
}

// Initialize fully initializes the AddressServicer
func (s *AddressServicer) Initialize(ctx context.Context) {
	logging.GetTracedLogEntry(ctx).Info()

	s.Cache = repository.NewRedisCache(ctx, s.serviceConfig.Redis)
	s.SwitchClient = featureflag.NewSwitchClient(ctx, s.serviceConfig.Switch)

	s.SmartyStreetsClient = smartystreets.NewSmartyStreetClient(ctx, s.serviceConfig.Smarty)
	s.ExperianClient = experian.NewExperianClient(ctx, s.serviceConfig.Experian)

	//use smartystreets as default address validator client
	s.AddressValidatorClient = s.SmartyStreetsClient
	s.addressValidatorClientInUse = SmartyStreets

}
