package services

import (
	"context"
	"go.ftdr.com/go-utils/common/logging"
	"go.ftdr.com/go-utils/instrumentation/v3"
	"golang.frontdoorhome.com/software/address/internal/literals"
	"golang.frontdoorhome.com/software/address/internal/mongo_dal"
	addressErrors "golang.frontdoorhome.com/software/address/pkg/errors"
)

// GetAddressByID calls DAO.QueryAddressByID and converts address==nil to error
// TODO: consider refactoring this
func (s *AddressServicer) GetAddressByID(
	ctx context.Context,
	id string) (address *mongo_dal.AddressRecord, err *addressErrors.ServerError) {
	fields := map[string]interface{}{"AddressID": id}
	log := logging.GetTracedLogEntry(ctx)
	log = log.WithFields(fields)
	log.Info()
	spanID := instrumentation.ComponentSpanStart(ctx, literals.GetAddressByID, literals.ComponentTypeAddressService)
	if id == "" {
		e := addressErrors.InvalidIDError.NewFormatted(id)
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, e
	}

	addressRecord, queryErr := s.DAO.QueryAddressByID(ctx, id)
	if queryErr != nil {
		log.WithError(queryErr).Warn("ADDRESS-QUERY-ERROR: GetAddressRecordByID failed")
		fields["Error"] = queryErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, queryErr
	}
	if addressRecord == nil {
		err = addressErrors.AddressIDNotFoundError.NewFormatted(id)
		log.Info(err.Error())
		fields["Error"] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, err
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return addressRecord, nil
}

// UseAddressValidatorDefinedByFeatureFlag checks split.io for ADDRESS_VALIDATOR feature flag and
// switches between experian and smartystreets as needed
func (s *AddressServicer) UseAddressValidatorDefinedByFeatureFlag(ctx context.Context) {
	log := logging.GetTracedLogEntry(ctx)
	fields := map[string]interface{}{}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.UseAddressValidatorDefinedByFeatureFlag, literals.ComponentTypeAddressService)
	featureFlagResp, err := s.SwitchClient.CheckFlag(ctx, literals.AddressValidatorFeatureFlag)
	if err != nil {
		log.WithError(err).Error("failed to fetch feature flag")
		fields["Error"] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return
	}
	if featureFlagResp == s.addressValidatorClientInUse.String() {
		// client did not change
		fields["featureFlag"] = featureFlagResp
		instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
		return
	}
	if featureFlagResp == Experian.String() {
		s.AddressValidatorClient = s.ExperianClient
		log.Info("using experian as address validator")
	} else if featureFlagResp == SmartyStreets.String() {
		s.AddressValidatorClient = s.SmartyStreetsClient
		log.Info("using smartystreets as address validator")
	}

	fields["featureFlag"] = featureFlagResp
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
}

func (s *AddressServicer) getStatesForZipcode(ctx context.Context, zipcode string) ([]string, *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)
	var states []string
	fields := map[string]interface{}{"zipcode": zipcode}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.GetStatesForZipcode, literals.ComponentTypeAddressService)
	zipcodeDocuments, err := s.DAO.QueryStateByZipCode(ctx, zipcode)
	if err != nil {
		log.WithError(err).Error("ADDRESS-QUERY-ERROR: QueryAddressByZipCode failed")
		fields["Error"] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, err
	}
	if len(zipcodeDocuments) > 0 {
		for _, zipcodeDocument := range zipcodeDocuments {
			states = append(states, zipcodeDocument.State)
		}
		instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
		return states, nil
	}

	log.Warnf("state for zipcode %s not found, querying 3rd party address validator", zipcode)
	zipRes, err := s.AddressValidatorClient.GetZipCodeDetails(ctx, zipcode)
	if err != nil {
		if err.Code == addressErrors.ZipNotFoundError.Code {
			log.Warn("zipcode not found in smartystreets")
			instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
			return nil, nil
		}
		log.WithError(err).Error("get zipcode detail request failed")
		fields["Error"] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, err
	}
	for _, zip := range zipRes.ZipCodes {
		states = append(states, zip.StateAbbreviation)
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return states, nil
}
