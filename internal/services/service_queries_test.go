package services

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"go.ftdr.com/go-utils/common/constants"
	starConfig "go.ftdr.com/go-utils/xormwrapper/v2/config"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.frontdoorhome.com/software/address/internal/clients/addressvalidator"
	"golang.frontdoorhome.com/software/address/internal/clients/featureflag"
	"golang.frontdoorhome.com/software/address/internal/literals"
	mongo2 "golang.frontdoorhome.com/software/address/internal/mongo_dal"
	"golang.frontdoorhome.com/software/address/mock"
	addressErrors "golang.frontdoorhome.com/software/address/pkg/errors"
	"golang.frontdoorhome.com/software/protos/go/addresspb"
)

var (
	testCtx   = context.WithValue(context.Background(), constants.TraceIDContextKey, "00000000-0000-0000-0000-000000000000")
	testID    = "01234567-1234-4123-8910-123456789012"
	testIDBin = func() primitive.Binary {
		id, _ := mongo2.UUIDRawToBson(testCtx, testID)
		return id
	}

	testStreet1            = "1234 LEGEND ST"
	testUnitValue          = "500A"
	testUnitType           = "APT"
	testLegacyID    uint32 = 1500
	testCity               = "METROCITY"
	testCounty             = "QUEENS"
	testState              = "CO"
	testCountryISO3        = "USA"
	testZip                = "10101"
	testZipLocal           = testZip + "-0101"
	testDPB                = "012345678999"
	testAddress            = mongo2.AddressRecord{
		UUID:                 testIDBin(),
		LegacyIDList:         []uint32{testLegacyID},
		DeliveryPointBarcode: testDPB,
		Street1:              testStreet1,
		UnitValue:            testUnitValue,
		UnitType:             testUnitType,
		City:                 testCity,
		State:                testState,
		County:               testCounty,
		Zip:                  testZip,
		ZipLocal:             testZipLocal,
		CountryIso3:          testCountryISO3,
		Latitude:             -40.1,
		Longitude:            41.2,
		Status:               "VERIFIED",
		LastUpdated:          primitive.NewDateTimeFromTime(time.Now()),
		TimeZoneName:         "MST",
		UTCOffset:            -12,
		DSTObserved:          true,
		DocVersion:           mongo2.CurrentDocVersion,
	}

	testDifferingAddress = &mongo2.AddressRecord{
		UUID:                 testIDBin(),
		DeliveryPointBarcode: testDPB,
		Street1:              testStreet1,
		UnitValue:            testUnitValue,
		UnitType:             testUnitType,
		City:                 "METRO-CITY",
		County:               "MAPLES",
		State:                testState,
		Zip:                  testZip,
		ZipLocal:             testZipLocal,
		CountryIso3:          testCountryISO3,
		LegacyIDList:         []uint32{testLegacyID},
		Latitude:             -40.1,
		Longitude:            41.2,
		Status:               "VERIFIED",
		LastUpdated:          primitive.NewDateTimeFromTime(time.Now()),
		TimeZoneName:         "MST",
		UTCOffset:            -12,
		DSTObserved:          true,
	}

	testLookup = &mongo2.AddressRecord{
		Street1:     testStreet1,
		UnitValue:   testUnitValue,
		UnitType:    testUnitType,
		City:        testCity,
		County:      testCounty,
		CountryIso3: testCountryISO3,
		State:       testState,
		Zip:         testZip,
	}
)

func TestDeleteLegacyID(t *testing.T) {
	type input struct {
		id       string
		legacyID uint32
	}
	type output struct {
		err *addressErrors.ServerError
	}
	type mocks struct {
		mongoDAO *mock.MockDAO
	}
	type test struct {
		name    string
		args    input
		want    output
		prepare func(m *mocks)
	}

	tests := []test{
		{
			name: "success",
			args: input{
				id:       testID,
				legacyID: testLegacyID,
			},
			want: output{err: nil},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByID(testCtx, testID).Return(&mongo2.AddressRecord{LegacyIDList: []uint32{testLegacyID}}, nil)
				m.mongoDAO.EXPECT().DeleteLegacyID(testCtx, testID, testLegacyID).Return(int64(1), nil)
			},
		},
		{
			name: "nothing is deleted",
			args: input{
				id:       testID,
				legacyID: testLegacyID,
			},
			want: output{err: addressErrors.LegacyIDNotFoundError.NewFormatted(testLegacyID)},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByID(testCtx, testID).Return(&mongo2.AddressRecord{LegacyIDList: []uint32{testLegacyID}}, nil)
				m.mongoDAO.EXPECT().DeleteLegacyID(testCtx, testID, testLegacyID).Return(int64(0), nil)
			},
		},
		{
			name: "delete error",
			args: input{
				id:       testID,
				legacyID: testLegacyID,
			},
			want: output{err: addressErrors.DaoUpdateError.New()},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByID(testCtx, testID).Return(&mongo2.AddressRecord{LegacyIDList: []uint32{testLegacyID}}, nil)
				m.mongoDAO.EXPECT().DeleteLegacyID(testCtx, testID, testLegacyID).Return(int64(0), addressErrors.DaoUpdateError.New())
			},
		},
		{
			name: "address not found",
			args: input{
				id:       testID,
				legacyID: testLegacyID,
			},
			want: output{err: addressErrors.AddressIDNotFoundError.NewFormatted(testID)},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByID(testCtx, testID).Return(nil, nil)
			},
		},
		{
			name: "empty id",
			args: input{
				id:       "",
				legacyID: testLegacyID,
			},
			want: output{err: addressErrors.InvalidIDError.NewFormatted("")},
			prepare: func(m *mocks) {
			},
		},
		{
			name: "invalid legacyID",
			args: input{
				id:       testID,
				legacyID: 0,
			},
			want: output{err: addressErrors.InvalidLegacyIDError.NewFormatted(0)},
			prepare: func(m *mocks) {
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			mocks := mocks{
				mongoDAO: mock.NewMockDAO(mockCtrl),
			}
			tt.prepare(&mocks)
			service := AddressServicer{
				DAO: mocks.mongoDAO,
			}

			err := service.DeleteLegacyID(testCtx, tt.args.id, tt.args.legacyID, addresspb.ExternalSource_UNKNOWN_SOURCE)
			assert.Equal(t, tt.want.err, err)
		})
	}
}

func TestAllDeleteLegacyID(t *testing.T) {
	type input struct {
		legacyID uint32
	}
	type output struct {
		err *addressErrors.ServerError
	}
	type mocks struct {
		mongoDAO *mock.MockDAO
	}
	type test struct {
		name    string
		args    input
		want    output
		prepare func(m *mocks)
	}

	tests := []test{
		{
			name: "success",
			args: input{
				legacyID: testLegacyID,
			},
			want: output{err: nil},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().DeleteAllLegacyID(testCtx, testLegacyID).Return(int64(1), nil)
			},
		},
		{
			name: "nothing is deleted",
			args: input{
				legacyID: testLegacyID,
			},
			want: output{err: addressErrors.LegacyIDNotFoundError.NewFormatted(testLegacyID)},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().DeleteAllLegacyID(testCtx, testLegacyID).Return(int64(0), nil)
			},
		},
		{
			name: "delete error",
			args: input{
				legacyID: testLegacyID,
			},
			want: output{err: addressErrors.DaoUpdateError.New()},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().DeleteAllLegacyID(testCtx, testLegacyID).Return(int64(0), addressErrors.DaoUpdateError.New())
			},
		},
		{
			name: "invalid legacyID",
			args: input{
				legacyID: 0,
			},
			want: output{err: addressErrors.InvalidLegacyIDError.NewFormatted(0)},
			prepare: func(m *mocks) {
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			mocks := mocks{
				mongoDAO: mock.NewMockDAO(mockCtrl),
			}
			tt.prepare(&mocks)
			service := AddressServicer{
				DAO: mocks.mongoDAO,
			}

			err := service.DeleteAllLegacyID(testCtx, tt.args.legacyID, addresspb.ExternalSource_UNKNOWN_SOURCE)
			assert.Equal(t, tt.want.err, err)
		})
	}
}

func TestGetAddressRecord(t *testing.T) {
	testAddressCopy := testAddress
	type args struct {
		lookup mongo2.AddressRecord
	}
	type mockArgs struct {
		queryAddress     *mongo2.AddressRecord
		queryErr         *addressErrors.ServerError
		validatorAddress *mongo2.AddressRecord
		validatorErr     *addressErrors.ServerError
		redisAddress     *mongo2.AddressRecord
		archiveTxn       mongo.SessionContext
	}
	type expected struct {
		address *mongo2.AddressRecord
		err     *addressErrors.ServerError
	}

	tests := []struct {
		id   int
		name string
		args args
		mock mockArgs
		want expected
	}{
		{
			id:   1,
			name: "success case found in db returns address",
			args: args{lookup: *testLookup},
			mock: mockArgs{queryAddress: &testAddressCopy},
			want: expected{address: testAddress.Format()},
		},
		{
			id:   2,
			name: "not found case in addressvalidator returns not verifiable error",
			args: args{lookup: *testLookup},
			mock: mockArgs{validatorAddress: nil, validatorErr: nil},
			want: expected{err: addressErrors.AddressNotVerifiableError.New()},
		},
		{
			id:   3,
			name: "error case in addressvalidator returns error",
			args: args{lookup: *testLookup},
			mock: mockArgs{validatorAddress: nil, validatorErr: addressErrors.SmartyStreetsLookupError.New()},
			want: expected{err: addressErrors.SmartyStreetsLookupError.New()},
		},
		{
			id:   4,
			name: "existing address found case without differences returns existing address and nil error",
			args: args{lookup: *testLookup},
			mock: mockArgs{validatorAddress: &testAddressCopy, redisAddress: &testAddressCopy},
			want: expected{address: testAddress.Format()},
		},
		{
			id:   5,
			name: "existing address found case with differences archives old record, updates, and returns updated address and nil error",
			args: args{lookup: *testLookup},
			mock: mockArgs{validatorAddress: testDifferingAddress, redisAddress: &testAddressCopy, archiveTxn: mongo.NewSessionContext(testCtx, mongo.SessionFromContext(testCtx))},
			want: expected{address: testDifferingAddress.Format()},
		},
		{
			id:   6,
			name: "unique and new address saves address and returns the address",
			args: args{lookup: *testLookup},
			mock: mockArgs{validatorAddress: &testAddressCopy},
			want: expected{address: testAddressCopy.Format()},
		},
		{
			id:   7,
			name: "error case during query address returns query error",
			args: args{lookup: *testLookup},
			mock: mockArgs{queryErr: addressErrors.DaoQueryError.New()},
			want: expected{err: addressErrors.DaoQueryError.New()},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			mockDao := mock.NewMockDAO(mockCtrl)
			mockCache := mock.NewMockRedisCache(mockCtrl)
			mockAddressValidator := mock.NewMockValidatorService(mockCtrl)
			mockSwitch := mock.NewMockSwitchService(mockCtrl)
			mockEngine := mock.NewMockIStarEngine(mockCtrl)
			mockTransaction := mock.NewMockIStarTransaction(mockCtrl)

			s := &AddressServicer{
				DAO:                    mockDao,
				Cache:                  mockCache,
				AddressValidatorClient: mockAddressValidator,
				OracleDB:               mockEngine,
				SwitchClient:           mockSwitch,
				serviceConfig:          &ServiceConfig{Oracle: starConfig.OracleConfig{Username: "testUser"}},
			}

			mockEngine.EXPECT().StartTransaction(testCtx, "testUser", 0).Return(mockTransaction, nil).AnyTimes()
			mockTransaction.EXPECT().UpdatePropertyWithAddressInStarDB(testCtx, gomock.Any(), gomock.Any()).Return().AnyTimes()
			mockTransaction.EXPECT().CreatePropertyWithAddressInStarDB(testCtx, gomock.Any()).Return(int64(1), nil).AnyTimes()
			mockTransaction.EXPECT().Commit(testCtx).Return(nil).AnyTimes()

			mockSwitch.
				EXPECT().
				CheckFlag(testCtx, literals.AddressStarIntegrationFeatureFlag).Return("on", nil).AnyTimes()

			mockDao.EXPECT().QueryAddress(testCtx, &tt.args.lookup).Return(tt.mock.queryAddress, tt.mock.queryErr) // getAddressRecordFromMongoDB
			if tt.mock.queryAddress == nil && tt.mock.queryErr == nil {

				mockSwitch.
					EXPECT().
					CheckFlag(testCtx, literals.AddressValidatorFeatureFlag).Return("experian", nil).AnyTimes()
				mockAddressValidator.EXPECT().GetAddress(testCtx, &tt.args.lookup, addressvalidator.Strict).Return(tt.mock.validatorAddress, tt.mock.validatorErr) // upsertAddressFromSmartyStreets -> getAddressFromAddressValidator

				if tt.mock.validatorAddress != nil {
					mockCache.EXPECT().IsEnabled().Return(true).MaxTimes(3) // upsertAddressFromSmartyStreets -> verifyUniqueAddress
					key := getAddressDPBCRedisKey(tt.mock.validatorAddress.DeliveryPointBarcode, tt.mock.validatorAddress.UnitType, tt.mock.validatorAddress.UnitValue)
					var addressBytes []byte
					if tt.mock.redisAddress != nil {
						addressBytes, _ = json.Marshal(tt.mock.redisAddress)
					}
					mockCache.EXPECT().Get(testCtx, key).Return(addressBytes, nil) // upsertAddressFromSmartyStreets -> verifyUniqueAddress -> getAddressByDPBAndUnitFromRedis
					keyAPICall := getAddressRedisKey(tt.args.lookup.ToByteArray())
					mockCache.EXPECT().Get(testCtx, keyAPICall).Return(nil, nil)
					mockCache.EXPECT().SetEx(testCtx, keyAPICall, gomock.AssignableToTypeOf(tt.mock.validatorAddress.ToByteArray()), 300).Return(nil)

					if tt.mock.redisAddress == nil {
						mockDao.EXPECT().QueryAddress(testCtx, &mongo2.AddressRecord{
							DeliveryPointBarcode: tt.mock.validatorAddress.DeliveryPointBarcode,
							Street1:              tt.args.lookup.Street1,
							Street2:              tt.args.lookup.Street2,
							UnitType:             tt.args.lookup.UnitType,
							UnitValue:            tt.args.lookup.UnitValue,
							City:                 tt.args.lookup.City,
							State:                tt.args.lookup.State,
							Zip:                  tt.args.lookup.Zip,
							RecordType:           tt.args.lookup.RecordType,
							DocVersion:           mongo2.CurrentDocVersion,
						}).Return(nil, nil)
						mockCache.EXPECT().IsEnabled().Return(true).MaxTimes(2)
						mockCache.EXPECT().SetEx(testCtx, key, gomock.AssignableToTypeOf(tt.mock.validatorAddress.ToByteArray()), 10).Return(nil)
						mockDao.EXPECT().WithTransaction(testCtx, gomock.Any()).Return(nil)
					} else if !tt.mock.validatorAddress.IsEquivalent(testCtx, tt.mock.redisAddress) { // Address DPBC exists, but the addresses are not equivalent. Only tests success case
						mockDao.EXPECT().WithTransaction(testCtx, gomock.Any()).Return(nil)
					}
				} else if tt.mock.validatorErr == nil || tt.mock.validatorErr.Code == addressErrors.AddressNotVerifiableError.Code {
					mockCache.EXPECT().IsEnabled().Return(false).MaxTimes(2)
					mockAddressValidator.EXPECT().GetAddress(testCtx, &tt.args.lookup, addressvalidator.Range).Return(nil, tt.mock.validatorErr)
				} else {
					mockCache.EXPECT().IsEnabled().Return(false).MaxTimes(2)
				}
			} else if tt.mock.queryAddress != nil {
				mockCache.EXPECT().IsEnabled().Return(false).MaxTimes(2)
			} else {
				mockCache.EXPECT().IsEnabled().Return(false).MaxTimes(2)
			}

			gotAddress, gotErr := s.GetAddressRecord(testCtx, &tt.args.lookup, addresspb.ExternalSource_UNKNOWN_SOURCE)
			assert.Equal(t, tt.want.address, gotAddress)
			assert.EqualValues(t, tt.want.err, gotErr)
			mockCtrl.Finish()

		})
	}
}

func TestGetAddressRecordByID(t *testing.T) {
	type input struct {
		id string
	}
	type output struct {
		address *mongo2.AddressRecord
		err     *addressErrors.ServerError
	}
	type mocks struct {
		mongoDAO    *mock.MockDAO
		cache       *mock.MockRedisCache
		featureflag *mock.MockSwitchService
		validator   *mock.MockValidatorService
		engine      *mock.MockIStarEngine
		transaction *mock.MockIStarTransaction
	}
	type test struct {
		name    string
		args    input
		want    output
		prepare func(m *mocks)
	}

	tests := []test{
		{
			name: "success",
			args: input{
				id: testID,
			},
			want: output{
				address: &mongo2.AddressRecord{LegacyIDList: []uint32{1}},
				err:     nil,
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByID(testCtx, testID).Return(&mongo2.AddressRecord{}, nil)
				// mocks below mock function calls in UpsertAddressFromSmartyStreets
				m.cache.EXPECT().IsEnabled().Return(false)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressValidatorFeatureFlag).Return("", nil)
				m.validator.EXPECT().GetAddress(testCtx, &mongo2.AddressRecord{}, addressvalidator.Strict).Return(&mongo2.AddressRecord{}, nil)
				m.mongoDAO.EXPECT().QueryAddress(testCtx, &mongo2.AddressRecord{}).Return(&mongo2.AddressRecord{}, nil)
				m.engine.EXPECT().StartTransaction(testCtx, "testUser", 0).Return(m.transaction, nil)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressStarIntegrationFeatureFlag).Return(featureflag.On.String(), nil)
				m.transaction.EXPECT().CreatePropertyWithAddressInStarDB(testCtx, &mongo2.AddressRecord{}).Return(int64(1), nil)
				m.mongoDAO.EXPECT().WithTransaction(testCtx, gomock.Any()).Return(nil)
				m.transaction.EXPECT().Commit(testCtx).Return(nil)
			},
		},
		{
			name: "doc version is current",
			args: input{
				id: testID,
			},
			want: output{
				address: &mongo2.AddressRecord{DocVersion: mongo2.CurrentDocVersion},
				err:     nil,
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByID(testCtx, testID).Return(&mongo2.AddressRecord{DocVersion: mongo2.CurrentDocVersion}, nil)
			},
		},
		{
			name: "GetAddressByID error",
			args: input{
				id: testID,
			},
			want: output{
				address: nil,
				err:     addressErrors.DaoQueryError.New(),
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByID(testCtx, testID).Return(nil, addressErrors.DaoQueryError.New())
			},
		},
		{
			name: "empty id",
			args: input{
				id: "",
			},
			want: output{
				address: nil,
				err:     addressErrors.InvalidIDError.NewFormatted(""),
			},
			prepare: func(m *mocks) {
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			mocks := mocks{
				mongoDAO:    mock.NewMockDAO(mockCtrl),
				cache:       mock.NewMockRedisCache(mockCtrl),
				featureflag: mock.NewMockSwitchService(mockCtrl),
				validator:   mock.NewMockValidatorService(mockCtrl),
				engine:      mock.NewMockIStarEngine(mockCtrl),
				transaction: mock.NewMockIStarTransaction(mockCtrl),
			}
			tt.prepare(&mocks)
			service := AddressServicer{
				serviceConfig:          &ServiceConfig{Oracle: starConfig.OracleConfig{Username: "testUser"}},
				DAO:                    mocks.mongoDAO,
				Cache:                  mocks.cache,
				SwitchClient:           mocks.featureflag,
				AddressValidatorClient: mocks.validator,
				OracleDB:               mocks.engine,
			}

			address, err := service.GetAddressRecordByID(testCtx, tt.args.id)
			assert.Equal(t, tt.want.address, address)
			assert.Equal(t, tt.want.err, err)
		})
	}
}

func TestGetAddressByLegacyID(t *testing.T) {
	type input struct {
		legacyID uint32
	}
	type output struct {
		address *mongo2.AddressRecord
		err     *addressErrors.ServerError
	}
	type mocks struct {
		mongoDAO    *mock.MockDAO
		cache       *mock.MockRedisCache
		featureflag *mock.MockSwitchService
		validator   *mock.MockValidatorService
		engine      *mock.MockIStarEngine
		session     *mock.MockIStarSession
		transaction *mock.MockIStarTransaction
	}
	type test struct {
		name    string
		args    input
		want    output
		prepare func(m *mocks)
	}

	tests := []test{
		{
			name: "success - address found by legacyID",
			args: input{
				legacyID: testLegacyID,
			},
			want: output{
				address: &mongo2.AddressRecord{},
				err:     nil,
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByLegacyID(testCtx, testLegacyID).Return(&mongo2.AddressRecord{}, nil)
			},
		},
		{
			name: "success - address not found by legacyID",
			args: input{
				legacyID: testLegacyID,
			},
			want: output{
				address: &mongo2.AddressRecord{
					LegacyIDList: []uint32{testLegacyID},
				},
				err: nil,
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByLegacyID(testCtx, testLegacyID).Return(nil, nil)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressStarIntegrationFeatureFlag).Return(featureflag.On.String(), nil).Times(2)
				m.engine.EXPECT().StartSession(testCtx).Return(m.session)
				m.session.EXPECT().GetAddress(testCtx, testLegacyID).Return(&mongo2.AddressRecord{LegacyIDList: []uint32{testLegacyID}}, nil)
				m.session.EXPECT().Close(testCtx).Return(nil)
				m.cache.EXPECT().IsEnabled().Return(false).AnyTimes()
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressValidatorFeatureFlag).Return("", nil)
				m.validator.EXPECT().GetAddress(testCtx, &mongo2.AddressRecord{LegacyIDList: []uint32{testLegacyID}}, addressvalidator.Strict).Return(&mongo2.AddressRecord{}, nil)
				m.mongoDAO.EXPECT().QueryAddress(testCtx, &mongo2.AddressRecord{}).Return(nil, nil)
				m.mongoDAO.EXPECT().WithTransaction(testCtx, gomock.Any()).Return(nil)
				m.engine.EXPECT().StartTransaction(testCtx, "testUser", 0).Return(m.transaction, nil)
				m.transaction.EXPECT().UpdatePropertyWithAddressInStarDB(testCtx, gomock.Any(), testLegacyID).Return()
				m.transaction.EXPECT().Commit(testCtx).Return(nil)
			},
		},
		{
			name: "failure - address not found by legacyID, mongo error",
			args: input{
				legacyID: testLegacyID,
			},
			want: output{
				address: nil,
				err:     addressErrors.DaoInsertError.New(),
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByLegacyID(testCtx, testLegacyID).Return(nil, nil)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressStarIntegrationFeatureFlag).Return(featureflag.On.String(), nil).Times(2)
				m.engine.EXPECT().StartSession(testCtx).Return(m.session)
				m.session.EXPECT().GetAddress(testCtx, testLegacyID).Return(&mongo2.AddressRecord{LegacyIDList: []uint32{testLegacyID}}, nil)
				m.session.EXPECT().Close(testCtx).Return(nil)
				m.cache.EXPECT().IsEnabled().Return(false).AnyTimes()
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressValidatorFeatureFlag).Return("", nil)
				m.validator.EXPECT().GetAddress(testCtx, &mongo2.AddressRecord{LegacyIDList: []uint32{testLegacyID}}, addressvalidator.Strict).Return(&mongo2.AddressRecord{}, nil)
				m.mongoDAO.EXPECT().QueryAddress(testCtx, &mongo2.AddressRecord{}).Return(nil, nil)
				m.mongoDAO.EXPECT().WithTransaction(testCtx, gomock.Any()).Return(addressErrors.DaoInsertError.New())
				m.engine.EXPECT().StartTransaction(testCtx, "testUser", 0).Return(m.transaction, nil)
				m.transaction.EXPECT().UpdatePropertyWithAddressInStarDB(testCtx, gomock.Any(), testLegacyID).Return()
				m.transaction.EXPECT().Rollback(testCtx).Return(nil)
			},
		},
		{
			name: "failure - address not found by legacyID, getAddressFromStarAndValidateIt error",
			args: input{
				legacyID: testLegacyID,
			},
			want: output{
				address: nil,
				err:     addressErrors.PropertyNotFound.New(),
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByLegacyID(testCtx, testLegacyID).Return(nil, nil)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressStarIntegrationFeatureFlag).Return(featureflag.On.String(), nil)
				m.engine.EXPECT().StartSession(testCtx).Return(m.session)
				m.session.EXPECT().GetAddress(testCtx, testLegacyID).Return(nil, addressErrors.PropertyNotFound.New())
				m.session.EXPECT().Close(testCtx).Return(nil)
			},
		},
		{
			name: "failure - address not found by legacyID, QueryAddressByLegacyID error",
			args: input{
				legacyID: testLegacyID,
			},
			want: output{
				address: nil,
				err:     addressErrors.DaoQueryError.New(),
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByLegacyID(testCtx, testLegacyID).Return(nil, addressErrors.DaoQueryError.New())
			},
		},
		{
			name: "failure - legacyID is 0",
			args: input{
				legacyID: 0,
			},
			want: output{
				address: nil,
				err:     addressErrors.InvalidLegacyIDError.NewFormatted(0),
			},
			prepare: func(m *mocks) {
			},
		},
		{
			name: "failure - address not found by legacyID",
			args: input{
				legacyID: testLegacyID,
			},
			want: output{
				address: nil,
				err:     addressErrors.LegacyIDNotFoundError.NewFormatted(testLegacyID),
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByLegacyID(testCtx, testLegacyID).Return(nil, nil)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressStarIntegrationFeatureFlag).Return(featureflag.Off.String(), nil)
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			mocks := mocks{
				mongoDAO:    mock.NewMockDAO(mockCtrl),
				cache:       mock.NewMockRedisCache(mockCtrl),
				featureflag: mock.NewMockSwitchService(mockCtrl),
				validator:   mock.NewMockValidatorService(mockCtrl),
				engine:      mock.NewMockIStarEngine(mockCtrl),
				session:     mock.NewMockIStarSession(mockCtrl),
				transaction: mock.NewMockIStarTransaction(mockCtrl),
			}
			tt.prepare(&mocks)
			service := AddressServicer{
				serviceConfig:          &ServiceConfig{Oracle: starConfig.OracleConfig{Username: "testUser"}},
				DAO:                    mocks.mongoDAO,
				Cache:                  mocks.cache,
				SwitchClient:           mocks.featureflag,
				AddressValidatorClient: mocks.validator,
				OracleDB:               mocks.engine,
			}

			address, err := service.GetAddressByLegacyID(testCtx, tt.args.legacyID, addresspb.ExternalSource_UNKNOWN_SOURCE)
			//can't compare whole address
			if address == nil || tt.want.address == nil {
				assert.Equal(t, tt.want.address, address)
			} else {
				assert.Equal(t, tt.want.address.LegacyIDList, address.LegacyIDList)
			}
			assert.Equal(t, tt.want.err, err)
		})
	}
}

func TestGetZipCodeDetails(t *testing.T) {
	type input struct {
		zipcode string
	}
	type output struct {
		res *addresspb.ZipCodeDetailsResponse
		err *addressErrors.ServerError
	}
	type mocks struct {
		switchClient *mock.MockSwitchService
		mongoDAO     *mock.MockDAO
	}
	type test struct {
		name    string
		args    input
		want    output
		prepare func(m *mocks)
	}

	tests := []test{
		{
			name: "success",
			args: input{
				testZip,
			},
			want: output{
				res: &addresspb.ZipCodeDetailsResponse{
					ZipCodes: []*addresspb.ZipCodeDetailsResponse_ZipCode{{
						ZipCode:           testZip,
						DefaultCity:       testCity,
						State:             literals.StateAbbreviations[testState],
						StateAbbreviation: testState,
						Latitude:          21,
						Longitude:         37,
					}},
					CityStates: []*addresspb.ZipCodeDetailsResponse_CityState{{
						City:              testCity,
						State:             literals.StateAbbreviations[testState],
						StateAbbreviation: testState,
					}},
				},
				err: nil,
			},
			prepare: func(m *mocks) {
				m.switchClient.EXPECT().CheckFlag(testCtx, literals.AddressUseMongoZipcodeData).Return(featureflag.On.String(), nil)
				m.mongoDAO.EXPECT().QueryStateByZipCode(testCtx, testZip).Return([]*mongo2.ZipcodeRecord{{
					ZipCode:   testZip,
					City:      testCity,
					State:     testState,
					Latitude:  21,
					Longitude: 37,
				}}, nil)
			},
		},
		{
			name: "mongo error",
			args: input{
				testZip,
			},
			want: output{
				res: nil,
				err: addressErrors.DaoQueryError.New(),
			},
			prepare: func(m *mocks) {
				m.switchClient.EXPECT().CheckFlag(testCtx, literals.AddressUseMongoZipcodeData).Return(featureflag.On.String(), nil)
				m.mongoDAO.EXPECT().QueryStateByZipCode(testCtx, testZip).Return(nil, addressErrors.DaoQueryError.New())
			},
		},
		{
			name: "empty zip",
			args: input{
				"",
			},
			want: output{
				res: nil,
				err: addressErrors.InvalidZipCodeError.NewFormatted(""),
			},
			prepare: func(m *mocks) {
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			mocks := mocks{
				switchClient: mock.NewMockSwitchService(mockCtrl),
				mongoDAO:     mock.NewMockDAO(mockCtrl),
			}
			tt.prepare(&mocks)
			service := AddressServicer{
				SwitchClient: mocks.switchClient,
				DAO:          mocks.mongoDAO,
			}

			res, err := service.GetZipCodeDetails(testCtx, tt.args.zipcode)
			assert.Equal(t, tt.want.res, res)
			assert.Equal(t, tt.want.err, err)
		})
	}
}

func TestGetTypeAheadSuggestions(t *testing.T) {
	type input struct {
		getTypeAheadRequest *addresspb.AddressByTypeAheadRequest
	}
	type output struct {
		suggestions []*addresspb.TypeAheadAddress
	}
	type mocks struct {
		featureflag *mock.MockSwitchService
		validator   *mock.MockValidatorService
	}
	type test struct {
		name    string
		args    input
		want    output
		prepare func(m *mocks)
	}

	tests := []test{
		{
			name: "success",
			args: input{
				getTypeAheadRequest: &addresspb.AddressByTypeAheadRequest{},
			},
			want: output{
				suggestions: []*addresspb.TypeAheadAddress{},
			},
			prepare: func(m *mocks) {
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressValidatorFeatureFlag).Return("", nil)
				m.validator.EXPECT().GetTypeaheadSuggestions(testCtx, &addresspb.AddressByTypeAheadRequest{}, 1).Return([]*addresspb.TypeAheadAddress{})
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			mocks := mocks{
				featureflag: mock.NewMockSwitchService(mockCtrl),
				validator:   mock.NewMockValidatorService(mockCtrl),
			}
			tt.prepare(&mocks)
			service := AddressServicer{
				serviceConfig:          &ServiceConfig{LookaheadLimit: 1},
				SwitchClient:           mocks.featureflag,
				AddressValidatorClient: mocks.validator,
			}
			suggestions := service.GetTypeAheadSuggestions(testCtx, tt.args.getTypeAheadRequest)
			assert.Equal(t, tt.want.suggestions, suggestions)
		})
	}
}

func TestLogCollectionStats(t *testing.T) {
	type input struct {
	}
	type output struct {
		err *addressErrors.ServerError
	}
	type mocks struct {
		mongoDAO *mock.MockDAO
	}
	type test struct {
		name    string
		args    input
		want    output
		prepare func(m *mocks)
	}

	tests := []test{
		{
			name: "success",
			args: input{},
			want: output{
				err: nil,
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryCollectionsStats(testCtx).Return(map[string]interface{}{
					"address": 21,
					"archive": 37,
				}, nil)
			},
		},
		{
			name: "mongo query error",
			args: input{},
			want: output{
				err: addressErrors.DaoQueryError.New(),
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryCollectionsStats(testCtx).Return(nil, addressErrors.DaoQueryError.New())
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			mocks := mocks{
				mongoDAO: mock.NewMockDAO(mockCtrl),
			}
			tt.prepare(&mocks)
			service := AddressServicer{
				DAO: mocks.mongoDAO,
			}
			err := service.LogCollectionStats(testCtx)
			assert.Equal(t, tt.want.err, err)
		})
	}
}

func TestLogFilterStats(t *testing.T) {
	type input struct {
		collection string
		filter     map[string]interface{}
	}
	type output struct {
		err *addressErrors.ServerError
	}
	type mocks struct {
		mongoDAO *mock.MockDAO
	}
	type test struct {
		name    string
		args    input
		want    output
		prepare func(m *mocks)
	}

	tests := []test{
		{
			name: "success",
			args: input{
				collection: "address",
				filter:     map[string]interface{}{},
			},
			want: output{
				err: nil,
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryFilterStats(testCtx, "address", map[string]interface{}{}).Return(int64(123), nil)
			},
		},
		{
			name: "query error",
			args: input{
				collection: "address",
				filter:     map[string]interface{}{},
			},
			want: output{
				err: addressErrors.DaoQueryError.New(),
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryFilterStats(testCtx, "address", map[string]interface{}{}).Return(int64(0), addressErrors.DaoQueryError.New())
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			mocks := mocks{
				mongoDAO: mock.NewMockDAO(mockCtrl),
			}
			tt.prepare(&mocks)
			service := AddressServicer{
				DAO: mocks.mongoDAO,
			}
			err := service.LogFilterStats(testCtx, tt.args.collection, tt.args.filter)
			assert.Equal(t, tt.want.err, err)
		})
	}
}

func TestPostLegacyID(t *testing.T) {
	type input struct {
		id       string
		legacyID uint32
		source   addresspb.ExternalSource
	}
	type output struct {
		err *addressErrors.ServerError
	}
	type mocks struct {
		mongoDAO    *mock.MockDAO
		featureflag *mock.MockSwitchService
		engine      *mock.MockIStarEngine
		transaction *mock.MockIStarTransaction
	}
	type test struct {
		name    string
		args    input
		want    output
		prepare func(m *mocks)
	}

	tests := []test{
		{
			name: "success",
			args: input{
				id:       testID,
				legacyID: testLegacyID,
			},
			want: output{
				err: nil,
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByID(testCtx, testID).Return(&mongo2.AddressRecord{}, nil)
				m.mongoDAO.EXPECT().QueryAddressByLegacyID(testCtx, testLegacyID).Return(nil, nil)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressStarIntegrationFeatureFlag).Return(featureflag.On.String(), nil)
				m.engine.EXPECT().StartTransaction(testCtx, "testUser", 0).Return(m.transaction, nil)
				m.transaction.EXPECT().HandleEndOfTransaction(testCtx, gomock.Any()).Return(nil)
				m.transaction.EXPECT().UpdatePropertyWithAddressInStarDB(testCtx, &mongo2.AddressRecord{}, testLegacyID).Return()
				m.mongoDAO.EXPECT().InsertLegacyID(testCtx, testID, testLegacyID).Return(int64(1), nil)
			},
		},
		{
			name: "failure - nothing updated",
			args: input{
				id:       testID,
				legacyID: testLegacyID,
			},
			want: output{
				err: addressErrors.NoRecordsUpdatedError.New(),
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByID(testCtx, testID).Return(&mongo2.AddressRecord{}, nil)
				m.mongoDAO.EXPECT().QueryAddressByLegacyID(testCtx, testLegacyID).Return(nil, nil)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressStarIntegrationFeatureFlag).Return(featureflag.On.String(), nil)
				m.engine.EXPECT().StartTransaction(testCtx, "testUser", 0).Return(m.transaction, nil)
				m.transaction.EXPECT().HandleEndOfTransaction(testCtx, gomock.Any()).Return(nil)
				m.transaction.EXPECT().UpdatePropertyWithAddressInStarDB(testCtx, &mongo2.AddressRecord{}, testLegacyID).Return()
				m.mongoDAO.EXPECT().InsertLegacyID(testCtx, testID, testLegacyID).Return(int64(0), nil)
			},
		},
		{
			name: "failure - update error",
			args: input{
				id:       testID,
				legacyID: testLegacyID,
			},
			want: output{
				err: addressErrors.DaoInsertError.New(),
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByID(testCtx, testID).Return(&mongo2.AddressRecord{}, nil)
				m.mongoDAO.EXPECT().QueryAddressByLegacyID(testCtx, testLegacyID).Return(nil, nil)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressStarIntegrationFeatureFlag).Return(featureflag.On.String(), nil)
				m.engine.EXPECT().StartTransaction(testCtx, "testUser", 0).Return(m.transaction, nil)
				m.transaction.EXPECT().HandleEndOfTransaction(testCtx, gomock.Any()).Return(nil)
				m.transaction.EXPECT().UpdatePropertyWithAddressInStarDB(testCtx, &mongo2.AddressRecord{}, testLegacyID).Return()
				m.mongoDAO.EXPECT().InsertLegacyID(testCtx, testID, testLegacyID).Return(int64(0), addressErrors.DaoInsertError.New())
			},
		},
		{
			name: "failure - update error",
			args: input{
				id:       testID,
				legacyID: testLegacyID,
			},
			want: output{
				err: addressErrors.StarTransactionError.New(),
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByID(testCtx, testID).Return(&mongo2.AddressRecord{}, nil)
				m.mongoDAO.EXPECT().QueryAddressByLegacyID(testCtx, testLegacyID).Return(nil, nil)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressStarIntegrationFeatureFlag).Return(featureflag.On.String(), nil)
				m.engine.EXPECT().StartTransaction(testCtx, "testUser", 0).Return(nil, fmt.Errorf("error"))
			},
		},
		{
			name: "failure - legacyID already exists",
			args: input{
				id:       testID,
				legacyID: testLegacyID,
			},
			want: output{
				err: addressErrors.LegacyIDAlreadyExistsError.NewFormatted(testLegacyID, testID),
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByID(testCtx, testID).Return(&mongo2.AddressRecord{}, nil)
				m.mongoDAO.EXPECT().QueryAddressByLegacyID(testCtx, testLegacyID).Return(&mongo2.AddressRecord{
					UUID: testIDBin(),
				}, nil)
			},
		},
		{
			name: "failure - QueryAddressByLegacyID error",
			args: input{
				id:       testID,
				legacyID: testLegacyID,
			},
			want: output{
				err: addressErrors.DaoQueryError.New(),
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByID(testCtx, testID).Return(&mongo2.AddressRecord{}, nil)
				m.mongoDAO.EXPECT().QueryAddressByLegacyID(testCtx, testLegacyID).Return(nil, addressErrors.DaoQueryError.New())
			},
		},
		{
			name: "failure - legacyID ID combo already exists",
			args: input{
				id:       testID,
				legacyID: testLegacyID,
			},
			want: output{
				err: addressErrors.LegacyIDAlreadyExistsError.NewFormatted(testLegacyID, testID),
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByID(testCtx, testID).Return(&mongo2.AddressRecord{
					UUID: testIDBin(),
					LegacyIDList: []uint32{
						testLegacyID,
					},
				}, nil)
			},
		},
		{
			name: "failure - address not found",
			args: input{
				id:       testID,
				legacyID: testLegacyID,
			},
			want: output{
				err: addressErrors.AddressIDNotFoundError.NewFormatted(testID),
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByID(testCtx, testID).Return(nil, nil)
			},
		},
		{
			name: "failure - empty id",
			args: input{
				id:       "",
				legacyID: testLegacyID,
			},
			want: output{
				err: addressErrors.InvalidIDError.NewFormatted(""),
			},
			prepare: func(m *mocks) {
			},
		},
		{
			name: "failure - empty legacyID",
			args: input{
				id:       "",
				legacyID: 0,
			},
			want: output{
				err: addressErrors.InvalidLegacyIDError.NewFormatted(0),
			},
			prepare: func(m *mocks) {
			},
		},
		{
			name: "success - COMMERCIAL_PROPERTY",
			args: input{
				id:       testID,
				legacyID: testLegacyID,
				source:   addresspb.ExternalSource_COMMERCIAL_PROPERTY,
			},
			want: output{
				err: nil,
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByID(testCtx, testID).Return(&mongo2.AddressRecord{}, nil)
				m.mongoDAO.EXPECT().QueryAddressByExternalSourceID(testCtx, addresspb.ExternalSource_COMMERCIAL_PROPERTY.String(), testLegacyID).Return(nil, nil)
				m.mongoDAO.EXPECT().InsertExternalSourceID(testCtx, testID, addresspb.ExternalSource_COMMERCIAL_PROPERTY.String(), testLegacyID).Return(int64(1), nil)
			},
		},
		{
			name: "failure - COMMERCIAL_PROPERTY nothing updated",
			args: input{
				id:       testID,
				legacyID: testLegacyID,
				source:   addresspb.ExternalSource_COMMERCIAL_PROPERTY,
			},
			want: output{
				err: addressErrors.NoRecordsUpdatedError.New(),
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByID(testCtx, testID).Return(&mongo2.AddressRecord{}, nil)
				m.mongoDAO.EXPECT().QueryAddressByExternalSourceID(testCtx, addresspb.ExternalSource_COMMERCIAL_PROPERTY.String(), testLegacyID).Return(nil, nil)
				m.mongoDAO.EXPECT().InsertExternalSourceID(testCtx, testID, addresspb.ExternalSource_COMMERCIAL_PROPERTY.String(), testLegacyID).Return(int64(0), nil)
			},
		},
		{
			name: "failure - COMMERCIAL_PROPERTY update error",
			args: input{
				id:       testID,
				legacyID: testLegacyID,
				source:   addresspb.ExternalSource_COMMERCIAL_PROPERTY,
			},
			want: output{
				err: addressErrors.DaoInsertError.New(),
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByID(testCtx, testID).Return(&mongo2.AddressRecord{}, nil)
				m.mongoDAO.EXPECT().QueryAddressByExternalSourceID(testCtx, addresspb.ExternalSource_COMMERCIAL_PROPERTY.String(), testLegacyID).Return(nil, nil)
				m.mongoDAO.EXPECT().InsertExternalSourceID(testCtx, testID, addresspb.ExternalSource_COMMERCIAL_PROPERTY.String(), testLegacyID).Return(int64(0), addressErrors.DaoInsertError.New())
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			mocks := mocks{
				mongoDAO:    mock.NewMockDAO(mockCtrl),
				featureflag: mock.NewMockSwitchService(mockCtrl),
				engine:      mock.NewMockIStarEngine(mockCtrl),
				transaction: mock.NewMockIStarTransaction(mockCtrl),
			}
			tt.prepare(&mocks)
			service := AddressServicer{
				serviceConfig: &ServiceConfig{Oracle: starConfig.OracleConfig{Username: "testUser"}},
				DAO:           mocks.mongoDAO,
				SwitchClient:  mocks.featureflag,
				OracleDB:      mocks.engine,
			}
			err := service.PostLegacyID(testCtx, tt.args.id, tt.args.legacyID, tt.args.source)
			assert.Equal(t, tt.want.err, err)
		})
	}
}

func TestPostUnverifiedAddress(t *testing.T) {
	type input struct {
		address *mongo2.AddressRecord
	}
	type output struct {
		address *mongo2.AddressRecord
		err     *addressErrors.ServerError
	}
	type mocks struct {
		cache       *mock.MockRedisCache
		featureflag *mock.MockSwitchService
		validator   *mock.MockValidatorService
		engine      *mock.MockIStarEngine
		transaction *mock.MockIStarTransaction
		mongoDAO    *mock.MockDAO
	}
	type test struct {
		name    string
		args    input
		want    output
		prepare func(m *mocks)
	}

	tests := []test{
		{
			name: "success",
			args: input{
				address: &mongo2.AddressRecord{},
			},
			want: output{
				address: &mongo2.AddressRecord{},
				err:     nil,
			},
			prepare: func(m *mocks) {
				m.cache.EXPECT().IsEnabled().Return(false).AnyTimes()
				m.mongoDAO.EXPECT().QueryAddress(testCtx, &mongo2.AddressRecord{}).Return(nil, nil)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressValidatorFeatureFlag).Return("", nil)
				m.validator.EXPECT().GetAddress(testCtx, &mongo2.AddressRecord{}, addressvalidator.Strict).Return(nil, nil)
				m.validator.EXPECT().GetAddress(testCtx, &mongo2.AddressRecord{}, addressvalidator.Range).Return(nil, nil)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressStarIntegrationFeatureFlag).Return(featureflag.On.String(), nil)
				m.engine.EXPECT().StartTransaction(testCtx, "testUser", 0).Return(m.transaction, nil)
				m.transaction.EXPECT().Commit(testCtx).Return(nil)
				m.transaction.EXPECT().CreatePropertyWithAddressInStarDB(testCtx, gomock.Any()).Return(int64(testLegacyID), nil)
				m.mongoDAO.EXPECT().InsertAddress(testCtx, gomock.Any()).Return(nil)
			},
		},
		{
			name: "failure - insert error",
			args: input{
				address: &mongo2.AddressRecord{},
			},
			want: output{
				address: nil,
				err:     addressErrors.DaoInsertError.New(),
			},
			prepare: func(m *mocks) {
				m.cache.EXPECT().IsEnabled().Return(false).AnyTimes()
				m.mongoDAO.EXPECT().QueryAddress(testCtx, &mongo2.AddressRecord{}).Return(nil, nil)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressValidatorFeatureFlag).Return("", nil)
				m.validator.EXPECT().GetAddress(testCtx, &mongo2.AddressRecord{}, addressvalidator.Strict).Return(nil, nil)
				m.validator.EXPECT().GetAddress(testCtx, &mongo2.AddressRecord{}, addressvalidator.Range).Return(nil, nil)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressStarIntegrationFeatureFlag).Return(featureflag.On.String(), nil)
				m.engine.EXPECT().StartTransaction(testCtx, "testUser", 0).Return(m.transaction, nil)
				m.transaction.EXPECT().Rollback(testCtx).Return(nil)
				m.transaction.EXPECT().CreatePropertyWithAddressInStarDB(testCtx, gomock.Any()).Return(int64(testLegacyID), nil)
				m.mongoDAO.EXPECT().InsertAddress(testCtx, gomock.Any()).Return(addressErrors.DaoInsertError.New())
			},
		},
		{
			name: "failure - upsertEntryInExternalSource error",
			args: input{
				address: &mongo2.AddressRecord{},
			},
			want: output{
				address: nil,
				err:     addressErrors.StarOperationError.New(),
			},
			prepare: func(m *mocks) {
				m.cache.EXPECT().IsEnabled().Return(false).AnyTimes()
				m.mongoDAO.EXPECT().QueryAddress(testCtx, &mongo2.AddressRecord{}).Return(nil, nil)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressValidatorFeatureFlag).Return("", nil)
				m.validator.EXPECT().GetAddress(testCtx, &mongo2.AddressRecord{}, addressvalidator.Strict).Return(nil, nil)
				m.validator.EXPECT().GetAddress(testCtx, &mongo2.AddressRecord{}, addressvalidator.Range).Return(nil, nil)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressStarIntegrationFeatureFlag).Return(featureflag.On.String(), nil)
				m.engine.EXPECT().StartTransaction(testCtx, "testUser", 0).Return(m.transaction, nil)
				m.transaction.EXPECT().Rollback(testCtx).Return(nil)
				m.transaction.EXPECT().CreatePropertyWithAddressInStarDB(testCtx, gomock.Any()).Return(int64(0), addressErrors.StarOperationError.New())
			},
		},
		{
			name: "failure - address verifiable",
			args: input{
				address: &mongo2.AddressRecord{},
			},
			want: output{
				address: nil,
				err:     addressErrors.VerifiableAddressError.NewFormatted("failed to convert"),
			},
			prepare: func(m *mocks) {
				m.cache.EXPECT().IsEnabled().Return(false).AnyTimes()
				m.mongoDAO.EXPECT().QueryAddress(testCtx, &mongo2.AddressRecord{}).Return(nil, nil)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressValidatorFeatureFlag).Return("", nil)
				m.validator.EXPECT().GetAddress(testCtx, &mongo2.AddressRecord{}, addressvalidator.Strict).Return(&mongo2.AddressRecord{}, nil)
				m.mongoDAO.EXPECT().QueryAddress(testCtx, &mongo2.AddressRecord{}).Return(&mongo2.AddressRecord{}, nil)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressStarIntegrationFeatureFlag).Return(featureflag.On.String(), nil)
				m.engine.EXPECT().StartTransaction(testCtx, "testUser", 0).Return(m.transaction, nil)
				m.transaction.EXPECT().CreatePropertyWithAddressInStarDB(testCtx, gomock.Any()).Return(int64(testLegacyID), nil)
				m.transaction.EXPECT().Commit(testCtx).Return(nil)
				m.mongoDAO.EXPECT().WithTransaction(testCtx, gomock.Any()).Return(nil)
				m.validator.EXPECT().IsVerifiable(testCtx, &mongo2.AddressRecord{LegacyIDList: []uint32{testLegacyID}}, addressvalidator.Strict).Return(true, nil)
			},
		},
		{
			name: "success - found unverifiable in mongo",
			args: input{
				address: &mongo2.AddressRecord{},
			},
			want: output{
				address: &mongo2.AddressRecord{DocVersion: mongo2.CurrentDocVersion},
				err:     nil,
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddress(testCtx, &mongo2.AddressRecord{}).Return(&mongo2.AddressRecord{DocVersion: mongo2.CurrentDocVersion}, nil)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressStarIntegrationFeatureFlag).Return(featureflag.On.String(), nil)
				m.engine.EXPECT().StartTransaction(testCtx, "testUser", 0).Return(m.transaction, nil)
				m.transaction.EXPECT().Commit(testCtx).Return(nil)
				m.transaction.EXPECT().CreatePropertyWithAddressInStarDB(testCtx, gomock.Any()).Return(int64(testLegacyID), nil)
				m.mongoDAO.EXPECT().WithTransaction(testCtx, gomock.Any()).Return(nil)
			},
		},
		{
			name: "failure - found unverifiable in mongo, update failed",
			args: input{
				address: &mongo2.AddressRecord{},
			},
			want: output{
				address: nil,
				err:     addressErrors.DaoUpdateError.New(),
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddress(testCtx, &mongo2.AddressRecord{}).Return(&mongo2.AddressRecord{DocVersion: mongo2.CurrentDocVersion}, nil)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressStarIntegrationFeatureFlag).Return(featureflag.On.String(), nil)
				m.engine.EXPECT().StartTransaction(testCtx, "testUser", 0).Return(m.transaction, nil)
				m.transaction.EXPECT().Rollback(testCtx).Return(nil)
				m.transaction.EXPECT().CreatePropertyWithAddressInStarDB(testCtx, gomock.Any()).Return(int64(testLegacyID), nil)
				m.mongoDAO.EXPECT().WithTransaction(testCtx, gomock.Any()).Return(addressErrors.DaoUpdateError.New())
			},
		},
		{
			name: "success - found unverifiable in mongo, upsertEntryInExternalSource failed",
			args: input{
				address: &mongo2.AddressRecord{},
			},
			want: output{
				address: &mongo2.AddressRecord{DocVersion: mongo2.CurrentDocVersion},
				err:     nil,
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddress(testCtx, &mongo2.AddressRecord{}).Return(&mongo2.AddressRecord{DocVersion: mongo2.CurrentDocVersion}, nil)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressStarIntegrationFeatureFlag).Return(featureflag.On.String(), nil)
				m.engine.EXPECT().StartTransaction(testCtx, "testUser", 0).Return(m.transaction, nil)
				m.transaction.EXPECT().Rollback(testCtx).Return(nil)
				m.transaction.EXPECT().CreatePropertyWithAddressInStarDB(testCtx, gomock.Any()).Return(int64(0), addressErrors.StarOperationError.New())
			},
		},
		{
			name: "success - found unverifiable in mongo, star integration disabled",
			args: input{
				address: &mongo2.AddressRecord{},
			},
			want: output{
				address: &mongo2.AddressRecord{DocVersion: mongo2.CurrentDocVersion},
				err:     nil,
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddress(testCtx, &mongo2.AddressRecord{}).Return(&mongo2.AddressRecord{DocVersion: mongo2.CurrentDocVersion}, nil)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressStarIntegrationFeatureFlag).Return(featureflag.Off.String(), nil)
			},
		},
		{
			name: "failure - found verified in mongo",
			args: input{
				address: &mongo2.AddressRecord{},
			},
			want: output{
				address: nil,
				err:     addressErrors.VerifiableAddressError.NewFormatted("failed to convert"),
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddress(testCtx, &mongo2.AddressRecord{}).Return(&mongo2.AddressRecord{DocVersion: mongo2.CurrentDocVersion, Status: mongo2.VerifiedStatus}, nil)
			},
		},
		{
			name: "failure - QueryAddress error",
			args: input{
				address: &mongo2.AddressRecord{},
			},
			want: output{
				address: nil,
				err:     addressErrors.DaoQueryError.New(),
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddress(testCtx, &mongo2.AddressRecord{}).Return(nil, addressErrors.DaoQueryError.New())
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			mocks := mocks{
				cache:       mock.NewMockRedisCache(mockCtrl),
				featureflag: mock.NewMockSwitchService(mockCtrl),
				validator:   mock.NewMockValidatorService(mockCtrl),
				engine:      mock.NewMockIStarEngine(mockCtrl),
				transaction: mock.NewMockIStarTransaction(mockCtrl),
				mongoDAO:    mock.NewMockDAO(mockCtrl),
			}
			tt.prepare(&mocks)
			service := AddressServicer{
				serviceConfig:          &ServiceConfig{Oracle: starConfig.OracleConfig{Username: "testUser"}},
				Cache:                  mocks.cache,
				SwitchClient:           mocks.featureflag,
				AddressValidatorClient: mocks.validator,
				OracleDB:               mocks.engine,
				DAO:                    mocks.mongoDAO,
			}
			_, err := service.PostUnverifiedAddress(testCtx, tt.args.address, addresspb.ExternalSource_UNKNOWN_SOURCE, false)
			assert.Equal(t, tt.want.err, err)
		})
	}
}

func TestQueryAddressID(t *testing.T) {
	type input struct {
		id string
	}
	type output struct {
		err *addressErrors.ServerError
	}
	type mocks struct {
		mongoDAO *mock.MockDAO
	}
	type test struct {
		name    string
		args    input
		want    output
		prepare func(m *mocks)
	}

	tests := []test{
		{
			name: "success",
			args: input{
				id: testID,
			},
			want: output{
				err: nil,
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByID(testCtx, testID).Return(&mongo2.AddressRecord{}, nil)
			},
		},
		{
			name: "address not found",
			args: input{
				id: testID,
			},
			want: output{
				err: addressErrors.AddressIDNotFoundError.NewFormatted(testID),
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByID(testCtx, testID).Return(nil, nil)
			},
		},
		{
			name: "mongo query error",
			args: input{
				id: testID,
			},
			want: output{
				err: addressErrors.DaoQueryError.New(),
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryAddressByID(testCtx, testID).Return(nil, addressErrors.DaoQueryError.New())
			},
		},
		{
			name: "invalid id",
			args: input{
				id: "",
			},
			want: output{
				err: addressErrors.InvalidIDError.NewFormatted(""),
			},
			prepare: func(m *mocks) {
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			mocks := mocks{
				mongoDAO: mock.NewMockDAO(mockCtrl),
			}
			tt.prepare(&mocks)
			service := AddressServicer{
				DAO: mocks.mongoDAO,
			}
			err := service.QueryAddressID(testCtx, tt.args.id)
			assert.Equal(t, tt.want.err, err)
		})
	}
}

func TestVerifyAddress(t *testing.T) {
	type input struct {
		address *mongo2.AddressRecord
	}
	type output struct {
		address *mongo2.AddressRecord
		err     *addressErrors.ServerError
	}
	type mocks struct {
		cache       *mock.MockRedisCache
		featureflag *mock.MockSwitchService
		validator   *mock.MockValidatorService
		engine      *mock.MockIStarEngine
		transaction *mock.MockIStarTransaction
		mongoDAO    *mock.MockDAO
	}
	type test struct {
		name    string
		args    input
		want    output
		prepare func(m *mocks)
	}

	tests := []test{
		{
			name: "success",
			args: input{
				address: &mongo2.AddressRecord{
					LegacyIDList: []uint32{testLegacyID},
				},
			},
			want: output{
				address: &mongo2.AddressRecord{
					LegacyIDList: []uint32{testLegacyID},
				},
				err: nil,
			},
			prepare: func(m *mocks) {
				m.cache.EXPECT().IsEnabled().Return(false)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressValidatorFeatureFlag).Return("", nil)
				m.validator.EXPECT().GetAddress(testCtx, &mongo2.AddressRecord{
					LegacyIDList: []uint32{testLegacyID},
				}, addressvalidator.Strict).Return(&mongo2.AddressRecord{}, nil)
				m.validator.EXPECT().IsVerifiable(testCtx, &mongo2.AddressRecord{}, addressvalidator.Strict).Return(true, nil)
				m.engine.EXPECT().StartTransaction(testCtx, "testUser", 0).Return(m.transaction, nil)
				m.transaction.EXPECT().Commit(testCtx).Return(nil)
				m.transaction.EXPECT().UpdatePropertyWithAddressInStarDB(testCtx, &mongo2.AddressRecord{}, testLegacyID).Return()
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressStarIntegrationFeatureFlag).Return(featureflag.On.String(), nil)
				m.mongoDAO.EXPECT().UpdateAddressWithArchiving(testCtx,
					&mongo2.AddressRecord{LegacyIDList: []uint32{testLegacyID}},
					&mongo2.AddressRecord{LegacyIDList: []uint32{testLegacyID}},
				).Return(&mongo2.AddressRecord{LegacyIDList: []uint32{testLegacyID}}, nil)
			},
		},
		{
			name: "failure - updateAddressInMongoDB error",
			args: input{
				address: &mongo2.AddressRecord{
					LegacyIDList: []uint32{testLegacyID},
				},
			},
			want: output{
				address: nil,
				err:     addressErrors.DaoUpdateError.New(),
			},
			prepare: func(m *mocks) {
				m.cache.EXPECT().IsEnabled().Return(false)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressValidatorFeatureFlag).Return("", nil)
				m.validator.EXPECT().GetAddress(testCtx, &mongo2.AddressRecord{
					LegacyIDList: []uint32{testLegacyID},
				}, addressvalidator.Strict).Return(&mongo2.AddressRecord{}, nil)
				m.validator.EXPECT().IsVerifiable(testCtx, &mongo2.AddressRecord{}, addressvalidator.Strict).Return(true, nil)
				m.engine.EXPECT().StartTransaction(testCtx, "testUser", 0).Return(m.transaction, nil)
				m.transaction.EXPECT().Rollback(testCtx).Return(nil)
				m.transaction.EXPECT().UpdatePropertyWithAddressInStarDB(testCtx, &mongo2.AddressRecord{}, testLegacyID).Return()
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressStarIntegrationFeatureFlag).Return(featureflag.On.String(), nil)
				m.mongoDAO.EXPECT().UpdateAddressWithArchiving(testCtx,
					&mongo2.AddressRecord{LegacyIDList: []uint32{testLegacyID}},
					&mongo2.AddressRecord{LegacyIDList: []uint32{testLegacyID}},
				).Return(nil, addressErrors.DaoUpdateError.New())
			},
		},
		{
			name: "failure - star transaction error",
			args: input{
				address: &mongo2.AddressRecord{
					LegacyIDList: []uint32{testLegacyID},
				},
			},
			want: output{
				address: nil,
				err:     addressErrors.StarTransactionError.New(),
			},
			prepare: func(m *mocks) {
				m.cache.EXPECT().IsEnabled().Return(false)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressValidatorFeatureFlag).Return("", nil)
				m.validator.EXPECT().GetAddress(testCtx, &mongo2.AddressRecord{
					LegacyIDList: []uint32{testLegacyID},
				}, addressvalidator.Strict).Return(&mongo2.AddressRecord{}, nil)
				m.validator.EXPECT().IsVerifiable(testCtx, &mongo2.AddressRecord{}, addressvalidator.Strict).Return(true, nil)
				m.engine.EXPECT().StartTransaction(testCtx, "testUser", 0).Return(nil, fmt.Errorf("error"))
			},
		},
		{
			name: "failure - validateAddress error",
			args: input{
				address: &mongo2.AddressRecord{
					LegacyIDList: []uint32{testLegacyID},
				},
			},
			want: output{
				address: nil,
				err:     addressErrors.SmartyStreetsLookupError.New(),
			},
			prepare: func(m *mocks) {
				m.cache.EXPECT().IsEnabled().Return(false)
				m.featureflag.EXPECT().CheckFlag(testCtx, literals.AddressValidatorFeatureFlag).Return("", nil)
				m.validator.EXPECT().GetAddress(testCtx, &mongo2.AddressRecord{
					LegacyIDList: []uint32{testLegacyID},
				}, addressvalidator.Strict).Return(nil, addressErrors.SmartyStreetsLookupError.New())
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			mocks := mocks{
				cache:       mock.NewMockRedisCache(mockCtrl),
				featureflag: mock.NewMockSwitchService(mockCtrl),
				validator:   mock.NewMockValidatorService(mockCtrl),
				engine:      mock.NewMockIStarEngine(mockCtrl),
				transaction: mock.NewMockIStarTransaction(mockCtrl),
				mongoDAO:    mock.NewMockDAO(mockCtrl),
			}
			tt.prepare(&mocks)
			service := AddressServicer{
				serviceConfig:          &ServiceConfig{Oracle: starConfig.OracleConfig{Username: "testUser"}},
				Cache:                  mocks.cache,
				SwitchClient:           mocks.featureflag,
				AddressValidatorClient: mocks.validator,
				OracleDB:               mocks.engine,
				DAO:                    mocks.mongoDAO,
			}
			address, err := service.VerifyAddress(testCtx, tt.args.address)
			assert.Equal(t, tt.want.address, address)
			assert.Equal(t, tt.want.err, err)
		})
	}
}

func TestZipCodeStateCheck(t *testing.T) {
	type args struct {
		zipcodes []string
		states   []string
	}
	type expect struct {
		zipcodesInStates    []string
		zipcodesNotInStates []string
		err                 *addressErrors.ServerError
	}
	type mocks struct {
		mongoDAO         *mock.MockDAO
		addressValidator *mock.MockValidatorService
	}

	type test struct {
		name    string
		input   args
		want    expect
		prepare func(m *mocks)
	}

	tests := []test{
		{
			name: "success - zipcode found in mongo and matches",
			input: args{
				zipcodes: []string{
					"90210",
				},
				states: []string{
					"CA",
				},
			},
			want: expect{
				zipcodesInStates: []string{
					"90210",
				},
				zipcodesNotInStates: nil,
				err:                 nil,
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryStateByZipCode(testCtx, "90210").Return([]*mongo2.ZipcodeRecord{{
					ZipCode: "90210",
					State:   "CA",
				}}, nil)
			},
		},
		{
			name: "success - zipcode found in smartystreets and matches",
			input: args{
				zipcodes: []string{
					"90210",
				},
				states: []string{
					"CA",
				},
			},
			want: expect{
				zipcodesInStates: []string{
					"90210",
				},
				zipcodesNotInStates: nil,
				err:                 nil,
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryStateByZipCode(testCtx, "90210").Return([]*mongo2.ZipcodeRecord{}, nil)
				m.addressValidator.EXPECT().GetZipCodeDetails(testCtx, "90210").Return(
					&addresspb.ZipCodeDetailsResponse{
						ZipCodes: []*addresspb.ZipCodeDetailsResponse_ZipCode{{
							StateAbbreviation: "CA",
						}},
					},
					nil)
			},
		},
		{
			name: "success - invalid zipcode",
			input: args{
				zipcodes: []string{
					"00000",
				},
				states: []string{
					"CA",
				},
			},
			want: expect{
				zipcodesInStates: nil,
				zipcodesNotInStates: []string{
					"00000",
				},
				err: nil,
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryStateByZipCode(testCtx, "00000").Return([]*mongo2.ZipcodeRecord{}, nil)
				m.addressValidator.EXPECT().GetZipCodeDetails(testCtx, "00000").Return(nil, addressErrors.ZipNotFoundError.New())
			},
		},
		{
			name: "success - zipcode found in mongo and doesn't match",
			input: args{
				zipcodes: []string{
					"90210",
				},
				states: []string{
					"AZ",
				},
			},
			want: expect{
				zipcodesInStates: nil,
				zipcodesNotInStates: []string{
					"90210",
				},
				err: nil,
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryStateByZipCode(testCtx, "90210").Return([]*mongo2.ZipcodeRecord{{
					ZipCode: "90210",
					State:   "CA",
				}}, nil)
			},
		},
		{
			name: "success - zipcode found in smartystreets and doesn't match",
			input: args{
				zipcodes: []string{
					"90210",
				},
				states: []string{
					"AZ",
				},
			},
			want: expect{
				zipcodesInStates: nil,
				zipcodesNotInStates: []string{
					"90210",
				},
				err: nil,
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryStateByZipCode(testCtx, "90210").Return([]*mongo2.ZipcodeRecord{}, nil)
				m.addressValidator.EXPECT().GetZipCodeDetails(testCtx, "90210").Return(
					&addresspb.ZipCodeDetailsResponse{
						ZipCodes: []*addresspb.ZipCodeDetailsResponse_ZipCode{{
							StateAbbreviation: "CA",
						}},
					},
					nil)
			},
		},
		{
			name: "failure - mongo error",
			input: args{
				zipcodes: []string{
					"90210",
				},
				states: []string{
					"CA",
				},
			},
			want: expect{
				zipcodesInStates:    nil,
				zipcodesNotInStates: nil,
				err:                 addressErrors.DaoQueryError.New(),
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryStateByZipCode(testCtx, "90210").Return(nil, addressErrors.DaoQueryError.New())
			},
		},
		{
			name: "success - smartystreets error",
			input: args{
				zipcodes: []string{
					"90210",
				},
				states: []string{
					"CA",
				},
			},
			want: expect{
				zipcodesInStates:    nil,
				zipcodesNotInStates: nil,
				err:                 addressErrors.SmartyStreetsLookupError.New(),
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryStateByZipCode(testCtx, "90210").Return([]*mongo2.ZipcodeRecord{}, nil)
				m.addressValidator.EXPECT().GetZipCodeDetails(testCtx, "90210").Return(nil, addressErrors.SmartyStreetsLookupError.New())
			},
		},
		{
			name: "success - zipcode found in mongo",
			input: args{
				zipcodes: []string{
					"90210",
				},
				states: []string{},
			},
			want: expect{
				zipcodesInStates: []string{
					"90210",
				},
				zipcodesNotInStates: nil,
				err:                 nil,
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryStateByZipCode(testCtx, "90210").Return([]*mongo2.ZipcodeRecord{{
					ZipCode: "90210",
					State:   "CA",
				}}, nil)
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			mocks := mocks{
				mongoDAO:         mock.NewMockDAO(mockCtrl),
				addressValidator: mock.NewMockValidatorService(mockCtrl),
			}
			tt.prepare(&mocks)
			service := AddressServicer{
				DAO:                    mocks.mongoDAO,
				AddressValidatorClient: mocks.addressValidator,
			}

			found, notFound, err := service.ZipCodeStateCheck(testCtx, tt.input.zipcodes, tt.input.states)
			assert.Equal(t, tt.want.zipcodesInStates, found)
			assert.Equal(t, tt.want.zipcodesNotInStates, notFound)
			assert.Equal(t, tt.want.err, err)
		})
	}
}

func TestStateZipCodes(t *testing.T) {
	testStates := []string{"CA"}
	testZipCodes := []string{"90001"}
	testZipCodeDocuments := []*mongo2.ZipcodeRecord{
		{
			ZipCode: testZipCodes[0],
			State:   testStates[0],
		},
	}
	type args struct {
		states []string
	}
	type expect struct {
		zipcodes []string
		err      *addressErrors.ServerError
	}
	type mocks struct {
		mongoDAO *mock.MockDAO
	}

	type test struct {
		name    string
		input   args
		want    expect
		prepare func(m *mocks)
	}

	tests := []test{
		{
			name: "success",
			input: args{
				states: testStates,
			},
			want: expect{
				zipcodes: testZipCodes,
				err:      nil,
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryZipCodesByState(testCtx, testStates).Return(testZipCodeDocuments, nil)
			},
		},
		{
			name: "mongo error",
			input: args{
				states: testStates,
			},
			want: expect{
				zipcodes: nil,
				err:      addressErrors.DaoQueryError.New(),
			},
			prepare: func(m *mocks) {
				m.mongoDAO.EXPECT().QueryZipCodesByState(testCtx, testStates).Return(nil, addressErrors.DaoQueryError.New())
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			mocks := mocks{
				mongoDAO: mock.NewMockDAO(mockCtrl),
			}
			tt.prepare(&mocks)
			service := AddressServicer{
				DAO: mocks.mongoDAO,
			}

			zipcodes, err := service.StateZipCodes(testCtx, tt.input.states)
			assert.Equal(t, tt.want.zipcodes, zipcodes)
			assert.Equal(t, tt.want.err, err)
		})
	}
}
