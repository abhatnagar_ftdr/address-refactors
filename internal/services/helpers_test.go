package services

import (
	"encoding/json"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	mongo2 "golang.frontdoorhome.com/software/address/internal/mongo_dal"
	"golang.frontdoorhome.com/software/address/mock"
	addressErrors "golang.frontdoorhome.com/software/address/pkg/errors"
	"testing"
)

const (
	ExternalSourceName          = "EXTERNAL_SOURCE"
	DifferentExternalSourceName = "DIFFERENT_EXTERNAL_SOURCE"
)

func TestAddressServicer_verifyUniqueAddress(t *testing.T) {
	testAddressCopy := testAddress
	type args struct {
		address *mongo2.AddressRecord
	}
	type mockArgs struct {
		redisAddress *mongo2.AddressRecord
		redisErr     *addressErrors.ServerError
		queryAddress *mongo2.AddressRecord
		queryErr     *addressErrors.ServerError
	}
	type expected struct {
		existingAddress *mongo2.AddressRecord
		err             *addressErrors.ServerError
	}
	tests := []struct {
		name string
		args args
		mock mockArgs
		want expected
	}{
		{
			name: "nil input address returns nil pointer error",
			args: args{address: nil},
			mock: mockArgs{},
			want: expected{err: addressErrors.NilPointerError.New()},
		},
		{
			name: "empty DPB returns nil address and nil error",
			args: args{address: &mongo2.AddressRecord{DeliveryPointBarcode: ""}},
			mock: mockArgs{},
			want: expected{existingAddress: nil, err: nil},
		},
		{
			name: "found address in redis returns existing address and address exists error",
			args: args{address: &testAddressCopy},
			mock: mockArgs{redisAddress: &testAddressCopy},
			want: expected{existingAddress: &testAddressCopy, err: addressErrors.AddressExistsError.New()},
		},
		{
			name: "found address in repository with same unit value and type returns existing address and address exists error",
			args: args{address: &testAddressCopy},
			mock: mockArgs{queryAddress: &testAddressCopy},
			want: expected{existingAddress: &testAddressCopy, err: addressErrors.AddressExistsError.New()},
		},
	}

	mockCtrl := gomock.NewController(t)
	mockDao := mock.NewMockDAO(mockCtrl)
	mockCache := mock.NewMockRedisCache(mockCtrl)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			mockEngine := mock.NewMockIStarEngine(mockCtrl)

			// // hook up the session calls for county query
			// // hook up for inserting new address into star
			s := &AddressServicer{
				DAO:      mockDao,
				OracleDB: mockEngine,
				Cache:    mockCache,
			}

			if tt.args.address != nil {
				if tt.args.address.DeliveryPointBarcode != "" {
					mockCache.EXPECT().IsEnabled().Return(true)
					var addressBytes []byte
					if tt.mock.redisAddress != nil {
						addressBytes, _ = json.Marshal(tt.mock.redisAddress)
					}
					key := getAddressDPBCRedisKey(tt.args.address.DeliveryPointBarcode, tt.args.address.UnitType, tt.args.address.UnitValue)
					mockCache.EXPECT().Get(testCtx, key).Return(addressBytes, tt.mock.redisErr)

					if tt.mock.redisAddress == nil {
						mockDao.EXPECT().QueryAddress(testCtx, &mongo2.AddressRecord{
							DeliveryPointBarcode: tt.args.address.DeliveryPointBarcode,
							Street1:              tt.args.address.Street1,
							Street2:              tt.args.address.Street2,
							UnitType:             tt.args.address.UnitType,
							UnitValue:            tt.args.address.UnitValue,
							City:                 tt.args.address.City,
							RecordType:           tt.args.address.RecordType,
							Zip:                  tt.args.address.Zip,
							State:                tt.args.address.State,
							DocVersion:           mongo2.CurrentDocVersion,
						}).Return(tt.mock.queryAddress, nil)
					}
				} else {
					mockDao.EXPECT().QueryAddress(testCtx, &mongo2.AddressRecord{}).Return(nil, nil)
				}
			}

			gotExistingAddress, gotErr := s.verifyUniqueAddress(testCtx, tt.args.address)
			assert.Equal(t, tt.want.existingAddress, gotExistingAddress)
			assert.EqualValues(t, tt.want.err, gotErr)
		})
	}
}

func TestAreExternalSourceListsEqual(t *testing.T) {
	type args struct {
		address1 *mongo2.AddressRecord
		address2 *mongo2.AddressRecord
	}
	tests := []struct {
		name   string
		args   args
		result bool
	}{
		{
			name: "empty",
			args: args{
				address1: &mongo2.AddressRecord{},
				address2: &mongo2.AddressRecord{},
			},
			result: true,
		},
		{
			name: "first legacyIDList not empty",
			args: args{
				address1: &mongo2.AddressRecord{
					LegacyIDList: []uint32{1},
				},
				address2: &mongo2.AddressRecord{},
			},
			result: false,
		},
		{
			name: "second legacyIDList not empty",
			args: args{
				address1: &mongo2.AddressRecord{},
				address2: &mongo2.AddressRecord{
					LegacyIDList: []uint32{1},
				},
			},
			result: false,
		},
		{
			name: "first ExternalSourceList not empty",
			args: args{
				address1: &mongo2.AddressRecord{
					ExternalSourceList: []mongo2.ExternalSource{{
						Name: ExternalSourceName,
						ID:   1,
					}},
				},
				address2: &mongo2.AddressRecord{},
			},
			result: false,
		},
		{
			name: "second ExternalSourceList not empty",
			args: args{
				address1: &mongo2.AddressRecord{},
				address2: &mongo2.AddressRecord{
					ExternalSourceList: []mongo2.ExternalSource{{
						Name: ExternalSourceName,
						ID:   1,
					}},
				},
			},
			result: false,
		},
		{
			name: "LegacyIDList equal",
			args: args{
				address1: &mongo2.AddressRecord{
					LegacyIDList: []uint32{1, 2, 3},
				},
				address2: &mongo2.AddressRecord{
					LegacyIDList: []uint32{1, 2, 3},
				},
			},
			result: true,
		},
		{
			name: "LegacyIDList equal unordered",
			args: args{
				address1: &mongo2.AddressRecord{
					LegacyIDList: []uint32{1, 2, 3},
				},
				address2: &mongo2.AddressRecord{
					LegacyIDList: []uint32{3, 1, 2},
				},
			},
			result: true,
		},
		{
			name: "ExternalSourceList equal",
			args: args{
				address1: &mongo2.AddressRecord{
					ExternalSourceList: []mongo2.ExternalSource{
						{
							Name: ExternalSourceName,
							ID:   1,
						},
						{
							Name: ExternalSourceName,
							ID:   2,
						},
					},
				},
				address2: &mongo2.AddressRecord{
					ExternalSourceList: []mongo2.ExternalSource{
						{
							Name: ExternalSourceName,
							ID:   1,
						},
						{
							Name: ExternalSourceName,
							ID:   2,
						},
					},
				},
			},
			result: true,
		},
		{
			name: "ExternalSourceList equal unordered",
			args: args{
				address1: &mongo2.AddressRecord{
					ExternalSourceList: []mongo2.ExternalSource{
						{
							Name: ExternalSourceName,
							ID:   1,
						},
						{
							Name: ExternalSourceName,
							ID:   2,
						},
					},
				},
				address2: &mongo2.AddressRecord{
					ExternalSourceList: []mongo2.ExternalSource{
						{
							Name: ExternalSourceName,
							ID:   2,
						},
						{
							Name: ExternalSourceName,
							ID:   1,
						},
					},
				},
			},
			result: true,
		},
		{
			name: "LegacyIDList unequal",
			args: args{
				address1: &mongo2.AddressRecord{
					LegacyIDList: []uint32{1, 2, 3},
				},
				address2: &mongo2.AddressRecord{
					LegacyIDList: []uint32{3, 2, 3},
				},
			},
			result: false,
		},
		{
			name: "ExternalSourceList unequal",
			args: args{
				address1: &mongo2.AddressRecord{
					ExternalSourceList: []mongo2.ExternalSource{
						{
							Name: ExternalSourceName,
							ID:   1,
						},
						{
							Name: ExternalSourceName,
							ID:   2,
						},
					},
				},
				address2: &mongo2.AddressRecord{
					ExternalSourceList: []mongo2.ExternalSource{
						{
							Name: DifferentExternalSourceName,
							ID:   3,
						},
						{
							Name: ExternalSourceName,
							ID:   2,
						},
					},
				},
			},
			result: false,
		},
		{
			name: "ExternalSourceList unequal - different name",
			args: args{
				address1: &mongo2.AddressRecord{
					ExternalSourceList: []mongo2.ExternalSource{
						{
							Name: ExternalSourceName,
							ID:   1,
						},
						{
							Name: ExternalSourceName,
							ID:   2,
						},
					},
				},
				address2: &mongo2.AddressRecord{
					ExternalSourceList: []mongo2.ExternalSource{
						{
							Name: DifferentExternalSourceName,
							ID:   1,
						},
						{
							Name: ExternalSourceName,
							ID:   2,
						},
					},
				},
			},
			result: false,
		},
		{
			name: "ExternalSourceList unequal - different id",
			args: args{
				address1: &mongo2.AddressRecord{
					ExternalSourceList: []mongo2.ExternalSource{
						{
							Name: ExternalSourceName,
							ID:   1,
						},
						{
							Name: ExternalSourceName,
							ID:   2,
						},
					},
				},
				address2: &mongo2.AddressRecord{
					ExternalSourceList: []mongo2.ExternalSource{
						{
							Name: ExternalSourceName,
							ID:   3,
						},
						{
							Name: ExternalSourceName,
							ID:   2,
						},
					},
				},
			},
			result: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			res := areExternalSourceListsEqual(tt.args.address1, tt.args.address2)
			assert.Equal(t, tt.result, res)
		})
	}
}
