package services

import (
	"context"
	"golang.frontdoorhome.com/software/address/internal/clients/addressvalidator"

	"golang.frontdoorhome.com/software/address/internal/external_source"

	"go.ftdr.com/go-utils/common/logging"
	"go.ftdr.com/go-utils/instrumentation/v3"
	"golang.frontdoorhome.com/software/address/internal/external_source/star"
	"golang.frontdoorhome.com/software/address/internal/literals"
	"golang.frontdoorhome.com/software/address/internal/mongo_dal"
	addressErrors "golang.frontdoorhome.com/software/address/pkg/errors"
	pb "golang.frontdoorhome.com/software/protos/go/addresspb"
)

// DeleteLegacyID will attempt to delete the legacyID from the address at the ID
func (s *AddressServicer) DeleteLegacyID(
	ctx context.Context,
	id string,
	legacyID uint32, source pb.ExternalSource) (err *addressErrors.ServerError) {
	fields := map[string]interface{}{"AddressID": id, "LegacyID": legacyID, "source": source.String()}
	log := logging.GetTracedLogEntry(ctx)
	log = log.WithFields(fields)
	log.Info()
	spanID := instrumentation.ComponentSpanStart(ctx, literals.DeleteLegacyID, literals.ComponentTypeAddressService)
	if legacyID == 0 {
		e := addressErrors.InvalidLegacyIDError.NewFormatted(legacyID)
		fields["Error"] = e.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return e
	}

	if id == "" {
		e := addressErrors.InvalidIDError.NewFormatted(id)
		fields["Error"] = e.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return e
	}
	address, err := s.GetAddressByID(ctx, id)
	if err != nil {
		log.WithError(err).Warn("ADDRESS-DELETE-ERROR: DeleteLegacyID failed for GetAddressRecordByID")
		fields["Error"] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return err
	}
	var (
		updateCount int64
		updateErr   *addressErrors.ServerError
	)
	if source == pb.ExternalSource_UNKNOWN_SOURCE {
		if len(address.LegacyIDList) > 0 {
			updateCount, updateErr = s.DAO.DeleteLegacyID(ctx, id, legacyID)
		}
	} else {
		if len(address.ExternalSourceList) > 0 {
			name := external_source.ExternalSourcePBName(source)
			updateCount, updateErr = s.DAO.DeleteExternalSourceID(ctx, id, name, legacyID)
		}
	}
	if updateErr != nil {
		fields["Error"] = updateErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return updateErr
	} else if updateCount == 0 {
		err = addressErrors.LegacyIDNotFoundError.NewFormatted(legacyID)
		log.Info()
		fields["Error"] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return err
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return nil
}

// DeleteAllLegacyID will attempt to delete the legacyID from all documents
func (s *AddressServicer) DeleteAllLegacyID(
	ctx context.Context,
	legacyID uint32, source pb.ExternalSource) (err *addressErrors.ServerError) {
	fields := map[string]interface{}{"LegacyID": legacyID}
	log := logging.GetTracedLogEntry(ctx)
	log = log.WithFields(fields)
	log.Info()
	spanID := instrumentation.ComponentSpanStart(ctx, literals.DeleteAllLegacyID, literals.ComponentTypeAddressService)
	if legacyID == 0 {
		e := addressErrors.InvalidLegacyIDError.NewFormatted(legacyID)
		fields["Error"] = e.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return e
	}
	var (
		updateCount int64
		updateErr   *addressErrors.ServerError
	)
	if source == pb.ExternalSource_UNKNOWN_SOURCE {
		fields["source"] = pb.ExternalSource_UNKNOWN_SOURCE.String()
		updateCount, updateErr = s.DAO.DeleteAllLegacyID(ctx, legacyID)
	} else {
		name := external_source.ExternalSourcePBName(source)
		fields["source"] = name
		updateCount, updateErr = s.DAO.DeleteAllExternalSourceID(ctx, name, legacyID)
	}
	if updateErr != nil {
		fields["Error"] = updateErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return updateErr
	} else if updateCount == 0 {
		err = addressErrors.LegacyIDNotFoundError.NewFormatted(legacyID)
		log.Info()
		fields["Error"] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return err
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return nil
}

// GetAddressRecord will return a validated address by search terms
// when address is not present in mongo collection it will be validated using external source
// and saved in mongo and starDB
func (s *AddressServicer) GetAddressRecord(ctx context.Context, lookup *mongo_dal.AddressRecord, source pb.ExternalSource) (address *mongo_dal.AddressRecord, err *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)
	fields := map[string]interface{}{"lookup": lookup, "source": source}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.GetAddressRecord, literals.ComponentTypeAddressService)
	address, err = s.DAO.QueryAddress(ctx, lookup)
	if err != nil {
		log.WithError(err).
			Warn("ADDRESS-MONGO-ERROR: GetAddressRecord returned an error")
		fields["Error"] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, err
	}

	if address != nil &&
		(address.DocVersion == mongo_dal.CurrentDocVersion || address.Status == mongo_dal.UnverifiedStatus) {
		log.Info("found matching address without calling Smarty")
		updateAddress := *address
		updateAddress.LegacyIDList = lookup.LegacyIDList
		updateAddress.ExternalSourceList = lookup.ExternalSourceList
		address, err = s.updateAddress(ctx, address, &updateAddress, source)
		if err != nil {
			log.WithError(err).Error("updateAddress failed")
			fields["Error"] = err.Error()
			instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
			return nil, err
		}
		instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
		return address, nil
	}

	// The address was not found by the system, so check SmartyStreets
	address, err = s.upsertAddressFromSmartyStreets(ctx, lookup, source)
	if err != nil {
		fields[literals.LLErrorMessage] = err.Error()
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, (err == nil), fields)
	return address, err
}

// GetAddressRecordByID searches for an existing address by ID, if there is a query error or the record isn't found an error is returned
func (s *AddressServicer) GetAddressRecordByID(
	ctx context.Context,
	id string) (address *mongo_dal.AddressRecord, err *addressErrors.ServerError) {

	fields := map[string]interface{}{"AddressID": id}
	log := logging.GetTracedLogEntry(ctx)
	log = log.WithFields(fields)
	log.Info()
	spanID := instrumentation.ComponentSpanStart(ctx, literals.GetAddressRecordByID, literals.ComponentTypeAddressService)
	if id == "" {
		e := addressErrors.InvalidIDError.NewFormatted(id)
		fields["Error"] = e.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, e
	}

	addressRecord, queryErr := s.GetAddressByID(ctx, id)
	if queryErr != nil {
		log.WithError(queryErr).Warn("ADDRESS-QUERY-ERROR: GetAddressRecordByID failed")
		fields["Error"] = queryErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, queryErr
	}
	if addressRecord.DocVersion == mongo_dal.CurrentDocVersion || addressRecord.Status == mongo_dal.UnverifiedStatus {
		instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
		return addressRecord, nil
	}
	//The address is in the old version, let's try to update the document
	address, err = s.upsertAddressFromSmartyStreets(ctx, &mongo_dal.AddressRecord{
		DeliveryPointBarcode: addressRecord.DeliveryPointBarcode,
		Street1:              addressRecord.Street1,
		Street2:              addressRecord.Street2,
		City:                 addressRecord.City,
		DefaultCity:          addressRecord.DefaultCity,
		Zip:                  addressRecord.Zip,
		ZipLocal:             addressRecord.ZipLocal,
	}, pb.ExternalSource_UNKNOWN_SOURCE)

	instrumentation.ComponentSpanEnd(ctx, spanID, (err == nil), fields)
	return address, err

}

// GetAddressByLegacyID searches for an existing address by its unique Star ID
func (s *AddressServicer) GetAddressByLegacyID(
	ctx context.Context,
	legacyID uint32, source pb.ExternalSource) (addressRecord *mongo_dal.AddressRecord, err *addressErrors.ServerError) {

	fields := map[string]interface{}{"LegacyID": legacyID}
	log := logging.GetTracedLogEntry(ctx)
	log = log.WithFields(fields)
	log.Info()
	spanID := instrumentation.ComponentSpanStart(ctx, literals.GetAddressByLegacyID, literals.ComponentTypeAddressService)
	if legacyID == 0 {
		e := addressErrors.InvalidLegacyIDError.NewFormatted(legacyID)
		fields["Error"] = e.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, e
	}
	if source == pb.ExternalSource_UNKNOWN_SOURCE {
		fields["source"] = pb.ExternalSource_UNKNOWN_SOURCE.String()
		addressRecord, err = s.DAO.QueryAddressByLegacyID(ctx, legacyID)
	} else {
		sourceName := external_source.ExternalSourcePBName(source)
		fields["source"] = sourceName
		addressRecord, err = s.DAO.QueryAddressByExternalSourceID(ctx, sourceName, legacyID)
	}
	if err != nil {
		log.WithError(err).Warn("ADDRESS-QUERY-ERROR: GetAddressByLegacyID failed")
		fields["Error"] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, err
	}

	if addressRecord != nil {
		instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
		return addressRecord, nil
	} else if !s.isStarIntegrationEnabled(ctx) {
		e := addressErrors.LegacyIDNotFoundError.NewFormatted(legacyID)
		log.Info(e.Error())
		fields["Error"] = e.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, e
	}

	// if the address is not found in mongo try pulling the address from property

	addressRecord, err = s.getAddressFromExternalSource(ctx, legacyID, source)
	if err != nil {
		log.WithError(err).Warnf("failed to find address in star db with property id %d", legacyID)
		fields["Error"] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, err
	}
	// We don't want to pass a ZZ unit type to Smarty if there is no unit value
	if addressRecord.UnitType == "ZZ" && addressRecord.UnitValue == "" {
		addressRecord.UnitType = ""
	}
	smartyAddress, err := s.validateAddress(ctx, addressRecord)
	if err != nil && err.Code != addressErrors.AddressNotVerifiableError.Code {
		log.WithError(err).Warnf("failed to validate address")
		fields["Error"] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, err
	}

	if smartyAddress == nil {
		smartyAddress = addressRecord
	} else {
		smartyAddress.LegacyIDList = addressRecord.LegacyIDList
		smartyAddress.ExternalSourceList = addressRecord.ExternalSourceList
	}

	addressRecord, err = s.upsertAddress(ctx, smartyAddress, source)
	if err != nil {
		log.WithError(err).Warnf("failed to upsert address")
		fields["Error"] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, err
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return addressRecord.Format(), nil
}

// GetZipCodeDetails returns details for a passed in zip code
func (s *AddressServicer) GetZipCodeDetails(
	ctx context.Context, zip string) (zipResponse *pb.ZipCodeDetailsResponse, err *addressErrors.ServerError) {
	// for now we cannot use Experian to get zip code details
	// once the endpoint is implemented we should use AddressValidatorClient instead of SmartyStreetsClient
	fields := map[string]interface{}{"zip": zip}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.GetZipCodeDetails, literals.ComponentTypeAddressService)
	if zip == "" || len(zip) == 0 {
		e := addressErrors.InvalidZipCodeError.NewFormatted(zip)
		fields["Error"] = e.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, e
	}

	if !s.readZipcodeFromMongo(ctx) {
		instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
		return s.SmartyStreetsClient.GetZipCodeDetails(ctx, zip)
	}
	zipcodeDetails, err := s.DAO.QueryStateByZipCode(ctx, zip)
	if err != nil {
		fields["Error"] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, err
	}
	if len(zipcodeDetails) == 0 {
		zipErr := addressErrors.ZipNotFoundError.NewFormatted("Mongo", zip)
		return nil, zipErr
	}
	zipResponse = &pb.ZipCodeDetailsResponse{
		ZipCodes:   []*pb.ZipCodeDetailsResponse_ZipCode{},
		CityStates: []*pb.ZipCodeDetailsResponse_CityState{},
	}
	for _, zipcode := range zipcodeDetails {
		zipResponse.ZipCodes = append(zipResponse.ZipCodes, &pb.ZipCodeDetailsResponse_ZipCode{
			ZipCode:           zipcode.ZipCode,
			DefaultCity:       zipcode.City,
			CountyName:        "",
			State:             literals.StateAbbreviations[zipcode.State],
			StateAbbreviation: zipcode.State,
			Latitude:          zipcode.Latitude,
			Longitude:         zipcode.Longitude,
		})
		zipResponse.CityStates = append(zipResponse.CityStates, &pb.ZipCodeDetailsResponse_CityState{
			City:              zipcode.City,
			State:             literals.StateAbbreviations[zipcode.State],
			StateAbbreviation: zipcode.State,
		})
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return zipResponse, nil
}

// GetTypeAheadSuggestions searches for address suggestions by search terms
func (s *AddressServicer) GetTypeAheadSuggestions(ctx context.Context, getTypeAheadRequest *pb.AddressByTypeAheadRequest) (suggestions []*pb.TypeAheadAddress) {
	s.UseAddressValidatorDefinedByFeatureFlag(ctx)
	return s.AddressValidatorClient.GetTypeaheadSuggestions(ctx, getTypeAheadRequest, int(s.serviceConfig.LookaheadLimit))
}

// LogCollectionStats queries the db for the count of all documents in all collections and logs the collection to document count
func (s *AddressServicer) LogCollectionStats(ctx context.Context) *addressErrors.ServerError {

	fields := map[string]interface{}{}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.LogCollectionStats, literals.ComponentTypeAddressService)
	collectionCounts, err := s.DAO.QueryCollectionsStats(ctx)
	if err != nil {
		return err
	}

	for collection, docCount := range collectionCounts {
		fields[collection] = docCount
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return nil
}

// LogFilterStats queries the db for the count of documents matching a filter in a collection and logs the result
func (s *AddressServicer) LogFilterStats(
	ctx context.Context,
	collection string,
	filter map[string]interface{}) *addressErrors.ServerError {
	log := logging.GetTracedLogEntry(ctx)
	fields := map[string]interface{}{}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.LogFilterStats, literals.ComponentTypeAddressService)
	collectionCounts, err := s.DAO.QueryFilterStats(ctx, collection, filter)
	if err != nil {
		fields["Error"] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return err
	}

	fields["Collection"] = collection
	fields["Filter"] = filter
	fields["DocumentCount"] = collectionCounts
	log.WithFields(fields).Info()
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return nil
}

// PostLegacyID adds the passed legacy id to the address
func (s *AddressServicer) PostLegacyID(ctx context.Context, id string, legacyID uint32, source pb.ExternalSource) (err *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)
	fields := map[string]interface{}{"ID": id, "LegacyID": legacyID, "source": source}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.PostLegacyID, literals.ComponentTypeAddressService)
	if legacyID == 0 {
		e := addressErrors.InvalidLegacyIDError.NewFormatted(legacyID)
		fields["Error"] = e.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return e
	}

	if id == "" {
		e := addressErrors.InvalidIDError.NewFormatted(id)
		fields["Error"] = e.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return e
	}

	var addressRecord *mongo_dal.AddressRecord
	addressRecord, err = s.GetAddressByID(ctx, id)
	if err != nil {
		log.WithError(err).Warn("ADDRESS-POST-ERROR: PostLegacyID failed")
		fields["Error"] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return err
	}

	if source == pb.ExternalSource_UNKNOWN_SOURCE {
		instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
		return s.InsertLegacyID(ctx, id, addressRecord, legacyID)
	} else {
		name := external_source.ExternalSourcePBName(source)
		fields["sourceName"] = name
		instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
		return s.InsertExternalSourceID(ctx, id, addressRecord, name, legacyID)
	}
}

func (s *AddressServicer) InsertLegacyID(ctx context.Context, uuid string, addressRecord *mongo_dal.AddressRecord, id uint32) (err *addressErrors.ServerError) {
	fields := map[string]interface{}{"LegacyID": id, "uuid": uuid}
	log := logging.GetTracedLogEntry(ctx)
	log = log.WithFields(fields)
	log.Info()
	spanID := instrumentation.ComponentSpanStart(ctx, literals.InsertLegacyID, literals.ComponentTypeAddressService)
	for _, currentID := range addressRecord.LegacyIDList {
		if currentID == id {
			existingID, _ := mongo_dal.BsonUUIDToRaw(ctx, addressRecord.UUID.Data)
			log.Info("id already exists in address record")
			fields["currentLegacyID"] = currentID
			fields["existingLegacyID"] = id
			fields["addressExistingUUID"] = existingID
			instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
			return addressErrors.LegacyIDAlreadyExistsError.NewFormatted(id, existingID)
		}
	}
	existingLegacyIDAddress, queryErr := s.DAO.QueryAddressByLegacyID(ctx, id)
	if queryErr != nil {
		fields["Error"] = queryErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return queryErr
	}

	if existingLegacyIDAddress != nil {
		existingID, _ := mongo_dal.BsonUUIDToRaw(ctx, existingLegacyIDAddress.UUID.Data)
		fields["ExistingUUID"] = existingID
		log.Info("id already exists in another address record")
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return addressErrors.LegacyIDAlreadyExistsError.NewFormatted(id, existingID)
	}
	starTransactionEndType := star.Commit
	if s.isStarIntegrationEnabled(ctx) {
		transaction, err := s.OracleDB.StartTransaction(ctx, s.serviceConfig.Oracle.Username, 0)
		if err != nil {
			log.Info(addressErrors.NoRecordsUpdatedError.Error())
			fields["Error"] = err.Error()
			instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
			return addressErrors.StarTransactionError.New()
		}
		// if the post update is succeeds, we do the star update here
		transaction.UpdatePropertyWithAddressInStarDB(ctx, addressRecord, id)
		defer transaction.HandleEndOfTransaction(ctx, &starTransactionEndType)
	}

	updateCount, updateErr := s.DAO.InsertLegacyID(ctx, uuid, id)

	if updateErr != nil {
		fields["Error"] = updateErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return updateErr
	} else if updateCount == 0 {
		starTransactionEndType = star.Rollback
		log.Info(addressErrors.NoRecordsUpdatedError.Error())
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return addressErrors.NoRecordsUpdatedError.New()
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return nil
}

func (s *AddressServicer) InsertExternalSourceID(ctx context.Context, uuid string, addressRecord *mongo_dal.AddressRecord, name string, id uint32) (err *addressErrors.ServerError) {
	fields := map[string]interface{}{"LegacyID": id, "uuid": uuid, "name": name}
	log := logging.GetTracedLogEntry(ctx)
	log = log.WithFields(fields)
	log.Info()
	spanID := instrumentation.ComponentSpanStart(ctx, literals.InsertExternalSourceID, literals.ComponentTypeAddressService)
	for _, currentID := range addressRecord.ExternalSourceList {
		if currentID.ID == id && currentID.Name == name {
			existingID, _ := mongo_dal.BsonUUIDToRaw(ctx, addressRecord.UUID.Data)
			log.Info("legacyID already exists in address record")
			fields["existingLegacyID"] = id
			fields["addressUUID"] = existingID
			instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
			return addressErrors.LegacyIDAlreadyExistsError.NewFormatted(id, existingID)
		}
	}
	existingLegacyIDAddress, queryErr := s.DAO.QueryAddressByExternalSourceID(ctx, name, id)
	if queryErr != nil {
		fields["Error"] = queryErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return queryErr
	}

	if existingLegacyIDAddress != nil {
		existingID, _ := mongo_dal.BsonUUIDToRaw(ctx, existingLegacyIDAddress.UUID.Data)
		fields["ExistingUUID"] = existingID
		log.Info("legacyID already exists in another address record")
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return addressErrors.LegacyIDAlreadyExistsError.NewFormatted(id, existingID)
	}

	updateCount, updateErr := s.DAO.InsertExternalSourceID(ctx, uuid, name, id)

	if updateErr != nil {
		fields["Error"] = updateErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return updateErr
	} else if updateCount == 0 {
		log.Info(addressErrors.NoRecordsUpdatedError.Error())
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return addressErrors.NoRecordsUpdatedError.New()
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return nil
}

// PostUnverifiedAddress first searches for an address in the db, then in SmartyStreets, then inserts into db
func (s *AddressServicer) PostUnverifiedAddress(ctx context.Context, address *mongo_dal.AddressRecord, source pb.ExternalSource, force bool) (addressRecord *mongo_dal.AddressRecord, err *addressErrors.ServerError) {
	address.Format()
	fields := map[string]interface{}{"Search": address, "source": source}
	log := logging.GetTracedLogEntry(ctx)
	log = log.WithFields(fields)
	spanID := instrumentation.ComponentSpanStart(ctx, literals.PostUnverifiedAddress, literals.ComponentTypeAddressService)
	log.Info("POST-UNVERIFIED-ADDRESS-ERROR: executing post unverified address")
	existingAddressMongo, err := s.DAO.QueryAddress(ctx, address)
	if err != nil {
		fields["Error"] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, err
	}

	// The address exists in mongo and has the current version of the document
	if existingAddressMongo != nil && existingAddressMongo.DocVersion == mongo_dal.CurrentDocVersion &&
		existingAddressMongo.IsEquivalent(ctx, address) {
		if existingAddressMongo.Status == mongo_dal.VerifiedStatus {
			log.WithField("Unverified", "Mongo").Infof("Unverified address found in MongoDB.")
			existingAddressID, err := mongo_dal.BsonUUIDToRaw(ctx, existingAddressMongo.UUID.Data)
			if err != nil {
				log.WithError(err).Error("POST-UNVERIFIED-ADDRESS-ERROR: failed to convert binary to ID")
				existingAddressID = "failed to convert"
				fields["Error"] = err.Error()
			}
			instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
			return nil, addressErrors.VerifiableAddressError.NewFormatted(existingAddressID)
		}
		address.Status = mongo_dal.UnverifiedStatus
		address.DocVersion = mongo_dal.CurrentDocVersion
		address, err = s.updateAddress(ctx, existingAddressMongo, address, source)
		if err != nil {
			log.WithError(err).Error("updateAddress failed")
			fields["Error"] = err.Error()
			instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
			return nil, err
		}
		instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
		return address, nil
	}

	if !force {
		//Try to find an address by using  smarty street API first
		existingAddressSmarty, err := s.validateAddress(ctx, address)
		if err == nil {
			existingAddressSmarty, err = s.upsertAddress(ctx, existingAddressSmarty, source)
			isVerifiable, _ := s.AddressValidatorClient.IsVerifiable(ctx, existingAddressSmarty, addressvalidator.Strict)
			if isVerifiable {
				log.WithField("Unverified", "SmartyStreets").Infof("Unverified address found in SmartyStreets.")
				existingAddressID, err := mongo_dal.BsonUUIDToRaw(ctx, existingAddressSmarty.UUID.Data)
				if err != nil {
					log.WithError(err).Error("failed to convert binary to ID")
					existingAddressID = "failed to convert"
					fields["Error"] = err.Error()
				}
				instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
				return nil, addressErrors.VerifiableAddressError.NewFormatted(existingAddressID)
			}
		} else if err.Code != addressErrors.AddressNotVerifiableError.Code {
			log.WithError(err).Warn("POST-UNVERIFIED-ADDRESS-ERROR: smartystreets API call failed")
		}
	}

	//following code will run only when upsertAddressFromSmartyStreets returns no address,
	//meaning no star/mongo operations were done and integrity is maintained
	address.Status = mongo_dal.UnverifiedStatus
	address.DocVersion = mongo_dal.CurrentDocVersion

	if existingAddressMongo == nil {
		addressID, err := mongo_dal.GenerateUUID(ctx)
		if err != nil {
			log.Warn("failed to generate ID for address, abandoning storage")
			fields[literals.LLErrorMessage] = err.Error()
			instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
			return nil, addressErrors.DaoUUIDGenerationError.New()
		}
		address.UUID = *addressID
	} else {
		address.UUID = existingAddressMongo.UUID
		address.LegacyIDList = existingAddressMongo.LegacyIDList
		address.ExternalSourceList = existingAddressMongo.ExternalSourceList
	}

	var transaction external_source.ITransaction
	if s.isStarIntegrationEnabled(ctx) {
		transaction, err = s.upsertEntryInExternalSource(ctx, address, source)
		if err != nil {
			if err.IsServerFault() {
				log.WithError(err).Error("failed to register new address on property in star db from post unverified address")
			} else {
				log.WithError(err).Warn("failed to register new address on property in star db from post unverified address")
			}
			fields[literals.LLErrorMessage] = err.Error()
			instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
			return nil, err
		}
	}
	if existingAddressMongo != nil {
		_, err = s.DAO.UpdateAddressWithArchiving(ctx, existingAddressMongo, address)
		if err != nil {
			if transaction != nil {
				transaction.Rollback(ctx)
			}
			fields[literals.LLErrorMessage] = err.Error()
			instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
			return nil, addressErrors.DaoUpdateError.New()
		}
	} else {
		err = s.DAO.InsertAddress(ctx, address)
		if err != nil {
			if transaction != nil {
				transaction.Rollback(ctx)
			}
			fields[literals.LLErrorMessage] = err.Error()
			instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
			return nil, err
		}
	}

	if transaction != nil {
		transaction.Commit(ctx)
	}
	log.WithField("Unverified", "Added").Infof("unverified address added to MongoDB")
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return address, nil
}

// QueryAddressID returns no error for an id that exists in DAO, otherwise an error will exist
func (s *AddressServicer) QueryAddressID(ctx context.Context, id string) (err *addressErrors.ServerError) {
	fields := map[string]interface{}{"AddressID": id}
	log := logging.GetTracedLogEntry(ctx)
	log = log.WithFields(fields)
	log.Info()
	spanID := instrumentation.ComponentSpanStart(ctx, literals.QueryAddressID, literals.ComponentTypeAddressService)
	if id == "" {
		e := addressErrors.InvalidIDError.NewFormatted(id)
		fields["Error"] = e.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return e
	}

	addressRecord, queryErr := s.DAO.QueryAddressByID(ctx, id)
	if queryErr != nil {
		log.WithError(queryErr).Warn("ADDRESS-QUERY-ERROR: QueryAddressID failed")
		fields["Error"] = queryErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return queryErr
	}
	if addressRecord == nil {
		err = addressErrors.AddressIDNotFoundError.NewFormatted(id)
		log.Info(err.Error())
		fields["Error"] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return err
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return nil
}

func (s *AddressServicer) VerifyAddress(ctx context.Context, address *mongo_dal.AddressRecord) (*mongo_dal.AddressRecord, *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)
	fields := map[string]interface{}{"address": address}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.VerifyAddress, literals.ComponentTypeAddressService)
	addressRecord, err := s.validateAddress(ctx, address)

	if err != nil {
		log.Infof("verifying address failed with error code %s and message %s", err.Code, err.Message)
		fields["Error"] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, err
	}

	isVerifiable, _ := s.AddressValidatorClient.IsVerifiable(ctx, addressRecord, addressvalidator.Strict)
	if !isVerifiable {
		log.Info("address is not verifiable")
		instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
		return nil, addressErrors.AddressNotVerifiableError.New()
	}

	transaction, transactionErr := s.OracleDB.StartTransaction(ctx, s.serviceConfig.Oracle.Username, 0)
	if transactionErr != nil {
		fields["Error"] = transactionErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, addressErrors.StarTransactionError.New()
	}
	if s.isStarIntegrationEnabled(ctx) {
		if len(address.LegacyIDList) > 0 {
			id := address.LegacyIDList[len(address.LegacyIDList)-1]
			fields["updateID"] = id
			transaction.UpdatePropertyWithAddressInStarDB(ctx, addressRecord, id)
		}
	}
	addressRecord.LegacyIDList = address.LegacyIDList
	addressRecord.ExternalSourceList = address.ExternalSourceList
	updatedAddress, err := s.DAO.UpdateAddressWithArchiving(ctx, address, addressRecord)
	if err != nil {
		log.Infof("updating address in mongo failed with error code %s and message %s", err.Code, err.Message)
		transaction.Rollback(ctx)
		fields["Error"] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, err
	}
	transaction.Commit(ctx)
	return updatedAddress, nil
}

func (s *AddressServicer) ZipCodeStateCheck(ctx context.Context, zipcodes []string, states []string) ([]string, []string, *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)
	fields := map[string]interface{}{"zipcodes": zipcodes, "states": states}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.ZipCodeStateCheck, literals.ComponentTypeAddressService)
	var matched []string
	var notMatched []string

	foundStatesInGivenStates := func(foundStates, givenStates []string) bool {
		if len(foundStates) == 0 {
			return false
		}
		if len(givenStates) == 0 {
			// empty givenStates list means we match anything
			return true
		}
		for _, givenState := range givenStates {
			for _, foundState := range foundStates {
				if givenState == foundState {
					return true
				}
			}
		}
		return false
	}

	for _, zipcode := range zipcodes {
		foundStates, err := s.getStatesForZipcode(ctx, zipcode)
		if err != nil {
			log.WithError(err).Errorf("get states for zipcode %s failed", zipcode)
			fields["zipcode"] = zipcode
			fields["Error"] = err.Error()
			instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
			return nil, nil, err
		}
		if foundStatesInGivenStates(foundStates, states) {
			matched = append(matched, zipcode)
		} else {
			notMatched = append(notMatched, zipcode)
		}
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return matched, notMatched, nil
}

func (s *AddressServicer) StateZipCodes(ctx context.Context, states []string) ([]string, *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)
	zipcodes := []string{}
	fields := map[string]interface{}{"states": states}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.StateZipCodes, literals.ComponentTypeAddressService)
	zipcodeDocs, err := s.DAO.QueryZipCodesByState(ctx, states)
	if err != nil {
		log.WithError(err).Error("get zipcodes failed")
		fields["Error"] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, err
	}

	for _, zipcodeDoc := range zipcodeDocs {
		zipcodes = append(zipcodes, zipcodeDoc.ZipCode)
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return zipcodes, nil
}
