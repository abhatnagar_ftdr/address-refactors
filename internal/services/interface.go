package services

import (
	"context"

	"golang.frontdoorhome.com/software/address/internal/mongo_dal"

	addressErrors "golang.frontdoorhome.com/software/address/pkg/errors"
	pb "golang.frontdoorhome.com/software/protos/go/addresspb"
)

// AddressService is the interface for interacting with the business layer
type AddressService interface {
	DeleteLegacyID(ctx context.Context, id string, legacyID uint32, source pb.ExternalSource) (err *addressErrors.ServerError)

	DeleteAllLegacyID(ctx context.Context, legacyID uint32, source pb.ExternalSource) (err *addressErrors.ServerError)

	GetAddressRecord(ctx context.Context, lookup *mongo_dal.AddressRecord, source pb.ExternalSource) (address *mongo_dal.AddressRecord, err *addressErrors.ServerError)

	GetAddressRecordByID(ctx context.Context, id string) (address *mongo_dal.AddressRecord, err *addressErrors.ServerError)

	GetAddressByLegacyID(ctx context.Context, legacyID uint32, source pb.ExternalSource) (addressRecord *mongo_dal.AddressRecord, err *addressErrors.ServerError)

	GetZipCodeDetails(ctx context.Context, zip string) (zipResponse *pb.ZipCodeDetailsResponse, err *addressErrors.ServerError)

	GetTypeAheadSuggestions(ctx context.Context, getTypeAheadRequest *pb.AddressByTypeAheadRequest) (suggestions []*pb.TypeAheadAddress)

	LogCollectionStats(ctx context.Context) *addressErrors.ServerError

	LogFilterStats(ctx context.Context, collection string, filter map[string]interface{}) *addressErrors.ServerError

	PostLegacyID(ctx context.Context, id string, legacyID uint32, source pb.ExternalSource) (err *addressErrors.ServerError)

	PostUnverifiedAddress(ctx context.Context, address *mongo_dal.AddressRecord, source pb.ExternalSource, force bool) (addressRecord *mongo_dal.AddressRecord, err *addressErrors.ServerError)

	QueryAddressID(ctx context.Context, id string) (err *addressErrors.ServerError)

	VerifyAddress(ctx context.Context, address *mongo_dal.AddressRecord) (*mongo_dal.AddressRecord, *addressErrors.ServerError)

	ZipCodeStateCheck(ctx context.Context, zipcodes []string, states []string) ([]string, []string, *addressErrors.ServerError)

	StateZipCodes(ctx context.Context, state []string) ([]string, *addressErrors.ServerError)
}
