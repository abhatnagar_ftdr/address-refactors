package services

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"sort"
	"time"

	"go.mongodb.org/mongo-driver/mongo"

	b64 "encoding/base64"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.frontdoorhome.com/software/address/internal/external_source"
	"golang.frontdoorhome.com/software/address/internal/mongo_dal"

	"go.ftdr.com/go-utils/common/logging"
	"go.ftdr.com/go-utils/instrumentation/v3"
	"golang.frontdoorhome.com/software/address/internal/clients/addressvalidator"
	"golang.frontdoorhome.com/software/address/internal/clients/featureflag"
	"golang.frontdoorhome.com/software/address/internal/literals"
	"golang.frontdoorhome.com/software/address/internal/repository"
	addressErrors "golang.frontdoorhome.com/software/address/pkg/errors"
	pb "golang.frontdoorhome.com/software/protos/go/addresspb"
)

/*
 * MULTI-CLIENT HELPERS - helpers that span multiple clients (Redis, Mongo, SmartyStreets, etc.)
 */

// upsertAddressFromSmartyStreets will use a lookup to query smarty streets. If the address is found in smarty streets, verify it is unique.
// If the address is unique, insert the new record and return. If the address is not unique, compare for differences and update the found record and
// archive for history if flagged for archive
func (s *AddressServicer) upsertAddressFromSmartyStreets(
	ctx context.Context,
	lookup *mongo_dal.AddressRecord, source pb.ExternalSource) (address *mongo_dal.AddressRecord, err *addressErrors.ServerError) {

	fields := map[string]interface{}{
		"lookup": lookup,
		"source": source,
	}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.UpsertAddressFromSmartyStreets, literals.ComponentTypeAddressService)
	addressRecord, err := s.validateAddress(ctx, lookup)
	if err != nil {
		fields["Error"] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, err
	}

	addressRecord.LegacyIDList = lookup.LegacyIDList
	addressRecord.ExternalSourceList = lookup.ExternalSourceList

	address, err = s.upsertAddress(ctx, addressRecord, source)
	if err != nil {
		fields["Error"] = err.Error()
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, (err == nil), fields)
	return address, err
}

// upsertAddress upserts address in mongo and external source (PROPERTY or COMMERCIAL_PROPERTY table in star)
func (s *AddressServicer) upsertAddress(
	ctx context.Context,
	addressRecord *mongo_dal.AddressRecord,
	source pb.ExternalSource) (address *mongo_dal.AddressRecord, err *addressErrors.ServerError) {
	spanID := instrumentation.ComponentSpanStart(
		ctx,
		literals.UpsertAddress,
		literals.ComponentTypeAddressService)

	componentContextData := map[string]interface{}{}

	if existingAddress, err := s.verifyUniqueAddress(ctx, addressRecord); err != nil {
		componentContextData["Search"] = map[string]string{
			"Street1":   addressRecord.Street1,
			"UnitType":  addressRecord.UnitType,
			"UnitValue": addressRecord.UnitValue,
			"City":      addressRecord.City,
			"State":     addressRecord.State,
			"County":    addressRecord.County,
			"Zip":       addressRecord.Zip,
			"ZipLocal":  addressRecord.ZipLocal,
		}
		if err.Code != addressErrors.AddressExistsError.Code {
			componentContextData["Error"] = err.Error()
			instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
			return nil, err
		}
		address, err = s.updateAddress(ctx, existingAddress, addressRecord, source)
		if err != nil {
			componentContextData["Error"] = err.Error()
			instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
			return nil, err
		}
		instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
		return address, nil
	}
	address, err = s.insertAddress(ctx, addressRecord, source)
	if err != nil {
		componentContextData["Error"] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return nil, err
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return address, nil
}

// updateAddress updates address in mongo db and based on source parameter upserts entry in external source
func (s *AddressServicer) updateAddress(
	ctx context.Context,
	existingAddress, addressRecord *mongo_dal.AddressRecord,
	source pb.ExternalSource) (address *mongo_dal.AddressRecord, serr *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)

	spanID := instrumentation.ComponentSpanStart(
		ctx,
		literals.UpdateAddress,
		literals.ComponentTypeAddressService)

	componentContextData := map[string]interface{}{
		"address":         fmt.Sprintf("%#v", addressRecord),
		"existingAddress": fmt.Sprintf("%v", existingAddress),
		"source":          source,
	}

	var addressRecordsExternalSources []mongo_dal.ExternalSource
	if addressRecord.IsExternalSourcePresent(pb.ExternalSource_COMMERCIAL_PROPERTY) {
		// save addressRecord.ExternalSourceList before combining with existingAddress.ExternalSourceList
		// so to run DeleteAllExternalSourceID only on IDs added with UpdateAddress
		addressRecordsExternalSources = addressRecord.ExternalSourceList
	}
	addressRecord.LegacyIDList = append(addressRecord.LegacyIDList, existingAddress.LegacyIDList...)
	if addressRecord.IsExternalSourcePresent(pb.ExternalSource_COMMERCIAL_PROPERTY) || source == pb.ExternalSource_COMMERCIAL_PROPERTY {
		// in case of source being set to COMMERCIAL_PROPERTY or user passing COMMERCIAL_PROPERTY id
		// per vendor management request we should delete all old COMMERCIAL_PROPERTY ids from document
		commercialPropertyName := external_source.ExternalSourcePBName(pb.ExternalSource_COMMERCIAL_PROPERTY)
		for _, externalSource := range existingAddress.ExternalSourceList {
			if externalSource.Name != commercialPropertyName {
				addressRecord.ExternalSourceList = append(addressRecord.ExternalSourceList, externalSource)
			}
		}
	} else {
		addressRecord.ExternalSourceList = append(addressRecord.ExternalSourceList, existingAddress.ExternalSourceList...)
	}

	if s.isStarIntegrationEnabled(ctx) {
		var transaction external_source.ITransaction
		// if we have have issues adding the address to star db this should not disturb adding it to
		// mongodb while we report the issue on splunk for review
		if !addressRecord.IsExternalSourcePresent(source) {
			transaction, serr = s.insertEntryInExternalSource(ctx, addressRecord, source)
			if serr != nil {
				log.WithError(serr).
					Error("failed to register new address on property in star db")
				componentContextData["txnError"] = serr.Error()
			}
		} else if !addressRecord.IsEquivalent(ctx, existingAddress) {
			transaction, serr = s.updateEntryInExternalSource(ctx, addressRecord, source)
			if serr != nil {
				log.WithError(serr).
					Error("failed to update property in star db")
				componentContextData["txnError"] = serr.Error()
			}
		}
		defer func() {
			if transaction == nil {
				return
			}
			if serr == nil {
				_ = transaction.Commit(ctx)
			} else {
				_ = transaction.Rollback(ctx)
			}
		}()
	}
	// Check equivalency and if not equal, archive and update record
	if !addressRecord.IsEquivalent(ctx, existingAddress) || !areExternalSourceListsEqual(existingAddress, addressRecord) {
		addressRecord.UUID = existingAddress.UUID
		err := s.DAO.WithTransaction(ctx, func(sessCtx mongo.SessionContext) (interface{}, error) {
			if len(addressRecordsExternalSources) > 0 {
				serr = s.deleteCommercialPropertyIDsFromOtherDocuments(ctx, addressRecordsExternalSources, addressRecord.UUID)
				if serr != nil {
					return nil, serr
				}
			}
			serr = s.DAO.ArchiveAddress(ctx, *existingAddress)
			if serr != nil {
				return nil, serr
			}
			serr = s.DAO.UpdateAddress(ctx, addressRecord)
			if serr != nil {
				return nil, serr
			}
			return addressRecord, nil
		})
		if err != nil {
			componentContextData[literals.ContextErrorMessage] = err.Error()
			//assign error to serr for the purpose of rolling back star transaction
			serr = addressErrors.DaoUpdateError.New()
			instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
			return nil, serr
		}
		instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
		return addressRecord, nil
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return existingAddress, nil
}

// insertAddress inserts address in mongo db and based on source parameter upserts entry in external source
func (s *AddressServicer) insertAddress(
	ctx context.Context,
	addressRecord *mongo_dal.AddressRecord,
	source pb.ExternalSource) (address *mongo_dal.AddressRecord, serr *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)

	spanID := instrumentation.ComponentSpanStart(
		ctx,
		literals.InsertAddress,
		literals.ComponentTypeAddressService)

	componentContextData := map[string]interface{}{
		"address": fmt.Sprintf("%#v", addressRecord),
		"source":  source,
	}

	addressID, err := mongo_dal.GenerateUUID(ctx)
	if err != nil {
		log.Warn("failed to generate ID for address, abandoning storage")
		componentContextData[literals.ContextErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return nil, addressErrors.DaoUUIDGenerationError.New()
	}
	addressRecord.UUID = *addressID
	componentContextData["address_id"] = addressID

	addressRecordsExternalSources := addressRecord.ExternalSourceList

	if s.isStarIntegrationEnabled(ctx) {
		var transaction external_source.ITransaction
		// if we have have issues adding the address to star db this should not disturb adding it to
		// mongodb while we report the issue on splunk for review
		if !addressRecord.IsExternalSourcePresent(source) {
			transaction, serr = s.insertEntryInExternalSource(ctx, addressRecord, source)
			if serr != nil {
				log.WithError(serr).
					Error("failed to register new address on property in star db")
				componentContextData["txnError"] = serr.Error()
			}
		} else {
			transaction, serr = s.updateEntryInExternalSource(ctx, addressRecord, source)
			if serr != nil {
				log.WithError(serr).
					Error("failed to update property in star db")
				componentContextData["txnError"] = serr.Error()
			}
		}
		defer func() {
			if transaction == nil {
				return
			}
			if serr == nil {
				_ = transaction.Commit(ctx)
			} else {
				_ = transaction.Rollback(ctx)
			}
		}()
	}
	// Cache to Redis with short duration, for speed. Will delete itself if fails to save to MongoDB
	s.saveAddressToRedis(ctx, addressRecord, 10)
	err = s.DAO.WithTransaction(ctx, func(sessionContext mongo.SessionContext) (interface{}, error) {
		serr = s.deleteCommercialPropertyIDsFromOtherDocuments(ctx, addressRecordsExternalSources, addressRecord.UUID)
		if serr != nil {
			return nil, serr
		}
		serr = s.DAO.InsertAddress(ctx, addressRecord)
		if serr != nil {
			return nil, serr
		}
		return addressRecord.Format(), nil
	})
	if err != nil {
		componentContextData[literals.ContextErrorMessage] = err.Error()
		serr = addressErrors.DaoInsertError.New()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return nil, serr
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return addressRecord.Format(), nil
}

func areExternalSourceListsEqual(address1, address2 *mongo_dal.AddressRecord) bool {
	if len(address2.LegacyIDList) != len(address1.LegacyIDList) || len(address2.ExternalSourceList) != len(address1.ExternalSourceList) {
		return false
	}

	sort.Slice(address1.LegacyIDList, func(i, j int) bool {
		return address1.LegacyIDList[i] < address1.LegacyIDList[j]
	})
	sort.Slice(address2.LegacyIDList, func(i, j int) bool {
		return address2.LegacyIDList[i] < address2.LegacyIDList[j]
	})
	for i := range address1.LegacyIDList {
		if address2.LegacyIDList[i] != address1.LegacyIDList[i] {
			return false
		}
	}
	sort.Slice(address1.ExternalSourceList, func(i, j int) bool {
		if address1.ExternalSourceList[i].Name == address1.ExternalSourceList[j].Name {
			return address1.ExternalSourceList[i].ID < address1.ExternalSourceList[j].ID
		}
		return address1.ExternalSourceList[i].Name < address1.ExternalSourceList[j].Name
	})
	sort.Slice(address2.ExternalSourceList, func(i, j int) bool {
		if address2.ExternalSourceList[i].Name == address2.ExternalSourceList[j].Name {
			return address2.ExternalSourceList[i].ID < address2.ExternalSourceList[j].ID
		}
		return address2.ExternalSourceList[i].Name < address2.ExternalSourceList[j].Name
	})
	for i := range address1.ExternalSourceList {
		if address2.ExternalSourceList[i].Name != address1.ExternalSourceList[i].Name || address2.ExternalSourceList[i].ID != address1.ExternalSourceList[i].ID {
			return false
		}
	}
	return true
}

func (s *AddressServicer) deleteCommercialPropertyIDsFromOtherDocuments(ctx context.Context, externalSources []mongo_dal.ExternalSource, uuid primitive.Binary) *addressErrors.ServerError {
	commercialPropertyName := external_source.ExternalSourcePBName(pb.ExternalSource_COMMERCIAL_PROPERTY)
	for _, externalSource := range externalSources {
		if externalSource.Name != commercialPropertyName {
			continue
		}
		_, err := s.DAO.DeleteAllExternalSourceIDWithException(ctx, externalSource.Name, externalSource.ID, uuid)
		if err != nil {
			return err
		}
	}
	return nil
}

// upsertEntryInExternalSource
// PROPERTY: if id exists we update it and if not we create a new entry in PROPERTY table in starDB
// COMMERCIAL_PROPERTY: if id doesn't exist we create a new entry in COMMERCIAL_PROPERTY table in starDB
func (s *AddressServicer) upsertEntryInExternalSource(ctx context.Context, address *mongo_dal.AddressRecord, source pb.ExternalSource) (external_source.ITransaction, *addressErrors.ServerError) {
	fields := map[string]interface{}{
		"address": address,
		"source":  source,
	}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.UpsertEntryInExternalSource, literals.ComponentTypeAddressService)

	transaction, err := s.OracleDB.StartTransaction(ctx, s.serviceConfig.Oracle.Username, 0)
	if err != nil {
		fields[literals.LLErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, addressErrors.StarTransactionError.New()
	}

	var (
		id   int64
		serr *addressErrors.ServerError
	)
	sourceName := external_source.ExternalSourcePBName(source)
	externalSourceExists := address.IsExternalSourcePresent(source)
	switch source {
	case pb.ExternalSource_UNKNOWN_SOURCE:
		if !externalSourceExists {
			id, serr = transaction.CreatePropertyWithAddressInStarDB(ctx, address)
			if serr == nil {
				address.LegacyIDList = append(address.LegacyIDList, uint32(id))
			}
		} else {
			for _, id := range address.LegacyIDList {
				transaction.UpdatePropertyWithAddressInStarDB(ctx, address, id)
			}
		}
	case pb.ExternalSource_COMMERCIAL_PROPERTY:
		if !externalSourceExists {
			id, serr = transaction.CreateCommercialPropertyWithAddress(ctx, address)
			if serr == nil {
				address.ExternalSourceList = append(address.ExternalSourceList, mongo_dal.ExternalSource{
					Name:        sourceName,
					ID:          uint32(id),
					LastUpdated: primitive.NewDateTimeFromTime(time.Now()),
				})
			}
		}
	default:
		serr = addressErrors.UnexpectedExternalSourceError.New()
	}
	if serr != nil {
		transaction.Rollback(ctx)
		fields[literals.LLErrorMessage] = serr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, serr
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return transaction, nil
}

// insertEntryInExternalSource
func (s *AddressServicer) insertEntryInExternalSource(ctx context.Context, address *mongo_dal.AddressRecord, source pb.ExternalSource) (external_source.ITransaction, *addressErrors.ServerError) {
	fields := map[string]interface{}{
		"address": address,
		"source":  source,
	}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.InsertEntryInExternalSource, literals.ComponentTypeAddressService)

	transaction, err := s.OracleDB.StartTransaction(ctx, s.serviceConfig.Oracle.Username, 0)
	if err != nil {
		fields[literals.LLErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, addressErrors.StarTransactionError.New()
	}

	var (
		id   int64
		serr *addressErrors.ServerError
	)
	sourceName := external_source.ExternalSourcePBName(source)
	switch source {
	case pb.ExternalSource_UNKNOWN_SOURCE:
		id, serr = transaction.CreatePropertyWithAddressInStarDB(ctx, address)
		if serr == nil {
			address.LegacyIDList = append(address.LegacyIDList, uint32(id))
		}
	case pb.ExternalSource_COMMERCIAL_PROPERTY:
		id, serr = transaction.CreateCommercialPropertyWithAddress(ctx, address)
		if serr == nil {
			address.ExternalSourceList = append(address.ExternalSourceList, mongo_dal.ExternalSource{
				Name:        sourceName,
				ID:          uint32(id),
				LastUpdated: primitive.NewDateTimeFromTime(time.Now()),
			})
		}
	default:
		serr = addressErrors.UnexpectedExternalSourceError.New()
	}
	if serr != nil {
		transaction.Rollback(ctx)
		fields[literals.LLErrorMessage] = serr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, serr
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return transaction, nil
}

// updateEntryInExternalSource
func (s *AddressServicer) updateEntryInExternalSource(ctx context.Context, address *mongo_dal.AddressRecord, source pb.ExternalSource) (external_source.ITransaction, *addressErrors.ServerError) {
	fields := map[string]interface{}{
		"address": address,
		"source":  source,
	}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.InsertEntryInExternalSource, literals.ComponentTypeAddressService)

	transaction, err := s.OracleDB.StartTransaction(ctx, s.serviceConfig.Oracle.Username, 0)
	if err != nil {
		fields[literals.LLErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, addressErrors.StarTransactionError.New()
	}

	var (
		serr *addressErrors.ServerError
	)
	switch source {
	case pb.ExternalSource_UNKNOWN_SOURCE:
		for _, id := range address.LegacyIDList {
			transaction.UpdatePropertyWithAddressInStarDB(ctx, address, id)
		}
	case pb.ExternalSource_COMMERCIAL_PROPERTY:
		// we don't want to update COMMERCIAL_PROPERTY table in star
	default:
		serr = addressErrors.UnexpectedExternalSourceError.New()
	}
	if serr != nil {
		transaction.Rollback(ctx)
		fields[literals.LLErrorMessage] = serr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, serr
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return transaction, nil
}

func (s *AddressServicer) validateAddress(
	ctx context.Context,
	lookup *mongo_dal.AddressRecord) (address *mongo_dal.AddressRecord, err *addressErrors.ServerError) {
	var addressRecord *mongo_dal.AddressRecord
	fields := map[string]interface{}{"lookup": lookup}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.ValidateAddress, literals.ComponentTypeAddressService)
	cached, err := s.getAddressByLookupFromRedis(ctx, lookup)
	if err != nil {
		addressRecord, err = s.getAddressFromAddressValidator(ctx, lookup)
		if err != nil {
			if err.Code == addressErrors.AddressNotVerifiableError.Code {
				// If no address was previously found, it is written in the cash with no value
				s.saveGetAddressQueryToRedis(ctx, lookup, &mongo_dal.AddressRecord{}, 5*60) //5 min
			}
			fields[literals.LLErrorMessage] = err.Error()
			instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
			return nil, err
		}
		if len(addressRecord.DeliveryPointBarcode) > 0 { // cache the query in case when someone re-requests this data
			s.saveGetAddressQueryToRedis(ctx, lookup, addressRecord, 5*60) //5 min
		}

	}
	if cached != nil && addressRecord == nil {
		// If no address was previously found, it is written in the cash with no value
		if len(cached.Zip) == 0 || len(cached.DeliveryPointBarcode) == 0 {
			log := logging.GetTracedLogEntry(ctx)
			log.Warn("This request has been saved as an unknown address")
			return nil, addressErrors.AddressNotVerifiableError.New()
		}
		addressRecord = cached
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return addressRecord, nil
}

// verifyUniqueAddress checks if the address is truly unique to the system.
// If an existing address is found, both the existingAddress and an AddressExistsError are returned. Callers need only check the existence of
// the error code to know if an existingAddress was found.
func (s *AddressServicer) verifyUniqueAddress(
	ctx context.Context,
	address *mongo_dal.AddressRecord) (existingAddress *mongo_dal.AddressRecord, err *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)
	fields := map[string]interface{}{"address": address}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.VerifyUniqueAddress, literals.ComponentTypeAddressService)
	if address == nil {
		log.Error(addressErrors.NilPointerError.Error())
		fields[literals.LLErrorMessage] = addressErrors.NilPointerError.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, addressErrors.NilPointerError.New()
	}
	log = log.WithFields(
		map[string]interface{}{
			"Street1":              address.Street1,
			"Zipcode":              address.Zip,
			"DeliveryPointBarcode": address.DeliveryPointBarcode,
			"UnitType":             address.UnitType,
			"UnitValue":            address.UnitValue,
		},
	)

	if isValidDeliveryPointBarcode(address.DeliveryPointBarcode) {
		return s.verifyUniqueAddressUsingDPB(ctx, address, spanID, fields)
	}
	// Check database to see if there are addresses ( the unverified address may not have DPBC )
	existingAddress, queryErr := s.DAO.QueryAddress(ctx, &mongo_dal.AddressRecord{
		Street1:     address.Street1,
		Street2:     address.Street2,
		UnitType:    address.UnitType,
		UnitValue:   address.UnitValue,
		City:        address.City,
		DefaultCity: address.DefaultCity,
		Zip:         address.Zip,
		ZipLocal:    address.ZipLocal,
		State:       address.State,
	})
	if queryErr != nil {
		fields[literals.LLErrorMessage] = queryErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, queryErr
	}
	if existingAddress != nil {
		log.WithField("ExistingAddress", existingAddress.UUID).Info("address is not unique")
		instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
		return existingAddress.Format(), addressErrors.AddressExistsError.New()
	}

	log.Info("address is unique")
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return nil, nil
}

// verifyUniqueAddressUsingDPB is a helper function for verifyUniqueAddress. Check verifyUniqueAddress for details
func (s *AddressServicer) verifyUniqueAddressUsingDPB(ctx context.Context, address *mongo_dal.AddressRecord, spanID string,
	fields map[string]interface{}) (existingAddress *mongo_dal.AddressRecord, err *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)
	existingAddress, _ = s.getAddressByDPBAndUnitFromRedis(ctx, address.DeliveryPointBarcode, address.UnitType, address.UnitValue)
	if existingAddress != nil {
		log.Info("address is not unique")
		instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
		return existingAddress.Format(), addressErrors.AddressExistsError.New()
	}

	existingAddress, queryErr := s.DAO.QueryAddress(ctx, &mongo_dal.AddressRecord{
		DeliveryPointBarcode: address.DeliveryPointBarcode,
		Street1:              address.Street1,
		Street2:              address.Street2,
		UnitType:             address.UnitType,
		UnitValue:            address.UnitValue,
		City:                 address.City,
		Zip:                  address.Zip,
		State:                address.State,
		RecordType:           address.RecordType, //QueryAddress ignores this
		DocVersion:           mongo_dal.CurrentDocVersion,
	})
	if queryErr != nil {
		fields[literals.LLErrorMessage] = queryErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, queryErr
	}
	if existingAddress != nil {
		log.WithField("ExistingAddress", existingAddress.UUID).Warn("address is not unique")
		instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
		return existingAddress.Format(), addressErrors.AddressExistsError.New()
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return nil, nil
}

func isValidDeliveryPointBarcode(deliveryPointBarcode string) bool {
	//The PostNet barcode is used mainly in 3 variants, that differ in the length of the data:
	//5 digits POSTNET barcode: 5 digit long zip code
	//ZIP + 4 POSTNET barcodes: 9 digit long zip code
	//DPBC POSTNET barcode (Delivery Point barcode): 9 digit long zip code + 2 DPBC digits
	if deliveryPointBarcodeRegex.MatchString(deliveryPointBarcode) {
		switch len(deliveryPointBarcode) {
		case 5, 9, 11, 12:
			return true
		}
	}
	return false
}

/*
 * REDIS HELPERS
 */
func (s *AddressServicer) saveAddressToRedis(ctx context.Context, record *mongo_dal.AddressRecord, expireInSeconds int) {
	fields := map[string]interface{}{"record": record, "expiresInSeconds": expireInSeconds}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.SaveAddressToRedis, literals.ComponentTypeAddressService)
	if s.Cache.IsEnabled() {
		log := logging.GetTracedLogEntry(ctx)
		log = log.WithField("DeliveryPointBarcode", record.DeliveryPointBarcode)
		log.Debug("save address to Redis")
		err := s.Cache.SetEx(ctx, getAddressDPBCRedisKey(record.DeliveryPointBarcode, record.UnitType, record.UnitValue), record.ToByteArray(), expireInSeconds)
		if err != nil {
			fields[literals.LLErrorMessage] = err.Error()
			log.WithError(err).Error("unable to cache address")
		}
		instrumentation.ComponentSpanEnd(ctx, spanID, (err == nil), fields)
	} else {
		instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	}
}

func (s *AddressServicer) saveGetAddressQueryToRedis(ctx context.Context, lookup *mongo_dal.AddressRecord, record *mongo_dal.AddressRecord, expireInSeconds int) {
	if !s.Cache.IsEnabled() {
		return //cache is disabled
	}
	fields := map[string]interface{}{"lookup": lookup, "record": record}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.SaveGetAddressQueryToRedis, literals.ComponentTypeAddressService)
	log := logging.GetTracedLogEntry(ctx)
	log.Debug("save address query to Redis")
	err := s.Cache.SetEx(ctx, getAddressRedisKey(lookup.ToByteArray()), record.ToByteArray(), expireInSeconds)
	if err != nil {
		fields[literals.LLErrorMessage] = err.Error()
		log.WithError(err).Info("unable to cache address query")
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, (err == nil), fields)
}

func (s *AddressServicer) getAddressByLookupFromRedis(ctx context.Context, lookup *mongo_dal.AddressRecord) (cachedAddress *mongo_dal.AddressRecord, err *addressErrors.ServerError) {
	if !s.Cache.IsEnabled() {
		return nil, addressErrors.CacheUnavailableError.New()
	}
	fields := map[string]interface{}{"lookup": lookup}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.GetAddressByLookupFromRedis, literals.ComponentTypeAddressService)
	cachedAddress = &mongo_dal.AddressRecord{}
	data, err := s.Cache.Get(ctx, getAddressRedisKey(lookup.ToByteArray()))
	if err != nil {
		fields[literals.LLErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, err
	}

	if data == nil {
		fields[literals.LLErrorMessage] = addressErrors.AddressNotFoundError.New().Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, addressErrors.AddressNotFoundError.New()
	}

	decodeErr := json.NewDecoder(bytes.NewReader(data)).Decode(cachedAddress)
	if decodeErr != nil {
		fields[literals.LLErrorMessage] = decodeErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, addressErrors.JSONDecodeError.New()
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return cachedAddress, nil
}

func (s *AddressServicer) getAddressByDPBAndUnitFromRedis(ctx context.Context, deliveryPointBarcode, unitType, unitValue string) (cachedAddress *mongo_dal.AddressRecord, err *addressErrors.ServerError) {
	fields := map[string]interface{}{"DeliveryPointBarcode": deliveryPointBarcode, "DataStore": "Redis"}
	log := logging.GetTracedLogEntry(ctx)
	log.WithFields(fields).Info()
	spanID := instrumentation.ComponentSpanStart(ctx, literals.GetAddressByDPBAndUnitFromRedis, literals.ComponentTypeAddressService)
	if !s.Cache.IsEnabled() {
		log.Info(addressErrors.CacheUnavailableError.Error())
		fields[literals.LLErrorMessage] = addressErrors.CacheUnavailableError.New().Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, addressErrors.CacheUnavailableError.New()
	}

	cachedAddress = &mongo_dal.AddressRecord{}
	data, err := s.Cache.Get(ctx, getAddressDPBCRedisKey(deliveryPointBarcode, unitType, unitValue))
	if err != nil {
		fields[literals.LLErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, err
	}

	if data == nil {
		dpbcNotFoundErr := addressErrors.AddressDPBCNotFoundError.NewFormatted(deliveryPointBarcode)
		log.WithFields(fields).Info(dpbcNotFoundErr.Error())
		fields[literals.LLErrorMessage] = dpbcNotFoundErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, dpbcNotFoundErr
	}

	decodeErr := json.NewDecoder(bytes.NewReader(data)).Decode(cachedAddress)
	if decodeErr != nil {
		fields["Error"] = decodeErr
		log.WithFields(fields).Info("false positive in most cases")

		// Decode may still work in this case, data integrity for UUID's and timestamps can throw decode errors but still decode correctly
		if cachedAddress.Street1 == "" {
			fields["Error"] = addressErrors.JSONDecodeError.Error()
			log.WithFields(fields).Warn("deleting bad key from cache")
			deleteErr := s.Cache.Delete(getAddressDPBCRedisKey(deliveryPointBarcode, unitType, unitValue))
			if deleteErr != nil {
				log.WithFields(fields).Warn("failed to delete bad key from cache")
			}

			instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
			return nil, addressErrors.JSONDecodeError.New()
		}
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return cachedAddress, nil
}

func (s *AddressServicer) deleteAddressFromRedis(ctx context.Context, record *mongo_dal.AddressRecord) {
	fields := map[string]interface{}{}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.DeleteAddressFromRedis, literals.ComponentTypeAddressService)
	if s.Cache.IsEnabled() {
		fields = map[string]interface{}{
			"DeliveryPointBarcode": record.DeliveryPointBarcode,
			"UnitType":             record.UnitType,
			"UnitValue":            record.UnitValue,
		}
		logging.GetTracedLogEntry(ctx).WithFields(fields).Debug("deleting address from Redis")
		_ = s.Cache.Delete(getAddressDPBCRedisKey(record.DeliveryPointBarcode, record.UnitType, record.UnitValue))
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
}

func getAddressDPBCRedisKey(ID string, unitType string, unitValue string) (key string) {
	return fmt.Sprintf(repository.DeliveryPointBarcodeRedisKeyFormat, ID, unitType, unitValue)
}

func getAddressRedisKey(object []byte) (key string) {
	b := b64.StdEncoding.EncodeToString(object)
	return fmt.Sprintf(repository.DeliveryPointRedisKeyFormat, b)
}

/*
 * SMARTY STREETS HELPERS
 */
func (s *AddressServicer) getAddressFromAddressValidator(ctx context.Context, lookup *mongo_dal.AddressRecord) (address *mongo_dal.AddressRecord, err *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)
	s.UseAddressValidatorDefinedByFeatureFlag(ctx)
	fields := map[string]interface{}{
		"lookup": lookup,
	}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.GetAddressFromAddressValidator, literals.ComponentTypeAddressService)
	addressRecord, smartyErr := s.AddressValidatorClient.GetAddress(ctx, lookup, addressvalidator.Strict)
	if smartyErr != nil {
		fields[literals.LLErrorMessage] = smartyErr.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, smartyErr
	}

	if addressRecord == nil {
		// address wasn't found via strict strategy, try range strategy
		log.Info("Address wasn't found via strict strategy, try range strategy")
		addressRecord, smartyErr = s.AddressValidatorClient.GetAddress(ctx, lookup, addressvalidator.Range)
		if smartyErr != nil {
			fields[literals.LLErrorMessage] = smartyErr.Error()
			instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
			return nil, smartyErr
		}

		if addressRecord == nil {
			fields[literals.LLErrorMessage] = addressErrors.AddressNotVerifiableError.New().Error()
			instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
			return nil, addressErrors.AddressNotVerifiableError.New()
		}

		log.WithField("Lookup", lookup).Info("address record not found via strict strategy, but found with range strategy")
	}

	addressRecord.Format()

	if addressRecord.DeliveryPointBarcode == "" {
		log.WithField("Address", addressRecord).Warn(addressErrors.EmptyDPBCError.Error())
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return addressRecord, nil
}

func (s *AddressServicer) getAddressFromExternalSource(ctx context.Context, id uint32, source pb.ExternalSource) (*mongo_dal.AddressRecord, *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)
	fields := map[string]interface{}{
		"id":     id,
		"source": source,
	}

	spanID := instrumentation.ComponentSpanStart(ctx, literals.GetAddressFromExternalSource, literals.ComponentTypeAddressService)
	if id == uint32(0) {
		fields[literals.LLErrorMessage] = addressErrors.InvalidLegacyIDError.NewFormatted(id).Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, addressErrors.InvalidLegacyIDError.NewFormatted(id)
	}

	session := s.OracleDB.StartSession(ctx)
	defer session.Close(ctx)
	var (
		address *mongo_dal.AddressRecord
		err     *addressErrors.ServerError
	)
	switch source {
	case pb.ExternalSource_UNKNOWN_SOURCE:
		address, err = session.GetAddress(ctx, id)
	case pb.ExternalSource_COMMERCIAL_PROPERTY:
		address, err = session.GetAddressFromCommercialProperty(ctx, id)
	default:
		fields[literals.LLErrorMessage] = addressErrors.UnexpectedExternalSourceError.New().Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, addressErrors.UnexpectedExternalSourceError.New()
	}
	if err != nil {
		fields[literals.LLErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, err
	}

	addressBytes, _ := json.Marshal(address)

	log.WithField("Address", string(addressBytes)).
		Infof("address reconstructed from property in star ")

	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return address, nil
}

func (s *AddressServicer) isStarIntegrationEnabled(ctx context.Context) bool {
	log := logging.GetTracedLogEntry(ctx)
	flagStatus := false
	spanID := instrumentation.
		ComponentSpanStart(ctx, literals.CheckAddressStarIntegrationFeatureFlag, literals.ComponentTypeSwitch)
	contextData := map[string]interface{}{}

	flag, err := s.SwitchClient.CheckFlag(ctx, literals.AddressStarIntegrationFeatureFlag)
	if err != nil {
		log.WithError(err).Warn("failed to fetch address star integration feature flag status")
		instrumentation.ComponentSpanEnd(ctx, spanID, false, contextData)
		return flagStatus
	}

	if flag == "" || len(flag) == 0 {
		log.Warn("address star integration feature flag response is empty")
		instrumentation.ComponentSpanEnd(ctx, spanID, true, contextData)
		return flagStatus
	}

	if flag == featureflag.On.String() {
		flagStatus = true
	}

	return flagStatus
}

func (s *AddressServicer) readZipcodeFromMongo(ctx context.Context) bool {
	log := logging.GetTracedLogEntry(ctx)
	spanID := instrumentation.ComponentSpanStart(ctx, literals.CheckZipcodeDataSourceFeatureFlag, literals.ComponentTypeSwitch)
	contextData := map[string]interface{}{}

	flag, err := s.SwitchClient.CheckFlag(ctx, literals.AddressUseMongoZipcodeData)
	if err != nil {
		log.WithError(err).Warn("failed to fetch address zipcode data source feature flag")
		instrumentation.ComponentSpanEnd(ctx, spanID, false, contextData)
		return false
	}
	if flag == "" || len(flag) == 0 {
		log.Warn("address zipcode data source feature flag response is empty")
		instrumentation.ComponentSpanEnd(ctx, spanID, true, contextData)
		return false
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, true, contextData)
	if flag == featureflag.On.String() {
		return true
	}
	return false
}
