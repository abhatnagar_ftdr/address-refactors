package types

// Street1Components is used represent the components of a street1 address
type Street1Components struct {
	StreetName      string
	StreetDirection string
	StreetNumber    string
}
