package external_source

import (
	"context"
	pb "golang.frontdoorhome.com/software/protos/go/addresspb"
)

type ITransaction interface {
	Commit(ctx context.Context) error
	Rollback(ctx context.Context) error
}

func ExternalSourcePBName(source pb.ExternalSource) string {
	switch source {
	case pb.ExternalSource_UNKNOWN_SOURCE:
		return "PROPERTY"
	default:
		return source.String()
	}
}
