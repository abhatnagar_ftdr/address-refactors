package star

import (
	"github.com/golang/mock/gomock"
	"go.ftdr.com/go-utils/xormwrapper/v2"
	mockXormwrapper "go.ftdr.com/go-utils/xormwrapper/v2/mock"
	"strconv"
)

func MockFindCitySuccess(transaction *mockXormwrapper.MockITransaction, city string, cityCode int64) {
	options := []CITIES{}
	transaction.EXPECT().FindByStruct(&options, &CITIES{CityName: city}, nil).DoAndReturn(func(data, filter, conditions interface{}) error {
		options := data.(*[]CITIES)
		(*options) = append(*options, CITIES{
			CityCode: cityCode,
			CityName: city,
		})
		return nil
	}).AnyTimes()
}

func MockFindCityNotFound(transaction *mockXormwrapper.MockITransaction, city string, err error) {
	options := []CITIES{}
	transaction.EXPECT().FindByStruct(&options, &CITIES{CityName: city}, nil).Return(err).AnyTimes()
}

func MockFindCityCodeZipCodeSuccess(transaction *mockXormwrapper.MockITransaction, zipcode string, citycode int64) {
	options := []CITY_CODE_ZIP_CODE{}
	transaction.EXPECT().FindByStruct(&options, &CITY_CODE_ZIP_CODE{ZipCode: zipcode, CityCode: strconv.FormatInt(citycode, 10)}, nil).DoAndReturn(func(data, filter, conditions interface{}) error {
		options := data.(*[]CITY_CODE_ZIP_CODE)
		(*options) = append(*options, CITY_CODE_ZIP_CODE{
			ZipCode:  zipcode,
			CityCode: strconv.FormatInt(citycode, 10),
		})
		return nil
	}).AnyTimes()
}

func MockFindCityCodeZipCodeNotFound(transaction *mockXormwrapper.MockITransaction, zipcode string, citycode int64, err error) {
	options := []CITY_CODE_ZIP_CODE{}
	transaction.EXPECT().FindByStruct(&options, &CITY_CODE_ZIP_CODE{ZipCode: zipcode, CityCode: strconv.FormatInt(citycode, 10)}, nil).Return(err).AnyTimes()
}

func MockFindNextIdSuccess(session *mockXormwrapper.MockISession, sequence string, nextId int64) {
	options := []DUAL{}
	conds := xormwrapper.QueryOptions{
		CustomSQL: "select " + sequence + ".nextval from dual",
	}

	session.EXPECT().Find(&options, &conds).DoAndReturn(func(data, conditions interface{}) error {
		options := data.(*[]DUAL)
		(*options) = append(*options, DUAL{
			Nextval: nextId,
		})
		return nil
	}).AnyTimes()
}

func MockFindNextIdNotFound(session *mockXormwrapper.MockISession, sequence string, err error) {
	options := []DUAL{}
	conds := xormwrapper.QueryOptions{
		CustomSQL: "select " + sequence + ".nextval from dual",
	}
	session.EXPECT().Find(&options, &conds).Return(err).AnyTimes()
}

func MockInsertOne(transaction *mockXormwrapper.MockITransaction, dataType interface{}, id int64, err error) {
	transaction.EXPECT().InsertOne(gomock.AssignableToTypeOf(dataType)).Return(id, err)
}

func MockUpdateOne(transaction *mockXormwrapper.MockITransaction, dataType interface{}, count int64, err error) {
	transaction.EXPECT().Update(gomock.AssignableToTypeOf(dataType), gomock.AssignableToTypeOf(&xormwrapper.QueryOptions{})).Return(count, err)
}

func MockFindCountyZipCodeSuccess(transaction *mockXormwrapper.MockITransaction, zipcode string, countyCode int64) {
	options := []COUNTY_ZIP_CODE{}
	transaction.EXPECT().FindByStruct(&options, &COUNTY_ZIP_CODE{ZipCode: zipcode}, nil).DoAndReturn(func(data, filter, conditions interface{}) error {
		options := data.(*[]COUNTY_ZIP_CODE)
		(*options) = append(*options, COUNTY_ZIP_CODE{
			ZipCode:    zipcode,
			CountyCode: strconv.FormatInt(countyCode, 10),
		})
		return nil
	}).AnyTimes()
}

func MockFindCountyZipCodeNotFound(transaction *mockXormwrapper.MockITransaction, zipcode string, err error) {
	options := []COUNTY_ZIP_CODE{}
	transaction.EXPECT().FindByStruct(&options, &COUNTY_ZIP_CODE{ZipCode: zipcode}, nil).Return(err).AnyTimes()
}

func MockFindCountyZipCode2Success(transaction *mockXormwrapper.MockITransaction, zipcode string, countyCode int64) {
	options := []COUNTY_ZIP_CODE{}
	transaction.EXPECT().FindByStruct(&options, &COUNTY_ZIP_CODE{ZipCode: zipcode, CountyCode: strconv.FormatInt(countyCode, 10)}, nil).DoAndReturn(func(data, filter, conditions interface{}) error {
		options := data.(*[]COUNTY_ZIP_CODE)
		(*options) = append(*options, COUNTY_ZIP_CODE{
			ZipCode:    zipcode,
			CountyCode: strconv.FormatInt(countyCode, 10),
		})
		return nil
	}).AnyTimes()
}

func MockFindCountyZipCode2NotFound(transaction *mockXormwrapper.MockITransaction, zipcode string, countycode int64, err error) {
	options := []COUNTY_ZIP_CODE{}
	transaction.EXPECT().FindByStruct(&options, &COUNTY_ZIP_CODE{ZipCode: zipcode, CountyCode: strconv.FormatInt(countycode, 10)}, nil).Return(err).AnyTimes()
}

func MockFindCountiesSuccess(transaction *mockXormwrapper.MockITransaction, county string, countyCode int64) {
	options := []COUNTIES{}
	transaction.EXPECT().FindByStruct(&options, &COUNTIES{CountyName: county}, nil).DoAndReturn(func(data, filter, conditions interface{}) error {
		cityOptions := data.(*[]COUNTIES)
		(*cityOptions) = append(*cityOptions, COUNTIES{
			CountyCode: countyCode,
			CountyName: county,
		})
		return nil
	}).AnyTimes()
}

func MockFindCountiesNotFound(transaction *mockXormwrapper.MockITransaction, county string, err error) {
	options := []COUNTIES{}
	transaction.EXPECT().FindByStruct(&options, &COUNTIES{CountyName: county}, nil).Return(err).AnyTimes()
}

func MockFindZipCodeSuccess(transaction *mockXormwrapper.MockITransaction, zipcode string) {
	options := []ZIP_CODE{}
	transaction.EXPECT().FindByStruct(&options, &ZIP_CODE{ZipCode: zipcode}, nil).DoAndReturn(func(data, filter, conditions interface{}) error {
		options := data.(*[]ZIP_CODE)
		(*options) = append(*options, ZIP_CODE{
			ZipCode: zipcode,
		})
		return nil
	}).AnyTimes()
}

func MockFindZipCodeNotFound(transaction *mockXormwrapper.MockITransaction, zipcode string, err error) {
	options := []ZIP_CODE{}
	transaction.EXPECT().FindByStruct(&options, &ZIP_CODE{ZipCode: zipcode}, nil).Return(err).AnyTimes()
}

func MockFindPropertySuccess(transaction *mockXormwrapper.MockITransaction, propertyId int64) {
	options := []PROPERTY{}
	transaction.EXPECT().FindByStruct(&options, &PROPERTY{PropertyID: propertyId}, nil).DoAndReturn(func(data, filter, conditions interface{}) error {
		options := data.(*[]PROPERTY)
		(*options) = append(*options, PROPERTY{
			PropertyID: propertyId,
		})
		return nil
	}).AnyTimes()
}

func MockFindCommercialPropertySuccess(transaction *mockXormwrapper.MockITransaction, propertyId int64) {
	options := []COMMERCIAL_PROPERTY{}
	transaction.EXPECT().FindByStruct(&options, &COMMERCIAL_PROPERTY{CommercialPropertyID: propertyId}, nil).DoAndReturn(func(data, filter, conditions interface{}) error {
		options := data.(*[]COMMERCIAL_PROPERTY)
		(*options) = append(*options, COMMERCIAL_PROPERTY{
			CommercialPropertyID: propertyId,
		})
		return nil
	}).AnyTimes()
}
