package star

import (
	"context"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	mockXormwrapper "go.ftdr.com/go-utils/xormwrapper/v2/mock"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/mongo_dal"
	addressErrors "golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors"
	"testing"
)

const (
	TestCity            = "WADOWICE"
	TestCityCode        = int64(10)
	TestNonExistingCity = "SOSNOWIEC"

	TestCounty            = "TEST_COUNTY"
	TestCountyCode        = int64(20)
	TestNonExistingCounty = "TEST_NON_EXISTING_COUNTY"

	TestZipCode            = "21370"
	TestNonExistingZipCode = "00000"

	TestPropertyId = int64(100)
)

func TestStarTransaction_GetCityCode(t *testing.T) {
	type args struct {
		city    string
		zipcode string
	}
	type expected struct {
		cityCode int64
		err      *addressErrors.ServerError
	}
	type mocks struct {
		transaction *mockXormwrapper.MockITransaction
		session     *mockXormwrapper.MockISession
	}

	type test struct {
		name    string
		args    args
		want    expected
		prepare func(m *mocks)
	}

	tests := []test{
		{
			name: "success",
			args: args{
				city:    TestCity,
				zipcode: TestZipCode,
			},
			want: expected{
				cityCode: TestCityCode,
				err:      nil,
			},
			prepare: func(m *mocks) {
				MockFindCitySuccess(m.transaction, TestCity, TestCityCode)
				MockFindCityCodeZipCodeSuccess(m.transaction, TestZipCode, TestCityCode)
			},
		},
		{
			name: "non existing city",
			args: args{
				city:    TestNonExistingCity,
				zipcode: TestZipCode,
			},
			want: expected{
				cityCode: TestCityCode,
				err:      nil,
			},
			prepare: func(m *mocks) {
				MockFindCityNotFound(m.transaction, TestNonExistingCity, nil)
				MockFindNextIdSuccess(m.session, literals.CityTableSequence, TestCityCode)
				MockInsertOne(m.transaction, &CITIES{}, TestCityCode, nil)
				MockFindCityCodeZipCodeSuccess(m.transaction, TestZipCode, TestCityCode)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			mockTransaction := mockXormwrapper.NewMockITransaction(mockCtrl)
			mockSession := mockXormwrapper.NewMockISession(mockCtrl)
			tt.prepare(&mocks{
				transaction: mockTransaction,
				session:     mockSession,
			})

			transaction := StarTransaction{
				transaction: mockTransaction,
				session:     &StarSession{session: mockSession},
				username:    "test",
			}

			cityCode, err := transaction.GetCityCode(context.Background(), tt.args.city, tt.args.zipcode)
			assert.Equal(t, tt.want.cityCode, cityCode)
			assert.Equal(t, tt.want.err, err)
		})
	}
}

func TestStarTransaction_GetCountyCode(t *testing.T) {
	type args struct {
		county  string
		zipcode string
	}
	type expected struct {
		countyCode int64
		err        *addressErrors.ServerError
	}
	type mocks struct {
		transaction *mockXormwrapper.MockITransaction
		session     *mockXormwrapper.MockISession
	}

	type test struct {
		name    string
		args    args
		want    expected
		prepare func(m *mocks)
	}

	tests := []test{
		{
			name: "zip missing",
			args: args{
				county:  "",
				zipcode: "",
			},
			want: expected{
				countyCode: 0,
				err:        addressErrors.RequiredFieldMissing.NewFormatted("zip"),
			},
			prepare: func(m *mocks) {
			},
		},
		{
			name: "county missing",
			args: args{
				county:  "",
				zipcode: TestZipCode,
			},
			want: expected{
				countyCode: TestCountyCode,
				err:        nil,
			},
			prepare: func(m *mocks) {
				MockFindCountyZipCodeSuccess(m.transaction, TestZipCode, TestCountyCode)
			},
		},
		{
			name: "county missing - non existing zip",
			args: args{
				county:  "",
				zipcode: TestNonExistingZipCode,
			},
			want: expected{
				countyCode: 0,
				err:        addressErrors.CountyZipCodeNotFound.New(),
			},
			prepare: func(m *mocks) {
				MockFindCountyZipCodeNotFound(m.transaction, TestNonExistingZipCode, nil)
			},
		},
		{
			name: "success",
			args: args{
				county:  TestCounty,
				zipcode: TestZipCode,
			},
			want: expected{
				countyCode: TestCountyCode,
				err:        nil,
			},
			prepare: func(m *mocks) {
				MockFindCountiesSuccess(m.transaction, TestCounty, TestCountyCode)
				MockFindCountyZipCode2Success(m.transaction, TestZipCode, TestCountyCode)
			},
		},
		{
			name: "success - non existing county",
			args: args{
				county:  TestNonExistingCounty,
				zipcode: TestZipCode,
			},
			want: expected{
				countyCode: TestCountyCode,
				err:        nil,
			},
			prepare: func(m *mocks) {
				MockFindCountiesNotFound(m.transaction, TestNonExistingCounty, nil)
				MockFindNextIdSuccess(m.session, literals.CountyTableSequence, TestCountyCode)
				MockInsertOne(m.transaction, &COUNTIES{}, TestCountyCode, nil)
				MockFindCountyZipCode2Success(m.transaction, TestZipCode, TestCountyCode)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			mockTransaction := mockXormwrapper.NewMockITransaction(mockCtrl)
			mockSession := mockXormwrapper.NewMockISession(mockCtrl)
			tt.prepare(&mocks{
				transaction: mockTransaction,
				session:     mockSession,
			})

			transaction := StarTransaction{
				transaction: mockTransaction,
				session:     &StarSession{session: mockSession},
				username:    "test",
			}

			cityCode, serr := transaction.GetCountyCode(context.Background(), tt.args.county, tt.args.zipcode)
			assert.Equal(t, tt.want.countyCode, cityCode)
			assert.Equal(t, tt.want.err, serr)
		})
	}
}

func TestStarTransaction_CreatePropertyWithAddressInStarDB(t *testing.T) {
	type args struct {
		address *mongo_dal.AddressRecord
	}
	type expected struct {
		propertyId int64
		err        *addressErrors.ServerError
	}
	type mocks struct {
		ctrl        *gomock.Controller
		engine      *mockXormwrapper.MockIEngine
		transaction *mockXormwrapper.MockITransaction
		session     *mockXormwrapper.MockISession
	}

	type test struct {
		name    string
		args    args
		want    expected
		prepare func(m *mocks)
	}

	tests := []test{
		{
			name: "success",
			args: args{
				address: &mongo_dal.AddressRecord{
					Zip:    TestZipCode,
					County: TestCounty,
					City:   TestCity,
				},
			},
			want: expected{
				propertyId: TestPropertyId,
				err:        nil,
			},
			prepare: func(m *mocks) {
				MockFindZipCodeSuccess(m.transaction, TestZipCode)
				MockFindCountiesSuccess(m.transaction, TestCounty, TestCountyCode)
				MockFindCountyZipCode2Success(m.transaction, TestZipCode, TestCountyCode)
				MockFindCitySuccess(m.transaction, TestCity, TestCityCode)
				MockFindCityCodeZipCodeSuccess(m.transaction, TestZipCode, TestCityCode)
				MockFindNextIdSuccess(m.session, literals.PropertyTableSequence, TestPropertyId)
				MockInsertOne(m.transaction, &PROPERTY{}, TestPropertyId, nil)
			},
		},
		{
			name: "non existing zip and no county",
			args: args{
				address: &mongo_dal.AddressRecord{
					Zip:    TestNonExistingZipCode,
					County: "",
					City:   TestCity,
				},
			},
			want: expected{
				propertyId: 0,
				err:        addressErrors.ZipCodeInvalid.New(),
			},
			prepare: func(m *mocks) {
				MockFindZipCodeNotFound(m.transaction, TestNonExistingZipCode, nil)
				MockInsertOne(m.transaction, &ZIP_CODE{}, 0, nil)
				MockFindCountyZipCodeNotFound(m.transaction, TestNonExistingZipCode, nil)
			},
		},
		{
			name: "success - non existing zip",
			args: args{
				address: &mongo_dal.AddressRecord{
					Zip:    TestNonExistingZipCode,
					County: TestCounty,
					City:   TestCity,
				},
			},
			want: expected{
				propertyId: TestPropertyId,
				err:        nil,
			},
			prepare: func(m *mocks) {
				MockFindZipCodeNotFound(m.transaction, TestNonExistingZipCode, nil)
				MockInsertOne(m.transaction, &ZIP_CODE{}, 0, nil)
				MockFindCountiesSuccess(m.transaction, TestCounty, TestCountyCode)
				MockFindCountyZipCode2Success(m.transaction, TestNonExistingZipCode, TestCountyCode)
				MockFindCitySuccess(m.transaction, TestCity, TestCityCode)
				MockFindCityCodeZipCodeSuccess(m.transaction, TestNonExistingZipCode, TestCityCode)
				MockFindNextIdSuccess(m.session, literals.PropertyTableSequence, TestPropertyId)
				MockInsertOne(m.transaction, &PROPERTY{}, TestPropertyId, nil)
			},
		},
		{
			name: "success - non existing county",
			args: args{
				address: &mongo_dal.AddressRecord{
					Zip:    TestZipCode,
					County: TestNonExistingCounty,
					City:   TestCity,
				},
			},
			want: expected{
				propertyId: TestPropertyId,
				err:        nil,
			},
			prepare: func(m *mocks) {
				MockFindZipCodeSuccess(m.transaction, TestZipCode)
				MockFindCountiesNotFound(m.transaction, TestNonExistingCounty, nil)
				MockFindNextIdSuccess(m.session, literals.CountyTableSequence, TestCountyCode)
				MockInsertOne(m.transaction, &COUNTIES{}, TestCountyCode, nil)
				MockFindCountyZipCode2Success(m.transaction, TestZipCode, TestCountyCode)
				MockFindCitySuccess(m.transaction, TestCity, TestCityCode)
				MockFindCityCodeZipCodeSuccess(m.transaction, TestZipCode, TestCityCode)
				MockFindNextIdSuccess(m.session, literals.PropertyTableSequence, TestPropertyId)
				MockInsertOne(m.transaction, &PROPERTY{}, TestPropertyId, nil)
			},
		},
		{
			name: "success - non existing city",
			args: args{
				address: &mongo_dal.AddressRecord{
					Zip:    TestZipCode,
					County: TestCounty,
					City:   TestNonExistingCity,
				},
			},
			want: expected{
				propertyId: TestPropertyId,
				err:        nil,
			},
			prepare: func(m *mocks) {
				MockFindZipCodeSuccess(m.transaction, TestZipCode)
				MockFindCountiesSuccess(m.transaction, TestCounty, TestCountyCode)
				MockFindCountyZipCode2Success(m.transaction, TestZipCode, TestCountyCode)
				MockFindCityNotFound(m.transaction, TestNonExistingCity, nil)
				MockFindNextIdSuccess(m.session, literals.CityTableSequence, TestCityCode)
				MockInsertOne(m.transaction, &CITIES{}, TestCityCode, nil)
				MockFindCityCodeZipCodeSuccess(m.transaction, TestZipCode, TestCityCode)
				MockFindNextIdSuccess(m.session, literals.PropertyTableSequence, TestPropertyId)
				MockInsertOne(m.transaction, &PROPERTY{}, TestPropertyId, nil)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			mockTransaction := mockXormwrapper.NewMockITransaction(mockCtrl)
			mockSession := mockXormwrapper.NewMockISession(mockCtrl)
			tt.prepare(&mocks{
				transaction: mockTransaction,
				session:     mockSession,
			})

			transaction := StarTransaction{
				transaction: mockTransaction,
				session:     &StarSession{session: mockSession},
				username:    "test",
			}
			propertyId, serr := transaction.CreatePropertyWithAddressInStarDB(context.Background(), tt.args.address)
			assert.Equal(t, tt.want.propertyId, propertyId)
			assert.Equal(t, tt.want.err, serr)
		})
	}
}

func TestStarTransaction_UpdatePropertyWithAddressInStarDB(t *testing.T) {
	type args struct {
		address    *mongo_dal.AddressRecord
		propertyId uint32
	}
	type expected struct {
	}
	type mocks struct {
		transaction *mockXormwrapper.MockITransaction
		session     *mockXormwrapper.MockISession
	}

	type test struct {
		name    string
		args    args
		want    expected
		prepare func(m *mocks)
	}

	tests := []test{
		{
			name: "success",
			args: args{
				address: &mongo_dal.AddressRecord{
					Zip:    TestZipCode,
					County: TestCounty,
					City:   TestCity,
				},
				propertyId: uint32(TestPropertyId),
			},
			prepare: func(m *mocks) {
				MockFindPropertySuccess(m.transaction, TestPropertyId)
				MockFindZipCodeSuccess(m.transaction, TestZipCode)
				MockFindCountiesSuccess(m.transaction, TestCounty, TestCountyCode)
				MockFindCountyZipCode2Success(m.transaction, TestZipCode, TestCountyCode)
				MockFindCitySuccess(m.transaction, TestCity, TestCityCode)
				MockFindCityCodeZipCodeSuccess(m.transaction, TestZipCode, TestCityCode)
				MockUpdateOne(m.transaction, &PROPERTY{}, 1, nil)
			},
		},
		{
			name: "non existing zip and no county",
			args: args{
				address: &mongo_dal.AddressRecord{
					Zip:    TestNonExistingZipCode,
					County: "",
					City:   TestCity,
				},
				propertyId: uint32(TestPropertyId),
			},
			prepare: func(m *mocks) {
				MockFindPropertySuccess(m.transaction, TestPropertyId)
				MockFindZipCodeNotFound(m.transaction, TestNonExistingZipCode, nil)
				MockInsertOne(m.transaction, &ZIP_CODE{}, 0, nil)
				MockFindCitySuccess(m.transaction, TestCity, TestCityCode)
				MockFindCityCodeZipCodeSuccess(m.transaction, TestNonExistingZipCode, TestCityCode)
				MockUpdateOne(m.transaction, &PROPERTY{}, 1, nil)
			},
		},
		{
			name: "success - non existing zip",
			args: args{
				address: &mongo_dal.AddressRecord{
					Zip:    TestNonExistingZipCode,
					County: TestCounty,
					City:   TestCity,
				},
				propertyId: uint32(TestPropertyId),
			},
			prepare: func(m *mocks) {
				MockFindPropertySuccess(m.transaction, TestPropertyId)
				MockFindZipCodeNotFound(m.transaction, TestNonExistingZipCode, nil)
				MockInsertOne(m.transaction, &ZIP_CODE{}, 0, nil)
				MockFindCountiesSuccess(m.transaction, TestCounty, TestCountyCode)
				MockFindCountyZipCode2Success(m.transaction, TestNonExistingZipCode, TestCountyCode)
				MockFindCitySuccess(m.transaction, TestCity, TestCityCode)
				MockFindCityCodeZipCodeSuccess(m.transaction, TestNonExistingZipCode, TestCityCode)
				MockUpdateOne(m.transaction, &PROPERTY{}, 1, nil)
			},
		},
		{
			name: "success - non existing county",
			args: args{
				address: &mongo_dal.AddressRecord{
					Zip:    TestZipCode,
					County: TestNonExistingCounty,
					City:   TestCity,
				},
				propertyId: uint32(TestPropertyId),
			},
			prepare: func(m *mocks) {
				MockFindPropertySuccess(m.transaction, TestPropertyId)
				MockFindZipCodeSuccess(m.transaction, TestZipCode)
				MockFindCountiesNotFound(m.transaction, TestNonExistingCounty, nil)
				MockFindNextIdSuccess(m.session, literals.CountyTableSequence, TestCountyCode)
				MockInsertOne(m.transaction, &COUNTIES{}, TestCountyCode, nil)
				MockFindCountyZipCode2Success(m.transaction, TestZipCode, TestCountyCode)
				MockFindCitySuccess(m.transaction, TestCity, TestCityCode)
				MockFindCityCodeZipCodeSuccess(m.transaction, TestZipCode, TestCityCode)
				MockUpdateOne(m.transaction, &PROPERTY{}, 1, nil)
			},
		},
		{
			name: "success - non existing city",
			args: args{
				address: &mongo_dal.AddressRecord{
					Zip:    TestZipCode,
					County: TestCounty,
					City:   TestNonExistingCity,
				},
				propertyId: uint32(TestPropertyId),
			},
			prepare: func(m *mocks) {
				MockFindPropertySuccess(m.transaction, TestPropertyId)
				MockFindZipCodeSuccess(m.transaction, TestZipCode)
				MockFindCountiesSuccess(m.transaction, TestCounty, TestCountyCode)
				MockFindCountyZipCode2Success(m.transaction, TestZipCode, TestCountyCode)
				MockFindCityNotFound(m.transaction, TestNonExistingCity, nil)
				MockFindNextIdSuccess(m.session, literals.CityTableSequence, TestCityCode)
				MockInsertOne(m.transaction, &CITIES{}, TestCityCode, nil)
				MockFindCityCodeZipCodeSuccess(m.transaction, TestZipCode, TestCityCode)
				MockUpdateOne(m.transaction, &PROPERTY{}, 1, nil)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			mockTransaction := mockXormwrapper.NewMockITransaction(mockCtrl)
			mockSession := mockXormwrapper.NewMockISession(mockCtrl)
			tt.prepare(&mocks{
				transaction: mockTransaction,
				session:     mockSession,
			})

			transaction := StarTransaction{
				transaction: mockTransaction,
				session:     &StarSession{session: mockSession},
				username:    "test",
			}
			transaction.UpdatePropertyWithAddressInStarDB(context.Background(), tt.args.address, tt.args.propertyId)
		})
	}
}

func TestStarTransaction_CreateCommercialPropertyWithAddress(t *testing.T) {
	type args struct {
		address *mongo_dal.AddressRecord
	}
	type expected struct {
		propertyId int64
		err        *addressErrors.ServerError
	}
	type mocks struct {
		transaction *mockXormwrapper.MockITransaction
		session     *mockXormwrapper.MockISession
	}

	type test struct {
		name    string
		args    args
		want    expected
		prepare func(m *mocks)
	}

	tests := []test{
		{
			name: "success",
			args: args{
				address: &mongo_dal.AddressRecord{
					Zip:    TestZipCode,
					County: TestCounty,
					City:   TestCity,
				},
			},
			want: expected{
				propertyId: TestPropertyId,
				err:        nil,
			},
			prepare: func(m *mocks) {
				MockFindZipCodeSuccess(m.transaction, TestZipCode)
				MockFindCountiesSuccess(m.transaction, TestCounty, TestCountyCode)
				MockFindCountyZipCode2Success(m.transaction, TestZipCode, TestCountyCode)
				MockFindCitySuccess(m.transaction, TestCity, TestCityCode)
				MockFindCityCodeZipCodeSuccess(m.transaction, TestZipCode, TestCityCode)
				MockFindNextIdSuccess(m.session, literals.CommercialPropertyTableSequence, TestPropertyId)
				MockInsertOne(m.transaction, &COMMERCIAL_PROPERTY{}, TestPropertyId, nil)
			},
		},
		{
			name: "non existing zip and no county",
			args: args{
				address: &mongo_dal.AddressRecord{
					Zip:    TestNonExistingZipCode,
					County: "",
					City:   TestCity,
				},
			},
			want: expected{
				propertyId: 0,
				err:        addressErrors.ZipCodeInvalid.New(),
			},
			prepare: func(m *mocks) {
				MockFindZipCodeNotFound(m.transaction, TestNonExistingZipCode, nil)
				MockInsertOne(m.transaction, &ZIP_CODE{}, 0, nil)
				MockFindCountyZipCodeNotFound(m.transaction, TestNonExistingZipCode, nil)
			},
		},
		{
			name: "success - non existing zip",
			args: args{
				address: &mongo_dal.AddressRecord{
					Zip:    TestNonExistingZipCode,
					County: TestCounty,
					City:   TestCity,
				},
			},
			want: expected{
				propertyId: TestPropertyId,
				err:        nil,
			},
			prepare: func(m *mocks) {
				MockFindZipCodeNotFound(m.transaction, TestNonExistingZipCode, nil)
				MockInsertOne(m.transaction, &ZIP_CODE{}, 0, nil)
				MockFindCountiesSuccess(m.transaction, TestCounty, TestCountyCode)
				MockFindCountyZipCode2Success(m.transaction, TestNonExistingZipCode, TestCountyCode)
				MockFindCitySuccess(m.transaction, TestCity, TestCityCode)
				MockFindCityCodeZipCodeSuccess(m.transaction, TestNonExistingZipCode, TestCityCode)
				MockFindNextIdSuccess(m.session, literals.CommercialPropertyTableSequence, TestPropertyId)
				MockInsertOne(m.transaction, &COMMERCIAL_PROPERTY{}, TestPropertyId, nil)
			},
		},
		{
			name: "success - non existing county",
			args: args{
				address: &mongo_dal.AddressRecord{
					Zip:    TestZipCode,
					County: TestNonExistingCounty,
					City:   TestCity,
				},
			},
			want: expected{
				propertyId: TestPropertyId,
				err:        nil,
			},
			prepare: func(m *mocks) {
				MockFindZipCodeSuccess(m.transaction, TestZipCode)
				MockFindCountiesNotFound(m.transaction, TestNonExistingCounty, nil)
				MockFindNextIdSuccess(m.session, literals.CountyTableSequence, TestCountyCode)
				MockInsertOne(m.transaction, &COUNTIES{}, TestCountyCode, nil)
				MockFindCountyZipCode2Success(m.transaction, TestZipCode, TestCountyCode)
				MockFindCitySuccess(m.transaction, TestCity, TestCityCode)
				MockFindCityCodeZipCodeSuccess(m.transaction, TestZipCode, TestCityCode)
				MockFindNextIdSuccess(m.session, literals.CommercialPropertyTableSequence, TestPropertyId)
				MockInsertOne(m.transaction, &COMMERCIAL_PROPERTY{}, TestPropertyId, nil)
			},
		},
		{
			name: "success - non existing city",
			args: args{
				address: &mongo_dal.AddressRecord{
					Zip:    TestZipCode,
					County: TestCounty,
					City:   TestNonExistingCity,
				},
			},
			want: expected{
				propertyId: TestPropertyId,
				err:        nil,
			},
			prepare: func(m *mocks) {
				MockFindZipCodeSuccess(m.transaction, TestZipCode)
				MockFindCountiesSuccess(m.transaction, TestCounty, TestCountyCode)
				MockFindCountyZipCode2Success(m.transaction, TestZipCode, TestCountyCode)
				MockFindCityNotFound(m.transaction, TestNonExistingCity, nil)
				MockFindNextIdSuccess(m.session, literals.CityTableSequence, TestCityCode)
				MockInsertOne(m.transaction, &CITIES{}, TestCityCode, nil)
				MockFindCityCodeZipCodeSuccess(m.transaction, TestZipCode, TestCityCode)
				MockFindNextIdSuccess(m.session, literals.CommercialPropertyTableSequence, TestPropertyId)
				MockInsertOne(m.transaction, &COMMERCIAL_PROPERTY{}, TestPropertyId, nil)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			mockTransaction := mockXormwrapper.NewMockITransaction(mockCtrl)
			mockSession := mockXormwrapper.NewMockISession(mockCtrl)
			tt.prepare(&mocks{
				transaction: mockTransaction,
				session:     mockSession,
			})

			transaction := StarTransaction{
				transaction: mockTransaction,
				session:     &StarSession{session: mockSession},
				username:    "test",
			}
			propertyId, serr := transaction.CreateCommercialPropertyWithAddress(context.Background(), tt.args.address)
			assert.Equal(t, tt.want.propertyId, propertyId)
			assert.Equal(t, tt.want.err, serr)
		})
	}
}

func TestStarTransaction_UpdateCommercialPropertyWithAddress(t *testing.T) {
	type args struct {
		address    *mongo_dal.AddressRecord
		propertyId uint32
	}
	type expected struct {
		affected int64
		err      *addressErrors.ServerError
	}
	type mocks struct {
		transaction *mockXormwrapper.MockITransaction
		session     *mockXormwrapper.MockISession
	}

	type test struct {
		name    string
		args    args
		want    expected
		prepare func(m *mocks)
	}

	tests := []test{
		{
			name: "success",
			args: args{
				address: &mongo_dal.AddressRecord{
					Zip:    TestZipCode,
					County: TestCounty,
					City:   TestCity,
				},
				propertyId: uint32(TestPropertyId),
			},
			want: expected{
				affected: 1,
				err:      nil,
			},
			prepare: func(m *mocks) {
				MockFindCommercialPropertySuccess(m.transaction, TestPropertyId)
				MockFindZipCodeSuccess(m.transaction, TestZipCode)
				MockFindCountiesSuccess(m.transaction, TestCounty, TestCountyCode)
				MockFindCountyZipCode2Success(m.transaction, TestZipCode, TestCountyCode)
				MockFindCitySuccess(m.transaction, TestCity, TestCityCode)
				MockFindCityCodeZipCodeSuccess(m.transaction, TestZipCode, TestCityCode)
				MockUpdateOne(m.transaction, &COMMERCIAL_PROPERTY{}, 1, nil)
			},
		},
		{
			name: "non existing zip and no county",
			args: args{
				address: &mongo_dal.AddressRecord{
					Zip:    TestNonExistingZipCode,
					County: "",
					City:   TestCity,
				},
				propertyId: uint32(TestPropertyId),
			},
			want: expected{
				affected: 1,
				err:      nil,
			},
			prepare: func(m *mocks) {
				MockFindCommercialPropertySuccess(m.transaction, TestPropertyId)
				MockFindZipCodeNotFound(m.transaction, TestNonExistingZipCode, nil)
				MockInsertOne(m.transaction, &ZIP_CODE{}, 0, nil)
				MockFindCitySuccess(m.transaction, TestCity, TestCityCode)
				MockFindCityCodeZipCodeSuccess(m.transaction, TestNonExistingZipCode, TestCityCode)
				MockUpdateOne(m.transaction, &COMMERCIAL_PROPERTY{}, 1, nil)
			},
		},
		{
			name: "success - non existing zip",
			args: args{
				address: &mongo_dal.AddressRecord{
					Zip:    TestNonExistingZipCode,
					County: TestCounty,
					City:   TestCity,
				},
				propertyId: uint32(TestPropertyId),
			},
			want: expected{
				affected: 1,
				err:      nil,
			},
			prepare: func(m *mocks) {
				MockFindCommercialPropertySuccess(m.transaction, TestPropertyId)
				MockFindZipCodeNotFound(m.transaction, TestNonExistingZipCode, nil)
				MockInsertOne(m.transaction, &ZIP_CODE{}, 0, nil)
				MockFindCountiesSuccess(m.transaction, TestCounty, TestCountyCode)
				MockFindCountyZipCode2Success(m.transaction, TestNonExistingZipCode, TestCountyCode)
				MockFindCitySuccess(m.transaction, TestCity, TestCityCode)
				MockFindCityCodeZipCodeSuccess(m.transaction, TestNonExistingZipCode, TestCityCode)
				MockUpdateOne(m.transaction, &COMMERCIAL_PROPERTY{}, 1, nil)
			},
		},
		{
			name: "success - non existing county",
			args: args{
				address: &mongo_dal.AddressRecord{
					Zip:    TestZipCode,
					County: TestNonExistingCounty,
					City:   TestCity,
				},
				propertyId: uint32(TestPropertyId),
			},
			want: expected{
				affected: 1,
				err:      nil,
			},
			prepare: func(m *mocks) {
				MockFindCommercialPropertySuccess(m.transaction, TestPropertyId)
				MockFindZipCodeSuccess(m.transaction, TestZipCode)
				MockFindCountiesNotFound(m.transaction, TestNonExistingCounty, nil)
				MockFindNextIdSuccess(m.session, literals.CountyTableSequence, TestCountyCode)
				MockInsertOne(m.transaction, &COUNTIES{}, TestCountyCode, nil)
				MockFindCountyZipCode2Success(m.transaction, TestZipCode, TestCountyCode)
				MockFindCitySuccess(m.transaction, TestCity, TestCityCode)
				MockFindCityCodeZipCodeSuccess(m.transaction, TestZipCode, TestCityCode)
				MockUpdateOne(m.transaction, &COMMERCIAL_PROPERTY{}, 1, nil)
			},
		},
		{
			name: "success - non existing city",
			args: args{
				address: &mongo_dal.AddressRecord{
					Zip:    TestZipCode,
					County: TestCounty,
					City:   TestNonExistingCity,
				},
				propertyId: uint32(TestPropertyId),
			},
			want: expected{
				affected: 1,
				err:      nil,
			},
			prepare: func(m *mocks) {
				MockFindCommercialPropertySuccess(m.transaction, TestPropertyId)
				MockFindZipCodeSuccess(m.transaction, TestZipCode)
				MockFindCountiesSuccess(m.transaction, TestCounty, TestCountyCode)
				MockFindCountyZipCode2Success(m.transaction, TestZipCode, TestCountyCode)
				MockFindCityNotFound(m.transaction, TestNonExistingCity, nil)
				MockFindNextIdSuccess(m.session, literals.CityTableSequence, TestCityCode)
				MockInsertOne(m.transaction, &CITIES{}, TestCityCode, nil)
				MockFindCityCodeZipCodeSuccess(m.transaction, TestZipCode, TestCityCode)
				MockUpdateOne(m.transaction, &COMMERCIAL_PROPERTY{}, 1, nil)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			mockTransaction := mockXormwrapper.NewMockITransaction(mockCtrl)
			mockSession := mockXormwrapper.NewMockISession(mockCtrl)
			tt.prepare(&mocks{
				transaction: mockTransaction,
				session:     mockSession,
			})

			transaction := StarTransaction{
				transaction: mockTransaction,
				session:     &StarSession{session: mockSession},
				username:    "test",
			}
			affected, err := transaction.UpdateCommercialPropertyWithAddress(context.Background(), tt.args.address, tt.args.propertyId)
			assert.Equal(t, tt.want.affected, affected)
			assert.Equal(t, tt.want.err, err)
		})
	}
}
