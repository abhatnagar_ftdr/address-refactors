package star

import (
	"context"
	"go.ftdr.com/go-utils/xormwrapper/v2"
	"strings"

	"go.ftdr.com/go-utils/common/logging"
	"go.ftdr.com/go-utils/instrumentation/v3"
	"go.ftdr.com/go-utils/xormwrapper/v2/config"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
)

type IStarEngine interface {
	StartTransaction(ctx context.Context, username string, transactionType int) (IStarTransaction, error)
	StartSession(ctx context.Context) IStarSession
	Ping() error
}

type StarEngine struct {
	xormwrapper.IEngine
}

func NewStarEngine(ctx context.Context, config *config.OracleConfig) (IStarEngine, error) {
	odb, err := xormwrapper.NewEngine(ctx, config)
	if err != nil {
		logging.GetTracedLogEntry(ctx).WithError(err).Error("xorm engine creation failed")
		return nil, err
	}
	return &StarEngine{odb}, nil
}

func (s *StarEngine) StartTransaction(ctx context.Context, username string, transactionType int) (IStarTransaction, error) {
	fields := map[string]interface{}{
		"txnType": transactionType,
	}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.StartTransaction, literals.ComponentTypeStar)
	transaction, err := s.GetNewTransaction(ctx, transactionType)
	if err != nil {
		logging.GetTracedLogEntry(ctx).WithError(err).Error("xorm transaction creation failed")
		fields[literals.LLErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, err
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return &StarTransaction{
		transaction: transaction,
		session:     &StarSession{session: transaction.GetSession()},
		username:    strings.ToUpper(username),
	}, nil
}

func (s *StarEngine) StartSession(ctx context.Context) IStarSession {
	fields := map[string]interface{}{}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.StartSession, literals.ComponentTypeStar)
	session := s.GetNewSession(ctx)
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return &StarSession{session: session}
}

func (s *StarEngine) Ping() error {
	return s.IEngine.Ping()
}
