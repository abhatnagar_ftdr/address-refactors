package star

import (
	"database/sql"
)

type DUAL struct {
	Nextval int64 `xorm:"'NEXTVAL'"`
}

// COUNTRIES represent an entry in the countries table
type COUNTRIES struct {
	CountryCode    int64        `xorm:"NUMBER(20) pk not null 'COUNTRY_CODE'"`
	CountryName    string       `xorm:"VARCHAR2(64) 'COUNTRY_NAME'"`
	LastModified   sql.NullTime `xorm:"DATE 'LAST_MODIFIED'"`
	LastModifiedBy string       `xorm:"VARCHAR2(31) 'LAST_MODIFIED_BY'"`
}

// CITIES represent an entry in the cities table
type CITIES struct {
	CityCode       int64        `xorm:"NUMBER(20) pk not null 'CITY_CODE'"`
	CityName       string       `xorm:"VARCHAR2(64) 'CITY_NAME'"`
	LastModified   sql.NullTime `xorm:"DATE 'LAST_MODIFIED'"`
	LastModifiedBy string       `xorm:"VARCHAR2(31) 'LAST_MODIFIED_BY'"`
}

// COUNTIES represent an entry in the counties table
type COUNTIES struct {
	CountyCode     int64        `xorm:"NUMBER(20) pk not null 'COUNTY_CODE'"`
	CountyName     string       `xorm:"VARCHAR2(64) 'COUNTY_NAME'"`
	LastModified   sql.NullTime `xorm:"DATE 'LAST_MODIFIED'"`
	LastModifiedBy string       `xorm:"VARCHAR2(31) 'LAST_MODIFIED_BY'"`
}

// PROPERTY represents a property table in the oracle db
type PROPERTY struct {
	PropertyID           int64         `xorm:"NUMBER(20) pk not null 'PROPERTY_ID'"`
	TypeCode             string        `xorm:"VARCHAR2(10) 'TYPE_CODE'"`
	TypePrefix           string        `xorm:"VARCHAR2(10) 'TYPE_PREFIX'"`
	DwellingTypeCode     string        `xorm:"VARCHAR2(10) 'DWELLING_TYPE_CODE'"`
	DwellingTypePrefix   string        `xorm:"VARCHAR2(10) 'DWELLING_TYPE_PREFIX'"`
	StreetNumber         string        `xorm:"VARCHAR2(25) 'STREET_NUMBER'"`
	StreetDirection      string        `xorm:"CHAR(2) 'STREET_DIRECTION'"`
	StreetName           string        `xorm:"VARCHAR2(45) 'STREET_NAME'"`
	UnitNumber           string        `xorm:"VARCHAR2(15) 'UNIT_NUMBER'"`
	CityCode             string        `xorm:"VARCHAR2(10) 'CITY_CODE'"`
	State                string        `xorm:"CHAR(2) 'STATE'"`
	ZipCode              string        `xorm:"VARCHAR2(10) 'ZIP_CODE'"`
	UnitType             string        `xorm:"VARCHAR2(10) 'UNIT_TYPE'"`
	UnitPrefix           string        `xorm:"VARCHAR2(10) 'UNIT_PREFIX'"`
	CountyCode           string        `xorm:"VARCHAR2(10) 'COUNTY_CODE'"`
	CountryCode          string        `xorm:"VARCHAR2(10) 'COUNTRY_CODE'"`
	SquareFeet           sql.NullInt32 `xorm:"NUMBER(6) 'SQUARE_FEET'"`
	NumberMotherLawUnits sql.NullInt32 `xorm:"NUMBER(6) 'NUMBER_MOTHER_LAW_UNITS'"`
	YearBuilt            sql.NullTime  `xorm:"DATE 'YEAR_BUILT'"`
	PropertyValue        sql.NullInt32 `xorm:"NUMBER(12) 'PROPERTY_VALUE'"`
	ZipPlus4             string        `xorm:"VARCHAR2(4) 'ZIP_PLUS_4'"`
	SfiReqFlag           string        `xorm:"CHAR(1) 'SFI_REQ_FLAG'"`
	LastModified         sql.NullTime  `xorm:"DATE 'LAST_MODIFIED'"`
	LastModifiedBy       string        `xorm:"VARCHAR2(31) 'LAST_MODIFIED_BY'"`
	StandardizedStatus   string        `xorm:"VARCHAR2(10) 'STANDARDIZED_STATUS'"`
	StandardizedDesc     string        `xorm:"VARCHAR2(200) 'STANDARDIZED_DESC'"`
}

type COMMERCIAL_PROPERTY struct {
	CommercialPropertyID int64         `xorm:"NUMBER(20) pk not null 'COMMERCIAL_PROPERTY_ID'"`
	UnitDesignation      string        `xorm:"VARCHAR2(4) 'UNIT_DESIGNATION'"`
	StreetNumber         string        `xorm:"VARCHAR2(25) 'STREET_NUMBER'"`
	StreetDirection      string        `xorm:"CHAR(2) 'STREET_DIRECTION'"`
	StreetName           string        `xorm:"VARCHAR2(30) 'STREET_NAME'"`
	UnitNumber           string        `xorm:"VARCHAR2(6) 'UNIT_NUMBER'"`
	CityCode             string        `xorm:"VARCHAR2(10) 'CITY_CODE'"`
	State                string        `xorm:"CHAR(2) 'STATE'"`
	ZipCode              string        `xorm:"VARCHAR2(10) 'ZIP_CODE'"`
	ZipPlus4             string        `xorm:"VARCHAR2(4) 'ZIP_PLUS_4'"`
	UnitType             string        `xorm:"VARCHAR2(10) 'UNIT_TYPE'"`
	UnitPrefix           string        `xorm:"VARCHAR2(10) 'UNIT_PREFIX'"`
	CountyCode           string        `xorm:"VARCHAR2(10) 'COUNTY_CODE'"`
	CountryCode          string        `xorm:"VARCHAR2(10) 'COUNTRY_CODE'"`
	PersonId             sql.NullInt64 `xorm:"NUMBER(20) 'PERSON_ID'"`
	SfiReqFlag           string        `xorm:"CHAR(1) 'SFI_REQ_FLAG'"`
	LastModified         sql.NullTime  `xorm:"DATE 'LAST_MODIFIED'"`
	LastModifiedBy       string        `xorm:"VARCHAR2(31) 'LAST_MODIFIED_BY'"`
	StandardizedStatus   string        `xorm:"VARCHAR2(10) 'STANDARDIZED_STATUS'"`
	StandardizedDesc     string        `xorm:"VARCHAR2(200) 'STANDARDIZED_DESC'"`
}

// CITY_CODE_ZIP_CODE represents a property table in the oracle db
type CITY_CODE_ZIP_CODE struct {
	ZipCode        string       `xorm:"VARCHAR2(10) 'ZIP_CODE'"`
	CityCode       string       `xorm:"VARCHAR2(10) 'CITY_CODE'"`
	LastModified   sql.NullTime `xorm:"DATE 'LAST_MODIFIED'"`
	LastModifiedBy string       `xorm:"VARCHAR2(31) 'LAST_MODIFIED_BY'"`
}

// COUNTY_ZIP_CODE represents a property table in the oracle db
type COUNTY_ZIP_CODE struct {
	ZipCode        string       `xorm:"VARCHAR2(10) 'ZIP_CODE'"`
	CountyCode     string       `xorm:"VARCHAR2(10) 'COUNTY_CODE'"`
	LastModified   sql.NullTime `xorm:"DATE 'LAST_MODIFIED'"`
	LastModifiedBy string       `xorm:"VARCHAR2(31) 'LAST_MODIFIED_BY'"`
}

// ZIP_CODE represents a property table in the oracle db
type ZIP_CODE struct {
	ZipCode        string       `xorm:"VARCHAR2(10) 'ZIP_CODE'"`
	CountryCode    string       `xorm:"VARCHAR2(10) 'COUNTRY_CODE'"`
	LastModified   sql.NullTime `xorm:"DATE 'LAST_MODIFIED'"`
	LastModifiedBy string       `xorm:"VARCHAR2(31) 'LAST_MODIFIED_BY'"`
	Latitude       float64      `xorm:"NUMBER(9,4) 'LATITUDE'"`
	Longitude      float64      `xorm:"NUMBER(9,4) 'LONGITUDE'"`
}
