package star

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"go.ftdr.com/go-utils/common/logging"
	"go.ftdr.com/go-utils/instrumentation/v3"
	"go.ftdr.com/go-utils/xormwrapper/v2"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/mongo_dal"
	addressErrors "golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors"
)

type IStarSession interface {
	Close(ctx context.Context) error
	GetProperty(ctx context.Context, id uint32) (*PROPERTY, *addressErrors.ServerError)
	GetCommercialProperty(ctx context.Context, id uint32) (*COMMERCIAL_PROPERTY, *addressErrors.ServerError)
	GetAddress(ctx context.Context, id uint32) (*mongo_dal.AddressRecord, *addressErrors.ServerError)
	GetAddressFromCommercialProperty(ctx context.Context, id uint32) (*mongo_dal.AddressRecord, *addressErrors.ServerError)
	GetCityName(ctx context.Context, id int) (string, *addressErrors.ServerError)
	GetCountyName(ctx context.Context, id int) (string, *addressErrors.ServerError)
}

type StarSession struct {
	session xormwrapper.ISession
}

func (s *StarSession) Close(ctx context.Context) error {
	return s.session.Close()
}

func (s *StarSession) GetProperty(ctx context.Context, id uint32) (*PROPERTY, *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)
	fields := map[string]interface{}{
		"propertyID": id,
	}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.GetProperty, literals.ComponentPropertyStarDB)
	options := []PROPERTY{}
	err := s.session.FindByStruct(&options, &PROPERTY{PropertyID: int64(id)}, nil)
	if err != nil {
		fields[literals.LLErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, addressErrors.StarQueryError.NewFormatted(err.Error())
	}

	if len(options) == 0 {
		fields[literals.LLErrorMessage] = addressErrors.PropertyNotFound.New().Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, addressErrors.PropertyNotFound.New()
	}

	log.Infof("found %d entry in star db for legacy id %d", len(options), id)
	fields["entryCount"] = len(options)
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return &options[0], nil
}

func (s *StarSession) GetCommercialProperty(ctx context.Context, id uint32) (*COMMERCIAL_PROPERTY, *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)
	options := []COMMERCIAL_PROPERTY{}
	fields := map[string]interface{}{
		"propertyID": id,
	}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.GetCommercialProperty, literals.ComponentPropertyStarDB)
	err := s.session.FindByStruct(&options, &COMMERCIAL_PROPERTY{CommercialPropertyID: int64(id)}, nil)
	if err != nil {
		fields[literals.LLErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, addressErrors.StarQueryError.NewFormatted(err.Error())
	}

	if len(options) == 0 {
		fields[literals.LLErrorMessage] = addressErrors.PropertyNotFound.New().Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, addressErrors.PropertyNotFound.New()
	}

	log.Infof("found %d entry in star db for legacy id %d", len(options), id)
	fields["entryCount"] = len(options)
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return &options[0], nil
}

func (s *StarSession) GetAddress(ctx context.Context, id uint32) (*mongo_dal.AddressRecord, *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)
	fields := map[string]interface{}{
		"propertyID": id,
	}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.GetAddress, literals.ComponentPropertyStarDB)
	props, err := s.GetProperty(ctx, id)
	if err != nil {
		fields[literals.LLErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, err
	}
	log.WithField("Property From Star", PropertyToJSON(props)).Info("property from star")
	address := &mongo_dal.AddressRecord{}
	// if street direction is set to NA, it means that we don't have a direction on the
	// original address so we set don't set the direction in re-constructing the address
	if props.StreetDirection != "NA" {
		address.Street1 = props.StreetNumber + " " + props.StreetDirection + " " + props.StreetName
	} else {
		address.Street1 = props.StreetNumber + " " + props.StreetName
	}

	address.State = props.State
	if props.UnitType == "ZZ" && props.UnitNumber == "" {
		address.UnitType = ""
	} else {
		address.UnitType = props.UnitType
	}
	address.UnitValue = props.UnitNumber
	address.Zip = props.ZipCode

	if props.ZipPlus4 != "" {
		address.ZipLocal = fmt.Sprintf("%s-%s", props.ZipCode, props.ZipPlus4)
	} else {
		address.ZipLocal = props.ZipCode
	}

	address.LegacyIDList = append(address.LegacyIDList, uint32(props.PropertyID))
	// query for the city code
	cid, _ := strconv.Atoi(props.CityCode)

	cityName, err := s.getCityNameFromStarDB(ctx, cid)
	if err != nil {
		fields[literals.LLErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, err
	}

	// query for the country name
	ccid, _ := strconv.Atoi(props.CountryCode)
	if ccid != 1 {
		log.Warn("Country Code is not 1. CountryIso3 saved as \"USA\". ccid =", ccid)
	}

	address.City = cityName
	address.CountryIso3 = literals.CountryCodeUSA
	// if we have the county defined we need to get
	if props.CountyCode != "" {
		cid, _ := strconv.Atoi(props.CountyCode)
		address.County, _ = s.getCountyNameFromStarDB(ctx, cid)
	}

	address.Format()
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return address, nil
}

func (s *StarSession) GetAddressFromCommercialProperty(ctx context.Context, id uint32) (*mongo_dal.AddressRecord, *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)
	fields := map[string]interface{}{
		"propertyID": id,
	}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.GetAddressFromCommercialProperty, literals.ComponentPropertyStarDB)
	props, err := s.GetCommercialProperty(ctx, id)
	if err != nil {
		fields[literals.LLErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, err
	}
	log.WithField("Property From Star", CommercialPropertyToJSON(props)).Info("property from star")
	address := &mongo_dal.AddressRecord{}
	// if street direction is set to NA, it means that we don't have a direction on the
	// original address so we set don't set the direction in re-constructing the address
	if props.StreetDirection != "NA" {
		address.Street1 = props.StreetNumber + " " + props.StreetDirection + " " + props.StreetName
	} else {
		address.Street1 = props.StreetNumber + " " + props.StreetName
	}

	address.State = props.State
	if props.UnitType == "ZZ" && props.UnitNumber == "" {
		address.UnitType = ""
	} else {
		address.UnitType = props.UnitType
	}
	address.Zip = props.ZipCode

	if props.ZipPlus4 != "" {
		address.ZipLocal = fmt.Sprintf("%s-%s", props.ZipCode, props.ZipPlus4)
	} else {
		address.ZipLocal = props.ZipCode
	}

	address.ExternalSourceList = append(address.ExternalSourceList, mongo_dal.ExternalSource{
		Name:        "COMMERCIAL_PROPERTY",
		ID:          id,
		LastUpdated: primitive.NewDateTimeFromTime(time.Now()),
	})
	// query for the city code
	cid, _ := strconv.Atoi(props.CityCode)

	cityName, err := s.getCityNameFromStarDB(ctx, cid)
	if err != nil {
		fields[literals.LLErrorMessage] = err.Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return nil, err
	}

	// query for the country name
	ccid, _ := strconv.Atoi(props.CountryCode)
	if ccid != 1 {
		log.Warn("Country Code is not 1. CountryIso3 saved as \"USA\". ccid =", ccid)
	}

	address.City = cityName
	address.CountryIso3 = literals.CountryCodeUSA
	// if we have the county defined we need to get
	if props.CountyCode != "" {
		cid, _ := strconv.Atoi(props.CountyCode)
		address.County, _ = s.getCountyNameFromStarDB(ctx, cid)
	}

	address.Format()
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return address, nil
}

func (s *StarSession) getCityNameFromStarDB(ctx context.Context, id int) (string, *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)
	if id == 0 {
		return "", addressErrors.RequiredFieldMissing.NewFormatted("id")
	}

	spanID := instrumentation.ComponentSpanStart(ctx, literals.FetchCityName, literals.ComponentPropertyStarDB)
	componentContextData := map[string]interface{}{
		"CityCode": id,
	}

	log.Infof("querying city name table in star db for city code %d", id)
	options := []CITIES{}

	err := s.session.FindByStruct(&options, &CITIES{CityCode: int64(id)}, nil)
	if err != nil {
		componentContextData[literals.LLInternalError] = err.Error()
		componentContextData[literals.LLErrorMessage] = "failed to fetch city name from star"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return "", addressErrors.StarQueryError.NewFormatted(err.Error())
	}

	if len(options) == 0 {
		componentContextData[literals.LLErrorMessage] = "no record found in star for city code"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return "", addressErrors.CityNameNotFound.New()
	}

	log.Infof("found %d record in star db for city with city code %d", len(options), id)
	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return options[0].CityName, nil
}

func (s *StarSession) getCountyNameFromStarDB(ctx context.Context, id int) (string, *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)
	if id == 0 {
		return "", addressErrors.RequiredFieldMissing.NewFormatted("id")
	}

	spanID := instrumentation.ComponentSpanStart(ctx, literals.FetchCountyName, literals.ComponentPropertyStarDB)
	componentContextData := map[string]interface{}{
		"CountyCode": id,
	}

	options := []COUNTIES{}

	err := s.session.FindByStruct(&options, &COUNTIES{CountyCode: int64(id)}, nil)
	if err != nil {
		componentContextData[literals.LLInternalError] = err.Error()
		componentContextData[literals.LLErrorMessage] = "failed to fetch county name from star"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return "", addressErrors.StarQueryError.NewFormatted(err.Error())
	}

	if len(options) == 0 {
		componentContextData[literals.LLErrorMessage] = "no record found for county code in star"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return "", addressErrors.CountyNameNotFound.New()
	}

	log.Infof("found %d county name record in star for county code %d", len(options), id)
	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return options[0].CountyName, nil
}

func (s *StarSession) getCountryNameFromStarDB(ctx context.Context, id int) (string, *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)
	if id == 0 {
		return "", addressErrors.RequiredFieldMissing.NewFormatted("id")
	}

	spanID := instrumentation.ComponentSpanStart(ctx, literals.FetchCountryName, literals.ComponentPropertyStarDB)
	componentContextData := map[string]interface{}{
		"CountryCode": id,
	}

	options := []COUNTRIES{}

	err := s.session.FindByStruct(&options, &COUNTRIES{CountryCode: int64(id)}, nil)
	if err != nil {
		componentContextData[literals.LLInternalError] = err.Error()
		componentContextData[literals.LLErrorMessage] = "failed to fetch country name from star"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return "", addressErrors.StarQueryError.NewFormatted(err.Error())
	}

	if len(options) == 0 {
		componentContextData[literals.LLErrorMessage] = "no record found in star for given country code"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return "", addressErrors.CountryNameNotFound.New()
	}

	log.Infof("found %d country name record in star for country code %d", len(options), id)
	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return options[0].CountryName, nil
}
