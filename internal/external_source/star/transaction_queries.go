package star

import (
	"context"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	"go.ftdr.com/go-utils/common/logging"
	"go.ftdr.com/go-utils/instrumentation/v3"
	"go.ftdr.com/go-utils/xormwrapper/v2"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/mongo_dal"
	addressErrors "golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors"
)

// Don't query STAR. We are only dealing with US addresses.
const (
	countryCode = 1
)

func (s *StarTransaction) GetCityCode(ctx context.Context, cityName string, zip string) (int64, *addressErrors.ServerError) {
	if cityName == "" {
		return int64(0), addressErrors.RequiredFieldMissing.NewFormatted("cityName")
	}

	if zip == "" {
		return int64(0), addressErrors.RequiredFieldMissing.NewFormatted("zip")
	}

	spanID := instrumentation.ComponentSpanStart(ctx, literals.FetchCityCode, literals.ComponentPropertyStarDB)
	componentContextData := map[string]interface{}{
		"CityName": cityName,
		"Zip":      zip,
	}

	options := []CITIES{}

	err := s.transaction.FindByStruct(&options, &CITIES{CityName: strings.ToUpper(cityName)}, nil)
	if err != nil {
		componentContextData[literals.LLInternalError] = err.Error()
		componentContextData[literals.LLErrorMessage] = "failed to fetch city code from star"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return 0, addressErrors.StarQueryError.NewFormatted(err.Error())
	}

	var cityCode int64
	if len(options) > 0 {
		cityCode = options[len(options)-1].CityCode
	} else {
		cityCode, err = s.createCityCode(ctx, cityName, zip)
		if err != nil {
			componentContextData[literals.LLInternalError] = err.Error()
			componentContextData[literals.LLErrorMessage] = "failed to create city code"
			instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
			return 0, addressErrors.StarQueryError.NewFormatted(err.Error())
		}
	}

	err = s.createCityCodeZipCode(ctx, cityCode, zip)
	if err != nil {
		componentContextData[literals.LLInternalError] = err.Error()
		componentContextData[literals.LLErrorMessage] = "failed to create city code zip code"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return 0, addressErrors.StarQueryError.NewFormatted(err.Error())
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return cityCode, nil
}

func (s *StarTransaction) GetCountyCode(ctx context.Context, countyName string, zip string) (int64, *addressErrors.ServerError) {
	fields := map[string]interface{}{
		"countyName": countyName,
		"zip":        zip,
	}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.GetCountyCode, literals.ComponentPropertyStarDB)
	if zip == "" {
		fields[literals.LLErrorMessage] = addressErrors.RequiredFieldMissing.NewFormatted("zip").Error()
		instrumentation.ComponentSpanEnd(ctx, spanID, false, fields)
		return int64(0), addressErrors.RequiredFieldMissing.NewFormatted("zip")
	}
	if countyName == "" {
		instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
		return s.GetCountyCodeByZipCode(ctx, zip)
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, true, fields)
	return s.GetCountyCodeByCountyName(ctx, countyName, zip)
}

func (s *StarTransaction) GetCountyCodeByCountyName(ctx context.Context, countyName string, zip string) (int64, *addressErrors.ServerError) {
	spanID := instrumentation.ComponentSpanStart(ctx, literals.FetchCountyCode, literals.ComponentPropertyStarDB)
	componentContextData := map[string]interface{}{
		"CountyName": countyName,
		"Zip":        zip,
	}

	options := []COUNTIES{}
	err := s.transaction.FindByStruct(&options, &COUNTIES{CountyName: strings.ToUpper(countyName)}, nil)
	if err != nil {
		componentContextData[literals.LLInternalError] = err.Error()
		componentContextData[literals.LLErrorMessage] = "failed to fetch county code from star"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return 0, addressErrors.StarQueryError.NewFormatted(err.Error())
	}

	var countyCode int64
	if len(options) == 0 {
		countyCode, err = s.createCountyCode(ctx, countyName, zip)
		if err != nil {
			componentContextData[literals.LLInternalError] = err.Error()
			componentContextData[literals.LLErrorMessage] = "failed to create county code in star"
			instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
			return 0, addressErrors.StarQueryError.NewFormatted(err.Error())
		}
	} else {
		countyCode = options[len(options)-1].CountyCode
	}

	err = s.createCountyZipCode(ctx, countyCode, zip)
	if err != nil {
		componentContextData[literals.LLInternalError] = err.Error()
		componentContextData[literals.LLErrorMessage] = "failed to create county zip code in star"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return 0, addressErrors.StarQueryError.NewFormatted(err.Error())
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return countyCode, nil
}

func (s *StarTransaction) GetCountyCodeByZipCode(ctx context.Context, zip string) (int64, *addressErrors.ServerError) {
	spanID := instrumentation.ComponentSpanStart(ctx, literals.FetchZipcodeCountyCode, literals.ComponentPropertyStarDB)
	componentContextData := map[string]interface{}{
		"Zip": zip,
	}

	options := []COUNTY_ZIP_CODE{}

	err := s.transaction.FindByStruct(&options, &COUNTY_ZIP_CODE{ZipCode: zip}, nil)
	if err != nil {
		componentContextData[literals.LLInternalError] = err.Error()
		componentContextData[literals.LLErrorMessage] = "failed to fetch county zip code from star"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return 0, addressErrors.StarQueryError.NewFormatted(err.Error())
	}

	if len(options) >= 1 {
		var countyCode int64
		countyCode, err = strconv.ParseInt(options[len(options)-1].CountyCode, 10, 64)
		if err != nil {
			componentContextData[literals.LLInternalError] = err.Error()
			componentContextData[literals.LLErrorMessage] = "failed to convert county code from star"
			instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
			return 0, addressErrors.CountyCodeParseError.New()
		}
		instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
		return countyCode, nil
	}

	componentContextData[literals.LLErrorMessage] = "county zip code not found"
	instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
	return 0, addressErrors.CountyZipCodeNotFound.New()
}

func (s *StarTransaction) UpdatePropertyWithAddressInStarDB(ctx context.Context, address *mongo_dal.AddressRecord, id uint32) {
	log := logging.GetTracedLogEntry(ctx)
	if address == nil {
		return
	}

	if id == 0 {
		return
	}

	spanID := instrumentation.ComponentSpanStart(
		ctx,
		literals.UpdatePropertyWithAddress,
		literals.ComponentPropertyStarDB)

	addressBytes, _ := json.Marshal(address)

	componentContextData := map[string]interface{}{
		"Address":  string(addressBytes),
		"LegacyID": id,
	}
	// first we need to load from star db a record that correspond to the provided legacy id, we only
	// execute an update when a corresponding record is matched, otherwise we simply return

	options := []PROPERTY{}

	err := s.transaction.FindByStruct(&options, &PROPERTY{PropertyID: int64(id)}, nil)
	if err != nil {
		componentContextData[literals.LLInternalError] = err.Error()
		componentContextData[literals.LLErrorMessage] = "updating address in property failed"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return
	}

	if len(options) == 0 {
		componentContextData[literals.LLErrorMessage] = "no record is found in property for given legacy id"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return
	}

	var zips []string
	if address.ZipLocal != "" {
		zips = strings.Split(strings.TrimSpace(address.ZipLocal), "-")
	} else {
		zips = strings.Split(strings.TrimSpace(address.Zip), "-")
	}

	serr := s.createZipCode(ctx, zips[0])
	if serr != nil {
		componentContextData[literals.LLInternalError] = serr.Error()
		componentContextData[literals.LLErrorMessage] = "creating zip code for address update in property failed"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return
	}

	var (
		countyCode int64
		cityCode   int64
	)

	// first get the county code if we have a county in the address
	if address.County != "" {
		countyCode, serr = s.GetCountyCode(ctx, strings.TrimSpace(address.County), zips[0])
		if serr != nil {
			componentContextData[literals.LLInternalError] = serr.Error()
			componentContextData[literals.LLErrorMessage] = "loading county code for address update in property failed"
			instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
			return
		}
	}

	cityCode, serr = s.GetCityCode(ctx, strings.TrimSpace(address.City), zips[0])
	if serr != nil {
		componentContextData[literals.LLInternalError] = serr.Error()
		componentContextData[literals.LLErrorMessage] = "loading city code for address update in property failed"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return
	}

	// if (city code or zip code) is not defined, drop the update for
	// these are foreign keys in the property table and are required
	if cityCode == 0 || zips[0] == "" {
		log.Infof("dropping update to property in star db for legacy id %d, as "+
			"one or more required foreign keys are not defined, "+
			"cityCode : %d, countyCode : %d, zipCode: %s, countryCode :%d",
			id,
			cityCode,
			countyCode,
			zips[0],
			countryCode)

		componentContextData[literals.LLErrorMessage] = "missing one or more required foreign keys"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return
	}
	des := parseStreet(address.Street1, address.Street2)

	updateRequest := &PROPERTY{
		StreetNumber:    des.StreetNumber,
		StreetDirection: strings.TrimSpace(des.StreetDirection),
		StreetName:      strings.TrimSpace(des.StreetName),
		CityCode:        fmt.Sprintf("%d", cityCode),
		CountryCode:     fmt.Sprintf("%d", countryCode),
		State:           address.State,
		ZipCode:         zips[0],
		UnitNumber:      address.UnitValue,
		UnitType:        address.UnitType,
		UnitPrefix:      "UNITS",
		LastModifiedBy:  s.username,
	}
	updateRequest.LastModified.Scan(time.Now())

	updateConds := &xormwrapper.QueryOptions{
		SetColumns: []string{literals.StreetNumberColumn,
			literals.StreetDirectionColumn,
			literals.StreetNameColumn,
			literals.CityCodeColumn,
			literals.CountryCodeColumn,
			literals.StateColumn,
			literals.ZipCodeColumn,
			literals.UnitNumberColumn,
			literals.UnitTypeColumn,
			literals.UnitPrefixColumn,
			literals.ZipPlus4Column,
			literals.LastModifiedColumn,
			literals.LastModifiedByColumn},
		WhereAnd: []string{fmt.Sprintf(`"PROPERTY_ID" = %d`, id)},
	}

	// if county code is defined costruct an update with it, otherwise completely
	// omit it from the update
	if countyCode > 0 {
		componentContextData["countyCode"] = countyCode

		updateRequest.CountyCode = fmt.Sprintf("%d", countyCode)
		updateConds.SetColumns = append(updateConds.SetColumns, literals.CountyCodeColumn)
	}

	if len(zips) >= 2 {
		updateRequest.ZipPlus4 = zips[1]
	}

	// update the star db

	log.WithField("Property Address Update", PropertyToJSON(updateRequest)).
		Info("updating address in property")
	affected, err := s.transaction.Update(updateRequest, updateConds)
	if err != nil {
		// If UnitType is not empty, then we probably violated a foreign key
		// constraint in STAR on the UnitType field. Try to save it in STAR as a
		// default value. What the client submitted will be stored in Mongo
		// and returned to the client on lookup. This is a compromise solution.
		if updateRequest.UnitType != "" {
			updateRequest.UnitType = "#"
			affected, err = s.transaction.Update(updateRequest, updateConds)
		}

		if err != nil {
			componentContextData[literals.LLInternalError] = err.Error()
			componentContextData[literals.LLErrorMessage] = "address update in property table failed"
			instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
			return
		}
	}

	if affected == 0 {
		componentContextData[literals.LLErrorMessage] = "address update in property table did not effect any changes"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return
	}

	// if we get this far we need to changes where affected
	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	log.Infof("updating address in star db affected %d row(s) for legacy id %d", affected, id)
	return
}

func (s *StarTransaction) CreatePropertyWithAddressInStarDB(ctx context.Context, address *mongo_dal.AddressRecord) (int64, *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)
	// county is currently not available in the experian API
	var (
		countyCode int64
		cityCode   int64
		err        error
		serr       *addressErrors.ServerError
	)

	spanID := instrumentation.ComponentSpanStart(
		ctx,
		literals.CreatePropertyWithAddress,
		literals.ComponentPropertyStarDB)

	addressBytes, _ := json.Marshal(address)

	componentContextData := map[string]interface{}{
		"Address": string(addressBytes),
	}

	var zips []string
	if address.ZipLocal != "" {
		zips = strings.Split(strings.TrimSpace(address.ZipLocal), "-")
	} else {
		zips = strings.Split(strings.TrimSpace(address.Zip), "-")
	}

	serr = s.createZipCode(ctx, strings.TrimSpace(zips[0]))
	if serr != nil {
		componentContextData[literals.LLInternalError] = serr.Error()
		componentContextData[literals.LLErrorMessage] = "creating zip code for address update in property failed"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return 0, serr
	}

	// first get the county code if we have a county in the address
	countyCode, serr = s.GetCountyCode(ctx, strings.TrimSpace(address.County), zips[0])
	if serr != nil {
		if serr.Code == addressErrors.CountyZipCodeNotFound.Code {
			componentContextData[literals.LLInternalError] = serr.Error()
			componentContextData[literals.LLErrorMessage] = "county code not found based on provided zipcode"
			instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
			return 0, addressErrors.ZipCodeInvalid.New()
		}
		componentContextData[literals.LLInternalError] = serr.Error()
		componentContextData[literals.LLErrorMessage] = "failed to fetch county code"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return 0, serr
	}

	cityCode, serr = s.GetCityCode(ctx, strings.TrimSpace(address.City), zips[0])
	if serr != nil {
		componentContextData[literals.LLInternalError] = serr.Error()
		componentContextData[literals.LLErrorMessage] = "failed to fetch city code"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return 0, serr
	}

	// before attempting to write to star db, the following foreign keys must exists otherwise the
	// write will fail with error about constraint key violation
	if cityCode == 0 || countyCode == 0 || zips[0] == "" {
		// NOTE: looks like this condition is never met without raising an error before, consider removing this code
		addressID, _ := mongo_dal.BsonUUIDToRaw(ctx, address.UUID.Data)
		log.Warnf("dropping creation of property record for address with uuid : %s in star db because "+
			"one or more required foreign keys are not defined, "+
			"cityCode : %d, countyCode : %d, zipCode : %s, countryCode : %d",
			addressID,
			cityCode,
			countyCode,
			zips[0],
			countryCode)

		componentContextData[literals.LLErrorMessage] = "missing one or more required foreign keys"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		return 0, addressErrors.ForeignKeyViolationError.New()
	}
	des := parseStreet(address.Street1, address.Street2)

	//fetch the next property id
	var propertyId int64
	if propertyId, err = s.session.getNextID(ctx, literals.PropertyTableSequence); err != nil {

		componentContextData[literals.LLInternalError] = err.Error()
		componentContextData[literals.LLErrorMessage] = "failed to fetch the next sequence id for property table"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		return 0, addressErrors.StarQueryError.NewFormatted(err.Error())
	}

	var entry *PROPERTY

	entry = &PROPERTY{
		StreetNumber:    des.StreetNumber,
		StreetDirection: strings.TrimSpace(des.StreetDirection),
		StreetName:      strings.TrimSpace(des.StreetName),
		CityCode:        fmt.Sprintf("%d", cityCode),
		CountryCode:     fmt.Sprintf("%d", countryCode),
		State:           address.State,
		ZipCode:         zips[0],
		UnitNumber:      address.UnitValue,
		UnitType:        address.UnitType,
		UnitPrefix:      "UNITS",
		LastModifiedBy:  s.username,
	}
	entry.LastModified.Scan(time.Now())

	if len(zips) >= 2 {
		entry.ZipPlus4 = zips[1]
	}

	componentContextData["countyCode"] = countyCode
	entry.CountyCode = fmt.Sprintf("%d", countyCode)

	entry.PropertyID = propertyId
	componentContextData["propertyID"] = propertyId
	// write to the table
	log.WithField("Address To Property", PropertyToJSON(entry)).
		Info("writing address to property in starDB")

	sid, err := s.transaction.InsertOne(entry)
	if err != nil {
		// If UnitType is not empty, then we probably violated a foreign key
		// constraint in STAR on the UnitType field. Try to save it in STAR as a
		// default value. What the client submitted will be stored in Mongo
		// and returned to the client on lookup. This is a compromise solution.
		if entry.UnitType != "" {
			entry.UnitType = "#"
			sid, err = s.transaction.InsertOne(entry)
		}

		if err != nil {
			componentContextData["Property"] = fmt.Sprintf("%#v", entry)
			componentContextData[literals.LLInternalError] = err.Error()
			componentContextData[literals.LLErrorMessage] = "failed to insert address in property table"
			instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
			return 0, addressErrors.StarQueryError.NewFormatted(err.Error())
		}
	}

	// return the property id of the newly inserted record
	log.WithField("Address To Property", PropertyToJSON(entry)).
		Infof("address written to property in starDB with returned id : %d", sid)

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return entry.PropertyID, nil
}

func (s *StarTransaction) CreateCommercialPropertyWithAddress(ctx context.Context, address *mongo_dal.AddressRecord) (int64, *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)

	spanID := instrumentation.ComponentSpanStart(
		ctx,
		literals.CreateCommercialPropertyWithAddress,
		literals.ComponentPropertyStarDB)

	addressBytes, _ := json.Marshal(address)

	componentContextData := map[string]interface{}{
		"Address": string(addressBytes),
	}
	//zip contains zip 5+4 separated by hyphen, we split it into zip (5 digit one) and +4 parts
	var zips []string
	if address.ZipLocal != "" {
		zips = strings.Split(strings.TrimSpace(address.ZipLocal), "-")
	} else {
		zips = strings.Split(strings.TrimSpace(address.Zip), "-")
	}

	serr := s.createZipCode(ctx, strings.TrimSpace(zips[0]))
	if serr != nil {
		componentContextData[literals.LLInternalError] = serr.Error()
		componentContextData[literals.LLErrorMessage] = "creating zip code for address update in property failed"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return 0, serr
	}

	countyCode, serr := s.GetCountyCode(ctx, strings.TrimSpace(address.County), zips[0])
	if serr != nil {
		if serr.Code == addressErrors.CountyZipCodeNotFound.Code {
			componentContextData[literals.LLInternalError] = serr.Error()
			componentContextData[literals.LLErrorMessage] = "county code not found based on provided zipcode"
			instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
			return 0, addressErrors.ZipCodeInvalid.New()
		}
		componentContextData[literals.LLInternalError] = serr.Error()
		componentContextData[literals.LLErrorMessage] = "failed to fetch county code"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return 0, serr
	}

	cityCode, serr := s.GetCityCode(ctx, strings.TrimSpace(address.City), zips[0])
	if serr != nil {
		componentContextData[literals.LLInternalError] = serr.Error()
		componentContextData[literals.LLErrorMessage] = "failed to fetch city code"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return 0, serr
	}

	// before attempting to write to star db, the following foreign keys must exists otherwise the
	// write will fail with error about constraint key violation
	if cityCode == 0 || countyCode == 0 || zips[0] == "" {
		// NOTE: looks like this condition is never met without raising an error before, consider removing this code
		addressID, _ := mongo_dal.BsonUUIDToRaw(ctx, address.UUID.Data)
		log.Warnf("dropping creation of property record for address with uuid : %s in star db because "+
			"one or more required foreign keys are not defined, "+
			"cityCode : %d, countyCode : %d, zipCode : %s, countryCode : %d",
			addressID,
			cityCode,
			countyCode,
			zips[0],
			countryCode)

		componentContextData[literals.LLErrorMessage] = "missing one or more required foreign keys"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		return 0, addressErrors.ForeignKeyViolationError.New()
	}
	des := parseStreet(address.Street1, address.Street2)

	var (
		commercialPropertyID int64
		err                  error
	)
	if commercialPropertyID, err = s.session.getNextID(ctx, literals.CommercialPropertyTableSequence); err != nil {

		componentContextData[literals.LLInternalError] = err.Error()
		componentContextData[literals.LLErrorMessage] = "failed to fetch the next sequence id for property table"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		return 0, addressErrors.StarQueryError.NewFormatted(err.Error())
	}

	entry := &COMMERCIAL_PROPERTY{
		UnitDesignation:    "",
		StreetNumber:       des.StreetNumber,
		StreetDirection:    strings.TrimSpace(des.StreetDirection),
		StreetName:         strings.TrimSpace(des.StreetName),
		UnitNumber:         address.UnitValue,
		CityCode:           fmt.Sprintf("%d", cityCode),
		State:              address.State,
		ZipCode:            zips[0],
		ZipPlus4:           "",
		UnitType:           address.UnitType,
		UnitPrefix:         "UNITS",
		CountyCode:         "",
		CountryCode:        fmt.Sprintf("%d", countryCode),
		SfiReqFlag:         "",
		LastModifiedBy:     s.username,
		StandardizedStatus: "",
		StandardizedDesc:   "",
	}
	entry.LastModified.Scan(time.Now())

	if len(zips) >= 2 {
		entry.ZipPlus4 = zips[1]
	}

	componentContextData["countyCode"] = countyCode
	entry.CountyCode = fmt.Sprintf("%d", countyCode)

	entry.CommercialPropertyID = commercialPropertyID
	componentContextData["propertyID"] = commercialPropertyID

	log.WithField("Address To Property", CommercialPropertyToJSON(entry)).
		Info("writing address to property in starDB")

	sid, err := s.transaction.InsertOne(entry)
	if err != nil {
		// If UnitType is not empty, then we probably violated a foreign key
		// constraint in STAR on the UnitType field. Try to save it in STAR as a
		// default value. What the client submitted will be stored in Mongo
		// and returned to the client on lookup. This is a compromise solution.
		if entry.UnitType != "" {
			entry.UnitType = "#"
			sid, err = s.transaction.InsertOne(entry)
		}

		if err != nil {
			componentContextData["Property"] = fmt.Sprintf("%#v", entry)
			componentContextData[literals.LLInternalError] = err.Error()
			componentContextData[literals.LLErrorMessage] = "failed to insert address in property table"
			instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
			return 0, addressErrors.StarQueryError.NewFormatted(err.Error())
		}
	}

	log.WithField("Address To Property", CommercialPropertyToJSON(entry)).
		Infof("address written to property in starDB with returned id : %d", sid)

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return entry.CommercialPropertyID, nil
}

func (s *StarTransaction) UpdateCommercialPropertyWithAddress(ctx context.Context, address *mongo_dal.AddressRecord, id uint32) (int64, *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)

	if id == 0 {
		return 0, addressErrors.InvalidIDError.NewFormatted(id)
	}

	spanID := instrumentation.ComponentSpanStart(
		ctx,
		literals.UpdateCommercialPropertyWithAddress,
		literals.ComponentPropertyStarDB)

	addressBytes, _ := json.Marshal(address)

	componentContextData := map[string]interface{}{
		"Address":  string(addressBytes),
		"LegacyID": id,
	}

	commercialProperties := []COMMERCIAL_PROPERTY{}

	err := s.transaction.FindByStruct(&commercialProperties, &COMMERCIAL_PROPERTY{CommercialPropertyID: int64(id)}, nil)
	if err != nil {
		componentContextData[literals.LLInternalError] = err.Error()
		componentContextData[literals.LLErrorMessage] = "updating address in property failed"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return 0, addressErrors.StarQueryError.NewFormatted(err.Error())
	}

	if len(commercialProperties) == 0 {
		componentContextData[literals.LLErrorMessage] = "no record is found in property for given legacy id"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return 0, addressErrors.LegacyIDNotFoundInStarDBError.NewFormatted(id)
	}

	var zips []string
	if address.ZipLocal != "" {
		zips = strings.Split(strings.TrimSpace(address.ZipLocal), "-")
	} else {
		zips = strings.Split(strings.TrimSpace(address.Zip), "-")
	}

	serr := s.createZipCode(ctx, zips[0])
	if serr != nil {
		componentContextData[literals.LLInternalError] = serr.Error()
		componentContextData[literals.LLErrorMessage] = "creating zip code for address update in property failed"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return 0, serr
	}

	var countyCode int64
	if address.County != "" {
		countyCode, serr = s.GetCountyCode(ctx, strings.TrimSpace(address.County), zips[0])
		if serr != nil {
			componentContextData[literals.LLInternalError] = serr.Error()
			componentContextData[literals.LLErrorMessage] = "loading county code for address update in property failed"
			instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
			return 0, serr
		}
	}

	cityCode, serr := s.GetCityCode(ctx, strings.TrimSpace(address.City), zips[0])
	if serr != nil {
		componentContextData[literals.LLInternalError] = serr.Error()
		componentContextData[literals.LLErrorMessage] = "loading city code for address update in property failed"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return 0, serr
	}

	// if (city code or zip code) is not defined, drop the update for
	// these are foreign keys in the property table and are required
	if cityCode == 0 || zips[0] == "" {
		log.Infof("dropping update to property in star db for legacy id %d, as "+
			"one or more required foreign keys are not defined, "+
			"cityCode : %d, countyCode : %d, zipCode: %s, countryCode :%d",
			id,
			cityCode,
			countyCode,
			zips[0],
			countryCode)

		componentContextData[literals.LLErrorMessage] = "missing one or more required foreign keys"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return 0, addressErrors.ForeignKeyViolationError.New()
	}
	des := parseStreet(address.Street1, address.Street2)

	updateRequest := &COMMERCIAL_PROPERTY{
		StreetNumber:    des.StreetNumber,
		StreetDirection: strings.TrimSpace(des.StreetDirection),
		StreetName:      strings.TrimSpace(des.StreetName),
		CityCode:        fmt.Sprintf("%d", cityCode),
		CountryCode:     fmt.Sprintf("%d", countryCode),
		State:           address.State,
		ZipCode:         zips[0],
		UnitNumber:      address.UnitValue,
		UnitType:        address.UnitType,
		UnitPrefix:      "UNITS",
		LastModifiedBy:  s.username,
	}
	updateRequest.LastModified.Scan(time.Now())

	updateConds := &xormwrapper.QueryOptions{
		SetColumns: []string{literals.StreetNumberColumn,
			literals.StreetDirectionColumn,
			literals.StreetNameColumn,
			literals.CityCodeColumn,
			literals.CountryCodeColumn,
			literals.StateColumn,
			literals.ZipCodeColumn,
			literals.UnitNumberColumn,
			literals.UnitTypeColumn,
			literals.UnitPrefixColumn,
			literals.ZipPlus4Column,
			literals.LastModifiedColumn,
			literals.LastModifiedByColumn},
		WhereAnd: []string{fmt.Sprintf(`"COMMERCIAL_PROPERTY_ID" = %d`, id)},
	}

	// if county code is defined costruct an update with it, otherwise completely
	// omit it from the update
	if countyCode > 0 {
		componentContextData["countyCode"] = countyCode

		updateRequest.CountyCode = fmt.Sprintf("%d", countyCode)
		updateConds.SetColumns = append(updateConds.SetColumns, literals.CountyCodeColumn)
	}

	if len(zips) >= 2 {
		updateRequest.ZipPlus4 = zips[1]
	}

	log.WithField("Property Address Update", CommercialPropertyToJSON(updateRequest)).
		Info("updating address in property")
	affected, err := s.transaction.Update(updateRequest, updateConds)
	if err != nil {
		// If UnitType is not empty, then we probably violated a foreign key
		// constraint in STAR on the UnitType field. Try to save it in STAR as a
		// default value. What the client submitted will be stored in Mongo
		// and returned to the client on lookup. This is a compromise solution.
		if updateRequest.UnitType != "" {
			updateRequest.UnitType = "#"
			affected, err = s.transaction.Update(updateRequest, updateConds)
		}

		if err != nil {
			componentContextData[literals.LLInternalError] = err.Error()
			componentContextData[literals.LLErrorMessage] = "address update in property table failed"
			instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
			return 0, addressErrors.StarQueryError.NewFormatted(err.Error())
		}
	}

	if affected == 0 {
		componentContextData[literals.LLErrorMessage] = "address update in property table did not effect any changes"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return 0, nil
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	log.Infof("updating address in star db affected %d row(s) for legacy id %d", affected, id)
	return affected, nil
}
