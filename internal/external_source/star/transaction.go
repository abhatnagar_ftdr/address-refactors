package star

import (
	"context"
	"go.ftdr.com/go-utils/instrumentation/v3"
	"go.ftdr.com/go-utils/xormwrapper/v2"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/mongo_dal"
	addressErrors "golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors"
)

type TransactionEndType int

const (
	Commit TransactionEndType = iota
	Rollback
)

type IStarTransaction interface {
	Commit(ctx context.Context) error
	Rollback(ctx context.Context) error
	HandleEndOfTransaction(ctx context.Context, transType *TransactionEndType) error

	GetCityCode(ctx context.Context, cityName string, zip string) (int64, *addressErrors.ServerError)

	GetCountyCode(ctx context.Context, countyName string, zip string) (int64, *addressErrors.ServerError)
	GetCountyCodeByCountyName(ctx context.Context, countyName string, zip string) (int64, *addressErrors.ServerError)
	GetCountyCodeByZipCode(ctx context.Context, zip string) (int64, *addressErrors.ServerError)

	CreatePropertyWithAddressInStarDB(ctx context.Context, address *mongo_dal.AddressRecord) (int64, *addressErrors.ServerError)
	UpdatePropertyWithAddressInStarDB(ctx context.Context, address *mongo_dal.AddressRecord, id uint32)

	CreateCommercialPropertyWithAddress(ctx context.Context, address *mongo_dal.AddressRecord) (int64, *addressErrors.ServerError)
	UpdateCommercialPropertyWithAddress(ctx context.Context, address *mongo_dal.AddressRecord, id uint32) (int64, *addressErrors.ServerError)
}

type StarTransaction struct {
	transaction xormwrapper.ITransaction
	session     *StarSession
	username    string
}

func (s *StarTransaction) Commit(ctx context.Context) error {
	fields := map[string]interface{}{}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.CommitTransaction, literals.ComponentPropertyStarDB)
	err := s.transaction.Commit()
	if err != nil {
		fields[literals.LLErrorMessage] = err.Error()
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, (err == nil), fields)
	return err
}

func (s *StarTransaction) Rollback(ctx context.Context) error {
	fields := map[string]interface{}{}
	spanID := instrumentation.ComponentSpanStart(ctx, literals.RollbackTransaction, literals.ComponentPropertyStarDB)
	err := s.transaction.Rollback()
	if err != nil {
		fields[literals.LLErrorMessage] = err.Error()
	}
	instrumentation.ComponentSpanEnd(ctx, spanID, (err == nil), fields)
	return err
}

func (s *StarTransaction) HandleEndOfTransaction(ctx context.Context, transType *TransactionEndType) error {
	if *transType == Commit {
		return s.Commit(ctx)
	}
	return s.Rollback(ctx)
}
