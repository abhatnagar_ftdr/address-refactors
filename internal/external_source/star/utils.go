package star

import (
	"encoding/json"
	"fmt"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/types"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/utils"
	"reflect"
	"strings"
)

// propertyToJSON converts a property struct to a json payload with the table columns as specified
// in the xorm tag as the fields in the payload.
func PropertyToJSON(s *PROPERTY) string {

	fields := make(map[string]string)
	e := reflect.ValueOf(s).Elem()

	for i := 0; i < e.NumField(); i++ {
		tName := e.Type().Field(i).Tag.Get("xorm")
		parts := strings.Split(tName, " ")
		if len(parts) > 1 {
			field := parts[len(parts)-1]
			field = strings.ReplaceAll(field, "'", "")
			field = strings.TrimSpace(field)
			varValue := e.Field(i).Interface()
			fields[field] = fmt.Sprintf("%v", varValue)
		}
	}

	b, _ := json.Marshal(fields)
	return string(b)
}
func CommercialPropertyToJSON(s *COMMERCIAL_PROPERTY) string {

	fields := make(map[string]string)
	e := reflect.ValueOf(s).Elem()

	for i := 0; i < e.NumField(); i++ {
		tName := e.Type().Field(i).Tag.Get("xorm")
		parts := strings.Split(tName, " ")
		if len(parts) > 1 {
			field := parts[len(parts)-1]
			field = strings.ReplaceAll(field, "'", "")
			field = strings.TrimSpace(field)
			varValue := e.Field(i).Interface()
			fields[field] = fmt.Sprintf("%v", varValue)
		}
	}

	b, _ := json.Marshal(fields)
	return string(b)
}

func parseStreet(street1, street2 string) *types.Street1Components {
	des := utils.ParseStreetAddress(street1)
	if len(des.StreetName) == 0 {
		des2 := utils.ParseStreetAddress(street2)
		des.StreetName = des2.StreetName
	}
	return des
}
