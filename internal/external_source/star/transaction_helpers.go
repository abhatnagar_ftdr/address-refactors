package star

import (
	"context"
	"errors"
	"fmt"
	"go.ftdr.com/go-utils/instrumentation/v3"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
	addressErrors "golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors"
	"strconv"
	"strings"
	"time"
)

func (s *StarTransaction) createCityCode(ctx context.Context, cityName string, zip string) (int64, error) {
	spanID := instrumentation.ComponentSpanStart(ctx, literals.CreateCityCode, literals.ComponentPropertyStarDB)
	componentContextData := map[string]interface{}{
		"CityName": cityName,
		"Zip":      zip,
	}

	id, err := s.session.getNextID(ctx, literals.CityTableSequence)
	if err != nil {

		componentContextData[literals.LLInternalError] = err.Error()
		componentContextData[literals.LLErrorMessage] = "failed to fetch the next sequence id for city table"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		return 0, fmt.Errorf("error from getNextID method : %s", err.Error())
	}

	newCity := &CITIES{
		CityCode:       id,
		CityName:       strings.ToUpper(cityName),
		LastModifiedBy: s.username,
	}
	newCity.LastModified.Scan(time.Now())

	_, err = s.transaction.InsertOne(newCity)

	if err != nil {
		componentContextData[literals.LLInternalError] = err.Error()
		componentContextData[literals.LLErrorMessage] = "failed to write new city in star"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return 0, err
	}

	if newCity.CityCode > int64(0) {
		instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
		return newCity.CityCode, nil
	}

	componentContextData[literals.LLErrorMessage] = "failed to insert new city in star"
	instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
	return 0, errors.New("error failed to create a new entry in the cities table in star")
}

func (s *StarTransaction) createCityCodeZipCode(ctx context.Context, cityCode int64, zip string) error {
	spanID := instrumentation.ComponentSpanStart(ctx, literals.CreateCityCodeZipCode, literals.ComponentPropertyStarDB)
	componentContextData := map[string]interface{}{
		"CityCode": cityCode,
		"Zip":      zip,
	}

	options := []CITY_CODE_ZIP_CODE{}
	err := s.transaction.FindByStruct(&options, &CITY_CODE_ZIP_CODE{ZipCode: zip, CityCode: strconv.FormatInt(cityCode, 10)}, nil)
	if err != nil {
		componentContextData[literals.LLInternalError] = err.Error()
		componentContextData[literals.LLErrorMessage] = "failed to fetch city_code_zip_code from star"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return fmt.Errorf("error from createCityCodeZipCode from star db : %s", err.Error())
	}

	// if we failed to find a city_code_zip_code with this name, we need to create a new entry
	// in the star db
	if len(options) == 0 {
		newCityCodeZipCode := &CITY_CODE_ZIP_CODE{
			ZipCode:        zip,
			CityCode:       strconv.FormatInt(cityCode, 10),
			LastModifiedBy: s.username,
		}
		newCityCodeZipCode.LastModified.Scan(time.Now())

		_, err = s.transaction.InsertOne(newCityCodeZipCode)
		if err != nil {
			componentContextData[literals.LLInternalError] = err.Error()
			componentContextData[literals.LLErrorMessage] = "failed to insert new city_code_zip_code in star"
			instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
			return err
		}
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return nil
}

func (s *StarTransaction) createCountyCode(ctx context.Context, countyName string, zip string) (int64, error) {
	spanID := instrumentation.ComponentSpanStart(ctx, literals.CreateCountyCode, literals.ComponentPropertyStarDB)
	componentContextData := map[string]interface{}{
		"CountyName": countyName,
		"Zip":        zip,
	}

	id, err := s.session.getNextID(ctx, literals.CountyTableSequence)
	if err != nil {

		componentContextData[literals.LLInternalError] = err.Error()
		componentContextData[literals.LLErrorMessage] = "failed to fetch the next sequence id for county table"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)

		return 0, fmt.Errorf("error from getNextID method : %s", err.Error())
	}
	newCounty := &COUNTIES{
		CountyCode:     id,
		CountyName:     strings.ToUpper(countyName),
		LastModifiedBy: s.username,
	}
	newCounty.LastModified.Scan(time.Now())

	_, err = s.transaction.InsertOne(newCounty)
	if err != nil {
		componentContextData[literals.LLInternalError] = err.Error()
		componentContextData[literals.LLErrorMessage] = "failed to insert new county in star"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return 0, err
	}

	if newCounty.CountyCode > int64(0) {
		instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
		return newCounty.CountyCode, nil
	}

	componentContextData[literals.LLErrorMessage] = "failed to insert new county in star"
	instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
	return 0, errors.New("error failed to create a new entry in the counties table in star")
}

func (s *StarTransaction) createCountyZipCode(ctx context.Context, countyCode int64, zip string) error {
	spanID := instrumentation.ComponentSpanStart(ctx, literals.CreateCountyZipCode, literals.ComponentPropertyStarDB)
	componentContextData := map[string]interface{}{
		"CountyCode": countyCode,
		"Zip":        zip,
	}

	options := []COUNTY_ZIP_CODE{}

	err := s.transaction.FindByStruct(&options, &COUNTY_ZIP_CODE{ZipCode: zip, CountyCode: strconv.FormatInt(countyCode, 10)}, nil)
	if err != nil {
		componentContextData[literals.LLInternalError] = err.Error()
		componentContextData[literals.LLErrorMessage] = "failed to fetch county_zip_code from star"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return fmt.Errorf("error from createCountyZipCode from star db : %s", err.Error())
	}

	// if we failed to find a city_code_zip_code with this name, we need to create a new entry
	// in the star db
	if len(options) == 0 {
		newCountyZipCode := &COUNTY_ZIP_CODE{
			ZipCode:        zip,
			CountyCode:     strconv.FormatInt(countyCode, 10),
			LastModifiedBy: s.username,
		}
		newCountyZipCode.LastModified.Scan(time.Now())

		_, err = s.transaction.InsertOne(newCountyZipCode)
		if err != nil {
			componentContextData[literals.LLInternalError] = err.Error()
			componentContextData[literals.LLErrorMessage] = "failed to insert new county_zip_code in star"
			instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
			return err
		}
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return nil
}

func (s *StarTransaction) createZipCode(ctx context.Context, zip string) *addressErrors.ServerError {
	spanID := instrumentation.ComponentSpanStart(ctx, literals.CreateZipCode, literals.ComponentPropertyStarDB)
	componentContextData := map[string]interface{}{
		"Zip": zip,
	}

	options := []ZIP_CODE{}

	err := s.transaction.FindByStruct(&options, &ZIP_CODE{ZipCode: zip}, nil)
	if err != nil {
		componentContextData[literals.LLInternalError] = err.Error()
		componentContextData[literals.LLErrorMessage] = "failed to fetch zip code from star"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return addressErrors.StarQueryError.NewFormatted(err.Error())
	}

	// if we failed to find a city_code_zip_code with this name, we need to create a new entry
	// in the star db
	if len(options) == 0 {
		newZipCode := &ZIP_CODE{
			ZipCode:        zip,
			CountryCode:    strconv.FormatInt(countryCode, 10),
			LastModifiedBy: s.username,
		}
		newZipCode.LastModified.Scan(time.Now())

		_, err = s.transaction.InsertOne(newZipCode)
		if err != nil {
			componentContextData[literals.LLInternalError] = err.Error()
			componentContextData[literals.LLErrorMessage] = "failed to insert new zip code in star"
			instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
			return addressErrors.StarQueryError.NewFormatted(err.Error())
		}
	}

	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return nil
}

func (s *StarTransaction) getCountyCode(ctx context.Context, zip string) (int64, error) {
	spanID := instrumentation.ComponentSpanStart(ctx, literals.FetchZipcodeCountyCode, literals.ComponentPropertyStarDB)
	componentContextData := map[string]interface{}{
		"Zip": zip,
	}

	options := []COUNTY_ZIP_CODE{}

	err := s.transaction.FindByStruct(&options, &COUNTY_ZIP_CODE{ZipCode: zip}, nil)
	if err != nil {
		componentContextData[literals.LLInternalError] = err.Error()
		componentContextData[literals.LLErrorMessage] = "failed to fetch county zip code from star"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return 0, fmt.Errorf("error from getCountyCode from star db : %s", err.Error())
	}

	if len(options) >= 1 {
		var countyCode int64
		countyCode, err = strconv.ParseInt(options[len(options)-1].CountyCode, 10, 64)
		if err != nil {
			componentContextData[literals.LLInternalError] = err.Error()
			componentContextData[literals.LLErrorMessage] = "failed to convert county code from star"
			instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
			return 0, fmt.Errorf("error from getCountyCode from star db : %s", err.Error())
		}
		instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
		return countyCode, nil
	}

	componentContextData[literals.LLErrorMessage] = "county zip code not found"
	instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
	return 0, fmt.Errorf("county zip code not found")
}
