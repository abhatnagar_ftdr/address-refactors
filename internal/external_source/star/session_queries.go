package star

import (
	"context"
	"go.ftdr.com/go-utils/common/logging"
	"go.ftdr.com/go-utils/instrumentation/v3"
	"go.ftdr.com/go-utils/xormwrapper/v2"
	"golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/internal/literals"
	addressErrors "golang.frontdoorhome.com/abhatnagar_ftdr/address-refactor/pkg/errors"
)

func (s *StarSession) getNextID(ctx context.Context, sequenceName string) (int64, error) {

	sql := "select " + sequenceName + ".nextval from dual"

	conds := &xormwrapper.QueryOptions{
		CustomSQL: sql,
	}

	spanID := instrumentation.ComponentSpanStart(ctx, literals.FetchNextPropertyID, literals.ComponentPropertyStarDB)
	componentContextData := map[string]interface{}{
		"SequencyName": sequenceName,
	}

	dual := []DUAL{}

	err := s.session.Find(&dual, conds)
	if err != nil {
		componentContextData[literals.LLInternalError] = err.Error()
		componentContextData[literals.LLErrorMessage] = "failed to fetch next sequence id from dual table in star"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return 0, err
	}

	componentContextData["NextID"] = dual[0].Nextval
	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return dual[0].Nextval, nil
}

func (s *StarSession) GetCityName(ctx context.Context, id int) (string, *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)
	if id == 0 {
		return "", addressErrors.RequiredFieldMissing.NewFormatted("id")
	}

	spanID := instrumentation.ComponentSpanStart(ctx, literals.FetchCityName, literals.ComponentPropertyStarDB)
	componentContextData := map[string]interface{}{
		"CityCode": id,
	}

	log.Infof("querying city name table in star db for city code %d", id)
	options := []CITIES{}

	err := s.session.FindByStruct(&options, &CITIES{CityCode: int64(id)}, nil)
	if err != nil {
		componentContextData[literals.LLInternalError] = err.Error()
		componentContextData[literals.LLErrorMessage] = "failed to fetch city name from star"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return "", addressErrors.StarQueryError.NewFormatted(err.Error())
	}

	if len(options) == 0 {
		componentContextData[literals.LLErrorMessage] = "no record found in star for city code"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return "", addressErrors.CityNameNotFound.New()
	}

	log.Infof("found %d record in star db for city with city code %d", len(options), id)
	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return options[0].CityName, nil
}

func (s *StarSession) GetCountyName(ctx context.Context, id int) (string, *addressErrors.ServerError) {
	log := logging.GetTracedLogEntry(ctx)
	if id == 0 {
		return "", addressErrors.RequiredFieldMissing.NewFormatted("id")
	}

	spanID := instrumentation.ComponentSpanStart(ctx, literals.FetchCountyName, literals.ComponentPropertyStarDB)
	componentContextData := map[string]interface{}{
		"CountyCode": id,
	}

	options := []COUNTIES{}

	err := s.session.FindByStruct(&options, &COUNTIES{CountyCode: int64(id)}, nil)
	if err != nil {
		componentContextData[literals.LLInternalError] = err.Error()
		componentContextData[literals.LLErrorMessage] = "failed to fetch county name from star"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return "", addressErrors.StarQueryError.NewFormatted(err.Error())
	}

	if len(options) == 0 {
		componentContextData[literals.LLErrorMessage] = "no record found for county code in star"
		instrumentation.ComponentSpanEnd(ctx, spanID, false, componentContextData)
		return "", addressErrors.CountyNameNotFound.New()
	}

	log.Infof("found %d county name record in star for county code %d", len(options), id)
	instrumentation.ComponentSpanEnd(ctx, spanID, true, componentContextData)
	return options[0].CountyName, nil
}
